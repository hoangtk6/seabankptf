//
//  AppDelegate.swift
//  SeaBankPTF
//
//  Created by Hoang Vu Van on 03/09/2021.
//

import UIKit
import IQKeyboardManagerSwift
import CoreData

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow?
  
  func sharedInstance() -> AppDelegate {
    return UIApplication.shared.delegate as! AppDelegate
  }
  var orientationLock = UIInterfaceOrientationMask.portrait
  
  lazy var datastoreCoordinator: DatastoreCoordinator = {
    return DatastoreCoordinator()
  }()
  
  lazy var contextManager: ContextManager = {
    return ContextManager()
  }()
  
  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
    application.setupFirebase()
    application.setupTheme()
    application.setupIQKeyboard()
    
    self.window = UIWindow(frame: UIScreen.main.bounds)
    setupController()
    self.window?.backgroundColor = UIColor.white
    self.window?.makeKeyAndVisible()
    return true
  }
  
  // MARK: - Core Data stack

  lazy var persistentContainer: NSPersistentContainer = {
      let container = NSPersistentContainer(name: "MyStyleBook")
      container.loadPersistentStores(completionHandler: { (storeDescription, error) in
          if let error = error as NSError? {
              fatalError("Unresolved error \(error), \(error.userInfo)")
          }
      })
      return container
  }()

  // MARK: - Core Data Saving support

  func saveContext () {
      let context = persistentContainer.viewContext
      if context.hasChanges {
          do {
              try context.save()
          } catch {
              let nserror = error as NSError
              fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
          }
      }
  }
  
  func setupController() {
    let onBoardingVC = TaoKhoanVayMainVC()
    let nav = UINavigationController.init(rootViewController: onBoardingVC)
    self.window?.rootViewController = nav
  }
  
  func setupController11() {
    let tabBarController = PTFTabBarController()
    self.window?.rootViewController = tabBarController
  }
  
  

}

private extension UIApplication {
  func setupFirebase() {

  }
  
  func setupPush() {

  }
  
  func setupTheme() {
    var style = ToastStyle()
    style.messageFont = RobotoFont.fontWithType(.medium, size: 15)
    style.messageAlignment = .center
    style.titleAlignment = .center
    style.horizontalPadding = 15.0
    style.verticalPadding = 10.0
    ToastManager.shared.style = style
    ToastManager.shared.duration = 2.0
  }
  
  func setupIQKeyboard() {
    IQKeyboardManager.shared.enable = true
    IQKeyboardManager.shared.enableAutoToolbar = true
    IQKeyboardManager.shared.shouldResignOnTouchOutside = true
    IQKeyboardManager.shared.toolbarDoneBarButtonItemText = "Xong"
  }
}
