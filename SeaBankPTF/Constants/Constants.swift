//
//  Constants.swift
//  Comic
//
//  Created by Hoang Vu Van on 11/5/19.
//  Copyright © 2019 Techlab Corp. All rights reserved.
//

import UIKit
let emptyString = ""
let emptyValue = "--"
let defaulCountryCode = "vn" // default VietNam
let comicScheme = "comic://"

struct Constants {
  //MARK: - Varible
  struct BarButton {
    static let Width = CGFloat(40)
    static let Height = CGFloat(40)
  }
  
  
  struct ScreenSize {
    static let SCREEN_WIDTH = UIScreen.main.bounds.size.width
    static let SCREEN_HEIGHT = UIScreen.main.bounds.size.height
    static let STATUS_BAR_HEIGHT = UIApplication.shared.statusBarFrame.height
    static let NAVIGATION_HEIGHT: CGFloat = 44
  }
  
  struct FontSize {
    static let FONT_SIZE_6: CGFloat = 6
    static let FONT_SIZE_7: CGFloat = 7
    static let FONT_SIZE_8: CGFloat = 8
    static let FONT_SIZE_9: CGFloat = 9
    static let FONT_SIZE_10: CGFloat = 10
    static let FONT_SIZE_11: CGFloat = 11
    static let FONT_SIZE_12: CGFloat = 12
    static let FONT_SIZE_13: CGFloat = 13
    static let FONT_SIZE_14 :CGFloat = 14
    static let FONT_SIZE_15 : CGFloat = 15
    static let FONT_SIZE_16 : CGFloat = 16
    static let FONT_SIZE_17 : CGFloat = 17
    static let FONT_SIZE_18 : CGFloat = 18
    static let FONT_SIZE_19 : CGFloat = 19
    static let FONT_SIZE_20 : CGFloat = 20
    static let FONT_SIZE_22 : CGFloat = 22
    static let FONT_SIZE_23 : CGFloat = 23
    static let FONT_SIZE_24 : CGFloat = 24
    static let FONT_SIZE_26 : CGFloat = 26
    static let FONT_SIZE_30 : CGFloat = 30
    static let FONT_SIZE_32 : CGFloat = 32
  }
  
  struct StoryBoard {
    static let StoryBoardComicMain : String = "Main"
    static let StoryBoardComicDetail : String = "ComicDetail"
  }
  
  struct URL {
    static let TERM_URL : String = "https://Weeboo.vn/policy-terms.html"
    static let AUTHOR_SIGNUP:String = "https://Weeboo.vn/dang-ky-tac-gia"
    static let WEBSITE_Weeboo:String = "http://Weeboo.vn"
    static let FAN_PAGE:String = "https://www.facebook.com/Weeboovn"
    static let Weeboo_ID = "Weeboo.vn/@"
    static let URL_APPSTORE = "https://apps.apple.com/vn/app/id1538250920"
  }
  
  struct cornerRadius {
    static let RADIUS_10:CGFloat = 10.0
    static let RADIUS_8:CGFloat = 8.0
    static let RADIUS_5:CGFloat = 5.0
    static let RADIUS_3:CGFloat = 3.0
  }
  
  
  //
  struct ValueHardCode {
    static let CATEGORYID_MORE:String = "CATEGORYID_MORE"
  }
  
  struct LoginConstant {
    //funtoon 1499613190
    static let APP_ID = "1538250920"
    static let GOOGLE_APP_ID = "1044871170031-5f2ktj18cf4u16nl1r63fuhpj2078kps.apps.googleusercontent.com"
    static let FACEBOOK_APP_ID = "fb1345496975622573"
  }
  
//  struct GoogleSignIn {
//    static let scheme = "com.googleusercontent.apps.1044871170031-5f2ktj18cf4u16nl1r63fuhpj2078kps"
//    static let clientID = "1044871170031-5f2ktj18cf4u16nl1r63fuhpj2078kps.apps.googleusercontent.com"
//  }
  
  struct HTTPResponseCode {
    static let success_200 = 200
    static let success_204 = 204
    static let success_205 = 205
    static let EmptyData_404 = 404
    static let TokenExpire_401 = 401
    
    //The data in the receipt-data property was malformed or missing.
    static let Purchase_Status_21002 = 21002
    //The receipt could not be authenticated.
    static let Purchase_Status_21003 = 21003
    //This receipt is from the test environment, but it was sent to the production environment for verification.
    static let Purchase_Status_21007 = 21007
    //The user account cannot be found or has been deleted.
    static let Purchase_Status_21010 = 21010
    
    static let Exchange_Status_406 = 406
    static let Exchange_Status_412 = 412
  }
  
  struct LimitedLoadMore {
    static let Limited_8 = 8
    static let Limited_10 = 10
    static let Limited_20 = 20
    static let Limited_30 = 30
  }
  
  struct switchCategoryToon {
    static let ROMANTIC:String = "romantic"
    static let ACTION:String = "action"
  }
  
  //MARK: - Value
  struct Prefix {
    static let http = "http"
    static let primary = "primary"
    static let tokenExpire = "Token expire"
    static let Weeboo = "Weeboo"
    
  }
  
  struct DateFormat {
    static let ddMMyyyyHHmm = "dd/MM/yyyy • HH:mm"
    static let ddMMyyyy = "dd/MM/yyyy"
  }

  struct UnlockChapType {
    static let coin = "coin"
    static let free = "free"
    static let coin_ads = "coin_ads"
    static let ads = "ads"
  }
  
  struct ComicStatus {
    static let complete = "complete"
  }
  
  struct ComicCoin {
    static let GCoin = "C"
  }
  
  struct SpecialText {
    static let Small = "·"
    static let Medium = "•"
    static let Large = "●"
  }
  
  struct AdUnitId {
    #if PROD
//    static let unlockChapter = "ca-app-pub-2319990838494795/5378917946"
    static let unlockChapter = ""
    #else
    static let unlockChapter = "ca-app-pub-3940256099942544/1712485313"
    #endif
  }
  
  struct CoinDescription {
    static let useCoin1 = "Dùng Coin để mở khóa các nội dung hấp dẫn và bản quyền của Weeboo."
    static let useCoin2 = "Dùng Coin để mua quà tặng gửi đến các người đăng yêu thích."
    static let howToGetCoin1 = "Mua coin trên app Weeboo và thực hiện thanh toán qua App Store."
    static let howToGetCoin2 = "Mua coin trên website Weeboo.vn với nhiều hình thức thanh toán đa dạng đi kèm với các ưu đãi hấp dẫn."
    static let chargeCoinError = "Nếu bạn chưa nhận được Coin sau khi thanh toán, vui lòng liên lạc ngay kênh hỗ trợ chính thức của Weeboo qua Email: hotro@Weeboo.vn"
    static let pleaseLogin = "Weeboo khuyên bạn nên đăng nhập tài khoản trước khi nạp coin. Nếu không đăng nhập, số coin bạn mua sẽ chỉ sử dụng được trên thiết bị này và không thể phục hồi khi bạn sử dụng thiết bị mới"
  }
  
  struct AppsFlyer {
    static let appId = LoginConstant.APP_ID
    static let keyAppsFlyer = "cfgYFoj97okaEbzqp8q5w5"
  }
  
  struct admob {
    static let AdUnitIDBookCase = "ca-app-pub-7166463697735839/3000508177"
    static let AdUnitIDReading = "ca-app-pub-7166463697735839/9565916524"
    static let AdUnitIDNovelReading = "ca-app-pub-7166463697735839/5052381478"
    static let AdUnitIDNewsFeed = "ca-app-pub-7166463697735839/8487747928"
    
    //key test
    static let AdUnitIDKeyTest = "ca-app-pub-3940256099942544/2247696110"
  }
  
  struct LanguageLaunchDisplay {
    static let english = "English"
    static let vi = "Việt Nam"
  }
}
