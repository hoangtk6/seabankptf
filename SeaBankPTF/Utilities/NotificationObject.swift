//
//  NotificationObject.swift
//  Comic
//
//  Created by Hoang Vu on 12/25/19.
//  Copyright © 2019 Techlab Corp. All rights reserved.
//

import Foundation

enum NotifiCationType:String {
  case none
  case signInSuccess
  case signOut
  
  case userChangeAvatar
  case userChangeInfo
  case userChangeCoin
  
  case addNewPhoto
  case editPhoto
  
  case addNewVideo
  case editVideo
  
  case insertLoadingCell
  case editCellWithLoading
  case uploadSuccess
  case uploadFail
  case updateProgress
  case resetCellOrigin
  
  case showPopupPolicyViolation
  case showPopupPromote
  
  case updateNotiMess
  case didUpdateBadge
  
  case updateJoinStatus
  case updatePoints

  func value() -> String{
    return self.rawValue
  }
}

class NotificationObject {
  let type: NotifiCationType
  let object: Any?
  
  init(type:NotifiCationType, objectValue:Any?) {
    self.type = type
    self.object = objectValue
  }
}
