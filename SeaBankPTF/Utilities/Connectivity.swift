//
//  Connectivity.swift
//  Comic
//
//  Created by Hoang Vu on 12/9/19.
//  Copyright © 2019 Techlab Corp. All rights reserved.
//

import Foundation
import Alamofire

final class Connectivity {
  class func isConnectedToInternet() -> Bool {
    return NetworkReachabilityManager()!.isReachable
  }
}
