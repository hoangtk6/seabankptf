//
//  UIImage+Comic.swift
//  Comic
//
//  Created by Hoang Vu Van on 11/5/19.
//  Copyright © 2019 Techlab Corp. All rights reserved.
//

import Foundation

typealias ComicAsset = UIImage.Asset

import Foundation
import UIKit

extension UIImage {
  
  enum Asset : String {
    
    //MARK: - Common Icon
    case Common_back_black = "ic_back"
    case Common_Close_white = "ic_close_shadow"
    case Common_Close_black = "ic_close_black"
    case Common_back_light = "ic_back_white"
    case Common_login_back = "ic_login_back_screen"
    
    //MARK: - Tabar
    
    case Tabbar_ic_home_selected = "Tabbar_ic_home_selected"
    case Tabbar_ic_home_unselected = "Tabbar_ic_home_unselected"
    
    case Tabbar_ic_novel_selected = "Tabbar_ic_novel_selected"
    case Tabbar_ic_novel_unselected = "Tabbar_ic_novel_unselected"
    
    case Tabbar_ic_feedback_selected = "Tabbar_ic_feedback_selected"
    case Tabbar_ic_feedback_unselected = "Tabbar_ic_feedback_unselected"
    
    case Tabbar_ic_fun_selected = "Tabbar_ic_fun_selected"
    case Tabbar_ic_fun_unselected = "Tabbar_ic_fun_unselected"
    
    case Tabbar_ic_news_selected = "Tabbar_ic_news_selected"
    case Tabbar_ic_news_unselected = "Tabbar_ic_news_unselected"
    
    
    case Tabbar_ic_search_selected = "Tabbar_ic_search_selected"
    case Tabbar_ic_search_unselected = "Tabbar_ic_search_unselected"
    case Tabbar_ic_myprofile_selected = "Tabbar_ic_myprofile_selected"
    case Tabbar_ic_myprofile_unselected = "Tabbar_ic_myprofile_unselected"
    case Tabbar_ic_bookcase = "Tabbar_ic_bookcase"
    case Tabbar_ic_bookcase_selected = "Tabbar_ic_bookcase_selected"
    
    //MARK: - PlaceHolder
    case img_placeHolder_horizontal = "img_placeHolder_horizontal"
    case img_placeHolder_square = "img_placeHolder_square"
    case img_placeHolder_vertical = "img_placeHolder_vertical"
    case img_placeHolder_avatar = "avatar_default"
    
    //MARK: - Banner
    case img_empty_data = "ic_empty_data"
    case img_error_occurred = "ic_error_occurred"
    case ic_noInternetConnection = "ic_noInternetConnection"
    
    
    var image: UIImage {
      return UIImage(asset: self)
    }
    
    
  }
  
  convenience init!(asset: Asset) {
    self.init(named: asset.rawValue)
  }
}

extension UIImage {
  func resizeWithPercent(percentage: CGFloat) -> UIImage? {
    let imageView = UIImageView(frame: CGRect(origin: .zero, size: CGSize(width: size.width * percentage, height: size.height * percentage)))
    imageView.contentMode = .scaleAspectFit
    imageView.image = self
    UIGraphicsBeginImageContextWithOptions(imageView.bounds.size, false, scale)
    guard let context = UIGraphicsGetCurrentContext() else { return nil }
    imageView.layer.render(in: context)
    guard let result = UIGraphicsGetImageFromCurrentImageContext() else { return nil }
    UIGraphicsEndImageContext()
    return result
  }
  
  func resizeWithWidth(width: CGFloat) -> UIImage? {
    if self.size.width <  width{
      return self
    }
    let imageView = UIImageView(frame: CGRect(origin: .zero, size: CGSize(width: width, height: CGFloat(ceil(width/size.width * size.height)))))
    imageView.contentMode = .scaleAspectFit
    imageView.image = self
    UIGraphicsBeginImageContextWithOptions(imageView.bounds.size, false, scale)
    guard let context = UIGraphicsGetCurrentContext() else { return nil }
    imageView.layer.render(in: context)
    guard let result = UIGraphicsGetImageFromCurrentImageContext() else { return nil }
    UIGraphicsEndImageContext()
    return result
  }
  
  func resizedTo1MB() -> UIImage? {
    guard let imageData = self.pngData() else { return nil }
    
    var resizingImage = self
    var imageSizeKB = Double(imageData.count) / 1000.0 // ! Or devide for 1024 if you need KB but not kB
    
    while imageSizeKB > 1000 { // ! Or use 1024 if you need KB but not kB
      guard let resizedImage = resizingImage.resizeWithPercent(percentage: 0.95),
        let imageData = resizedImage.pngData()
        else { return nil }
      
      resizingImage = resizedImage
      imageSizeKB = Double(imageData.count) / 1000.0 // ! Or devide for 1024 if you need KB but not kB
    }
    
    return resizingImage
  }
}
