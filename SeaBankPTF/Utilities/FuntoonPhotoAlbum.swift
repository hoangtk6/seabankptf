//
//  FuntoonPhotoAlbum.swift
//  Comic
//
//  Copyright © 2020 Techlab Corp. All rights reserved.
//

import Foundation
import UIKit
import Photos


class FuntoonPhotoAlbum {
  
  static let albumName = "Weeboo"
  static let sharedInstance = FuntoonPhotoAlbum()
  var listSavedImage: [UIImage] = []
  var assetCollection: PHAssetCollection!
  
  init() {
    
    func fetchAssetCollectionForAlbum() -> PHAssetCollection! {
      
      let fetchOptions = PHFetchOptions()
      fetchOptions.predicate = NSPredicate(format: "title = %@", FuntoonPhotoAlbum.albumName)
      let collection = PHAssetCollection.fetchAssetCollections(with: .album, subtype: .any, options: fetchOptions)
      
      if let firstObject: AnyObject = collection.firstObject {
        return firstObject as? PHAssetCollection
      }
      
      return nil
    }
    
    if let assetCollection = fetchAssetCollectionForAlbum() {
      self.assetCollection = assetCollection
      return
    }
    
    PHPhotoLibrary.shared().performChanges({
      PHAssetCollectionChangeRequest.creationRequestForAssetCollection(withTitle: FuntoonPhotoAlbum.albumName)
    }) { success, _ in
      if success {
        self.assetCollection = fetchAssetCollectionForAlbum()
        if (self.listSavedImage.count > 0) {
          FuntoonPhotoAlbum.sharedInstance.saveImages(images: self.listSavedImage, start: 0)
        }
      }
    }
  }
  
  func saveImage(image: UIImage) {
    
    if assetCollection == nil {
      return // If there was an error upstream, skip the save.
    }
    
    PHPhotoLibrary.shared().performChanges({
      let assetChangeRequest = PHAssetChangeRequest.creationRequestForAsset(from: image)
      let assetPlaceholder = assetChangeRequest.placeholderForCreatedAsset
      let albumChangeRequest = PHAssetCollectionChangeRequest(for: self.assetCollection)
      albumChangeRequest?.addAssets([assetPlaceholder] as NSFastEnumeration)
    }) { (success, err) in
      
    }
  }
  
  func saveImages(images: [UIImage], start: Int) {
    let count = images.count
    if (start >= count) { return }
    if assetCollection == nil {
      return // If there was an error upstream, skip the save.
    }
    
    PHPhotoLibrary.shared().performChanges({
      let assetChangeRequest = PHAssetChangeRequest.creationRequestForAsset(from: images[start])
      let assetPlaceholder = assetChangeRequest.placeholderForCreatedAsset
      let albumChangeRequest = PHAssetCollectionChangeRequest(for: self.assetCollection)
      albumChangeRequest?.addAssets([assetPlaceholder] as NSFastEnumeration)
    }) { (success, err) in
      if (success) {
        self.saveImages(images: images, start: start + 1)
      }
    }
  }
}
