//
//  UIUtils.swift
//  SeaBankPTF
//
//  Created by Hoang Vu Van on 16/09/2021.
//

import Foundation

final class PTFUIUtils {
  static func setupButton(button:UIButton, title:String, color:UIColor = .white, bg:UIColor = .errorColor(), font:UIFont = RobotoFont.fontWithType(.bold, size: 20), radius:CGFloat = 8, isShowShadow:Bool = false){
    button.setTitleColor(color, for: .normal)
    button.backgroundColor = bg
    button.cornerRadius = radius
    button.layer.borderColor = color.cgColor
    button.layer.borderWidth = 1
    button.setTitle(title, for: .normal)
    button.titleLabel?.font = font

    if isShowShadow{
      button.layer.masksToBounds =  false
      button.shadowColor = UIColor.gray
      button.shadowRadius = 3
      button.shadowOpacity = 0.3;
      button.shadowOffset = CGSize(width: 1, height: 1)
      button.layer.cornerRadius = 10
      button.isUserInteractionEnabled = true
    }
  }
  
}
