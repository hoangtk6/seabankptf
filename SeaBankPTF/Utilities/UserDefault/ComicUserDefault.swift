//
//  ComicUserDefault.swift
//  Comic
//
//  Created by Hoang Vu on 11/30/19.
//  Copyright © 2019 Techlab Corp. All rights reserved.
//

import Foundation

class ComicUserDefault: UserDefaults {
    private static let sharedInstanceVar = ComicUserDefault()

    class func sharedInstance() -> ComicUserDefault {
        return sharedInstanceVar
    }

    override func set(_ value: Any?, forKey key: String) {
        var data: Data?
        if let value = value {
            data = NSKeyedArchiver.archivedData(withRootObject: value)
        }
        standardUserDefaults().set(data, forKey: getKey(ForString: key))
    }

    func getKey(ForString originalKey: String) -> String {
        return className + originalKey
    }

    func standardUserDefaults() -> UserDefaults {
        return UserDefaults.standard
    }

    func getObject(_ key: String) -> Any? {
        let value = standardUserDefaults().object(forKey: getKey(ForString: key))
        if let data = value as? Data {
            return NSKeyedUnarchiver.unarchiveObject(with: data)
        } else {
            return value
        }
    }

    func getArray(_ key: String) -> [Any]? {
        guard let data = standardUserDefaults().object(forKey: getKey(ForString: key)) as? Data else { return nil }
        return NSKeyedUnarchiver.unarchiveObject(with: data) as? [Any]
    }
}
