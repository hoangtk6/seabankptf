//
//  ConfigData.swift
//  Comic
//
//  Created by Hoang Vu on 2/17/20.
//  Copyright © 2020 Techlab Corp. All rights reserved.
//

import Foundation

final class ConfigData: ComicUserDefault {
  
  let defaultDomainImage = "https://cdn.funtoon.vn/"
  let defaultDomainVideo = "https://cdn.funtoon.vn/"
  private static var sharedInstanceVar = ConfigData()
  
  
  override static func sharedInstance() -> ConfigData {
    return sharedInstanceVar
  }
  
  struct ClassConstant {
    static let DOMAIN_IMAGE = "DOMAIN_IMAGE"
    static let DOMAIN_VIDEO = "DOMAIN_VIDEO"
  }
  

  var domainCdnImage:String{
    get {
      return getObject(ClassConstant.DOMAIN_IMAGE) as? String ?? defaultDomainImage
    }
    set {
      set(newValue, forKey: ClassConstant.DOMAIN_IMAGE)
    }
  }
  
  var domainCdnVideo:String?{
    get {
      return getObject(ClassConstant.DOMAIN_VIDEO) as? String 
    }
    set {
      set(newValue, forKey: ClassConstant.DOMAIN_VIDEO)
    }
  }
  
  
  var isEnableOneShotUpload: Bool = true
  var oneShotUploadDescription: String = "Tính năng đang được bảo trì"
}
