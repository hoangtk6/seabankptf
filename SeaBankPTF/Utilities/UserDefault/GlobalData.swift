//
//  GlobalData.swift
//  Comic
//
//  Created by Nguyễn Văn Tú on 9/17/20.
//  Copyright © 2020 Techlab Corp. All rights reserved.
//

import Foundation

final class GlobalData: NSObject {
  
  var didLoadTabMain = false
  private static var sharedInstanceVar = GlobalData()
  
  static func sharedInstance() -> GlobalData {
    return sharedInstanceVar
  }
  
}
