//
//  File.swift
//  Comic
//
//  Created by Hoang Vu on 11/30/19.
//  Copyright © 2019 Techlab Corp. All rights reserved.
//

import KeychainSwift

enum ComicLoginType:Int {
  case unknow = 0
  case comic = 1
  case facebook = 2
  case google = 3
  case apple = 4
  
  func value() -> Int {
    return self.rawValue
  }
}

final class UserData: ComicUserDefault {
  
  private static var sharedInstanceVar = UserData()
  private var keychain = KeychainSwift()
  
  override static func sharedInstance() -> UserData {
    return sharedInstanceVar
  }
  
  struct ClassConstant {
    static let IS_LOGIN = "isLogin"
    static let LOGIN_TYPE = "loginType"
    static let USERNAME = "username"
    static let PASSWORD = "password"
    static let EMAIL = "email"
    static let AVATAR = "avatar"
    static let REFRESH_TOKEN = "refreshToken"
    static let ACCESS_TOKEN = "accessToken"
    static let DEVICE_TOKEN = "deviceToken"
    static let DEVICE_ID = "deviceId"
    static let RECEIPT_DATA = "RECEIPT_DATA"
    static let PRODUCTID = "PRODUCTID"
    
    static let VALID_USERNAMES = "validUsernames"
    static let INVALID_USERNAMES = "invalidUsernames"
    static let COINS = "COINS"
    static let POINTS = "POINTS"
    static let PHONE = "PHONE"
    static let BIRTHDAY = "BIRTHDAY"
    static let USER_ID = "USER_ID"
    static let BIOGRAPHY = "BIOGRAPHY"
    static let CANCHANGEMIBOOID = "CANCHANGEMIBOOID"
    static let ID = "ID"
    
    static let CURRENT_LANGUAGE = "CURRENT_LANGUAGE"
  }
  
  var currentLanguage: String? {
    get {
      return getObject(ClassConstant.CURRENT_LANGUAGE) as? String
    }
    set {
      set(newValue, forKey: ClassConstant.CURRENT_LANGUAGE)
    }
  }
  
  var biography: String {
    get {
      return getObject(ClassConstant.BIOGRAPHY) as? String ?? emptyString
    }
    set {
      set(newValue, forKey: ClassConstant.BIOGRAPHY)
    }
  }
  
  var canChangeMibooId: Bool {
    get {
      return getObject(ClassConstant.CANCHANGEMIBOOID) as? Bool ?? false
    }
    set {
      set(newValue, forKey: ClassConstant.CANCHANGEMIBOOID)
    }
  }
  
  var isLogin: Bool {
    get {
      return getObject(ClassConstant.IS_LOGIN) as? Bool ?? false
    }
    set {
      set(newValue, forKey: ClassConstant.IS_LOGIN)
    }
  }
  
  var loginType: Int {
    get {
      return getObject(ClassConstant.LOGIN_TYPE) as? Int ?? 0
    }
    set {
      set(newValue, forKey: ClassConstant.LOGIN_TYPE)
    }
  }
  
  var username: String {
    get {
      return getObject(ClassConstant.USERNAME) as? String ?? emptyString
    }
    set {
      set(newValue, forKey: ClassConstant.USERNAME)
    }
  }
  
  var email: String {
    get {
      return getObject(ClassConstant.EMAIL) as? String ?? emptyString
    }
    set {
      set(newValue, forKey: ClassConstant.EMAIL)
    }
  }
  
  var avatar: String {
    get {
      return getObject(ClassConstant.AVATAR) as? String ?? emptyString
    }
    set {
      set(newValue, forKey: ClassConstant.AVATAR)
    }
  }
  
  var validUsernames: [String] {
    get {
      return getObject(ClassConstant.VALID_USERNAMES) as? [String] ?? []
    }
    set {
      set(newValue, forKey: ClassConstant.VALID_USERNAMES)
    }
  }
  
  var invalidUsernames: [String] {
    get {
      return getObject(ClassConstant.INVALID_USERNAMES) as? [String] ?? []
    }
    set {
      set(newValue, forKey: ClassConstant.INVALID_USERNAMES)
    }
  }
  
  var phone : String {
    get {
      return getObject(ClassConstant.PHONE) as? String ?? emptyString
    }
    set {
      set(newValue, forKey: ClassConstant.PHONE)
    }
  }
  var userId : String {
    get {
      return getObject(ClassConstant.USER_ID) as? String ?? emptyString
    }
    set {
      set(newValue, forKey: ClassConstant.USER_ID)
    }
  }
  
  var id : String {
    get {
      return getObject(ClassConstant.ID) as? String ?? emptyString
    }
    set {
      set(newValue, forKey: ClassConstant.ID)
    }
  }
  
  var birthday: String {
    get {
      return getObject(ClassConstant.BIRTHDAY) as? String ?? emptyString
    }
    set {
      set(newValue, forKey: ClassConstant.BIRTHDAY)
    }
  }
  
  var deviceToken: String {
    get {
      return getObject(ClassConstant.DEVICE_TOKEN) as? String ?? emptyString
    }
    set {
      set(newValue, forKey: ClassConstant.DEVICE_TOKEN)
    }
  }
  
  //MARK: - Keychain
  var password: String {
    get {
      return keychain.get(ClassConstant.PASSWORD) ?? emptyString
    }
    set {
      keychain.set(newValue, forKey: ClassConstant.PASSWORD)
    }
  }
  
  var coins: String {
    get {
      return keychain.get(ClassConstant.COINS) ?? "0"
    }
    set {
      keychain.set(newValue, forKey: ClassConstant.COINS)
    }
  }
  
  var points: String {
    get {
      return keychain.get(ClassConstant.POINTS) ?? "0"
    }
    set {
      keychain.set(newValue, forKey: ClassConstant.POINTS)
    }
  }
  
  var refreshToken: String {
    get {
      return keychain.get(ClassConstant.REFRESH_TOKEN) ?? emptyString
    }
    set {
      keychain.set(newValue, forKey: ClassConstant.REFRESH_TOKEN)
    }
  }
  
  var accessToken: String {
    get {
      return keychain.get(ClassConstant.ACCESS_TOKEN) ?? emptyString
    }
    set {
      keychain.set(newValue, forKey: ClassConstant.ACCESS_TOKEN)
    }
  }
  
  var deviceId: String {
    get {
      return keychain.get(ClassConstant.DEVICE_ID) ?? emptyString
    }
    set {
      keychain.set(newValue, forKey: ClassConstant.DEVICE_ID)
    }
  }
  
  var receiptData: String {
    get {
      return keychain.get(ClassConstant.RECEIPT_DATA) ?? emptyString
    }
    set {
      keychain.set(newValue, forKey: ClassConstant.RECEIPT_DATA)
    }
  }
  
  var productId: String {
    get {
      return keychain.get(ClassConstant.PRODUCTID) ?? emptyString
    }
    set {
      keychain.set(newValue, forKey: ClassConstant.PRODUCTID)
    }
  }

  // MARK: - Function, Lưu ý ko xoá deviceId
  func checkResetKeychain(){
    if !UserData.sharedInstance().isLogin {
      UserData.sharedInstance().password = emptyString
      UserData.sharedInstance().coins = "0"
      UserData.sharedInstance().points = "0"
      UserData.sharedInstance().refreshToken = emptyString
      UserData.sharedInstance().accessToken = emptyString
      //keychain.clear()
    }
  }
  
  func clearCache(){
    UserData.sharedInstance().isLogin = false
    UserData.sharedInstance().refreshToken = emptyString
    UserData.sharedInstance().accessToken = emptyString
    UserData.sharedInstance().username = emptyString
    UserData.sharedInstance().email = emptyString
    UserData.sharedInstance().coins = String(0)
    UserData.sharedInstance().avatar = emptyString
    UserData.sharedInstance().userId = emptyString
    UserData.sharedInstance().biography = emptyString
    UserData.sharedInstance().canChangeMibooId = false
    UserData.sharedInstance().points = "0"
  }
}
