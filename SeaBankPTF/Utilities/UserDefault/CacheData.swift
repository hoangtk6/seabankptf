//
//  CacheData.swift
//  Comic
//
//  Created by Hoang Vu on 12/9/19.
//  Copyright © 2019 Techlab Corp. All rights reserved.
//

import Foundation

final class CacheData: ComicUserDefault {
  
  private static var sharedInstanceVar = CacheData()
  
  override static func sharedInstance() -> CacheData {
    return sharedInstanceVar
  }
  
  enum NewsFeedType: String {
    case newest
    case trending
    case following
    case video
  }
  
  struct ClassConstant {
    static let CONFIG_DATA = "CONFIG_DATA"
    static let RECENT_SEARCHES = "RECENT_SEARCHES"
    static let NOTI_CACHE_DATA = "NOTI_CACHE_DATA"
    static let HOME_TYPE_DATA = "HOME_TYPE_DATA"
    static let ON_BOARDING = "ON_BOARDING"
    static let ON_BOARDING_CASE = "ON_BOARDING_CASE"
  }
  
  var configData: Data? {
    get {
      return getObject(ClassConstant.CONFIG_DATA) as? Data ?? nil
    }
    set {
      set(newValue, forKey: ClassConstant.CONFIG_DATA)
    }
  }
  
  var searchHistory:[String]{
    get {
      return getObject(ClassConstant.RECENT_SEARCHES) as? [String] ?? []
    }
    set {
      set(newValue, forKey: ClassConstant.RECENT_SEARCHES)
    }
  }
  
  var notiCacheData: Data? {
    get {
      return getObject(ClassConstant.NOTI_CACHE_DATA) as? Data ?? nil
    }
    set {
      set(newValue, forKey: ClassConstant.NOTI_CACHE_DATA)
    }
  }
  
  //save home type after select done
  var homeType: String? {
    get {
      return getObject(ClassConstant.HOME_TYPE_DATA) as? String ?? nil
    }
    set {
      set(newValue, forKey: ClassConstant.HOME_TYPE_DATA)
    }
  }
  
  var onBoarding: Bool? {
    get {
      return getObject(ClassConstant.ON_BOARDING) as? Bool
    }
    set {
      set(newValue, forKey: ClassConstant.ON_BOARDING)
    }
  }
  var onBoardingCase: Int{
    get {
      return getObject(ClassConstant.ON_BOARDING_CASE) as? Int ?? 0
    }
    set {
      set(newValue, forKey: ClassConstant.ON_BOARDING_CASE)
    }
  }
  
  func clearCache(){

  }
}
