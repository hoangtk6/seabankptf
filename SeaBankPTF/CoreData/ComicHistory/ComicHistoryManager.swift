//
//  ComicHistoryManager.swift
//  Comic
//
//  Created by Hoang Vu on 1/6/20.
//  Copyright © 2020 Techlab Corp. All rights reserved.
//

import Foundation
import CoreData

final class ComicHistoryManager {
  private let notificationCenter = NotificationCenter.default
  
  fileprivate let persistenceManager: PersistenceManager!
  fileprivate var mainContextInstance: NSManagedObjectContext!
  
  //Utilize Singleton pattern by instanciating ComicHistoryAPI only once.
  class var sharedInstance: ComicHistoryManager {
    struct Singleton {
      static let instance = ComicHistoryManager()
    }
    
    return Singleton.instance
  }
  
  init() {
    self.persistenceManager = PersistenceManager.sharedInstance
    self.mainContextInstance = persistenceManager.getMainContextInstance()
  }
  
  func getAllComicHistory(_ sortedByDate: Bool = true, sortAscending: Bool = true, _ offset:Int = 0, _ limit:Int = 0) -> Array<ComicHistory> {
    var fetchedResults: Array<ComicHistory> = Array<ComicHistory>()
    
    // Create request on ComicHistory entity
    let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: EntityTypes.ComicHistory.rawValue)
    if limit > 0{
      fetchRequest.fetchOffset = offset
      fetchRequest.fetchLimit = limit
    }
    //Create sort descriptor to sort retrieved ComicHistorys by Date, ascending
    if sortedByDate {
      let sortDescriptor = NSSortDescriptor(key: ComicHistoryAttributes.timeStamp.rawValue,
                                            ascending: sortAscending)
      let sortDescriptors = [sortDescriptor]
      fetchRequest.sortDescriptors = sortDescriptors
    }
    
    //Execute Fetch request
    do {
      fetchedResults = try  self.mainContextInstance.fetch(fetchRequest) as! [ComicHistory]
    } catch let fetchError as NSError {
      print("retrieveById error: \(fetchError.localizedDescription)")
      fetchedResults = Array<ComicHistory>()
    }
    
    return fetchedResults
  }
  
  
  func getComicHistoryNotSync(_ isSync: Bool) -> Array<ComicHistory> {
    var fetchedResults: Array<ComicHistory> = Array<ComicHistory>()
    
    // Create request on ComicHistory entity
    let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: EntityTypes.ComicHistory.rawValue)
    
    //Add a predicate to filter by ComicHistoryId
    let findByIdPredicate =
      NSPredicate(format: "\(ComicHistoryAttributes.isSync.rawValue) = %@", isSync)
    fetchRequest.predicate = findByIdPredicate
    
    //Execute Fetch request
    do {
      fetchedResults = try self.mainContextInstance.fetch(fetchRequest) as! [ComicHistory]
    } catch let fetchError as NSError {
      print("retrieveById error: \(fetchError.localizedDescription)")
      fetchedResults = Array<ComicHistory>()
    }
    
    return fetchedResults
  }
  
  func getComicHistoryById(_ comicId: String) -> Array<ComicHistory> {
    var fetchedResults: Array<ComicHistory> = Array<ComicHistory>()
    
    // Create request on ComicHistory entity
    let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: EntityTypes.ComicHistory.rawValue)
    
    //Add a predicate to filter by ComicHistoryId
    let findByIdPredicate =
      NSPredicate(format: "\(ComicHistoryAttributes.comicId.rawValue) = %@", comicId)
    fetchRequest.predicate = findByIdPredicate
    
    //Execute Fetch request
    do {
      fetchedResults = try self.mainContextInstance.fetch(fetchRequest) as! [ComicHistory]
    } catch let fetchError as NSError {
      print("retrieveById error: \(fetchError.localizedDescription)")
      fetchedResults = Array<ComicHistory>()
    }
    
    return fetchedResults
  }
  
  /*
  func saveComicHistory(_ comic: HomeComicModel, chapter: ChapterDetailModel?, chapReadIds:String?, currentPage:Int, offset:Float, indexOf:Int) {
    guard let comicId = comic.id else {
      return
    }
    
    let retrievedItems = getComicHistoryById(comicId)
    //Delete all ComicHistory items from persistance layer
    for item in retrievedItems {
      self.mainContextInstance.delete(item)
    }
    
    //Save and merge changes from Minion workers with Main context
    self.persistenceManager.mergeWithMainContext()
    
    
    
    //Minion Context worker with Private Concurrency type.
    let minionManagedObjectContextWorker: NSManagedObjectContext =
      NSManagedObjectContext.init(concurrencyType: NSManagedObjectContextConcurrencyType.privateQueueConcurrencyType)
    minionManagedObjectContextWorker.parent = self.mainContextInstance
    
    //Create new Object of ComicHistory entity
    let newComic = NSEntityDescription.insertNewObject(forEntityName: EntityTypes.ComicHistory.rawValue,
                                                       into: minionManagedObjectContextWorker) as! ComicHistory
    
    newComic.comicId = comicId
    newComic.comicName = comic.comicName ?? emptyString
    newComic.squareLogo = comic.squareLogo
    newComic.verticalLogo = comic.verticalLogo
    newComic.horizontalLogo = comic.horizontalLogo
    newComic.desc = comic.descriptionValue != nil ? comic.descriptionValue! : emptyString
    newComic.chapterId = chapter?.id
    newComic.chapterName = chapter?.chapterName
    newComic.currentPage = Int16(currentPage)
    newComic.offset = offset
    newComic.chapterNum = chapter?.chapterNum 
    newComic.cateName =  ComicUtils.getCategoryTitle(categories: comic.categories)
    newComic.timeStamp = NSDate.getCurrentTimeStamp()
    newComic.chapReading = chapReadIds
    newComic.indexOfChapReading = indexOf
    newComic.newestChapter = comic.newestChapter ?? emptyString
    
    
    //Save current work on Minion workers
    self.persistenceManager.saveWorkerContext(minionManagedObjectContextWorker)
    
    //Save and merge changes from Minion workers with Main context
    self.persistenceManager.mergeWithMainContext()
    
    //Post notification to update datasource of a given Viewcontroller/UITableView
    self.postUpdateNotification(notifyType: .comicHistoryInsertData)
  }
  */
  
  func updateComic(_ comic: ComicHistory, newComic: Dictionary<String, AnyObject>) {
    let minionManagedObjectContextWorker: NSManagedObjectContext =
      NSManagedObjectContext.init(concurrencyType: NSManagedObjectContextConcurrencyType.privateQueueConcurrencyType)
    minionManagedObjectContextWorker.parent = self.mainContextInstance
    
    //Assign field values
    for (key, value) in newComic {
      for attribute in ComicHistoryAttributes.getAll {
        if (key == attribute.rawValue) {
          comic.setValue(value, forKey: key)
        }
      }
    }
    
    //Persist new Event to datastore (via Managed Object Context Layer).
    self.persistenceManager.saveWorkerContext(minionManagedObjectContextWorker)
    self.persistenceManager.mergeWithMainContext()
  }
  
  fileprivate func postUpdateNotification(notifyType: NotifiCationType) {
    let notifyObj = NotificationObject.init(type:notifyType, objectValue: nil)
    self.notificationCenter
      .post(name: .comicHistoryChangeData,
            object: notifyObj)
  }
  
}
