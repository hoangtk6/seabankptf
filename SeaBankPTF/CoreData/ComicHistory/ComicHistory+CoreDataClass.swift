//
//  ComicHistory+CoreDataClass.swift
//  
//
//  Created by Hoang Vu on 1/6/20.
//
//

import Foundation
import CoreData

@objc(ComicHistory)
public class ComicHistory: NSManagedObject {
  var isEditState: Bool = false
  var isShowInfo: Bool = false
}
