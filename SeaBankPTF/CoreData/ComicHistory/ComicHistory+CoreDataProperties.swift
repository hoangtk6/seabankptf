//
//  ComicHistory+CoreDataProperties.swift
//  
//
//  Created by Hoang Vu on 1/6/20.
//
//

import Foundation
import CoreData

enum ComicHistoryAttributes: String {
  case
  comicId    = "comicId",
  comicName      = "comicName",
  logo      = "logo",
  timeStamp       = "timeStamp",
  chapterId = "chapterId",
  chapterName = "chapterName",
  chapterNum = "chapterNum",
  currentPage = "currentPage",
  offset = "offset",
  cateName = "cateName",
  desc       = "desc",
  chapReading = "chapReading",
  indexOfChapReading = "indexOfChapReading",
  isSync = "isSync",
  newestChapter = "newestChapter"
  
  
  static let getAll = [
    comicId,
    comicName,
    logo,
    timeStamp,
    chapterId,
    chapterName,
    chapterNum,
    currentPage,
    offset,
    cateName,
    desc,
    chapReading,
    indexOfChapReading,
    isSync,
    newestChapter
  ]
}

extension ComicHistory {
  
  @nonobjc public class func fetchRequest() -> NSFetchRequest<ComicHistory> {
    return NSFetchRequest<ComicHistory>(entityName: "ComicHistory")
  }
  
  @NSManaged public var comicId: String?
  @NSManaged public var timeStamp: Int64
  @NSManaged public var currentPage: Int16
  @NSManaged public var offset: Float
  @NSManaged public var squareLogo: String?
  @NSManaged public var horizontalLogo: String?
  @NSManaged public var verticalLogo: String?
  @NSManaged public var comicName: String?
  @NSManaged public var chapterId: String?
  @NSManaged public var chapterName: String?
  @NSManaged public var chapterNum: String?
  @NSManaged public var cateName: String?
  @NSManaged public var desc: String?
  @NSManaged public var chapReading: String?
  @NSManaged public var indexOfChapReading: Int
  @NSManaged public var isSync: Bool
  @NSManaged public var newestChapter: String?
  
}
