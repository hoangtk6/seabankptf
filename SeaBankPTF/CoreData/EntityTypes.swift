//
//  EntityTypes.swift
//  Comic
//
//  Created by Hoang Vu on 1/6/20.
//  Copyright © 2020 Techlab Corp. All rights reserved.
//

import Foundation
enum EntityTypes: String {
  case ComicHistory = "ComicHistory"
  case ComicDownload = "ComicDownload"
  case ChapterDownload = "ChapterDownload"
  case NovelHistory = "NovelHistory"
  case RetryItem = "RetryItem"
  //case Foo = "Foo"
  //case Bar = "Bar"
  
  static let getAll = [ComicHistory, ComicDownload, ChapterDownload, NovelHistory, RetryItem] //[Event, Foo,Bar]
}
