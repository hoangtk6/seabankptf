//
//  ComicMainNavigationController.swift
//  Comic
//
//  Created by Hoang Vu Van on 11/5/19.
//  Copyright © 2019 Techlab Corp. All rights reserved.
//


import UIKit

class ZNavigationController: UINavigationController {
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.navigationBar.tintColor = .black
    self.navigationBar.barTintColor = .white
    self.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor:UIColor.black]
    
    //Fix bug UI thừa View
    if #available(iOS 11.0, *) {
  
    } else {
      self.navigationBar.isTranslucent = false
    }
  }
  
  //    override var preferredStatusBarStyle : UIStatusBarStyle {
  //        return .lightContent
  //    }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
  }
  
}

extension ZNavigationController {
  open override var preferredStatusBarStyle: UIStatusBarStyle {
    return topViewController?.preferredStatusBarStyle ?? .default
  }
  
  func removeFromStack(vc:UIViewController){
    func removeViewController(_ controller: UIViewController.Type) {
      if let viewController = viewControllers.first(where: { $0.isKind(of: controller.self) }) {
        viewController.removeFromParent()
      }
    }
  }
}
