//
//  BaseViewController.swift
//  Comic
//
//  Created by Hoang Vu Van on 11/5/19.
//  Copyright © 2019 Techlab Corp. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
  
  //MARK:- View Lifecycle
  override func viewDidLoad() {
    super.viewDidLoad()
    
    applyLocalization()
    NotificationCenter.default.addObserver(self,
                                           selector: #selector(applyLocalization),
                                           name: LocalizationNotification.didChange,
                                           object: nil)
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
  }
  
  override func viewDidDisappear(_ animated: Bool) {
    super.viewDidDisappear(animated)
  }
  
  override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    super.touchesBegan(touches, with: event)
  }
  
  deinit {
    //log.info("[deinit class name] : \(self.classForCoder)")
  }
  
  @objc func applyLocalization() {}
}
