//
//  MainTabBarController.swift
//  Comic
//
//  Created by Hoang Vu Van on 11/5/19.
//  Copyright © 2019 Techlab Corp. All rights reserved.
//

import Foundation
import UIKit

final class PTFTabBarController: UITabBarController {
  
  static var previousController:UIViewController?
  var navigationVCArray = [ZNavigationController]()
  private let notificationCenter = NotificationCenter.default

  override func viewDidLoad() {
    super.viewDidLoad()
    self.delegate = self
    setupViewContollers()
    self.selectedIndex = 1
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)

  }
  
  func setupViewContollers() {
    let titleArray = [
      "Tab1",
      "Tab2",
      "Tab3",
      "Tab4"]
    
    let normalImagesArray = [
      ComicAsset.Tabbar_ic_fun_unselected.image,
      ComicAsset.Tabbar_ic_home_unselected.image,
      ComicAsset.Tabbar_ic_novel_unselected.image,
      //      ComicAsset.Tabbar_ic_news_unselected.image,
      ComicAsset.Tabbar_ic_bookcase.image
    ]
    
    let selectedImagesArray = [
      ComicAsset.Tabbar_ic_fun_selected.image,
      ComicAsset.Tabbar_ic_home_selected.image,
      ComicAsset.Tabbar_ic_novel_selected.image,
      //      ComicAsset.Tabbar_ic_news_selected.image,
      ComicAsset.Tabbar_ic_bookcase_selected.image
    ]
    
    let newsfeedVC = UIViewController()
    let homeVC = UIViewController()
    let novelVC = UIViewController()
    let bookCaseVC = UIViewController()

    let viewControllerArray = [
      newsfeedVC,
      homeVC,
      novelVC,
      bookCaseVC
    ]
    
    let selectAttributes: [NSAttributedString.Key : AnyObject] = [
      NSAttributedString.Key.foregroundColor : UIColor.colorWeeboo(),
      NSAttributedString.Key.font : UIFont.fontNavigationBarBag()
    ]
    
    let normalAttributes: [NSAttributedString.Key : AnyObject] = [
      NSAttributedString.Key.foregroundColor : UIColor.descTextColor(),
      NSAttributedString.Key.font :  UIFont.fontNavigationBarBag()
    ]
    
    
    for (index, controller) in viewControllerArray.enumerated() {
      controller.tabBarItem.title = titleArray[index]
      controller.tabBarItem.image = normalImagesArray[index].withRenderingMode(.alwaysOriginal)
      controller.tabBarItem.selectedImage = selectedImagesArray[index].withRenderingMode(.alwaysOriginal)
      controller.tabBarItem.setTitleTextAttributes(normalAttributes, for: .normal)
      controller.tabBarItem.setTitleTextAttributes(selectAttributes, for: .selected)
      
      let navigationController = ZNavigationController(rootViewController: controller)
      navigationVCArray.append(navigationController)
      
    }
    
    
    if #available(iOS 13, *) {
      let appearance = UITabBarAppearance()
      appearance.backgroundColor = .white
      appearance.stackedLayoutAppearance.normal.titleTextAttributes = normalAttributes
      appearance.stackedLayoutAppearance.selected.titleTextAttributes = selectAttributes
      
      self.tabBar.standardAppearance = appearance
    }
    
    self.viewControllers = navigationVCArray
    
    UITabBar.appearance().backgroundColor = UIColor.white
    self.extendedLayoutIncludesOpaqueBars = true
    self.edgesForExtendedLayout = UIRectEdge(rawValue: UIRectEdge.top.rawValue | UIRectEdge.left.rawValue | UIRectEdge.right.rawValue)
    
    //WeebooTabBarController.previousController = homeVC
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  func changeTabWithClass(vcClass:AnyClass, tabIndex:Int = 0){
    var index = 0
    var checkNav:UINavigationController!
    for nav in self.viewControllers! {
      let nav = nav as! UINavigationController
      if (nav.viewControllers.first?.isKind(of: vcClass.self))! {
        checkNav = nav
        break
      }
      index += 1
    }
    
    if checkNav == self.navigationVCArray[0]{
      
    } else if checkNav == self.navigationVCArray[1] {
      
    } else if checkNav == self.navigationVCArray[2] {
      
    }
    
    self.selectedIndex = index
  }
}

extension PTFTabBarController:UITabBarControllerDelegate{
  
  func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
    if viewController is UINavigationController {
      let controller = (viewController as! UINavigationController).viewControllers.first
      if PTFTabBarController.previousController == controller {
        guard let vc =  controller else {
          return
        }

      }
      PTFTabBarController.previousController = controller
    }
  }
  
}
