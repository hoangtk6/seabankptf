//
//  TTReorderableCollectionViewFlowLayout.m
//  KaraSlide
//
//  Created by TuanTN on 8/24/16.
//  Copyright © 2016 Storm. All rights reserved.
//

#import "TTReorderableCollectionViewFlowLayout.h"
#import <QuartzCore/QuartzCore.h>
#import <objc/runtime.h>

#ifndef CGGEOMETRY_TTSUPPORT_H_
CG_INLINE CGPoint
TTS_CGPointAdd(CGPoint point1, CGPoint point2) {
    return CGPointMake(point1.x + point2.x, point1.y + point2.y);
}
#endif

typedef NS_ENUM(NSInteger, TTScrollingDirection) {
    TTScrollingDirectionUnknown = 0,
    TTScrollingDirectionUp,
    TTScrollingDirectionDown,
    TTScrollingDirectionLeft,
    TTScrollingDirectionRight
};

static NSString * const kTTScrollingDirectionKey = @"TTScrollingDirection";
static NSString * const kTTCollectionViewKeyPath = @"collectionView";

@interface CADisplayLink (TT_userInfo)
@property (nonatomic, copy) NSDictionary *TT_userInfo;
@end

@implementation CADisplayLink (TT_userInfo)

- (void)setTT_userInfo:(NSDictionary *) TT_userInfo
{
    objc_setAssociatedObject(self, "TT_userInfo", TT_userInfo, OBJC_ASSOCIATION_COPY);
}

- (NSDictionary *)TT_userInfo
{
    return objc_getAssociatedObject(self, "TT_userInfo");
}

@end

@interface UICollectionViewCell (TTReorderableCollectionViewFlowLayout)

- (UIView *)TT_snapshotView;

@end

@implementation UICollectionViewCell (TTReorderableCollectionViewFlowLayout)

- (UIView *)TT_snapshotView
{
    UIGraphicsBeginImageContextWithOptions(self.bounds.size, NO, [UIScreen mainScreen].scale);
    [self.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return [[UIImageView alloc] initWithImage:image];
}

@end

@interface TTReorderableCollectionViewFlowLayout () <UIGestureRecognizerDelegate>

@property (nonatomic, strong) NSMutableDictionary *supplementaryAttributes;
@property (nonatomic, strong) NSMutableArray *cellAttributesBySection;
@property (nonatomic, assign, readwrite) CGFloat collectionViewContentLength;
@property (nonatomic, assign, readwrite) NSMutableArray *arrCollectionViewContentLength;

@property (strong, nonatomic) NSIndexPath *selectedItemIndexPath;
@property (strong, nonatomic) UIView *currentView;
@property (assign, nonatomic) CGPoint currentViewCenter;
@property (assign, nonatomic) CGPoint panTranslationInCollectionView;
@property (strong, nonatomic) CADisplayLink *displayLink;

@property (assign, nonatomic, readonly) id<TTReorderableCollectionViewDataSource> dataSource;
@property (assign, nonatomic, readonly) id<TTReorderableCollectionViewDelegateFlowLayout> delegate;

@end

@implementation TTReorderableCollectionViewFlowLayout

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self sharedInit];
        [self addObserver:self forKeyPath:kTTCollectionViewKeyPath options:NSKeyValueObservingOptionNew context:nil];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self sharedInit];
        [self addObserver:self forKeyPath:kTTCollectionViewKeyPath options:NSKeyValueObservingOptionNew context:nil];
    }
    return self;
}

- (void)dealloc
{
    [self invalidatesScrollTimer];
    [self tearDownCollectionView];
    
    @try {
        [self removeObserver:self forKeyPath:kTTCollectionViewKeyPath];
    }
    @catch (NSException *exception) {}
}

- (NSMutableArray *)arrCollectionViewContentLength
{
    if (!_arrCollectionViewContentLength) {
        _arrCollectionViewContentLength = [NSMutableArray array];
    }
    return _arrCollectionViewContentLength;
}

- (void)sharedInit
{
    _numberOfItemsPerLine = 1;
    _aspectRatio = 1;
    _sectionInset = UIEdgeInsetsZero;
    _interitemSpacing = 10;
    _lineSpacing = 10;
    _scrollDirection = UICollectionViewScrollDirectionVertical;
    _headerReferenceLength = 0;
    _footerReferenceLength = 0;
    
    _scrollingSpeed = 300.0f;
    _scrollingTriggerEdgeInsets = UIEdgeInsetsMake(50.0f, 50.0f, 50.0f, 50.0f);
}

- (void)setupCollectionView
{
    _longPressGestureRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPressGesture:)];
    _longPressGestureRecognizer.delegate = self;
    
    for (UIGestureRecognizer *gestureRecognizer in self.collectionView.gestureRecognizers) {
        if ([gestureRecognizer isKindOfClass:[UILongPressGestureRecognizer class]]) {
            [gestureRecognizer requireGestureRecognizerToFail:_longPressGestureRecognizer];
        }
    }
    
    [self.collectionView addGestureRecognizer:_longPressGestureRecognizer];
    
    _panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePanGesture:)];
    _panGestureRecognizer.delegate = self;
    [self.collectionView addGestureRecognizer:_panGestureRecognizer];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleApplicationWillResignActive:) name: UIApplicationWillResignActiveNotification object:nil];
}

- (void)tearDownCollectionView
{
    if (_longPressGestureRecognizer)
    {
        UIView *view = _longPressGestureRecognizer.view;
        if (view) {
            [view removeGestureRecognizer:_longPressGestureRecognizer];
        }
        
        _longPressGestureRecognizer.delegate = nil;
        _longPressGestureRecognizer = nil;
    }
    
    if (_panGestureRecognizer)
    {
        UIView *view = _panGestureRecognizer.view;
        if (view) {
            [view removeGestureRecognizer:_panGestureRecognizer];
        }
        
        _panGestureRecognizer.delegate = nil;
        _panGestureRecognizer = nil;
    }
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillResignActiveNotification object:nil];
}

- (id<TTReorderableCollectionViewDataSource>)dataSource {
    return (id<TTReorderableCollectionViewDataSource>)self.collectionView.dataSource;
}

- (id<TTReorderableCollectionViewDelegateFlowLayout>)delegate {
    return (id<TTReorderableCollectionViewDelegateFlowLayout>)self.collectionView.delegate;
}

- (void)invalidateLayoutIfNecessary
{
    NSIndexPath *newIndexPath = [self.collectionView indexPathForItemAtPoint:self.currentView.center];
    NSIndexPath *previousIndexPath = self.selectedItemIndexPath;
    
    if ((newIndexPath == nil) || [newIndexPath isEqual:previousIndexPath]) {
        return;
    }
    
    if ([self.dataSource respondsToSelector:@selector(collectionView:itemAtIndexPath:canMoveToIndexPath:)] &&
        ![self.dataSource collectionView:self.collectionView itemAtIndexPath:previousIndexPath canMoveToIndexPath:newIndexPath]) {
        return;
    }
    
    self.selectedItemIndexPath = newIndexPath;
    
    if ([self.dataSource respondsToSelector:@selector(collectionView:itemAtIndexPath:willMoveToIndexPath:)]) {
        [self.dataSource collectionView:self.collectionView itemAtIndexPath:previousIndexPath willMoveToIndexPath:newIndexPath];
    }
    
    __weak typeof(self) weakSelf = self;
    [self.collectionView performBatchUpdates:^{
        __strong typeof(self) strongSelf = weakSelf;
        if (strongSelf) {
            [strongSelf.collectionView deleteItemsAtIndexPaths:@[ previousIndexPath ]];
            [strongSelf.collectionView insertItemsAtIndexPaths:@[ newIndexPath ]];
        }
    } completion:^(BOOL finished) {
        __strong typeof(self) strongSelf = weakSelf;
        if ([strongSelf.dataSource respondsToSelector:@selector(collectionView:itemAtIndexPath:didMoveToIndexPath:)]) {
            [strongSelf.dataSource collectionView:strongSelf.collectionView itemAtIndexPath:previousIndexPath didMoveToIndexPath:newIndexPath];
        }
    }];
}

- (void)invalidatesScrollTimer
{
    if (!self.displayLink.paused) {
        [self.displayLink invalidate];
    }
    self.displayLink = nil;
}

- (void)setupScrollTimerInDirection:(TTScrollingDirection)direction
{
    if (!self.displayLink.paused)
    {
        TTScrollingDirection oldDirection = [self.displayLink.TT_userInfo[kTTScrollingDirectionKey] integerValue];
        if (direction == oldDirection) {
            return;
        }
    }
    
    [self invalidatesScrollTimer];
    
    self.displayLink = [CADisplayLink displayLinkWithTarget:self selector:@selector(handleScroll:)];
    self.displayLink.TT_userInfo = @{ kTTScrollingDirectionKey : @(direction) };
    
    [self.displayLink addToRunLoop:[NSRunLoop mainRunLoop] forMode:NSRunLoopCommonModes];
}

- (void)prepareLayout
{
    [self calculateContentSize];
    [self calculateLayoutAttributes];
}

- (CGSize)collectionViewContentSize
{
    if (self.scrollDirection == UICollectionViewScrollDirectionVertical) {
        return CGSizeMake(self.collectionView.bounds.size.width, self.collectionViewContentLength);
    }
    else {
        return CGSizeMake(self.collectionViewContentLength, self.collectionView.bounds.size.height);
    }
}


#pragma mark - UICollectionViewLayout OverriddenMethods

- (NSArray *)layoutAttributesForElementsInRect:(CGRect)rect
{
    NSMutableArray *visibleAttributes = [NSMutableArray array];
    for (NSArray *sectionAttributes in self.cellAttributesBySection) {
        for (UICollectionViewLayoutAttributes *layoutAttributes in sectionAttributes)
        {
            if (CGRectIntersectsRect(rect, layoutAttributes.frame)) {
                [visibleAttributes addObject:layoutAttributes];
                
                if (layoutAttributes.representedElementCategory == UICollectionElementCategoryCell) {
                    [self applyLayoutAttributes:layoutAttributes];
                }
            }
        }
    }
    
    [self.supplementaryAttributes enumerateKeysAndObjectsUsingBlock:^(NSString *kindKey, NSDictionary *attributesDict, BOOL *stop)
     {
         [attributesDict enumerateKeysAndObjectsUsingBlock:^(NSIndexPath *pathKey, UICollectionViewLayoutAttributes *layoutAttributes, BOOL *stop) {
             if (CGRectIntersectsRect(rect, layoutAttributes.frame)) {
                 [visibleAttributes addObject:layoutAttributes];
                 
                 if (layoutAttributes.representedElementCategory == UICollectionElementCategoryCell) {
                     [self applyLayoutAttributes:layoutAttributes];
                 }
             }
         }];
     }];
    
    return [visibleAttributes copy];
}

- (UICollectionViewLayoutAttributes *)layoutAttributesForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewLayoutAttributes *layoutAttributes = self.cellAttributesBySection[indexPath.section][indexPath.item];
    if (layoutAttributes.representedElementCategory == UICollectionElementCategoryCell) {
        [self applyLayoutAttributes:layoutAttributes];
    }
    
    return layoutAttributes;
}

- (UICollectionViewLayoutAttributes *)layoutAttributesForSupplementaryViewOfKind:(NSString *)elementKind atIndexPath:(NSIndexPath *)indexPath
{
    return self.supplementaryAttributes[elementKind][indexPath];
}

- (BOOL)shouldInvalidateLayoutForBoundsChange:(CGRect)newBounds
{
    return !CGSizeEqualToSize(newBounds.size, self.collectionView.bounds.size);
}

- (void)applyLayoutAttributes:(UICollectionViewLayoutAttributes *)layoutAttributes {
    if ([layoutAttributes.indexPath isEqual:self.selectedItemIndexPath]) {
        layoutAttributes.hidden = YES;
    }
    else {
        layoutAttributes.hidden = NO;
    }
}

- (void)calculateContentSize
{
    CGFloat contentLength = 0;
    
    NSInteger sections = [self.collectionView numberOfSections];
    for (NSInteger section = 0; section < sections; section++) {
        contentLength += [self contentLengthForSection:section];
    }
    
    self.collectionViewContentLength = contentLength;
}

//- (CGFloat)contentLengthForSection:(NSInteger)section
//{
//    NSInteger rowsInSection = [self rowsInSection:section];
//
//    CGFloat contentLength = (rowsInSection - 1) * [self lineSpacingForSection:section];
//    contentLength += [self lengthwiseInsetLengthInSection:section];
//
//    CGSize cellSize = [self cellSizeInSection:section];
//    if (self.scrollDirection == UICollectionViewScrollDirectionVertical) {
//        contentLength += rowsInSection * cellSize.height;
//    }
//    else {
//        contentLength += rowsInSection * cellSize.width;
//    }
//
//    contentLength += [self headerLengthForSection:section];
//    contentLength += [self footerLengthForSection:section];
//
//    return contentLength;
//}

- (CGFloat)contentLengthForSection:(NSInteger)section
{
    NSInteger rowsInSection = [self rowsInSection:section];
    NSInteger itemsInSection = [self.collectionView numberOfItemsInSection:section];
    
    CGFloat contentLength = (rowsInSection - 1) * [self lineSpacingForSection:section];
    contentLength += [self lengthwiseInsetLengthInSection:section];
    
    
    CGFloat totalHeightRow = 0;
    
    if (itemsInSection > self.numberOfItemsPerLine) {
        
        int j = 0;
        for (int i = 0; i < itemsInSection; i += self.numberOfItemsPerLine) {
            //NSLog(@" i --- %d", i);
            CGFloat rowCurrentHeight = 0;
            for (int k = i; k < i + self.numberOfItemsPerLine; k += 1) {
                if (rowCurrentHeight < [self cellSizeInIndexPath:[NSIndexPath indexPathForRow:k inSection:section]].height)
                    rowCurrentHeight = [self cellSizeInIndexPath:[NSIndexPath indexPathForRow:k inSection:section]].height;
            }
            
            totalHeightRow += rowCurrentHeight;
            j += 1;
        }
        // If last row is not full
        if (j < rowsInSection - 1) {
            CGFloat lastRowHeight = 0;
            for (int i = (int)(j*self.numberOfItemsPerLine); i < itemsInSection; i += 1) {
                if (lastRowHeight < [self cellSizeInIndexPath:[NSIndexPath indexPathForRow:i inSection:section]].height)
                    lastRowHeight = [self cellSizeInIndexPath:[NSIndexPath indexPathForRow:i inSection:section]].height;
            }
            totalHeightRow += lastRowHeight;
        }
    } else {
        for (int i = 0; i < itemsInSection; i += 1) {
            if (totalHeightRow < [self cellSizeInIndexPath:[NSIndexPath indexPathForRow:i inSection:section]].height)
                totalHeightRow = [self cellSizeInIndexPath:[NSIndexPath indexPathForRow:i inSection:section]].height;
        }
    }
    contentLength += totalHeightRow;
    
    contentLength += [self headerLengthForSection:section];
    contentLength += [self footerLengthForSection:section];
    
    return contentLength;
}

- (NSInteger)rowsInSection:(NSInteger)section
{
    NSInteger itemsInSection = [self.collectionView numberOfItemsInSection:section];
    NSInteger numberOfItemsPerLine = [self numberOfItemsPerLineForSection:section];
    
    if (numberOfItemsPerLine <= 0) return 1;
    
    NSInteger rowsInSection = itemsInSection / numberOfItemsPerLine + (itemsInSection % numberOfItemsPerLine > 0 ? 1 : 0);
    return rowsInSection;
}

- (CGFloat)lengthwiseInsetLengthInSection:(NSInteger)section
{
    UIEdgeInsets sectionInset = [self sectionInsetForSection:section];
    if (self.scrollDirection == UICollectionViewScrollDirectionVertical) {
        return sectionInset.top + sectionInset.bottom;
    } else {
        return sectionInset.left + sectionInset.right;
    }
}

- (void)calculateLayoutAttributes
{
    self.cellAttributesBySection = [NSMutableArray array];
    
    self.supplementaryAttributes = [NSMutableDictionary dictionary];
    self.supplementaryAttributes[UICollectionElementKindSectionHeader] = [NSMutableDictionary dictionary];
    self.supplementaryAttributes[UICollectionElementKindSectionFooter] = [NSMutableDictionary dictionary];
    
    for (NSInteger section = 0; section < [self.collectionView numberOfSections]; section++) {
        NSIndexPath *headerFooterPath = [NSIndexPath indexPathForItem:0 inSection:section];
        UICollectionViewLayoutAttributes *headerAttributes = [self headerAttributesForIndexPath:headerFooterPath];
        if (headerAttributes) {
            self.supplementaryAttributes[UICollectionElementKindSectionHeader][headerFooterPath] = headerAttributes;
        }
        
        [self.cellAttributesBySection addObject:[self layoutAttributesForItemsInSection:section]];
        
        UICollectionViewLayoutAttributes *footerAttributes = [self footerAttributesForIndexPath:headerFooterPath];
        if (footerAttributes) {
            self.supplementaryAttributes[UICollectionElementKindSectionFooter][headerFooterPath] = footerAttributes;
        }
    }
}

- (UICollectionViewLayoutAttributes *)headerAttributesForIndexPath:(NSIndexPath *)path
{
    CGFloat headerReferenceLength = [self headerLengthForSection:path.section];
    if (headerReferenceLength == 0) {
        return nil;
    }
    
    UICollectionViewLayoutAttributes *attributes = [UICollectionViewLayoutAttributes layoutAttributesForSupplementaryViewOfKind:UICollectionElementKindSectionHeader withIndexPath:path];
    
    CGRect frame = CGRectZero;
    
    if (self.scrollDirection == UICollectionViewScrollDirectionVertical) {
        frame.size.width = self.collectionViewContentSize.width;
        frame.size.height = headerReferenceLength;
        frame.origin.x = 0;
        frame.origin.y = [self startOfSection:path.section];
    } else {
        frame.size.width = headerReferenceLength;
        frame.size.height = self.collectionViewContentSize.height;
        frame.origin.x = [self startOfSection:path.section];
        frame.origin.y = 0;
    }
    
    attributes.frame = frame;
    
    return attributes;
}

- (UICollectionViewLayoutAttributes *)footerAttributesForIndexPath:(NSIndexPath *)path
{
    CGFloat footerReferenceLength = [self footerLengthForSection:path.section];
    if (footerReferenceLength == 0) {
        return nil;
    }
    
    UICollectionViewLayoutAttributes *attributes = [UICollectionViewLayoutAttributes layoutAttributesForSupplementaryViewOfKind:UICollectionElementKindSectionFooter withIndexPath:path];
    
    CGRect frame = CGRectZero;
    
    CGFloat sectionStart = [self startOfSection:path.section];
    CGFloat sectionLength = [self contentLengthForSection:path.section];
    
    CGFloat footerStart = sectionStart + sectionLength;
    if (footerReferenceLength > 0) {
        footerStart = footerStart - footerReferenceLength;
    }
    
    
    if (self.scrollDirection == UICollectionViewScrollDirectionVertical) {
        frame.size.width = self.collectionViewContentSize.width;
        frame.size.height = footerReferenceLength;
        frame.origin.x = 0;
        frame.origin.y = footerStart;
    } else {
        frame.size.width = footerReferenceLength;
        frame.size.height = self.collectionViewContentSize.height;
        frame.origin.x = footerStart;
        frame.origin.y = 0;
    }
    
    attributes.frame = frame;
    
    return attributes;
}

- (NSArray *)layoutAttributesForItemsInSection:(NSInteger)section
{
    NSMutableArray *attributes = [NSMutableArray array];
    for (NSInteger item = 0; item < [self.collectionView numberOfItemsInSection:section]; item++) {
        [attributes addObject:[self layoutAttributesForCellAtIndexPath:[NSIndexPath indexPathForItem:item inSection:section]]];
    }
    return attributes;
}

- (UIEdgeInsets)sectionInsetForSection:(NSInteger)section
{
    if ([self.collectionView.delegate respondsToSelector:@selector(collectionView:layout:insetForSectionAtIndex:)]) {
        return [(id)self.collectionView.delegate collectionView:self.collectionView layout:self insetForSectionAtIndex:section];
    } else {
        return self.sectionInset;
    }
}

- (CGFloat)lineSpacingForSection:(NSInteger)section
{
    if ([self.collectionView.delegate respondsToSelector:@selector(collectionView:layout:lineSpacingForSectionAtIndex:)]) {
        return [(id)self.collectionView.delegate collectionView:self.collectionView layout:self lineSpacingForSectionAtIndex:section];
    } else {
        return self.lineSpacing;
    }
}

- (CGFloat)interitemSpacingForSection:(NSInteger)section
{
    if ([self.collectionView.delegate respondsToSelector:@selector(collectionView:layout:interitemSpacingForSectionAtIndex:)]) {
        return [(id)self.collectionView.delegate collectionView:self.collectionView layout:self interitemSpacingForSectionAtIndex:section];
    } else {
        return self.interitemSpacing;
    }
}

- (CGFloat)headerLengthForSection:(NSInteger)section
{
    if ([self.collectionView.delegate respondsToSelector:@selector(collectionView:layout:referenceLengthForHeaderInSection:)]) {
        return [(id)self.collectionView.delegate collectionView:self.collectionView layout:self referenceLengthForHeaderInSection:section];
    } else {
        return self.headerReferenceLength;
    }
}

- (CGFloat)footerLengthForSection:(NSInteger)section
{
    if ([self.collectionView.delegate respondsToSelector:@selector(collectionView:layout:referenceLengthForFooterInSection:)]) {
        return [(id)self.collectionView.delegate collectionView:self.collectionView layout:self referenceLengthForFooterInSection:section];
    } else {
        return self.footerReferenceLength;
    }
}

- (NSInteger)numberOfItemsPerLineForSection:(NSInteger)section
{
    if ([self.collectionView.delegate respondsToSelector:@selector(collectionView:layout:numberItemsPerLineForSectionAtIndex:)]) {
        return [(id)self.collectionView.delegate collectionView:self.collectionView layout:self numberItemsPerLineForSectionAtIndex:section];
    } else {
        return self.numberOfItemsPerLine;
    }
}

//- (CGFloat)aspectRatioForSection:(NSInteger)section
//{
//    if ([self.collectionView.delegate respondsToSelector:@selector(collectionView:layout:aspectRatioForItemsInSectionAtIndex:)]) {
//        return [(id)self.collectionView.delegate collectionView:self.collectionView layout:self aspectRatioForItemsInSectionAtIndex:section];
//    } else {
//        return self.aspectRatio;
//    }
//}


- (CGFloat)aspectRatioForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.collectionView.delegate respondsToSelector:@selector(collectionView:layout:aspectRatioForItemsInSectionAtIndex:)]) {
        return [(id)self.collectionView.delegate collectionView:self.collectionView layout:self aspectRatioForItemsInSectionAtIndex:indexPath.section];
    } else {
        if (self.arrRatio != nil) {
            if (indexPath.row >= self.arrRatio.count) {
                return self.aspectRatio;
            }
            return [self.arrRatio[indexPath.row] floatValue];
        }
        return self.aspectRatio;
    }
}

- (UICollectionViewLayoutAttributes *)layoutAttributesForCellAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewLayoutAttributes *attributes = [UICollectionViewLayoutAttributes layoutAttributesForCellWithIndexPath:indexPath];
    
    attributes.frame = [self frameForItemAtIndexPath:indexPath];
    
    return attributes;
}

- (CGRect)frameForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize cellSize = [self cellSizeInIndexPath:indexPath];
    NSInteger numberOfItemsPerLine = [self numberOfItemsPerLineForSection:indexPath.section];
    NSInteger rowOfItem = indexPath.item / numberOfItemsPerLine;
    NSInteger locationInRowOfItem = indexPath.item % numberOfItemsPerLine;
    
    CGRect frame = CGRectZero;
    
    CGFloat sectionStart = [self startOfSection:indexPath.section];
    CGFloat headerReferenceLength = [self headerLengthForSection:indexPath.section];
    if (headerReferenceLength > 0) {
        sectionStart += headerReferenceLength;
    }
    
    UIEdgeInsets sectionInset = [self sectionInsetForSection:indexPath.section];
    CGFloat lineSpacing = [self lineSpacingForSection:indexPath.section];
    CGFloat interitemSpacing = [self interitemSpacingForSection:indexPath.section];
    
    CGFloat totalHeightRow = 0;
    
    if (indexPath.row >= self.numberOfItemsPerLine) {
        int j = 0;
        for (int i = (int)self.numberOfItemsPerLine; i <= indexPath.row; i += self.numberOfItemsPerLine) {
            //max height in Row
            CGFloat maxInRow = [self maxHeightInRow:j inSection:(int)indexPath.section];
            totalHeightRow += maxInRow;
            j += 1;
        }
    }
    
    
    if (self.scrollDirection == UICollectionViewScrollDirectionVertical) {
        frame.origin.x = sectionInset.left + (locationInRowOfItem * cellSize.width) + (interitemSpacing * locationInRowOfItem);
        frame.origin.y = sectionStart + sectionInset.top + totalHeightRow + (lineSpacing * rowOfItem);
    } else {
        frame.origin.x = sectionStart + sectionInset.left + (rowOfItem * cellSize.width) + (lineSpacing * rowOfItem);
        frame.origin.y = sectionInset.top + (locationInRowOfItem * cellSize.height) + (interitemSpacing * locationInRowOfItem);
    }
    frame.size = cellSize;
    
    return frame;
}

- (CGFloat)maxHeightInRow:(int)row inSection:(int)section
{
    CGFloat maxInRow = 0;
    for (int i = (int)(self.numberOfItemsPerLine*row); i < (int)(self.numberOfItemsPerLine*row) + self.numberOfItemsPerLine; i++) {
        if (maxInRow < [self cellSizeInIndexPath:[NSIndexPath indexPathForRow:i inSection:section]].height) {
            maxInRow = [self cellSizeInIndexPath:[NSIndexPath indexPathForRow:i inSection:section]].height;
        }
    }
    return maxInRow;
}

- (CGFloat)startOfSection:(NSInteger)section
{
    CGFloat startOfSection = 0;
    for (NSInteger currentSection = 0; currentSection < section; currentSection++) {
        startOfSection += [self contentLengthForSection:currentSection];
    }
    return startOfSection;
}

//- (CGSize)cellSizeInSection:(NSInteger)section
//{
//    CGFloat usableSpace = [self usableSpaceInSection:section];
//    CGFloat cellLength = usableSpace / [self numberOfItemsPerLineForSection:section];
//    CGFloat aspectRatio = [self aspectRatioForSection:section];
//
//    if (self.scrollDirection == UICollectionViewScrollDirectionVertical) {
//        return CGSizeMake(cellLength,
//                          cellLength * (1.0 / aspectRatio));
//    } else {
//        return CGSizeMake(cellLength * aspectRatio,
//                          cellLength);
//    }
//}

- (CGSize)cellSizeInIndexPath:(NSIndexPath *)indexPath
{
    CGFloat usableSpace = [self usableSpaceInSection:indexPath.section];
    CGFloat cellLength = usableSpace / [self numberOfItemsPerLineForSection:indexPath.section];
    CGFloat aspectRatio = [self aspectRatioForItemAtIndexPath:indexPath];
    
    if (self.scrollDirection == UICollectionViewScrollDirectionVertical) {
        return CGSizeMake(cellLength,
                          cellLength * (1.0 / aspectRatio));
    } else {
        return CGSizeMake(cellLength * aspectRatio,
                          cellLength);
    }
}

- (CGFloat)usableSpaceInSection:(NSInteger)section
{
    UIEdgeInsets sectionInset = [self sectionInsetForSection:section];
    CGFloat interitemSpacing = [self interitemSpacingForSection:section];
    NSInteger numberOfItemsPerLine = [self numberOfItemsPerLineForSection:section];
    
    if (self.scrollDirection == UICollectionViewScrollDirectionVertical) {
        return (self.collectionViewContentSize.width
                - sectionInset.left
                - sectionInset.right
                - ((numberOfItemsPerLine - 1) * interitemSpacing));
    } else {
        return (self.collectionViewContentSize.height
                - sectionInset.top
                - sectionInset.bottom
                - ((numberOfItemsPerLine - 1) * interitemSpacing));
    }
}

- (void)setNumberOfItemsPerLine:(NSInteger)numberOfItemsPerLine
{
    if (_numberOfItemsPerLine != numberOfItemsPerLine) {
        _numberOfItemsPerLine = numberOfItemsPerLine;
        
        [self invalidateLayout];
    }
}

- (void)setInteritemSpacing:(CGFloat)interitemSpacing {
    if (_interitemSpacing != interitemSpacing) {
        _interitemSpacing = interitemSpacing;
        
        [self invalidateLayout];
    }
}

- (void)setLineSpacing:(CGFloat)lineSpacing
{
    if (_lineSpacing != lineSpacing) {
        _lineSpacing = lineSpacing;
        
        [self invalidateLayout];
    }
}

- (void)setSectionInset:(UIEdgeInsets)sectionInset
{
    if (!UIEdgeInsetsEqualToEdgeInsets(_sectionInset, sectionInset)) {
        _sectionInset = sectionInset;
        
        [self invalidateLayout];
    }
}

- (void)setArrRatio:(NSArray *)arrRatio
{
    if (_arrRatio != arrRatio) {
        _arrRatio = arrRatio;
        
        [self invalidateLayout];
    }
}

- (void)setScrollDirection:(UICollectionViewScrollDirection)scrollDirection
{
    if (_scrollDirection != scrollDirection) {
        _scrollDirection = scrollDirection;
        
        [self invalidateLayout];
    }
}

#pragma mark - Target/Action methods

- (void)handleScroll:(CADisplayLink *)displayLink
{
    TTScrollingDirection direction = (TTScrollingDirection)[displayLink.TT_userInfo[kTTScrollingDirectionKey] integerValue];
    if (direction == TTScrollingDirectionUnknown) {
        return;
    }
    
    CGSize frameSize = self.collectionView.bounds.size;
    CGSize contentSize = self.collectionView.contentSize;
    CGPoint contentOffset = self.collectionView.contentOffset;
    UIEdgeInsets contentInset = self.collectionView.contentInset;
    
    CGFloat distance = rint(self.scrollingSpeed * displayLink.duration);
    CGPoint translation = CGPointZero;
    
    switch(direction)
    {
        case TTScrollingDirectionUp:
        {
            distance = -distance;
            CGFloat minY = 0.0f - contentInset.top;
            
            if ((contentOffset.y + distance) <= minY) {
                distance = -contentOffset.y - contentInset.top;
            }
            
            translation = CGPointMake(0.0f, distance);
            
            break;
        }
            
        case TTScrollingDirectionDown:
        {
            CGFloat maxY = MAX(contentSize.height, frameSize.height) - frameSize.height + contentInset.bottom;
            
            if ((contentOffset.y + distance) >= maxY) {
                distance = maxY - contentOffset.y;
            }
            
            translation = CGPointMake(0.0f, distance);
            
            break;
        }
            
        case TTScrollingDirectionLeft:
        {
            distance = -distance;
            CGFloat minX = 0.0f - contentInset.left;
            
            if ((contentOffset.x + distance) <= minX) {
                distance = -contentOffset.x - contentInset.left;
            }
            
            translation = CGPointMake(distance, 0.0f);
            
            break;
        }
            
        case TTScrollingDirectionRight:
        {
            CGFloat maxX = MAX(contentSize.width, frameSize.width) - frameSize.width + contentInset.right;
            
            if ((contentOffset.x + distance) >= maxX) {
                distance = maxX - contentOffset.x;
            }
            
            translation = CGPointMake(distance, 0.0f);
            
            break;
        }
            
        default:
            break;
    }
    
    self.currentViewCenter = TTS_CGPointAdd(self.currentViewCenter, translation);
    self.currentView.center = TTS_CGPointAdd(self.currentViewCenter, self.panTranslationInCollectionView);
    self.collectionView.contentOffset = TTS_CGPointAdd(contentOffset, translation);
}


- (void)handleLongPressGesture:(UILongPressGestureRecognizer *)gestureRecognizer
{
    switch(gestureRecognizer.state)
    {
        case UIGestureRecognizerStateBegan:
        {
            NSIndexPath *currentIndexPath = [self.collectionView indexPathForItemAtPoint:[gestureRecognizer locationInView:self.collectionView]];
            
            if ([self.dataSource respondsToSelector:@selector(collectionView:canMoveItemAtIndexPath:)]) {
                if (![self.dataSource collectionView:self.collectionView canMoveItemAtIndexPath:currentIndexPath]) {
                    return;
                }
            }
            else {
                return;
            }
            
            self.selectedItemIndexPath = currentIndexPath;
            
            if ([self.delegate respondsToSelector:@selector(collectionView:layout:willBeginDraggingItemAtIndexPath:)]) {
                [self.delegate collectionView:self.collectionView layout:self willBeginDraggingItemAtIndexPath:self.selectedItemIndexPath];
            }
            
            UICollectionViewCell *collectionViewCell = [self.collectionView cellForItemAtIndexPath:self.selectedItemIndexPath];
            
            self.currentView = [[UIView alloc] initWithFrame:collectionViewCell.frame];
            
            UIView *imageView = [collectionViewCell TT_snapshotView];
            imageView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
            
            [self.currentView addSubview:imageView];
            [self.collectionView addSubview:self.currentView];
            
            self.currentViewCenter = self.currentView.center;
            
            __weak typeof(self) weakSelf = self;
            [UIView animateWithDuration:0.25 delay:0.0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
                __strong typeof(self) strongSelf = weakSelf;
                if (strongSelf) {
                    strongSelf.currentView.transform = CGAffineTransformMakeScale(1.1f, 1.1f);
                }
            }
                             completion:^(BOOL finished) {
                                 __strong typeof(self) strongSelf = weakSelf;
                                 if (strongSelf) {
                                     if ([strongSelf.delegate respondsToSelector:@selector(collectionView:layout:didBeginDraggingItemAtIndexPath:)]) {
                                         [strongSelf.delegate collectionView:strongSelf.collectionView layout:strongSelf didBeginDraggingItemAtIndexPath:strongSelf.selectedItemIndexPath];
                                     }
                                 }
                             }];
            
            [self invalidateLayout];
            
            break;
        }
            
        case UIGestureRecognizerStateCancelled:
        case UIGestureRecognizerStateEnded:
        {
            NSIndexPath *currentIndexPath = self.selectedItemIndexPath;
            
            if (currentIndexPath) {
                if ([self.delegate respondsToSelector:@selector(collectionView:layout:willEndDraggingItemAtIndexPath:)]) {
                    [self.delegate collectionView:self.collectionView layout:self willEndDraggingItemAtIndexPath:currentIndexPath];
                }
                
                self.selectedItemIndexPath = nil;
                self.currentViewCenter = CGPointZero;
                
                self.longPressGestureRecognizer.enabled = NO;
                UICollectionViewLayoutAttributes *layoutAttributes = [self layoutAttributesForItemAtIndexPath:currentIndexPath];
                
                __weak typeof(self) weakSelf = self;
                [UIView animateWithDuration:0.25 delay:0.0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
                    __strong typeof(self) strongSelf = weakSelf;
                    if (strongSelf) {
                        strongSelf.currentView.transform = CGAffineTransformMakeScale(1.0f, 1.0f);
                        strongSelf.currentView.center = layoutAttributes.center;
                    }
                }
                                 completion:^(BOOL finished) {
                                     
                                     self.longPressGestureRecognizer.enabled = YES;
                                     
                                     __strong typeof(self) strongSelf = weakSelf;
                                     if (strongSelf) {
                                         [strongSelf.currentView removeFromSuperview];
                                         strongSelf.currentView = nil;
                                         [strongSelf invalidateLayout];
                                         
                                         if ([strongSelf.delegate respondsToSelector:@selector(collectionView:layout:didEndDraggingItemAtIndexPath:)]) {
                                             [strongSelf.delegate collectionView:strongSelf.collectionView layout:strongSelf didEndDraggingItemAtIndexPath:currentIndexPath];
                                         }
                                     }
                                 }];
            }
            
            break;
        }
            
        default:
            break;
    }
}

- (void)handlePanGesture:(UIPanGestureRecognizer *)gestureRecognizer
{
    switch (gestureRecognizer.state)
    {
        case UIGestureRecognizerStateBegan:
        case UIGestureRecognizerStateChanged:
        {
            self.panTranslationInCollectionView = [gestureRecognizer translationInView:self.collectionView];
            CGPoint viewCenter = self.currentView.center = TTS_CGPointAdd(self.currentViewCenter, self.panTranslationInCollectionView);
            
            [self invalidateLayoutIfNecessary];
            
            switch (self.scrollDirection)
            {
                case UICollectionViewScrollDirectionVertical:
                {
                    if (viewCenter.y < (CGRectGetMinY(self.collectionView.bounds) + self.scrollingTriggerEdgeInsets.top)) {
                        [self setupScrollTimerInDirection:TTScrollingDirectionUp];
                    }
                    else {
                        if (viewCenter.y > (CGRectGetMaxY(self.collectionView.bounds) - self.scrollingTriggerEdgeInsets.bottom)) {
                            [self setupScrollTimerInDirection:TTScrollingDirectionDown];
                        }
                        else {
                            [self invalidatesScrollTimer];
                        }
                    }
                    break;
                }
                    
                case UICollectionViewScrollDirectionHorizontal:
                {
                    if (viewCenter.x < (CGRectGetMinX(self.collectionView.bounds) + self.scrollingTriggerEdgeInsets.left)) {
                        [self setupScrollTimerInDirection:TTScrollingDirectionLeft];
                    }
                    else {
                        if (viewCenter.x > (CGRectGetMaxX(self.collectionView.bounds) - self.scrollingTriggerEdgeInsets.right)) {
                            [self setupScrollTimerInDirection:TTScrollingDirectionRight];
                        }
                        else {
                            [self invalidatesScrollTimer];
                        }
                    }
                    break;
                }
                    
                default:
                    break;
            }
            
            break;
        }
            
        case UIGestureRecognizerStateCancelled:
        case UIGestureRecognizerStateEnded:
        {
            [self invalidatesScrollTimer];
            break;
        }
            
        default:
            break;
    }
}

#pragma mark - UIGestureRecognizerDelegate methods

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    if ([self.panGestureRecognizer isEqual:gestureRecognizer]) {
        return (self.selectedItemIndexPath != nil);
    }
    return YES;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    
    if ([self.longPressGestureRecognizer isEqual:gestureRecognizer]) {
        return [self.panGestureRecognizer isEqual:otherGestureRecognizer];
    }
    
    if ([self.panGestureRecognizer isEqual:gestureRecognizer]) {
        return [self.longPressGestureRecognizer isEqual:otherGestureRecognizer];
    }
    
    return NO;
}

#pragma mark - Key-Value Observing methods

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([keyPath isEqualToString:kTTCollectionViewKeyPath]) {
        if (self.collectionView != nil) {
            [self setupCollectionView];
        }
        else {
            [self invalidatesScrollTimer];
            [self tearDownCollectionView];
        }
    }
}

#pragma mark - Notifications

- (void)handleApplicationWillResignActive:(NSNotification *)notification {
    self.panGestureRecognizer.enabled = YES;
}

@end


