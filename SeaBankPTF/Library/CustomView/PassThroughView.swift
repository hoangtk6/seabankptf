//
//  aaaaaaaa.swift
//  Comic
//
//  Created by Hoang Vu on 9/11/20.
//  Copyright © 2020 Techlab Corp. All rights reserved.
//

import Foundation
import UIKit

final class PassThroughView: UIView {
  override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
    for subview in subviews {
      if !subview.isHidden && subview.isUserInteractionEnabled && subview.point(inside: convert(point, to: subview), with: event) {
        return true
      }
    }
    return false
  }
}
