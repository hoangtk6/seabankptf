import UIKit

final class RoundedShadowView: UIView {
    
    // MARK: - Outlets
    
    @IBInspectable private var viewCornerRadius: CGFloat = 8
    @IBInspectable private var viewShadowRadius: CGFloat = 4
    @IBInspectable private var viewShadowColor: UIColor = UIColor.black.withAlphaComponent(0.1)
    @IBInspectable private var viewShadowOffset: CGSize = CGSize(width: 0, height: 0)
  
    // MARK: - Private Variables
    
    private var initialized: Bool = false
    private var originalBackgroundColor: UIColor!
    private lazy var shadowLayer: CAShapeLayer = { [unowned self] in
        let layer = CAShapeLayer()
        layer.fillColor = self.originalBackgroundColor.cgColor
        layer.shadowColor = self.viewShadowColor.cgColor
        layer.shadowOffset = self.viewShadowOffset
        layer.shadowOpacity = 1
        layer.shadowRadius = self.viewShadowRadius
        return layer
    }()
    
    // MARK: - Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        originalBackgroundColor = backgroundColor
        backgroundColor = UIColor.clear
        layer.insertSublayer(shadowLayer, at: 0)
        shadowLayer.fillColor = originalBackgroundColor.cgColor
        
        initialized = true
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let path = UIBezierPath(roundedRect: bounds, cornerRadius: viewCornerRadius)
        shadowLayer.path = path.cgPath
        shadowLayer.shadowPath = path.cgPath
        shadowLayer.fillColor = originalBackgroundColor.cgColor
    }

    override var backgroundColor: UIColor? {
        set {
            if newValue != nil && initialized {
                super.backgroundColor = UIColor.clear
                shadowLayer.fillColor = newValue!.cgColor
                originalBackgroundColor = newValue!
            } else {
                super.backgroundColor = newValue
            }
        }
        get {
            if let color = super.backgroundColor {
                return color
            }
            return UIColor.white
        }
    }
}
