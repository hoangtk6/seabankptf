//
//  SBSNumberView.swift
//  SBGold
//
//  Created by Anonymos on 07/06/2021.
//

import UIKit

final class SBSNumberView: ViewWithXib {
  
  @IBOutlet weak private var numberLb: UILabel!
  @IBOutlet weak private var increaseBtn: UIButton!
  @IBOutlet weak private var decreaseBtn: UIButton!
  @IBOutlet weak private var titleLb: UILabel!
  @IBOutlet weak private var containerView: UIView!
  
  @IBOutlet weak private var tangLb: UILabel!
  @IBOutlet weak private var giamLb: UILabel!
  @IBOutlet weak private var numberTf: SBSDebounceTF!
  
  var valueShouldChange: ((Int) -> ())?
  
  var unit: Int = 1
  
  var maxValue: Int = 1000000000 {
    didSet {
      numberTf.maxValue = maxValue
    }
  }
  var minValue: Int = 10
  
  private var workItem: DispatchWorkItem?
  
  override func setupView() {
    addObservable(buttons: [increaseBtn, decreaseBtn])
    Apply(containerView!) {
      $0.layer.cornerRadius = 5
      $0.clipsToBounds = true
      $0.layer.borderWidth = 1
      $0.layer.borderColor = #colorLiteral(red: 0.768627451, green: 0.768627451, blue: 0.768627451, alpha: 1)
    }
    numberLb.text = "0"
    
    numberTf.maxValue = maxValue
    numberTf.delegate = self
    numberTf.keyboardType = .numberPad
    numberTf.addDoneToolBar()
    numberTf.onChanged = { [weak self] value in
      self?.numberLb.text = value
    }
    numberTf.debounce(delay: 0.7) { [weak self] value in
      guard let `self` = self,
            let number = value?.sbsGold_intValue(), number >= self.minValue else { return }
      self.valueShouldChange?(number)
      self.numberTf.resignFirstResponder()
    }
  }
  
  override func didTapButton(_ btn: UIButton) {
    let value = Int(numberTf.text.stringValue()) ?? 0
    var valueChange: Int?
    switch btn {
    case increaseBtn:
      if value + unit <= maxValue {
        valueChange = value + unit
        numberTf.text = (value + unit).sbsGold_toString()
        numberLb.text = numberTf.text
      }
    case decreaseBtn:
      if value - unit > 0 {
        valueChange = value - unit
        numberTf.text = (value - unit).sbsGold_toString()
        numberLb.text = numberTf.text
      }
    default:
      break
    }
    self.workItem?.cancel()
    if let newValue = valueChange {
      let workItem = DispatchWorkItem(block: { [weak self] in
        self?.valueShouldChange?(newValue)
      })
      self.workItem = workItem
      DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: workItem)
    }
    updateUI()
  }
  
  func setValue(_ value: Int) {
    let newValue = max(unit, value)
    numberLb.text = newValue.sbsGold_toString()
    numberTf.text = newValue.sbsGold_toString()
    updateUI()
  }
  
  func setTitle(_ text: String) {
    titleLb.text = text
  }
  
  private func updateUI() {
    let value = Int(numberLb.text.stringValue()) ?? 0
    if value - unit <= 0 {
      giamLb.alpha = 0.1
    } else {
      giamLb.alpha = 1
    }
    if value + unit > maxValue {
      tangLb.alpha = 0.1
    } else {
      tangLb.alpha = 1
    }
  }
}

extension SBSNumberView: UITextFieldDelegate {
  func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
    let value = numberTf.text?.sbsGold_intValue() ?? 0
    if value < minValue {
      numberLb.text = minValue.sbsGold_toString()
      numberTf.text = minValue.sbsGold_toString()
      valueShouldChange?(minValue)
    }
    return true
  }
}

class SBSDebounceTF: UITextField {
  deinit {
    self.removeTarget(self, action: #selector(self.editingChanged(_:)), for: .editingChanged)
  }
  
  private var workItem: DispatchWorkItem?
  private var delay: Double = 0
  private var callback: ((String?) -> Void)? = nil
  
  var maxValue: Int = 200
  var onChanged: ((String) -> ())?
  
  func debounce(delay: Double, callback: @escaping ((String?) -> Void)) {
    self.delay = delay
    self.callback = callback
    DispatchQueue.main.async {
      self.callback?(self.text)
    }
    self.addTarget(self, action: #selector(self.editingChanged(_:)), for: .editingChanged)
  }
  
  @objc private func editingChanged(_ sender: UITextField) {
    let value = min(maxValue, sender.text?.sbsGold_intValue() ?? 0)
    sender.text = value.sbsGold_toString()
    onChanged?(sender.text.stringValue())
    self.workItem?.cancel()
    let workItem = DispatchWorkItem(block: { [weak self] in
      self?.callback?(sender.text)
    })
    self.workItem = workItem
    DispatchQueue.main.asyncAfter(deadline: .now() + self.delay, execute: workItem)
  }
}
