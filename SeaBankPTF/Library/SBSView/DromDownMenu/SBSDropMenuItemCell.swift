//
//  SBSDropMenuItemCell.swift
//  SBGold
//
//  Created by Anonymos on 08/08/2021.
//

import UIKit

final class SBSDropMenuItemCell: UICollectionViewCell {
  
  @IBOutlet weak var contentLb: UILabel!
  @IBOutlet weak var bgView: UIView!
  @IBOutlet weak var iconImv: UIImageView!
  
  var onSelected: (() -> ())?
  
  override func awakeFromNib() {
    super.awakeFromNib()
    let tapGes = UITapGestureRecognizer(target: self, action: #selector(didTap))
    contentView.isUserInteractionEnabled = true
    contentView.addGestureRecognizer(tapGes)
    
    bgView.clipsToBounds = true
    bgView.layer.cornerRadius = 30
  }
  
  @objc
  func didTap() {
    onSelected?()
  }

  func setValue(_ text: String, url: String, isSelected: Bool) {
    contentLb.text = text
    iconImv.sbsGold_setImageUrl(url, mode: .alwaysTemplate)
    
    let color = isSelected ? SBSColor._D72027 : SBSColor._929497
    contentLb.textColor = isSelected ? SBSColor._D72027 : SBSColor._292929
    iconImv.tintColor = color
  }
}
