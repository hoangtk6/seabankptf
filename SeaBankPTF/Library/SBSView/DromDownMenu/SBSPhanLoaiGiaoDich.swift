//
//  SBSPhanLoaiGiaoDich.swift
//  SeaBankPTF
//
//  Created by TrungNguyen on 9/17/21.
//

import Foundation

struct SBSPhanLoaiGiaoDich: Codable {
  var id: String
  var code: String
  var image: String
  var loaiGiaoDich: String? {
    get {
      return SBSGoldConfig.language == "vi" ? title : titleEn
    }
  }
  var title: String
  var titleEn: String
  var level: String
  var order: String?
  var parentId: String?
}
