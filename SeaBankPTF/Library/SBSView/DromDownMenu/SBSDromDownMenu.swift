//
//  SBSDromDownMenu.swift
//  SBGold
//
//  Created by Anonymos on 08/08/2021.
//

import UIKit

struct SBSGiaoDichItem {
  var parent: SBSPhanLoaiGiaoDich
  var childs: [SBSGiaoDichItemChild]
}

struct SBSGiaoDichItemChild {
  var parent: SBSPhanLoaiGiaoDich
  var childs: [SBSPhanLoaiGiaoDich]
}

final class SBSDromDownMenu: ViewWithXib {
  @IBOutlet weak var collectionView: UICollectionView!
  @IBOutlet weak var tbView: UITableView!
  @IBOutlet weak var blurView: UIView!
  @IBOutlet weak var searchTf: UITextField!
  @IBOutlet weak var bottomSpace: NSLayoutConstraint!
  
  var onSelected: ((SBSPhanLoaiGiaoDich) -> ())?
  
  var giaoDichs: [SBSGiaoDichItem] = [] {
    didSet {
      headerItems = giaoDichs.map({ $0.parent })
      menuItems = giaoDichs.first?.childs ?? []
    }
  }
  
  var headerItems: [SBSPhanLoaiGiaoDich] = [] {
    didSet {
      collectionView.reloadData()
    }
  }
  
  var menuItems: [SBSGiaoDichItemChild] = [] {
    didSet {
      tbView.reloadData()
    }
  }
  
  var tabIndex: Int = 0
  
  override func setupView() {
    super.awakeFromNib()
    collectionView.delegate = self
    collectionView.dataSource = self
    collectionView.register(SBSDropMenuItemCell.self)
    
    tbView.delegate = self
    tbView.dataSource = self
    tbView.register(SBSDropMenuLineCell.self)
    tbView.rowHeight = 60
    
    let tapGes = UITapGestureRecognizer(target: self, action: #selector(hideMenu))
    blurView.isUserInteractionEnabled = true
    blurView.addGestureRecognizer(tapGes)
    
    searchTf.delegate = self
    searchTf.addTarget(self, action: #selector(textDidChanged), for: .editingChanged)
  }
  
  @objc
  private func textDidChanged() {
    getSearchResult()
  }
  
  private func getSearchResult() {
    let items = giaoDichs[tabIndex].childs
    guard let text = searchTf.text?.sbsGold_unaccent(), !text.isEmpty else {
      return menuItems = items
    }
    
    var results: [SBSGiaoDichItemChild] = []
    for item in items {
      let parent = item.parent
      if item.childs.isEmpty {
        let isContain = item.parent.loaiGiaoDich?.sbsGold_unaccent().contains(text) ?? false
        if isContain {
          results.append(SBSGiaoDichItemChild(parent: parent, childs: []))
        }
      } else {
        let childs = item.childs.filter({ $0.loaiGiaoDich?.sbsGold_unaccent().contains(text) ?? false })
        if !childs.isEmpty {
          results.append(SBSGiaoDichItemChild(parent: parent, childs: childs))
        }
      }
    }
    menuItems = results
  }
  
  @objc
  private func hideMenu() {
    removeFromSuperview()
  }
}

extension SBSDromDownMenu: UITextFieldDelegate {
  func textFieldDidBeginEditing(_ textField: UITextField) {
    UIView.animate(withDuration: 0.2) {
      self.bottomSpace.constant = 100
    }
    self.layoutIfNeeded()
    self.tbView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 250, right: 0)
  }
  
  func textFieldDidEndEditing(_ textField: UITextField) {
    UIView.animate(withDuration: 0.2) {
      self.bottomSpace.constant = -20
    }
    self.layoutIfNeeded()
    self.tbView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
  }
}

extension SBSDromDownMenu: UICollectionViewDelegate,
                           UICollectionViewDataSource,
                           UICollectionViewDelegateFlowLayout {
  func collectionView(_ collectionView: UICollectionView,
                      numberOfItemsInSection section: Int) -> Int {
    return headerItems.count
  }
  func collectionView(_ collectionView: UICollectionView,
                      cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let item = headerItems[indexPath.row]
    let cell = collectionView.dequeue(SBSDropMenuItemCell.self, indexPath: indexPath)
    cell.setValue(item.loaiGiaoDich.stringValue(), url: item.image, isSelected: tabIndex == indexPath.row)
    cell.onSelected  = { [weak self] in
      guard let `self` = self else { return }
      self.tabIndex = indexPath.row
      self.getSearchResult()
      self.collectionView.reloadData()
    }
    return cell
  }
  
  func collectionView(_ collectionView: UICollectionView,
                      layout collectionViewLayout: UICollectionViewLayout,
                      sizeForItemAt indexPath: IndexPath) -> CGSize {
    return CGSize(width: 70, height: 100)
  }
  
  func collectionView(_ collectionView: UICollectionView,
                      layout collectionViewLayout: UICollectionViewLayout,
                      insetForSectionAt section: Int) -> UIEdgeInsets {
    return UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
  }
}

extension SBSDromDownMenu: UITableViewDelegate, UITableViewDataSource {
  func numberOfSections(in tableView: UITableView) -> Int {
    return menuItems.count
  }
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    let childs = menuItems[section].childs
    return childs.isEmpty ? 1 : childs.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let item = menuItems[indexPath.section]
    let child = item.childs.isEmpty ? item.parent : item.childs[indexPath.row]
    let cell = tbView.dequeue(SBSDropMenuLineCell.self)
    cell.bindData(child)
    cell.onSelected = { [weak self] in
      guard let `self` = self else { return }
      self.onSelected?(child)
      self.removeFromSuperview()
    }
    return cell
  }
  
  func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    let item = menuItems[section]
    if item.childs.isEmpty {
      return 0
    }
    return 40
  }
  
  func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    let item = menuItems[section]
    if item.childs.isEmpty {
      return nil
    }
    let header = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 40))
    header.backgroundColor = .white
    
    let titleLb = UILabel(frame: CGRect(x: 10, y: 10, width: 300, height: 20))
    titleLb.text = item.parent.loaiGiaoDich
    titleLb.font = UIFont.boldSystemFont(ofSize: 13)
    
    let lineView = UIView(frame: CGRect(x: 10, y: 39, width: UIScreen.main.bounds.width - 20, height: 1))
    lineView.backgroundColor = UIColor.darkGray.withAlphaComponent(0.3)
    header.addSubview(titleLb)
    header.addSubview(lineView)
    return header
  }
}
