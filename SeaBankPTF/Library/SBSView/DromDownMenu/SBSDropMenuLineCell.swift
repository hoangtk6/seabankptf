//
//  SBSDropMenuLineCell.swift
//  SBGold
//
//  Created by Anonymos on 08/08/2021.
//

import UIKit

final class SBSDropMenuLineCell: UITableViewCell {
  
  @IBOutlet private weak var iconImv: UIImageView!
  @IBOutlet private weak var titleLb: UILabel!
  
  var onSelected: (() -> ())?
  
  override func awakeFromNib() {
    super.awakeFromNib()
    let tapGes = UITapGestureRecognizer(target: self, action: #selector(didTap))
    contentView.isUserInteractionEnabled = true
    contentView.addGestureRecognizer(tapGes)
  }
  
  @objc
  func didTap() {
    onSelected?()
  }
  
  func bindData(_ gd: SBSPhanLoaiGiaoDich) {
    iconImv.sbsGold_setImageUrl(gd.image)
    titleLb.text = gd.loaiGiaoDich
  }
}
