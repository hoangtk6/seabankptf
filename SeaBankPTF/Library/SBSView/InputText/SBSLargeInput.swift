//
//  SBSLargeInput.swift
//  SeaBankPTF
//
//  Created by TrungNguyen on 9/19/21.
//

import UIKit

final class SBSLargeInput: ViewWithXib {
  
  // MARK: - IBOutlet
  @IBOutlet weak private var titleLb: UILabel!
  @IBOutlet weak private var starRequireLabel: UILabel!
  @IBOutlet weak private var bottomTitleLb: UILabel!
  @IBOutlet weak private(set) var inputTf: KMPlaceholderTextView! // Để private set còn có lúc sửa được thuộc tính textField cho dễ
  @IBOutlet weak private var containerView: UIView!
  
  private var onTapGes: (() -> ())?
  
  // MARK: - Custom proprety
  var keyboardType: UIKeyboardType = .default {
    didSet {
      inputTf.keyboardType = keyboardType
    }
  }
  
  var autocapitalizationType: UITextAutocapitalizationType = .words {
    didSet {
      inputTf.autocapitalizationType = autocapitalizationType
    }
  }
  
  var editEnable: Bool = true {
    didSet {
      inputTf.isUserInteractionEnabled = editEnable
    }
  }
  
  var isRequire: Bool = false {
    didSet {
      starRequireLabel.isHidden = !isRequire
    }
  }
  
  var title: String = "" {
    didSet {
      titleLb.text = title
    }
  }
  
  var titleAttr: NSAttributedString? {
    didSet {
      titleLb.attributedText = titleAttr
    }
  }
  
  var bottomTitle: String = "" {
    didSet {
      bottomTitleLb.text = bottomTitle
    }
  }
  
  var placeHolder: String = ""{
    didSet {
      inputTf.placeholder = placeHolder
    }
  }
  
  var content: String? {
    set {
      inputTf.text = newValue
    }
    
    get {
      return inputTf.text ?? ""
    }
  }
  
  var titleColor: UIColor = .darkText {
    didSet {
      titleLb.textColor = titleColor
    }
  }
  
  var contentColor: UIColor = .darkText {
    didSet {
      inputTf.textColor = contentColor
    }
  }
  
  var bottomTitleColor: UIColor = .darkText {
    didSet {
      bottomTitleLb.textColor = bottomTitleColor
    }
  }
  
  func firstResponder(_ enable: Bool) {
    if enable {
      inputTf.becomeFirstResponder()
    } else {
      inputTf.resignFirstResponder()
    }
  }

  func addTapGes(completionHandler: @escaping() -> ()) {
    onTapGes = completionHandler
    let tapGes = UITapGestureRecognizer(target: self, action: #selector(didTapAction))
    editEnable = false
    isUserInteractionEnabled = true
    addGestureRecognizer(tapGes)
  }
  
  @objc
  private func didTapAction() {
    onTapGes?()
  }
  
  override func setupView() {
    inputTf.textColor = SBSColor._8F8F8F
    titleLb.text = ""
    bottomTitleLb.text = ""
    Apply(containerView!) {
      $0.layer.borderWidth = 1
      $0.layer.borderColor = #colorLiteral(red: 0.8117647059, green: 0.8117647059, blue: 0.8117647059, alpha: 1)
      $0.clipsToBounds = true
      $0.layer.cornerRadius = 8
    }
    bottomTitleLb.textColor = mainColor
  }
}
