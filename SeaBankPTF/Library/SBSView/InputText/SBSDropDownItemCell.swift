import UIKit

class SBSDropDownItemCell: UITableViewCell {
  
  @IBOutlet weak private var contentLb: UILabel!
  
  var onSelected: (() -> ())?
  
  override func awakeFromNib() {
    let tapGes = UITapGestureRecognizer(target: self, action: #selector(didTap))
    contentView.isUserInteractionEnabled = true
    contentView.addGestureRecognizer(tapGes)
  }
  
  @objc
  private func didTap() {
    onSelected?()
  }
  
  var content: String = "" {
    didSet {
      contentLb.text = content
    }
  }
}
