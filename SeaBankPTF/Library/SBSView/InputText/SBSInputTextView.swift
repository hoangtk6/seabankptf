import UIKit

final class SBSInputTextView: ViewWithXib {
  
  // MARK: - IBOutlet
  @IBOutlet weak private var titleLb: UILabel!
  @IBOutlet weak private var starRequireLabel: UILabel!
  @IBOutlet weak private var bottomTitleLb: UILabel!
  @IBOutlet weak private(set) var inputTf: UITextField! // Để private set còn có lúc sửa được thuộc tính textField cho dễ
  @IBOutlet weak private var leadingSpace: NSLayoutConstraint!
  @IBOutlet weak private var containerView: UIView!
  @IBOutlet weak private var arrowImv: UIImageView!

  private lazy var tbView: UITableView = {
    let tbView = Init(UITableView()) {
      $0.register(SBSDropDownItemCell.self)
      $0.rowHeight = 40
      $0.tableFooterView = UIView()
      $0.clipsToBounds = true
      $0.cornerRadius = 5
      $0.separatorStyle = .none
      if #available(iOS 13.0, *) {
        $0.overrideUserInterfaceStyle = .light
      }
    }
    return tbView
  }()
  
  private lazy var dropDownView: SBSShadowView = {
    let view = Init(SBSShadowView()) {
      $0.layer.backgroundColor = UIColor.clear.cgColor
      $0.layer.shadowColor = UIColor.black.cgColor
      $0.layer.shadowOffset = CGSize(width: 0, height: 2.0)
      $0.layer.shadowOpacity = 0.2
      $0.layer.shadowRadius = 4.0
      $0.layer.borderWidth = 0.5
      $0.layer.borderColor = SBSColor._D72027.cgColor
      $0.isUserInteractionEnabled = true
    }
    return view
  }()
  
  private lazy var menuView = SBSDromDownMenu()
  private var onSelected: ((Int) -> ())?
  private var onSelecteGiaoDich: ((SBSPhanLoaiGiaoDich) -> ())?
  
  private var dropDownItems: [String] = [] {
    didSet {
      tbView.reloadData()
    }
  }
  
  private var onTapGes: (() -> ())?
  
  // MARK: - Custom proprety
  var keyboardType: UIKeyboardType = .default {
    didSet {
      inputTf.keyboardType = keyboardType
    }
  }
  
  var autocapitalizationType: UITextAutocapitalizationType = .words {
    didSet {
      inputTf.autocapitalizationType = autocapitalizationType
    }
  }
  
  var editEnable: Bool = true {
    didSet {
      inputTf.isUserInteractionEnabled = editEnable
    }
  }
  
  var isRequire: Bool = false {
    didSet {
      starRequireLabel.isHidden = !isRequire
    }
  }
  
  var isHiddenArrowDown: Bool = true {
    didSet {
      arrowImv.isHidden = isHiddenArrowDown
    }
  }
  
  var title: String = "" {
    didSet {
      titleLb.text = title
    }
  }
  
  var titleAttr: NSAttributedString? {
    didSet {
      titleLb.attributedText = titleAttr
    }
  }
  
  var bottomTitle: String = "" {
    didSet {
      bottomTitleLb.text = bottomTitle
    }
  }
  
  var placeHolder: String = ""{
    didSet {
      inputTf.placeholder = placeHolder
    }
  }
  
  var content: String? {
    set {
      inputTf.text = newValue
      inputTf.sendActions(for: .valueChanged)
    }
    
    get {
      return inputTf.text ?? ""
    }
  }
  
  var contentMaxLength: Int = 2000 {
    didSet {
      inputTf.maxLength = contentMaxLength
    }
  }
  
  var titleColor: UIColor = .darkText {
    didSet {
      titleLb.textColor = titleColor
    }
  }
  
  var contentColor: UIColor = .darkText {
    didSet {
      inputTf.textColor = contentColor
    }
  }
  
  var bottomTitleColor: UIColor = .darkText {
    didSet {
      bottomTitleLb.textColor = bottomTitleColor
    }
  }
  
  var isSecureTextEntry: Bool = false {
    didSet {
      inputTf.isSecureTextEntry = isSecureTextEntry
    }
  }
  
  var leading: CGFloat = 0 {
    didSet {
      leadingSpace.constant = leading
    }
  }
  
  var selectedIndex: Int = -1 {
    didSet {
      if selectedIndex >= 0 {
        tbView.scrollToRow(at: IndexPath(row: selectedIndex, section: 0),
                           at: .top, animated: false)
      }
      tbView.reloadData()
    }
  }
  
  var maximum: Int = 1000
  
  var onTextChange: ((String) -> Void)?
  var onFinish: ((String) -> Void)?
  
  func firstResponder(_ enable: Bool) {
    if enable {
      inputTf.becomeFirstResponder()
    } else {
      inputTf.resignFirstResponder()
    }
  }
  
  func showDropDown(_ items: [String], onSelected: @escaping (Int) -> ()) {
    guard let parent = self.superview, parent.subviews.filter({ $0 == dropDownView }).isEmpty else { return }
    
    let frame = self.convert(containerView.frame, to: parent)
    self.onSelected = onSelected
    let row = min(3, items.count)
    dropDownView.frame = CGRect(x: frame.origin.x,
                                y: frame.origin.y + frame.height + 2,
                                width: frame.width,
                                height: 40 * CGFloat(row))
    parent.addSubview(dropDownView)
    dropDownView.sbsGold_embedded(view: tbView)
    tbView.delegate = self
    tbView.dataSource = self
    
    dropDownItems = items
    addNotificationObservable()
  
    if let index = items.firstIndex(where: { $0 == content }) {
      selectedIndex = index
    } else {
      selectedIndex = -1
    }
  }
  
  func showDropDown(_ items: [String], parentView: UIView, onSelected: @escaping (Int) -> ()) {
    let frame = self.convert(containerView.frame, to: parentView)
    self.onSelected = onSelected
    let row = min(3, items.count)
    dropDownView.frame = CGRect(x: frame.origin.x,
                                y: frame.origin.y + frame.height + 2,
                                width: frame.width,
                                height: 40 * CGFloat(row))
    parentView.addSubview(dropDownView)
    dropDownView.sbsGold_embedded(view: tbView)
    tbView.delegate = self
    tbView.dataSource = self
    
    dropDownItems = items
    addNotificationObservable()
  
    if let index = items.firstIndex(where: { $0 == content }) {
      selectedIndex = index
    } else {
      selectedIndex = -1
    }
  }

  func addTapGes(completionHandler: @escaping() -> ()) {
    onTapGes = completionHandler
    let tapGes = UITapGestureRecognizer(target: self, action: #selector(didTapAction))
    editEnable = false
    isUserInteractionEnabled = true
    addGestureRecognizer(tapGes)
  }
  
  @objc
  private func didTapAction() {
    onTapGes?()
  }
  
  override func setupView() {
    inputTf.delegate = self
    inputTf.textColor = SBSColor._8F8F8F
    titleLb.text = ""
    bottomTitleLb.text = ""
    inputTf.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
    Apply(containerView!) {
      $0.layer.borderWidth = 1
      $0.layer.borderColor = #colorLiteral(red: 0.8117647059, green: 0.8117647059, blue: 0.8117647059, alpha: 1)
      $0.clipsToBounds = true
      $0.layer.cornerRadius = 8
    }
    bottomTitleLb.textColor = mainColor
    inputTf.addDoneToolBar()
  }
  
  @objc
  private func textFieldDidChange() {
    onTextChange?(inputTf.text.stringValue())
  }
  
  func addNotificationObservable() {
    NotificationCenter.default
      .post(name: Notification.Name(SBSInputTextView.className),
            object: nil,
            userInfo: nil)
    NotificationCenter.default
      .addObserver(self,
                   selector: #selector(removeDropDownView),
                   name: Notification.Name(SBSInputTextView.className),
                   object: nil)
  }
  
  @objc
  private func removeDropDownView() {
    NotificationCenter.default
      .removeObserver(self,
                      name: Notification.Name(SBSInputTextView.className),
                      object: nil)
    dropDownView.removeFromSuperview()
  }
}

extension SBSInputTextView: UITextFieldDelegate {
  func textField(_ textField: UITextField,
                 shouldChangeCharactersIn range: NSRange,
                 replacementString string: String) -> Bool {
    guard !string.isEmpty else { return true }
    let finalText = (textField.text.stringValue() + string).replacingOccurrences(of: ",", with: "")
    let shouldChange = finalText.count <= maximum
    return shouldChange
  }
}

extension SBSInputTextView: UITableViewDataSource, UITableViewDelegate {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return dropDownItems.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    return Init(tbView.dequeue(SBSDropDownItemCell.self)) {
      $0.contentView.backgroundColor = indexPath.row == selectedIndex ? #colorLiteral(red: 0.9607843137, green: 0.9607843137, blue: 0.9803921569, alpha: 1) : .white
      $0.content = dropDownItems[indexPath.row]
      $0.onSelected = { [weak self] in
        self?.onSelected?(indexPath.row)
        self?.removeDropDownView()
      }
    }
  }
}
