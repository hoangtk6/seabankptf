//
//  SBSDropDownKMCell.swift
//  SBGold
//
//  Created by Anonymos on 08/08/2021.
//

import UIKit

final class SBSDropDownKMCell: UITableViewCell {
  @IBOutlet weak private var leftLb: UILabel!
  @IBOutlet weak private var centerLb: UILabel!
  @IBOutlet weak private var rightLb: UILabel!
  
  var isHeader: Bool = true {
    didSet {
      [leftLb, centerLb, rightLb].forEach({
        $0?.textColor = isHeader ? SBSColor._D72027 : SBSColor._292929
        $0?.font = UIFont.systemFont(ofSize: 13, weight: .medium)
      })
    }
  }
  
  func binData(value1: String, value2: String, value3: String) {
    leftLb.text = value1
    centerLb.text = value2
    rightLb.text = value3
  }
}
