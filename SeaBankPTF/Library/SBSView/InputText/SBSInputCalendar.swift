import UIKit

final class SBSInputCalendar: ViewWithXib {
  
  // MARK: - IBOutlet
  @IBOutlet weak private var titleLb: UILabel!
  @IBOutlet weak private var starRequireLabel: UILabel!
  @IBOutlet weak private var bottomTitleLb: UILabel!
  @IBOutlet weak private(set) var inputTf: UITextField! // Để private set còn có lúc sửa được thuộc tính textField cho dễ
  @IBOutlet weak private var leadingSpace: NSLayoutConstraint!
  @IBOutlet weak private var containerView: UIView!
  
  // MARK: - Custom proprety
  var editEnable: Bool = true {
    didSet {
      inputTf.isUserInteractionEnabled = editEnable
    }
  }
  
  var isRequire: Bool = false {
    didSet {
      starRequireLabel.isHidden = !isRequire
    }
  }
  
  var title: String = "" {
    didSet {
      titleLb.text = title
    }
  }
  
  var titleAttr: NSAttributedString? {
    didSet {
      titleLb.attributedText = titleAttr
    }
  }
  
  var bottomTitle: String = "" {
    didSet {
      bottomTitleLb.text = bottomTitle
    }
  }
  
  var placeHolder: String = "dd/mm/yy" {
    didSet {
      inputTf.placeholder = placeHolder
    }
  }
  
  var content: String? {
    set {
      inputTf.text = newValue
      inputTf.sendActions(for: .valueChanged)
    }
    
    get {
      return inputTf.text ?? ""
    }
  }
  
  var titleColor: UIColor = .darkText {
    didSet {
      titleLb.textColor = titleColor
    }
  }
  
  var contentColor: UIColor = .darkText {
    didSet {
      inputTf.textColor = contentColor
    }
  }
  
  var bottomTitleColor: UIColor = .darkText {
    didSet {
      bottomTitleLb.textColor = bottomTitleColor
    }
  }
  
  var leading: CGFloat = 0 {
    didSet {
      leadingSpace.constant = leading
    }
  }
  
  func firstResponder(_ enable: Bool) {
    if enable {
      inputTf.becomeFirstResponder()
    } else {
      inputTf.resignFirstResponder()
    }
  }
  
  override func setupView() {
    inputTf.textColor = SBSColor._8F8F8F
    inputTf.addInputViewDatePicker(target: self, selector: #selector(touchDoneButton))
    titleLb.text = ""
    bottomTitleLb.text = ""
    Apply(containerView!) {
      $0.layer.borderWidth = 1
      $0.layer.borderColor = #colorLiteral(red: 0.8117647059, green: 0.8117647059, blue: 0.8117647059, alpha: 1)
      $0.clipsToBounds = true
      $0.layer.cornerRadius = 8
    }
    bottomTitleLb.textColor = mainColor
  }
  
  func addNotificationObservable() {
    NotificationCenter.default
      .post(name: Notification.Name(SBSInputCalendar.className),
            object: nil,
            userInfo: nil)
  }
  
  @objc private func removeDropDownView() {
    NotificationCenter.default
      .removeObserver(self,
                      name: Notification.Name(SBSInputCalendar.className),
                      object: nil)
  }
  
  @objc private func touchDoneButton() {
      if let datePicker = self.inputTf.inputView as? UIDatePicker {
          let dateFormatter = DateFormatter()
          dateFormatter.dateFormat = "dd/MM/yyyy"
          let dateString = dateFormatter.string(from: datePicker.date)
          inputTf.text = dateString
      }
    inputTf.resignFirstResponder()
  }
}
