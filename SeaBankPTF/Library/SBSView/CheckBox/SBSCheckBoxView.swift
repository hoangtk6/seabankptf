//
//  CheckBoxView.swift
//  SBGold
//
//  Created by Anonymos on 07/06/2021.
//

import UIKit

final class SBSCheckBoxView: ViewWithXib {
  
  @IBOutlet weak private var checkBtn: UIButton!
  @IBOutlet weak private var titleLb: UILabel!
  @IBOutlet weak private var messageLb: UILabel!
  @IBOutlet weak private var checkImv: UIImageView!
  
  var onValueChange: ((Bool) -> ())?
  var onSelectHightLight: ((String) -> ())?
  
  var messageColor: UIColor = SBSColor._D72027 {
    didSet {
      messageLb.textColor = messageColor
    }
  }
  
  var isSelected: Bool = false {
    didSet {
      let img = isSelected ? "sbs_gold_box_on" : "sbs_gold_box_off"
      checkImv.image = UIImage(named: img)
      if isSelected {
        checkImv.sbsGold_setColor(SBSGoldConfig.themeColor)
      } else {
        checkImv.sbsGold_setColor(UIColor.darkGray)
      }
    }
  }
  
  private var hightLight: [String] = []
  
  override func setupView() {
    addObservable(buttons: [checkBtn])
    if isSelected {
      checkImv.sbsGold_setColor(SBSGoldConfig.themeColor)
    } else {
      checkImv.sbsGold_setColor(UIColor.darkGray)
    }
    titleLb.isUserInteractionEnabled = true
    titleLb.addGestureRecognizer(UITapGestureRecognizer(target:self, action: #selector(tapLabel(gesture:))))
    messageLb.text = ""
    messageLb.textColor = messageColor
  }
  
  @objc
  private func tapLabel(gesture: UITapGestureRecognizer) {
    guard !hightLight.isEmpty, let text = titleLb.text else { return }
    for key in hightLight {
      let range = (text as NSString).range(of: key)
      if gesture.didTapAttributedTextInLabel(label: titleLb, inRange: range) {
        onSelectHightLight?(key)
      }
    }
  }
  
  override func didTapButton(_ btn: UIButton) {
    isSelected = !isSelected
    onValueChange?(isSelected)
  }
  
  func setTitle(_ text: String, hightLight: [String]) {
    self.hightLight = hightLight
    let nsText = text as NSString
    let att = NSMutableAttributedString(string: text)
    att.addAttributes([NSAttributedString.Key.font: UIFont.italicSystemFont(ofSize: 14)],
                      range: NSRange(location: 0, length: text.count))
    for key in hightLight {
      let range = nsText.range(of: key)
      if range.length > 0 {
        att.addAttributes([NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue,
                           NSAttributedString.Key.foregroundColor: UIColor.blue],
                          range: range)
      }
    }
    titleLb.attributedText = att
  }
  
  func setTitleAttribute(_ attr: NSAttributedString) {
    titleLb.attributedText = attr
  }
  
  func setMessage(_ text: String) {
    messageLb.text = text
  }
}
