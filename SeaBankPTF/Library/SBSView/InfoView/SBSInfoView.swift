import UIKit

final class SBSInfoView: ViewWithXib {
  
  @IBOutlet weak private var titleLb: UILabel!
  @IBOutlet weak private var contentLb: UILabel!
  
  override func setupView() {
    Apply(titleLb!) {
      $0.font = RobotoFont.fontWithType(.regular, size: 14)
      $0.textColor = SBSColor._313131
    }
    Apply(contentLb!) {
      $0.font = RobotoFont.fontWithType(.medium, size: 17)
      $0.textColor = SBSColor._313131
    }
  }
  
  var titleColor: UIColor? {
    didSet {
      titleLb.textColor = titleColor
    }
  }
  
  var contentFont: UIFont? {
    didSet {
      contentLb.font = contentFont
    }
  }
  
  func setTitle(_ text: String?) {
    titleLb.text = text
  }
  
  func setContent(_ text: String?) {
    contentLb.text = text
  }
}
