//
//  SBSString+Ext.swift
//  SBGold
//
//  Created by Anonymos on 07/06/2021.
//

import UIKit

extension String {
  func sbsGold_intValue() -> Int {
    return Int(self) ?? 0
  }
  
  func sbsGold_toDate(format: String) -> Date? {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = format
    return dateFormatter.date(from: self)
  }
  
  func sbsGold_unaccent() -> String {
    return self.folding(options: .diacriticInsensitive, locale: .current).lowercased()
  }
}
