//
//  SBSUIImageView+Ext.swift
//  SBGold
//
//  Created by Anonymos on 11/06/2021.
//

import UIKit

extension UIImageView {
  func setColor(_ color: UIColor) {
    let templateImage = self.image?.withRenderingMode(.alwaysTemplate)
    self.image = templateImage
    self.tintColor = color
  }
  
  func sbsGold_setColor(_ color: UIColor) {
    let templateImage = self.image?.withRenderingMode(.alwaysTemplate)
    self.image = templateImage
    self.tintColor = color
  }
  
  func sbsGold_setImageUrl(_ url: String, mode: UIImage.RenderingMode = .alwaysOriginal) {
    SBSImageLoader.shared.loadImage(url: url) { [weak self] image in
      guard let `self` = self else { return }
      DispatchQueue.main.async {
        self.image = image?.withRenderingMode(mode)
      }
    }
  }
}
