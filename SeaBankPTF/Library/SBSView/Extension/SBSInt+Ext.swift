//
//  SBSInt+Ext.swift
//  SBGold
//
//  Created by Anonymos on 07/06/2021.
//

import Foundation

extension Int {
  func sbsGold_toString() -> String {
    return "\(self)"
  }
  
  func sbsGold_withCommas() -> String {
    let numberFormatter = NumberFormatter()
    numberFormatter.numberStyle = .decimal
    numberFormatter.groupingSeparator = ","
    return numberFormatter.string(from: NSNumber(value: self)) ?? ""
  }
}

extension Double {
  func sbsGold_withCommas() -> String {
    let numberFormatter = NumberFormatter()
    numberFormatter.numberStyle = .decimal
    numberFormatter.groupingSeparator = ","
    return numberFormatter.string(from: NSNumber(value: self)) ?? ""
  }
  
  func sbsGold_toString() -> String {
    let numberFormatter = NumberFormatter()
    numberFormatter.numberStyle = .decimal
    numberFormatter.groupingSeparator = ""
    return numberFormatter.string(from: NSNumber(value: self)) ?? ""
  }
}
