//
//  SBSGoldConfig.swift
//  SBGold
//
//  Created by Anonymos on 24/08/2021.
//

import UIKit

let mainColor = #colorLiteral(red: 0.8431372549, green: 0.09803921569, blue: 0.1254901961, alpha: 1)
let bgColor = #colorLiteral(red: 0.9607843137, green: 0.9607843137, blue: 0.9803921569, alpha: 1)

struct SBSGoldConfig {
  static var sbsBgColor = SBSColor._F5F5FA
  static var themeColor = SBSColor._D72027
  
  static var bgAccountImage = "sbs_gold_account_background"
  static var bgGold = "sbs_gold_background"
  
  static var icGoldItem = "sbs_gold_item"
  static var icGoldUser = "sbs_gold_user"

  static var language: String = "vi"
  static var mainAccount: String = ""
  static var accessToken: String = ""
  
  static var sbsGoldUrl = "https://bmbsoft.com.vn:8440/GOLD_TEST/api/"
  static var sbsDevUrl = "https://bmbsoft.com.vn:8440/GOLD_DEV/api/"
  static var sbsSeaBankUrl = "https://apibillingtst.seabank.com.vn:8453/"
}

struct SBSColor {
  static let _313131 = #colorLiteral(red: 0.1921568627, green: 0.1921568627, blue: 0.1921568627, alpha: 1)
  static let _292929 = #colorLiteral(red: 0.1607843137, green: 0.1607843137, blue: 0.1607843137, alpha: 1)
  static let _929497 = #colorLiteral(red: 0.5725490196, green: 0.5803921569, blue: 0.5921568627, alpha: 1)
  static let _8F8F8F = #colorLiteral(red: 0.5607843137, green: 0.5607843137, blue: 0.5607843137, alpha: 1)
  static let _D72027 = #colorLiteral(red: 0.8431372549, green: 0.1254901961, blue: 0.1529411765, alpha: 1)
  static let _0000FF = #colorLiteral(red: 0, green: 0, blue: 1, alpha: 1)
  static let _F5F5FA = #colorLiteral(red: 0.9607843137, green: 0.9607843137, blue: 0.9803921569, alpha: 1)
}


public var SBSGOLD_HET_GIO_GIAO_DICH = "SBSGOLD_HET_GIO_GIAO_DICH"
public var SBSGOLD_HET_PHIEN_GIAO_DICH = "SBSGOLD_HET_PHIEN_GIAO_DICH"
