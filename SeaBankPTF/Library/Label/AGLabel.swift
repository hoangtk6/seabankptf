//
//  AGLabel.swift
//  Comic
//
//  Created by Hoang Vu on 7/20/20.
//  Copyright © 2020 Techlab Corp. All rights reserved.
//

import UIKit

final class AGLabel: UIView {
  
  var alignment : String = CATextLayerAlignmentMode.left.rawValue{
    didSet{
      configureText()
    }
  }
  var font : UIFont = UIFont.systemFont(ofSize: 16){
    didSet{
      configureText()
    }
  }
  var fontSize : CGFloat = 16.0{
    didSet{
      configureText()
    }
  }
  var textColor : UIColor = UIColor.black{
    didSet{
      configureText()
    }
  }
  
  var text : String = ""{
    didSet{
      configureText()
    }
  }
  
  
  override class var layerClass: AnyClass {
    get {
      return CATextLayer.self
    }
  }
  
  func configureText(){
    if let textLayer = self.layer as? CATextLayer{
      textLayer.foregroundColor = textColor.cgColor
      textLayer.font = font
      textLayer.fontSize = fontSize
      textLayer.string = text
      textLayer.contentsScale = UIScreen.main.scale
      textLayer.contentsGravity = CALayerContentsGravity.center
      textLayer.isWrapped = true
    }
  }
}
