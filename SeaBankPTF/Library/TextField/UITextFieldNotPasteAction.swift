//
//  UITextFieldNotPasteAction.swift
//  Comic
//
//  Created by Hoang Vu Van on 4/1/20.
//  Copyright © 2020 Techlab Corp. All rights reserved.
//

import UIKit

final class UITextFieldNotPasteAction: UITextField {
  override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
    if action == #selector(UIResponderStandardEditActions.paste(_:)) {
      return false
    }
    return super.canPerformAction(action, withSender: sender)
  }
}
