//
//  TidyTextField.swift
//
//  Created by Nguyen Thanh Trung on 11/25/20.
//  Copyright © 2020 Nguyen Thanh Trung. All rights reserved.
//

import UIKit

/*
 TextField này có cả UITextFieldNotPasteAction luôn rồi
 */
class PaddingTextField: UITextField {
  
  @IBInspectable var leftImage: UIImage? = nil
  @IBInspectable var leftPadding: CGFloat = 0
  @IBInspectable var gapPadding: CGFloat = 0
  
  @IBInspectable var placeholderColor : UIColor? {
    didSet {
      let rawString = attributedPlaceholder?.string != nil ? attributedPlaceholder!.string : ""
      let str = NSAttributedString(string: rawString, attributes: [NSAttributedString.Key.foregroundColor : placeholderColor!])
      attributedPlaceholder = str
    }
  }
  
  private var textPadding: UIEdgeInsets {
    let p: CGFloat = leftPadding + gapPadding + (leftView?.frame.width ?? 0)
    return UIEdgeInsets(top: 0, left: p, bottom: 0, right: 5)
  }
  
  override open func textRect(forBounds bounds: CGRect) -> CGRect {
    return bounds.inset(by: textPadding)
  }
  
  override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
    return bounds.inset(by: textPadding)
  }
  
  override open func editingRect(forBounds bounds: CGRect) -> CGRect {
    return bounds.inset(by: textPadding)
  }
  
  override func leftViewRect(forBounds bounds: CGRect) -> CGRect {
    var r = super.leftViewRect(forBounds: bounds)
    r.origin.x += leftPadding
    return r
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    setup()
  }
  
  override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
    if action == #selector(UIResponderStandardEditActions.paste(_:)) {
      return false
    }
    return super.canPerformAction(action, withSender: sender)
  }
  
  private func setup() {
    if let image = leftImage {
      if leftView != nil { return } // critical!
      
      let im = UIImageView(frame: CGRect(x: 10, y: 5, width: 30, height: 30))
      im.contentMode = .scaleAspectFit
      im.image = image
      
      let imageContainerView = UIView(frame: CGRect(x: 0, y: 0, width: 45, height: 40))
      imageContainerView.addSubview(im)
      
      leftViewMode = UITextField.ViewMode.always
      leftView = imageContainerView
      
    } else {
      leftViewMode = UITextField.ViewMode.never
      leftView = nil
    }
  }
}
