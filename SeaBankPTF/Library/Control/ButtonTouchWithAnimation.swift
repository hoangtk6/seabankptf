//
//  File.swift
//  Comic
//
//  Created by Hoang Vu on 10/16/20.
//  Copyright © 2020 Techlab Corp. All rights reserved.
//

import UIKit

final class ButtonTouchWithAnimation: UIButton {
  private var isLiked = false
  
  private var unlikedImage:UIImage?
  private var likedImage :UIImage?
  
  private var unlikedScale: CGFloat = 0.7
  private var likedScale: CGFloat = 1.3

  public func setupUI(isLiked:Bool, likeImage:UIImage, unLikeImage:UIImage, likedScale: CGFloat?, unlikedScale: CGFloat?){
    self.unlikedImage = unLikeImage
    self.likedImage = likeImage
    self.unlikedScale = unlikedScale ?? self.unlikedScale
    self.likedScale = likedScale ?? self.likedScale
    self.isLiked = isLiked
    if isLiked {
      setImage(likeImage, for: .normal)
    } else {
      setImage(unlikedImage, for: .normal)
    }
    
  }

  public func flipLikedState(isLike:Bool) {
    if self.isLiked != isLike{
      self.isLiked = isLike
      animate()
    }
  }

  private func animate() {
    UIView.animate(withDuration: 0.1, animations: {
      let newImage = self.isLiked ? self.likedImage : self.unlikedImage
      let newScale = self.isLiked ? self.likedScale : self.unlikedScale
      self.transform = self.transform.scaledBy(x: newScale, y: newScale)
      self.setImage(newImage, for: .normal)
    }, completion: { _ in
      UIView.animate(withDuration: 0.1, animations: {
        self.transform = CGAffineTransform.identity
      })
    })
  }
}

