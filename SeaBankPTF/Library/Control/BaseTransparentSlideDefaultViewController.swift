//
//  BaseTransparentSlideDefaultViewController.swift
//  Example
//
//  Created by Jiar on 2018/12/17.
//  Copyright © 2018 Jiar. All rights reserved.
//

import UIKit
import SegementSlide

class ConfigManager {
  static let shared = ConfigManager()
  
  let switcherConfig: SegementSlideDefaultSwitcherConfig
  
  init() {
    switcherConfig = SegementSlideDefaultSwitcherConfig(normalTitleColor: UIColor.gray, selectedTitleColor: UIColor.darkGray, indicatorColor: UIColor.darkGray, badgeHeightForPointType: 9, badgeHeightForCountType: 15, badgeHeightForCustomType: 14)
  }
}


class BaseTransparentSlideDefaultViewController: TransparentSlideDefaultViewController {
  
  override var switcherConfig: SegementSlideDefaultSwitcherConfig {
    return ConfigManager.shared.switcherConfig
  }
  
  override func scrollViewDidScroll(_ scrollView: UIScrollView, isParent: Bool) {
    super.scrollViewDidScroll(scrollView, isParent: isParent)
    guard isParent else {
      return
    }
    updateNavigationBarStyle(scrollView)
  }
  
  private func updateNavigationBarStyle(_ scrollView: UIScrollView) {
    if scrollView.contentOffset.y > headerStickyHeight {
      switcherView.layer.applySketchShadow(color: .black, alpha: 0.03, x: 0, y: 2.5, blur: 5)
      switcherView.layer.add(generateFadeAnimation(), forKey: "reloadSwitcherView")
    } else {
      switcherView.layer.applySketchShadow(color: .clear, alpha: 0, x: 0, y: 0, blur: 0)
      switcherView.layer.add(generateFadeAnimation(), forKey: "reloadSwitcherView")
    }
  }
  
  private func generateFadeAnimation() -> CATransition {
    let fadeTextAnimation = CATransition()
    fadeTextAnimation.duration = 0.25
    fadeTextAnimation.type = .fade
    return fadeTextAnimation
  }
}
