//
//  TimerCountDown.swift
//  Comic
//
//  Created by Hoang Vu Van on 03/03/2021.
//  Copyright © 2021 TechLab Corp. All rights reserved.
//

import Foundation
import SwiftyBeaver

func defaultUpdateActionHandler(_: String)->(){
  
}

func defaultCompletionActionHandler()->(){
  
}

public final class TimerCountDown {
  //static let shared = TimerCountDown()

  var countdownTimer: Timer!
  var totalTime = 60
  var UpdateActionHandler:(String)->() = defaultUpdateActionHandler
  var CompletionActionHandler:()->() = defaultCompletionActionHandler
  
  public init(){
    //countdownTimer = Timer()
    totalTime = 60
    UpdateActionHandler = defaultUpdateActionHandler
    CompletionActionHandler = defaultCompletionActionHandler
  }
  
  public func initializeTimer(time:String){
    if let targedDate = String.getDate(time: time) {
      
    // Setting Today's Date
    let currentDate = Date()

    
    // Calculating the difference of dates for timer
    let calendar = Calendar.current.dateComponents([.day, .hour, .minute, .second], from: currentDate, to: targedDate)
    let days = calendar.day!
    let hours = calendar.hour!
    let minutes = calendar.minute!
    let seconds = calendar.second!
    totalTime = hours * 60 * 60 + minutes * 60 + seconds
    totalTime = days * 60 * 60 * 24 + totalTime
    }
  }
  
  func numberOfDaysInMonth(month:Int) -> Int{
    let dateComponents = DateComponents(year: 2015, month: 7)
    let calendar = Calendar.current
    let date = calendar.date(from: dateComponents)!
    
    let range = calendar.range(of: .day, in: .month, for: date)!
    let numDays = range.count
    print(numDays)
    return numDays
  }
  
  public func startTimer(pUpdateActionHandler:@escaping (String)->(),pCompletionActionHandler:@escaping ()->()) {
    countdownTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
    self.CompletionActionHandler = pCompletionActionHandler
    self.UpdateActionHandler = pUpdateActionHandler
  }
  
  @objc func updateTime() {
    self.UpdateActionHandler(timeFormatted(totalTime))
    
    if totalTime > 0 {
      totalTime -= 1
    } else {
      endTimer()
    }
  }
  
  func endTimer() {
    self.CompletionActionHandler()
    countdownTimer.invalidate()

  }
  
  func timeFormatted(_ totalSeconds: Int) -> String {
    let seconds: Int = totalSeconds % 60
    let minutes: Int = (totalSeconds / 60) % 60
    let hours: Int = (totalSeconds / 60 / 60) % 24
    let days: Int = (totalSeconds / 60 / 60 / 24)
    //log.warning(seconds)
    return ("\(days),\(hours),\(minutes),\(seconds)")
  }
  
  deinit {
    endTimer()
  }
}
