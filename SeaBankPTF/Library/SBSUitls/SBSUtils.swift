//
//  SBSUtils.swift
//  SBGoldSDK
//
//  Created by Anonymos on 2021/06/01.
//

import UIKit

internal func Init<T>( _ object: T, block: (T) throws -> ()) rethrows -> T {
  try block(object)
  return object
}

internal func Apply<T>( _ object: T, block: (T) throws -> ()) rethrows {
  try block(object)
}

struct SBSUtils {
//  static func kyHan(from: String, to: String) -> Int {
//    guard let fromDate = from.sbsGold_toDate(format: "dd/MM/yyyy"),
//          let toDate = to.sbsGold_toDate(format: "dd/MM/yyyy") else { return 0 }
//
//    return loop(start: fromDate, end: toDate, index: 1)
//  }

//  static func loop(start: Date, end: Date, index: Int) -> Int {
//    let nextDate = start.sbsGold_dateByAddingMonth(index)
//    if nextDate.timeIntervalSince1970 < end.timeIntervalSince1970 {
//      return loop(start: start, end: end, index: index + 1)
//    } else {
//      return index
//    }
//  }

  static func phoneSecret(_ phone: String) -> String {
    let nsPhone = NSString(string: phone)
    var secret = ""
    for index in 0..<phone.count {
      if phone.count - index < 5 {
        secret += "x"
      } else {
        secret += nsPhone.substring(with: NSRange(location: index, length: 1))
      }
    }
    return secret
  }

  static var isViLanguage: Bool {
    return SBSGoldConfig.language == "vi"
  }
}
