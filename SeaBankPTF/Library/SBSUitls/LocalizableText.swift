//
//  LocalizableText.swift
//  SBGold
//
//  Created by Anonymos on 07/06/2021.
//

import Foundation

typealias Text = SBSLocalizableText

struct SBSLocalizableText {
  static func localizedString(_ text: String) -> String {
    let path = Bundle.main.path(forResource: SBSGoldConfig.language, ofType: "lproj")
    let bunddle = Bundle(path: path!)
    let string = bunddle?.localizedString(forKey: text, value: nil, table: nil)
    return string.stringValue()
  }
}
