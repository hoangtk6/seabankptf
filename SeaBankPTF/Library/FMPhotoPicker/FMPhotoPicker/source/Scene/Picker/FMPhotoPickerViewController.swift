//
//  FMPhotoPickerViewController.swift
//  FMPhotoPicker
//
//  Created by c-nguyen on 2018/01/23.
//  Copyright © 2018 Tribal Media House. All rights reserved.
//

import UIKit
import Photos
import AVKit
import SVProgressHUD

// MARK: - Delegate protocol

public protocol FMPhotoPickerViewControllerDelegate: class {
  func fmPhotoPickerController(_ picker: FMPhotoPickerViewController, didFinishPickingPhotoWith photos: [UIImage])
  func fmPhotoPickerController(_ picker: FMPhotoPickerViewController, didFinishPickingPhotoWith assets: [PHAsset])
}

public extension FMPhotoPickerViewControllerDelegate {
  func fmPhotoPickerController(_ picker: FMPhotoPickerViewController, didFinishPickingPhotoWith photos: [UIImage]) {}
  func fmPhotoPickerController(_ picker: FMPhotoPickerViewController, didFinishPickingPhotoWith assets: [PHAsset]) {}
}

public class FMPhotoPickerViewController: UIViewController {
  // MARK: - Outlet
  @IBOutlet weak var imageCollectionView: UICollectionView!
  @IBOutlet weak var numberOfSelectedPhotoContainer: UIView!
  @IBOutlet weak var numberOfSelectedPhoto: UILabel!
  @IBOutlet weak var determineButton: UIButton!
  @IBOutlet weak var cancelButton: UIButton!
  @IBOutlet weak var lblTitle: UILabel!
  var doneSelected = false
  // MARK: - Public
  public weak var delegate: FMPhotoPickerViewControllerDelegate? = nil
  
  // MARK: - Private
//  lazy var emptyView = EmtptyDataView()
  var isLoading = false
  // Index of photo that is currently displayed in PhotoPresenterViewController.
  // Track this to calculate the destination frame for dismissal animation
  // from PhotoPresenterViewController to this ViewController
  private var presentedPhotoIndex: Int?
  
  private let config: FMPhotoPickerConfig
  
  var presentVC : FMPhotoPresenterViewController?
  // The controller for multiple select/deselect
  private lazy var batchSelector: FMPhotoPickerBatchSelector = {
    return FMPhotoPickerBatchSelector(viewController: self, collectionView: self.imageCollectionView, dataSource: self.dataSource)
  }()
  
  private var dataSource: FMPhotosDataSource! {
    didSet {
      if self.config.selectMode == .multiple {
        // Enable batchSelector in multiple selection mode only
        self.batchSelector.enable()
      }
    }
  }
  
  // MARK: - Init
  public init(config: FMPhotoPickerConfig) {
    self.config = config
    super.init(nibName: "FMPhotoPickerViewController", bundle: Bundle(for: type(of: self)))
  }
  
  required public init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  
  // MARK: - Life cycle
  override public func viewDidLoad() {
    super.viewDidLoad()
    self.setupView()
  }
  
  override public func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    if self.dataSource == nil {
      self.requestAndFetchAssets()
    }
  }
  
  override public func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
    if !isLoading{
      isLoading = true
      self.view.showLoadingView()
    }
  }
  
  // MARK: - Setup View
  private func setupView() {
    let reuseCellNib = UINib(nibName: "FMPhotoPickerImageCollectionViewCell", bundle: Bundle(for: self.classForCoder))
    self.imageCollectionView.register(reuseCellNib, forCellWithReuseIdentifier: FMPhotoPickerImageCollectionViewCell.reuseId)
    self.imageCollectionView.dataSource = self
    self.imageCollectionView.delegate = self
    
    self.numberOfSelectedPhotoContainer.layer.cornerRadius = self.numberOfSelectedPhotoContainer.frame.size.width / 2
    self.numberOfSelectedPhotoContainer.isHidden = true
    self.determineButton.isHidden = true
    
    
    // set button title
    //self.cancelButton.setTitle(config.strings["picker_button_cancel"], for: .normal)
    //self.cancelButton.titleLabel!.font = UIFont.systemFont(ofSize: config.titleFontSize)
    //self.cancelButton.setTitleColor(.descTextColor(), for: .normal)
    
    
    self.determineButton.setTitle(config.strings["picker_button_select_done"], for: .normal)
    self.determineButton.titleLabel!.font = UIFont.systemFont(ofSize: config.titleFontSize)
    self.determineButton.setTitleColor(.colorWeeboo(), for: .normal)
    
    self.lblTitle.text = config.titleNavi
  }
  
  // MARK: - Target Actions
  @IBAction func onTapCancel(_ sender: Any) {
    self.dismiss(animated: true)
  }
  
  @IBAction func onTapDetermine(_ sender: Any) {
    self.doneSelected = true
    processDetermination()
  }
  
  // MARK: - Logic
  private func requestAndFetchAssets() {
    if Helper.canAccessPhotoLib() {
      self.fetchPhotos()
    } else {
      Helper.showDialog(in: self, ok: {
        Helper.requestAuthorizationForPhotoAccess(authorized: self.fetchPhotos, rejected: Helper.openIphoneSetting)
      })
    }
  }
  
  private func fetchPhotos() {
    let photoAssets = Helper.getAssets(allowMediaTypes: self.config.mediaTypes)
    let forceCropType = config.forceCropEnabled ? config.availableCrops.first! : nil
    let fmPhotoAssets = photoAssets.map { FMPhotoAsset(asset: $0, forceCropType: forceCropType) }
    self.dataSource = FMPhotosDataSource(photoAssets: fmPhotoAssets)
    
    if self.dataSource.numberOfPhotos > 0 {
      self.imageCollectionView.reloadData()
      self.imageCollectionView.selectItem(at: IndexPath(row: self.dataSource.numberOfPhotos - 1, section: 0),
                                          animated: false,
                                          scrollPosition: .bottom)
    }
    self.checkShowEmtpy()
  }
  
  public func updateControlBar() {
    if self.dataSource.numberOfSelectedPhoto() > 0 {
      self.determineButton.isHidden = false
      if self.config.selectMode == .multiple {
        self.numberOfSelectedPhotoContainer.isHidden = false
        self.numberOfSelectedPhoto.text = "\(self.dataSource.numberOfSelectedPhoto())"
      }
    } else {
      self.determineButton.isHidden = true
      self.numberOfSelectedPhotoContainer.isHidden = true
    }
  }
  

  private func processDetermination() {
    if config.shouldReturnAsset {
      let assets = dataSource.getSelectedPhotos().compactMap { $0.asset }
      self.delegate?.fmPhotoPickerController(self, didFinishPickingPhotoWith: assets)
      return
    }
    
    FMLoadingView.shared.show()
    
    var dict = [Int:UIImage]()
    
    DispatchQueue.global(qos: .userInitiated).async {
      let multiTask = DispatchGroup()
      for (index, element) in self.dataSource.getSelectedPhotos().enumerated() {
        multiTask.enter()
        element.requestFullSizePhoto(cropState: .edited, filterState: .edited) {
          guard let image = $0 else { return }
          dict[index] = image
          multiTask.leave()
        }
      }
      multiTask.wait()
      
      let result = dict.sorted(by: { $0.key < $1.key }).map { $0.value }
      DispatchQueue.main.async {
        FMLoadingView.shared.hide()
        self.delegate?.fmPhotoPickerController(self, didFinishPickingPhotoWith: result)
      }
    }
  }
}

// MARK: - UICollectionViewDataSource
extension FMPhotoPickerViewController: UICollectionViewDataSource {
  public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    if let total = self.dataSource?.numberOfPhotos {
      return total
    }
    return 0
  }
  
  public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: FMPhotoPickerImageCollectionViewCell.reuseId, for: indexPath) as? FMPhotoPickerImageCollectionViewCell,
          let photoAsset = self.dataSource.photo(atIndex: indexPath.item) else {
      return UICollectionViewCell()
    }
    
    cell.loadView(photoAsset: photoAsset,
                  selectMode: self.config.selectMode,
                  selectedIndex: self.dataSource.selectedIndexOfPhoto(atIndex: indexPath.item))
    cell.onTapSelect = { [unowned self, unowned cell] in
      if let selectedIndex = self.dataSource.selectedIndexOfPhoto(atIndex: indexPath.item) {
        self.dataSource.unsetSeclectedForPhoto(atIndex: indexPath.item)
        cell.performSelectionAnimation(selectedIndex: nil)
        self.reloadAffectedCellByChangingSelection(changedIndex: selectedIndex)
      } else {
        //self.tryToAddPhotoToSelectedList(photoIndex: indexPath.item)
        self.tryToAddPhotoToSelectedList(photoIndex: indexPath.item) { (isValid) in
          
        }
      }
      self.updateControlBar()
    }
    
    return cell
  }
  
  /**
   Reload all photocells that behind the deselected photocell
   - parameters:
   - changedIndex: The index of the deselected photocell in the selected list
   */
  public func reloadAffectedCellByChangingSelection(changedIndex: Int) {
    let affectedList = self.dataSource.affectedSelectedIndexs(changedIndex: changedIndex)
    let indexPaths = affectedList.map { return IndexPath(row: $0, section: 0) }
    self.imageCollectionView.reloadItems(at: indexPaths)
  }
  
  /**
   Try to insert the photo at specify index to selectd list.
   In Single selection mode, it will remove all the previous selection and add new photo to the selected list.
   In Multiple selection mode, If the current number of select image/video does not exceed the maximum number specified in the Config,
   the photo will be added to selected list. Otherwise, a warning dialog will be displayed and NOTHING will be added.
   */
  public func tryToAddPhotoToSelectedList(photoIndex index: Int, completion: @escaping (Bool) -> Void) {
    if self.config.selectMode == .multiple {
      guard let fmMediaType = self.dataSource.mediaTypeForPhoto(atIndex: index) else { return }
      
      var canBeAdded = true
      
      switch fmMediaType {
        case .image:
          if self.dataSource.countSelectedPhoto(byType: .image) >= self.config.maxImage {
            canBeAdded = false
            self.view.showToastWith(String(format: config.strings["picker_warning_over_image_select_format"]!, self.config.maxImage), position: .top, icon: .error, background: .red)
          }
        case .video:
          if self.dataSource.countSelectedPhoto(byType: .video) >= self.config.maxVideo {
            canBeAdded = false
            self.view.showToastWith(String(format: config.strings["picker_warning_over_video_select_format"]!, self.config.maxVideo), position: .top, icon: .error, background: .red)
          }
        case .unsupported:
          break
      }
      
      if canBeAdded {
        self.dataSource.setSeletedForPhoto(atIndex: index)
        
        if fmMediaType == .video{
          let assets = self.dataSource.getSelectedPhotos().compactMap { $0.asset }
          if let asset = assets.first {
            self.checkVideoDurationLimited(asset: asset) { (isValid) in
              if !isValid {
                self.dataSource.unsetSeclectedForPhoto(atIndex: index)
                DispatchQueue.main.async {
                  self.imageCollectionView.reloadItems(at: [IndexPath(row: index, section: 0)])
                  self.updateControlBar()
                }
                completion(false)
                return
              }else{
                DispatchQueue.main.async {
                  self.imageCollectionView.reloadItems(at: [IndexPath(row: index, section: 0)])
                  self.updateControlBar()
                }
                completion(true)
              }
            }
          }
        }else{
          self.imageCollectionView.reloadItems(at: [IndexPath(row: index, section: 0)])
          self.updateControlBar()
          completion(true)
        }
      }
    } else {  // single selection mode
      var indexPaths = [IndexPath]()
      self.dataSource.getSelectedPhotos().forEach { photo in
        guard let photoIndex = self.dataSource.index(ofPhoto: photo) else { return }
        indexPaths.append(IndexPath(row: photoIndex, section: 0))
        self.dataSource.unsetSeclectedForPhoto(atIndex: photoIndex)
      }
      
      self.dataSource.setSeletedForPhoto(atIndex: index)
      indexPaths.append(IndexPath(row: index, section: 0))
      self.imageCollectionView.reloadItems(at: indexPaths)
      self.updateControlBar()
      completion(true)
    }
  }
}

// MARK: - UICollectionViewDelegate
extension FMPhotoPickerViewController: UICollectionViewDelegate {
  public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    let vc = FMPhotoPresenterViewController(config: self.config, dataSource: self.dataSource, initialPhotoIndex: indexPath.item)
    
    self.presentedPhotoIndex = indexPath.item
    
    vc.didSelectPhotoHandler = { photoIndex in
      //self.tryToAddPhotoToSelectedList(photoIndex: photoIndex)
      self.tryToAddPhotoToSelectedList(photoIndex: photoIndex) {(isValid) in
        if !isValid{
          DispatchQueue.main.async {
            vc.updateData(config: self.config, dataSource: self.dataSource, index: photoIndex)
            vc.updateInfoBar()
          }
        }
      }
    }
    vc.didDeselectPhotoHandler = { photoIndex in
      if let selectedIndex = self.dataSource.selectedIndexOfPhoto(atIndex: photoIndex) {
        self.dataSource.unsetSeclectedForPhoto(atIndex: photoIndex)
        self.reloadAffectedCellByChangingSelection(changedIndex: selectedIndex)
        self.imageCollectionView.reloadItems(at: [IndexPath(row: photoIndex, section: 0)])
        self.updateControlBar()
      }
    }
    vc.didMoveToViewControllerHandler = { vc, photoIndex in
      self.presentedPhotoIndex = photoIndex
    }
    vc.didTapDetermine = {
      self.processDetermination()
    }
    
    vc.view.frame = self.view.frame
    vc.transitioningDelegate = self
    vc.modalPresentationStyle = .custom
    vc.modalPresentationCapturesStatusBarAppearance = true
    self.present(vc, animated: true)
    
  }
}

// MARK: - UIViewControllerTransitioningDelegate
extension FMPhotoPickerViewController: UIViewControllerTransitioningDelegate {
  public func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    let animationController = FMZoomInAnimationController()
    animationController.getOriginFrame = self.getOriginFrameForTransition
    return animationController
  }
  
  public func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    guard let photoPresenterViewController = dismissed as? FMPhotoPresenterViewController else { return nil }
    let animationController = FMZoomOutAnimationController(interactionController: photoPresenterViewController.swipeInteractionController)
    animationController.getDestFrame = self.getOriginFrameForTransition
    return animationController
  }
  
  open func interactionControllerForDismissal(using animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
    guard let animator = animator as? FMZoomOutAnimationController,
          let interactionController = animator.interactionController,
          interactionController.interactionInProgress
    else {
      return nil
    }
    
    interactionController.animator = animator
    return interactionController
  }
  
  func getOriginFrameForTransition() -> CGRect {
    guard let presentedPhotoIndex = self.presentedPhotoIndex,
          let cell = self.imageCollectionView.cellForItem(at: IndexPath(row: presentedPhotoIndex, section: 0))
    else {
      return CGRect(x: 0, y: self.view.frame.height, width: self.view.frame.size.width, height: self.view.frame.size.width)
    }
    return cell.convert(cell.bounds, to: self.view)
  }
}

//MARK:- Hoang Custom
extension FMPhotoPickerViewController{
  private func checkVideoDurationLimited(asset:PHAsset, completion: @escaping (Bool) -> Void){
    
    PHCachingImageManager().requestAVAsset(forVideo: asset, options: nil) { (assets, audioMix, info) in
      guard let asset = assets as? AVURLAsset else{
        completion(false)
        return
      }
      let duration = asset.duration
      let durationTime = CMTimeGetSeconds(duration)
      if Int(durationTime) > self.config.limitedVideoDuration{
        DispatchQueue.main.async {
          //let warning = FMWarningView.shared
          //warning.message = String(format: self.config.strings["picker_warning_over_video_duration"]!, self.config.maxVideoDuration)
          //warning.showAndAutoHide()
          
          self.view.showToastWith(String(format: self.config.strings["picker_warning_over_video_duration"]!, self.config.limitedVideoDuration), position: .top, icon: .error, background: .red)
        }
        completion(false)
      }
      else{
        completion(true)
      }
    }
  }
  
  private func checkShowEmtpy(){
    self.view.hideLoadingView()
    if self.dataSource.numberOfPhotos == 0 {
//      self.addEmptyView(multiplied: 0.9, marginTop: 0)
    }
  }
  
//  func addEmptyView(viewType:emtptyDataViewType = .emptyLibrary, multiplied:Float = 1, marginTop:CGFloat = 3, marginBottom:CGFloat = 0) {
//    self.removeEmptyView()
//    self.emptyView.setup(typeView: viewType)
//    self.emptyView.translatesAutoresizingMaskIntoConstraints = false
//
//    UIView.transition(with: self.view, duration: 0.25, options: [.transitionCrossDissolve], animations: {
//      self.view.addSubview(self.emptyView)
//      self.view.bringSubviewToFront(self.emptyView)
//
//      self.emptyView.snp.makeConstraints { (make) -> Void in
//        make.leading.trailing.equalTo(self.view)
//        make.bottom.equalTo(self.view).inset(marginBottom)
//        //make.top.equalTo(self.view).inset(0)
//        make.top.equalTo(self.view).inset(UIApplication.shared.statusBarFrame.size.height + (self.navigationController?.navigationBar.frame.size.height ?? 0) + 3 + marginTop)
//      }
//      self.emptyView.setMultipliedCenterY(multiplied: multiplied)
//    }, completion: nil)
//    self.emptyView.onClickTryAgain  = { [weak self]  in
//      self?.closeEmptyViewAndBack()
//    }
//    self.emptyView.onClickClose = { [weak self]  in
//      self?.closeEmptyViewAndBack()
//    }
//  }
  
//  func removeEmptyView() {
//    UIView.transition(with: self.view, duration: 0.25, options: [.transitionCrossDissolve], animations: {
//      self.emptyView.removeFromSuperview()
//    }, completion: nil)
//  }

  
  func closeEmptyViewAndBack() {
//    self.emptyView.removeFromSuperview()
    self.backButtonClick()
  }

}
