//
//  WBServicesAPI.swift
//  Comic
//
//  Created by Hoang Vu on 12/3/19.
//  Copyright © 2019 Techlab Corp. All rights reserved.
//

import Foundation
import Moya


enum ServicesAPI {
  //get
  case getByHashTagId(id:String,type:String, page:Int, limit:Int, sortBy:String?)
  case getPoinPackages
  
  //put
  
  
  case shareNewsFeed(id: String)
  
  
  // post
  case postExchangePoint(id: String)
  
  //delete
  case deleteComicHistory(comicId:String)
  
  
  
}

extension ServicesAPI: TargetType {
  
  var platform:String{
    return "ios"
  }
  
  var baseDownloadPageUrl: String{
    #if PROD
    return "https://cdn.funtoon.vn"
    #elseif STAGING
    return "https://dev-cdn.funtoon.vn"
    #else
    return "https://dev-cdn.funtoon.vn"
    #endif
  }
  
  var baseUrl: String {
    #if PROD
    return "https://api.funtoon.vn"
    #elseif STAGING
    return "https://stg-api.funtoon.vn"
    #else
    return "https://dev-api.funtoon.vn"
    #endif
  }
  
  var apiVersion: String {
    #if PROD
    //    switch self {
    //    case .getCategory, .uploadMediaFun, .shareNewsFeed, .deleteFun, .getUserInfo, .getVideoComment, .getConfig:
    //        return "/api/v2"
    //      case .getNextVideo, .getNotifications, .searchFull:
    //        return "/api/v3"
    //      case .getNewsFeed, .getHome, .getNovel:
    //        return "/api/v4"
    //      default:
    return "/api/v1"
    //}
    #elseif STAGING
    //switch self {
    //      case .getCategory, .uploadMediaFun, .shareNewsFeed, .deleteFun, .getUserInfo, .getVideoComment, .getConfig:
    //        return "/api/v2"
    //      case .getNextVideo, .getNotifications, .searchFull:
    //        return "/api/v3"
    //      case .getNewsFeed, .getHome, .getNovel:
    //        return "/api/v4"
    //      default:
    return "/api/v1"
    //}
    #else
    //switch self {
    //    case .getCategory, .uploadMediaFun, .shareNewsFeed, .deleteFun, .getUserInfo, .getVideoComment:
    //        return "/api/v2"
    //      case .getNextVideo, .getNotifications, .searchFull:
    //        return "/api/v3"
    //      case .getNewsFeed, .getHome, .getNovel:
    //        return "/api/v4"
    //      default:
    return "/api/v1"
    //}
    #endif
  }
  
  var baseURL: URL {
    //    switch self {
    //      case .downloadChapterPage:
    //        return URL(string: self.baseDownloadPageUrl)!
    //      default:
    //        return URL(string: self.baseUrl + self.apiVersion)!
    //    }
    return URL(string: self.baseUrl + self.apiVersion)!
  }
  
  
  
  var sampleData: Data {
    return Data()
  }
  
  
  
  var task: Task {
    switch self {
    case .getByHashTagId(_, let type, let page, let limit, let sortBy):
      var paramDict = [String:Any]()
      paramDict["page"] = page
      paramDict["limit"] = limit
      paramDict["type"] = type
      paramDict["sortBy"] = sortBy
      return .requestParameters(parameters: paramDict, encoding: URLEncoding.queryString)
      
    case .shareNewsFeed,.getPoinPackages, .deleteComicHistory:
      return .requestPlain
      
      
    case .postExchangePoint(let id):
      var paramDict = [String:Any]()
      paramDict["pointPackId"] = id
      return  .requestParameters(parameters: paramDict, encoding: JSONEncoding.default)
      
      
    }
  }
  
  var headers: [String : String]? {
    
    
    return["app_name" : "","platform":platform,"device_id":"","device":"","version":"","platform_version":"","mac_address":"","Content-type": "application/json"]
    
  }
  
  var path: String {
    switch self {
    //get
    case .getByHashTagId(let id,_, _, _, _): return "/tag/\(id)"
    case .getPoinPackages: return "/gamification/pointPacks"
      
    //put
    
    case .shareNewsFeed(let id): return "/newsFeed/\(id)/share"
      
    // post
    case .postExchangePoint:
      return "/gamification/exchangePoint"
      
    // delete
    case .deleteComicHistory(let comicId): return "/history/\(comicId)"
    }
  }
  
  
  
  var method: Moya.Method {
    switch self {
    case  .getPoinPackages, .getByHashTagId:
      return .get
      
    case .shareNewsFeed:
      return .put
      
    case  .postExchangePoint:
      return .post
      
    case .deleteComicHistory:
      return .delete
      
    }
  }
}


