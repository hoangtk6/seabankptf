//
//  ComicAdapter.swift
//  Comic
//
//  Created by Hoang Vu on 12/3/19.
//  Copyright © 2019 Techlab Corp. All rights reserved.
//

import Foundation
import SwiftyBeaver
import Moya

struct NetworkAdapter {
  static let timeoutClosure = {(endpoint: Endpoint, closure: MoyaProvider<ServicesAPI>.RequestResultClosure) -> Void in
    if var urlRequest = try? endpoint.urlRequest() {
      urlRequest.timeoutInterval = 20
      closure(.success(urlRequest))
    } else {
      closure(.failure(MoyaError.requestMapping(endpoint.url)))
    }
  }
  

  
  //static let provider = MoyaProvider<WBServicesAPI>()
  //  static let provider = MoyaProvider<WBServicesAPI>(requestClosure: timeoutClosure, plugins: [NetworkLoggerPlugin()])
  
  //  static let provider = MoyaProvider<WBServicesAPI>(plugins: [NetworkLoggerPlugin(verbose: true)])
  
  //static let provider = MoyaProvider<WBServicesAPI>(plugins: [NetworkLoggerPlugin(conf)])
  
  static var provider: MoyaProvider<ServicesAPI> {
    #if PROD
    //return MoyaProvider<WBServicesAPI>(requestClosure: timeoutClosure, plugins: [NetworkLoggerPlugin()])
    return MoyaProvider<ServicesAPI>(plugins: [NetworkLoggerPlugin(verbose: true)])
    #elseif STAGING
    //return MoyaProvider<WBServicesAPI>(requestClosure: timeoutClosure, plugins: [NetworkLoggerPlugin()])
    return  MoyaProvider<ServicesAPI>(plugins: [NetworkLoggerPlugin(verbose: true)])
    #else
    return  MoyaProvider<ServicesAPI>(plugins: [NetworkLoggerPlugin(verbose: true)])
    //return MoyaProvider<WBServicesAPI>(requestClosure: timeoutClosure, plugins: [NetworkLoggerPlugin()])
    #endif
  }
  
  
  static var requests: [Cancellable] = []
  static func request(target: ServicesAPI, success successCallback: @escaping (Response) -> Void, error errorCallback: @escaping (Swift.Error) -> Void, failure failureCallback: @escaping (MoyaError) -> Void) {
    let request = provider.request(target){ (result) in
      switch result {
        case .success(let response):
          // 1:
          if (response.statusCode >= 200 && response.statusCode <= 300 || response.statusCode == Constants.HTTPResponseCode.EmptyData_404  ){
            successCallback(response)
          }else if response.statusCode == Constants.HTTPResponseCode.TokenExpire_401 {//Token expire, need refresh token again
            //self.refreshToken()
            let error = NSError(domain:"com.comic.WBNetworkAdapter", code:Constants.HTTPResponseCode.TokenExpire_401, userInfo:[NSLocalizedDescriptionKey: Constants.Prefix.tokenExpire])
            errorCallback(error)
          }else if response.statusCode == Constants.HTTPResponseCode.Exchange_Status_406 || response.statusCode == Constants.HTTPResponseCode.Exchange_Status_412 {
            let error = NSError(domain:"com.comic.WBNetworkAdapter", code:response.statusCode, userInfo:[NSLocalizedDescriptionKey: "Parsing Error"])
            errorCallback(error) 
          }else {
            // 2:
            let error = NSError(domain:"com.comic.WBNetworkAdapter", code:0, userInfo:[NSLocalizedDescriptionKey: "Parsing Error"])
            errorCallback(error)
        }
        case .failure(let error):
          // 3:
          failureCallback(error)
      }
      
    }
    requests.append(request)
  }
  static func requestWinCancel(target: ServicesAPI, success successCallback: @escaping (Response) -> Void, error errorCallback: @escaping (Swift.Error) -> Void, failure failureCallback: @escaping (MoyaError) -> Void) -> Cancellable{
     let request = provider.request(target){ (result) in
       switch result {
         case .success(let response):
           // 1:
           if (response.statusCode >= 200 && response.statusCode <= 300 || response.statusCode == Constants.HTTPResponseCode.EmptyData_404  ){
             successCallback(response)
           }else if response.statusCode == Constants.HTTPResponseCode.TokenExpire_401 {//Token expire, need refresh token again
             //self.refreshToken()
             let error = NSError(domain:"com.comic.MibooNetworkAdapter", code:Constants.HTTPResponseCode.TokenExpire_401, userInfo:[NSLocalizedDescriptionKey: Constants.Prefix.tokenExpire])
             errorCallback(error)
           }else {
             // 2:
             let error = NSError(domain:"com.comic.MibooNetworkAdapter", code:0, userInfo:[NSLocalizedDescriptionKey: "Parsing Error"])
             errorCallback(error)
         }
         case .failure(let error):
           // 3:
           failureCallback(error)
       }
       
     }
     return request
   }

  
  static func requestProgress(target: ServicesAPI,progress: @escaping (ProgressResponse) -> Void, success successCallback: @escaping (Response) -> Void, error errorCallback: @escaping (Swift.Error) -> Void, failure failureCallback: @escaping (MoyaError) -> Void) {

    
     let request = provider.request(target, callbackQueue: DispatchQueue.main, progress: { (response) in
       progress(response)

     }) { (result) in
       switch result {
         case .success(let response):
           // 1:
           if (response.statusCode >= 200 && response.statusCode <= 300 || response.statusCode == Constants.HTTPResponseCode.EmptyData_404  ){
             successCallback(response)
           }else if response.statusCode == Constants.HTTPResponseCode.TokenExpire_401 {//Token expire, need refresh token again
             //self.refreshToken()
             let error = NSError(domain:"com.comic.WBNetworkAdapter", code:Constants.HTTPResponseCode.TokenExpire_401, userInfo:[NSLocalizedDescriptionKey: Constants.Prefix.tokenExpire])
             errorCallback(error)
           }else {
             // 2:
             let error = NSError(domain:"com.comic.WBNetworkAdapter", code:0, userInfo:[NSLocalizedDescriptionKey: "Parsing Error"])
             errorCallback(error)
         }
         case .failure(let error):
           // 3:
           failureCallback(error)
       }
       
     }
     requests.append(request)
   }
  

  
  static func cancelAllRequest(){
    if requests.count > 0 {
      requests.forEach { cancellable in cancellable.cancel() }
      requests.removeAll()
    }
  }
}


