//
//  DownloadFunOperation.swift
//  Comic
//  Copyright © 2020 Techlab Corp. All rights reserved.
//

import Foundation
import UIKit

final class DownloadFunOperation: Operation {
  
  private var task: URLSessionDataTask!
  private var taskNumber: Int!
  
  enum OperationState: Int {
    case ready
    case executing
    case finished
  }
  
  override func start() {
    if (self.isCancelled) {
      state = .finished
      return
    }
    
    state = .executing
    print("Downloading \(self.task.originalRequest?.url?.absoluteString ?? emptyString)")
    
    self.task.resume()
  }
  
  override func cancel() {
    super.cancel()
    
    // cancel downloading
    self.task.cancel()
  }
  
  
  private var state: OperationState = .ready {
    willSet {
      self.willChangeValue(forKey: "isExecuting")
      self.willChangeValue(forKey: "isFinished")
    }
    
    didSet {
      self.didChangeValue(forKey: "isExecuting")
      self.didChangeValue(forKey: "isFinished")
    }
  }
  
  override var isReady: Bool { return state == .ready }
  override var isExecuting: Bool { return state == .executing }
  override var isFinished: Bool { return state == .finished }
  
  
  init(session: URLSession, downloadTaskURL: URL, number: Int, completionHandler: ((Data?, URLResponse?, Error?, Int) -> Void)?) {
    super.init()
    taskNumber = number
    task = session.dataTask(with: downloadTaskURL) { [weak self] (data, response, error) in
      if let completionHandler = completionHandler {
        completionHandler(data, response, error, self?.taskNumber ?? 0)
      }
      
      self?.state = .finished
    }
  }
}
