//
//  NSDate+Extensions.swift
//  Comic
//
//  Created by Hoang Vu on 1/6/20.
//  Copyright © 2020 Techlab Corp. All rights reserved.
//

import Foundation


let D_SECOND = 1
let D_MINUTE = 60
let D_HOUR = 3600
let D_DAY = 86400
let D_WEEK = 604800
let D_YEAR = 31556926


extension NSDate{
  
  class func getCurrentTimeStamp() -> Int64
  {
    let nowUTC = Date()
    let ti = nowUTC.timeIntervalSince1970;
    return Int64(ti*1000);
  }
  
  class func getDateFromLongLong(_ timestamp: Int64) -> Date? {
    return Date(timeIntervalSince1970: TimeInterval(timestamp / 1000))
  }
  
}
