//
//  Strings+Localized.swift
//  Comic
//
//  Created by Hoang Vu Van on 11/5/19.
//  Copyright © 2019 Techlab Corp. All rights reserved.
//


import Foundation


fileprivate func NSLocalizedString(_ key: String) -> String {
  return NSLocalizedString(key, comment: "")
}

extension String {
  static let sHome = NSLocalizedString("Home")
  static let sCategory = NSLocalizedString("Category")
  static let sSearch = NSLocalizedString("Search")
  static let sFavorite = NSLocalizedString("Favorite")
  static let sProfile = NSLocalizedString("Profile")
}

//Tab home
extension String {
  static let sJoinNowForMore = NSLocalizedString("Join now for more...")
  static let sSignIn = NSLocalizedString("Sign in")
  static let sSeeAll = NSLocalizedString("See all")
  static let sRomance = NSLocalizedString("Romance")
  
  static let sDrama = NSLocalizedString("Drama")
  static let sFantasy = NSLocalizedString("Fantasy")
  static let sComedy = NSLocalizedString("Comedy")
  static let sThirller = NSLocalizedString("Thirller")
  static let sAction = NSLocalizedString("Action")
  
  static let sHorror = NSLocalizedString("Horror")
  static let sMore = NSLocalizedString("More")
  static let sDay = NSLocalizedString("Day")
  static let sWeek = NSLocalizedString("Week")
  static let sMonth = NSLocalizedString("Month")
  static let sChapter = NSLocalizedString("Chapter")
  static let sNextChapter = NSLocalizedString("Next Chapter")
  static let SReading = NSLocalizedString("Reading")
  static let sContactColaboration = NSLocalizedString("ContactColaboration")
  static let sFeedBack = NSLocalizedString("FeedBack")
  static let sMangaNews = NSLocalizedString("Manga News")
  
  static let sViewCategory = NSLocalizedString("ViewCategory")
  static let sHistorySection = NSLocalizedString("History Section")
 
}


//Tab Search
extension String {
  static let sSearchHotKeywrods = NSLocalizedString("Hot keywords")
  static let sSearchRecentSearches = NSLocalizedString("Recent searches")
}

//Tab Favorite
extension String {
  static let sNewlatest = NSLocalizedString("New latest")
  static let sNearly = NSLocalizedString("Nearly")
  static let sContinueReading = NSLocalizedString("Continue reading")
  static let sRequireSignIn = NSLocalizedString("Require sign in")
  static let sEmptyHistoryTitle = NSLocalizedString("EmptyHistoryTitle")
  static let sOpenInSafari = NSLocalizedString("Mở bằng Safari")
  static let sCopyLink = NSLocalizedString("Sao chép liên kết")
}


//Profile
extension String {
  static let sEditProfile = NSLocalizedString("Edit Profile")
  static let sEditNickName = NSLocalizedString("Edit nick name")
  static let sEditEmail = NSLocalizedString("Edit email")
  static let sEditPhoneNumber = NSLocalizedString("Edit phone number")
  
  static let sPassWord = NSLocalizedString("Password")
  static let sChangePassWord = NSLocalizedString("Change password")
  static let sOldPassWord = NSLocalizedString("Old password")
  static let sEnterPassWord = NSLocalizedString("Enter password")
  static let sNewPassWord = NSLocalizedString("New password")
  static let sEnterNewPass = NSLocalizedString("Enter new password")
  static let sEnterNewPassAgain = NSLocalizedString("Enter new password again")
  
  static let sTakeAPhoto = NSLocalizedString("Take a photo")
  static let sEditPhoto = NSLocalizedString("Edit photo")
  static let sGallery = NSLocalizedString("Gallery")
  
  static let sReceiveGold = NSLocalizedString("Receive gold")
  static let sComicFollowing = NSLocalizedString("Comic following")
  static let sReportError = NSLocalizedString("Report error")
  
  static let sHelp = NSLocalizedString("Help")
  static let sTermAndPolicy = NSLocalizedString("Term and policy")
  static let sSettings = NSLocalizedString("Settings")
  static let sTapHereToSignIn = NSLocalizedString("Tap here to Sign in")
  static let sPrepaid = NSLocalizedString("Prepaid")
  static let sSignOut = NSLocalizedString("SignOut")
}

//Profile Detail
extension String {
  static let sProfileInfo = NSLocalizedString("Profile info")
  static let sAvatarProfile = NSLocalizedString("Avatar profile")
  static let sNickName = NSLocalizedString("Nick name")
  static let sEmail = NSLocalizedString("Email")
  static let sPhoneNumber = NSLocalizedString("Phone number")
  static let sBirthOfDay = NSLocalizedString("Birth of day")
  static let sPersonalProfile = NSLocalizedString("Personal Profile")
  static let sCopySuccess = NSLocalizedString("Copy Success")
  
}


//ReadingPage
extension String {
  static let sFollowing = NSLocalizedString("Following")
  static let sFollowed = NSLocalizedString("Followed")
  static let sShare = NSLocalizedString("Share")
  static let sVote = NSLocalizedString("Vote")
  static let sGift = NSLocalizedString("Gift")
  static let sReportAndFeedback = NSLocalizedString("Report and feedback")
  static let sSwipeUp = NSLocalizedString("Swipe up to read more")
  static let sSetting = NSLocalizedString("Setting")
  static let sLike = NSLocalizedString("Like")
  static let sLiked = NSLocalizedString("Liked")
  static let sRotateScreen = NSLocalizedString("Rotate screen")
  static let sQualityPhoto = NSLocalizedString("Quality photo")
  static let sState = NSLocalizedString("State")
  static let sTheFirstChapter = NSLocalizedString("The First Chapter")
  static let sTheNewestChapter = NSLocalizedString("The Newest Chapter")
  static let sTheEndChapter = NSLocalizedString("The End Chapter")
  static let sLikeChapterToast = NSLocalizedString("Like chapter Successfully")
}

//NewComic
extension String {
  static let sFunny = NSLocalizedString("Funny")
  static let sSecret = NSLocalizedString("Secret")
  static let sNewComic = NSLocalizedString("NewComic")
}

//ComicDetail
extension String {
  static let sInfomation = NSLocalizedString("Infomation")
  static let sRelate = NSLocalizedString("Relate")
  static let sComicRelate = NSLocalizedString("ComicRelate")
  static let sRatingComic = NSLocalizedString("Rating comic")
  static let sSendRating = NSLocalizedString("Send rating")
  static let sRatingSuccessfully = NSLocalizedString("Send rating successfully")
  static let sUnfollowComic = NSLocalizedString("Unfollow comic")
  static let sListOfChapters = NSLocalizedString("List of chapters")
  static let sAddToFavoriteToast = NSLocalizedString("Add to favorite Successfully")
  static let sRemoveFromFavoriteToast = NSLocalizedString("Removed from favorite Successfully")
  static let sUnlockChapter = NSLocalizedString("UnlockChapter")
  static let sLatestChapter = NSLocalizedString("Latest Chapter")
}

//Ranking
extension String {
  static let sPopular = NSLocalizedString("Popular")
}

//Question Answer
extension String {
  static let sFrequentlyAskedQuestions = NSLocalizedString("Frequently Asked Questions")
  static let sNotification = NSLocalizedString("Notification")
}

//Report and Feedback
extension String {
  static let sRequestComic = NSLocalizedString("Request comic")
  static let sRequestAnyComic = NSLocalizedString("Request any comic")
  //static let sReportAndFeedback = NSLocalizedString("Report and feedback")
  static let sReportOurs = NSLocalizedString("Report ours")
  static let sSuggestImprove = NSLocalizedString("Suggest improve")
  static let sSuggestIdeas = NSLocalizedString("Suggest ideas")
  static let sSuggestMore = NSLocalizedString("Suggest more")
  static let sPleaseEnterContent = NSLocalizedString("Please enter content")
}

//Send report
extension String {
  static let sErrorMessageContent = NSLocalizedString("Error message content")
  static let sAttachPhoto = NSLocalizedString("Attach photo")
  static let sPleaseEnterContentReport = NSLocalizedString("Please enter content...")
  static let sReport = NSLocalizedString("Report")
}

//Type Login
extension String {
  static let sSignInWithApple = NSLocalizedString("Sign in with Apple")
  static let sTermAndServices = NSLocalizedString("Term and services")
  static let sSigninWithFacebook = NSLocalizedString("Sign in with facebook")
  static let sSigninWithGoogle = NSLocalizedString("Sign in with google")
  static let sSkip = NSLocalizedString("Skip")
  static let sSignInSuccessfully = NSLocalizedString("Sign in successfully")
  static let sRegisterSuccess = NSLocalizedString("Register Success")
}

// Verify Phone number
extension String {
  static let sVerifyPhone = NSLocalizedString("Verify phone number")
  static let sWeSend = NSLocalizedString("We send SMS via your phone number registered")
  static let sEnterOtp = NSLocalizedString("Enter OTP code sent to")
  static let sYouNotRecv = NSLocalizedString("You not receive OTP code")
  static let sResendOtpCode = NSLocalizedString("Resend OTP code")
  static let sVerifySuccess = NSLocalizedString("Verify successfully with")
  static let sOpps = NSLocalizedString("Opps!")
  static let sSendOtpCode = NSLocalizedString("Send otp code")
  static let sYourPhoneValid = NSLocalizedString("Your phone number is Verified")
  static let sChangePhone = NSLocalizedString("Change phone number")
  static let sYourPhoneInvalid = NSLocalizedString("Your phone number is not Verified")
}

//Category
extension String {
  static let sContentFavorite = NSLocalizedString("Content Favorite")
  static let sSelectCategoryTitle = NSLocalizedString("SelectCategoryTitle")
  static let sCategoryComic = NSLocalizedString("Category Comic")
  
  static let sNewChapter = NSLocalizedString("New Chapter")
  static let sViewTop = NSLocalizedString("View Top")
  static let sAlphabet = NSLocalizedString("Alphabet")
}

//Donate
extension String{
  static let sDonateTopUpCoin = NSLocalizedString("DonateTopUpCoin")
  static let sDonateInputCoin = NSLocalizedString("DonateInputCoin")
  static let sDonateMessageToAuthor = NSLocalizedString("DonateMessageToAuthor")
  static let sDonate = NSLocalizedString("Donate")
  static let sDonateMessageToUpCoin = NSLocalizedString("DonateMessageToUpCoin")
  static let sDonateMessageSuccess = NSLocalizedString("DonateMessageSuccess")
  static let sAuthor = NSLocalizedString("Author")
}

//Author
extension String{
  static let sComicOf = NSLocalizedString("ComicOf")
}

//IAP
extension String {
  static let sIapTitle = NSLocalizedString("IapTitle")
  static let sIapUnlockDesc = NSLocalizedString("IapUnlockDesc")
  static let sIapMultiUnlockDesc = NSLocalizedString("IapMultiUnlockDesc")
  static let sIapOutOfCoin = NSLocalizedString("IapOutOfCoin")
  static let sIapOutOfCoinDesc = NSLocalizedString("IapOutOfCoinDesc")
  static let sStore = NSLocalizedString("Store")
  static let sRecoverCoinPacket = NSLocalizedString("Recover Coin Packet")
  static let sTopupCoinSuccessfully = NSLocalizedString("Top up Coin Successfully")
  
  static let sPurchaseFailed = NSLocalizedString("Purchase failed")
  static let sClientInvalid = NSLocalizedString("clientInvalid")
  static let sPaymentInvalid = NSLocalizedString("paymentInvalid")
  static let sPaymentNotAllowed = NSLocalizedString("paymentNotAllowed")
  static let sStoreProductNotAvailable = NSLocalizedString("storeProductNotAvailable")
  static let sCloudServiceNetworkConnectionFailed = NSLocalizedString("cloudServiceNetworkConnectionFailed")
}

//Button
extension String {
  static let sOk = NSLocalizedString("Ok")
  static let sCancel = NSLocalizedString("Cancel")
  static let sUpdate = NSLocalizedString("Update")
  static let sDelete = NSLocalizedString("Delete")
  static let sVerify = NSLocalizedString("Verify")
  static let sContinue = NSLocalizedString("Continue")
  static let sBack = NSLocalizedString("Back")
  static let sSeeMore = NSLocalizedString("See more")
  static let sFinish = NSLocalizedString("Finish")
  static let sBackToHome = NSLocalizedString("Back to home")
  static let sEdit = NSLocalizedString("Edit")
  static let sAuto = NSLocalizedString("Auto")
  static let sRead = NSLocalizedString("Read")
  static let sFree = NSLocalizedString("Free")
  static let sYes = NSLocalizedString("Yes")
  static let sNo = NSLocalizedString("No")
  static let sComment = NSLocalizedString("Comment")
  static let sReply = NSLocalizedString("Reply")
}

//MessageBox
extension String {
  static let sMsgTitleUnfollowAuthor = NSLocalizedString("Msg_Title_Unfollow_Author")
  static let sMsgFollowAuthor = NSLocalizedString("Msg_Follow_Author")
  static let sMsgUnfollowAuthor = NSLocalizedString("Msg_Unfollow_Author")
}

//Other
extension String {
  static let sTotalCoin = NSLocalizedString("Total Coin")
  static let sWalletCoin = NSLocalizedString("Wallet Coin")
  static let sTopUpCoin = NSLocalizedString("Top up Coin")
  static let sNotify = NSLocalizedString("Notify")
  static let sPullToPreviousChapter = NSLocalizedString("Pull to Previous Chapter")
  static let sPullToNextChapter = NSLocalizedString("Pull to Next Chapter")
  static let sReleaseToRefresh = NSLocalizedString("Release to refresh")
  static let sPullToRefresh = NSLocalizedString("Pull to refresh")
  static let sLoading = NSLocalizedString("Loading")
  static let sAnErrorOccurred = NSLocalizedString("An error occurred. \nPlease try again later!")
  static let sErrorOccurred = NSLocalizedString("Error occurred... Please try again!")
  static let sCheckedIn = NSLocalizedString("Checked in")
    
  static let sNoInternetConnection = NSLocalizedString("No internet connection")
  static let sConnecting = NSLocalizedString("Connecting...")
  static let sConfirmSignOut = NSLocalizedString("Are you sure sign out?")
  static let sTryAgain = NSLocalizedString("Try again")
  static let sAll = NSLocalizedString("All")
  static let sEmptyData = NSLocalizedString("Empty Data")
  static let invalidEmail = NSLocalizedString("Invalid Email")
  static let invalidName = NSLocalizedString("Invalid Name")
  static let customerFeedback = NSLocalizedString("Customer Feedback")
  static let unlockedSuccessfully = NSLocalizedString("Unlocked Successfully")
  static let sRequestLoginIAP = NSLocalizedString("Request Login IAP")
  static let sRequestToUpCoin = NSLocalizedString("Request To Up Coin")
  static let sInvalidPhone = NSLocalizedString("Invalid Phone")
  static let sBecomeAuthor = NSLocalizedString("Become Author")
  static let sCreation = NSLocalizedString("Creation")
  static let sTopfan = NSLocalizedString("Top Fan")
  static let sDonator = NSLocalizedString("Donator")
  static let sListDonator = NSLocalizedString("List Donator")
  static let sTooLongName = NSLocalizedString("Too Long Name")
  static let sNumberOfLike = NSLocalizedString("Number of Like")
  static let sFan = NSLocalizedString("Fan")
  static let sTopVote = NSLocalizedString("TopVote")
  static let sAllAuthor = NSLocalizedString("All Author")
  static let sName = NSLocalizedString("Name")
  static let sCharacter = NSLocalizedString("Character")
  static let sScrollUpToSee = NSLocalizedString("Scroll Up To See")
  static let sMentionTool = NSLocalizedString("sMentionTool")
  static let sViewCount = NSLocalizedString("View Count")
  static let sUpdateLatest = NSLocalizedString("Update Latest")
  static let sSort = NSLocalizedString("Sort")
  static let sRatingMost = NSLocalizedString("RatingMost")
  static let sFeedbackDescription = NSLocalizedString("Feedback Description")
  static let sFeedbackPlaceholder = NSLocalizedString("Feedback Placeholder")
  static let sEmailFeedback = NSLocalizedString("Email Feedback")
  static let sPhoneFeedback = NSLocalizedString("Phone Feedback")
  static let sWarningFeedbackImage = NSLocalizedString("Warning Feedback Image")
  static let sConfirmCancel = NSLocalizedString("Confirm Cancel")
  static let sReportPlaceholder = NSLocalizedString("Report Placeholder")
  static let sFollowThisComic = NSLocalizedString("Follow This Comic")
  static let sViewAuthor = NSLocalizedString("View Author")
  static let sReportComic = NSLocalizedString("Report Comic")
  static let sWeebooId = NSLocalizedString("Weeboo ID")
  static let sBiography = NSLocalizedString("Biography")
  static let sWarningUsername = NSLocalizedString("Warning Username")
  static let sWarningUserID = NSLocalizedString("Warning Weeboo ID")
  static let sNotUpdate = NSLocalizedString("Not Update")
  static let sDescriptionWeebooID = NSLocalizedString("Description WeebooID")
  static let sCanNotEditWeebooID = NSLocalizedString("Can not edit Weeboo ID")
  static let sFollower = NSLocalizedString("Follower")
  static let sUpdatingSource = NSLocalizedString("Updating_Source")
}


