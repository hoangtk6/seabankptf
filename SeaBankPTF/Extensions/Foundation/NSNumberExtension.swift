//
//  NSNumberExtension.swift
//  Comic
//
//  Created by Hoang Vu on 3/11/20.
//  Copyright © 2020 Techlab Corp. All rights reserved.
//

import Foundation

extension NSNumber {

  static func formatMoney(price:NSNumber, locale:Locale)-> String{
    let formatter = NumberFormatter()
    formatter.numberStyle = .currency
    formatter.string(from: price)
    formatter.locale = locale // Locale(identifier: "vi-VN")
    return formatter.string(from: price) ?? emptyValue // $123"
  }
}
