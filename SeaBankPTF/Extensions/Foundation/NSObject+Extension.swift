//
//  NSObject+Extension.swift
//  MochaSwift
//
//  Created by HoangVu on 4/8/19.
//  Copyright © 2019 HoangVu. All rights reserved.
//

import Foundation

extension NSObject {
  class var nameOfClass: String {
    return NSStringFromClass(self).components(separatedBy: ".").last! as String
  }
  
  // reuse identifier
  class var identifier: String {
    return String(format: "%@_identifier", self.nameOfClass)
  }
  
  public var className: String {
    return String(describing: type(of: self))
  }
  
  public class var className: String {
    return String(describing: self)
  }
}
