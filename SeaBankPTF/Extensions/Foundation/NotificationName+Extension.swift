//
//  NotificationName+Extension.swift
//  Comic
//
//  Created by Hoang Vu on 12/25/19.
//  Copyright © 2019 Techlab Corp. All rights reserved.
//

import Foundation

extension Notification.Name {
  static let didUpdateBadge = Notification.Name("didUpdateBadge")
  static let updateNotiMess = Notification.Name("updateNotiMess")
  static let signIn = Notification.Name("signIn")
  static let comicHistoryChangeData = Notification.Name("comicHistoryChangeData")
  static let changeUserInfo = Notification.Name("changeUserInfo")
  static let comicUpdateInfo = Notification.Name("comicUpdateInfo")
  static let loadAdsCompleted = Notification.Name("loadAdsCompleted")
  static let updateAuthorInfo = Notification.Name("updateAuthorInfo")
  
  static let oneShotChangeData = Notification.Name("oneShotChangeData")
  static let downloadComic = Notification.Name("downloadComic")

  
  static let updateComment = Notification.Name("updateComment")
  static let showPopup = Notification.Name("showPopup")
  static let updateFollowingAuthor = Notification.Name("updateFollowingAuthor")
  static let updateLikeComicOneShot = Notification.Name("updateLikeComicOneShot")
  
  static let newsUpdateLikeInfo = Notification.Name("newsUpdateLikeInfo")
  static let newsUpdateShareInfo = Notification.Name("newsUpdateShareInfo")
  static let newsUpdateCommentInfo = Notification.Name("newsUpdateCommentInfo")
   
  static let novelHistoryChangeData = Notification.Name("novelHistoryChangeData")

  static let updateToonComment = Notification.Name("updateToonComment")
  static let showKeyboardRatingNovel = Notification.Name("showKeyboardRatingNovel")
  static let updateRatingNovel = Notification.Name("updateRatingNovel")
    
  static let reloadMissionData = Notification.Name("reloadMissionData")
  static let updatePoints = Notification.Name("updatePoints")
}
