//
//  Int+Extension.swift
//  Comic
//
//  Created by Hoang Vu on 3/23/20.
//  Copyright © 2020 Techlab Corp. All rights reserved.
//

import Foundation

public extension Int {
  private static var numberFormatter: NumberFormatter = {
    let numberFormatter = NumberFormatter()
    numberFormatter.numberStyle = .decimal
    numberFormatter.groupingSeparator = "."
    
    return numberFormatter
  }()
  
  var delimiter: String {
    return Int.numberFormatter.string(from: NSNumber(value: self)) ?? ""
  }
  
  var kmFormatted: String {
      if self == 0 {
          return "0"
      }
      if self < 1000 {
          return String(format: "%d", locale: Locale.current,self)
      }
      if self >= 1000, self <= 999999 {
          return String(format: "%.1fK", locale: Locale.current, Double(self)/Double(1000)).replacingOccurrences(of: ".0", with: "")
      }
      
      if self > 999999 {
          return String(format: "%.1fM", locale: Locale.current, Double(self)/Double(1000000)).replacingOccurrences(of: ".0", with: "")
      }
      
      return String(format: "%.1fB", locale: Locale.current, Double(self)/Double(1000000000)).replacingOccurrences(of: ".0", with: "")
  }
  
  var boolValue: Bool {
      return self > 0
  }
}

extension Int {
  func withCommas() -> String {
    let numberFormatter = NumberFormatter()
    numberFormatter.numberStyle = .decimal
    numberFormatter.groupingSeparator = ","
    return numberFormatter.string(from: NSNumber(value:self)) ?? ""
  }
  
  func toString() -> String {
    return "\(self)"
  }
  
  func toShortString() -> String {
    switch self {
    case _ where self < 10000 :
      return self.toString()
    case _ where self < 1000000:
      let value = self / 1000
      let offset = (self - value * 1000) / 100
      let result = offset == 0 ? value.toString() : value.toString() + "." + offset.toString()
      return result + "K"
    default:
      let value = self / 1000000
      let offset = (self - value * 1000000) / 1000
      let result = offset == 0 ? value.toString() : value.toString() + "." + offset.toString()
      return result + "M"
    }
  }
  
  func toDecimaString() -> String {
    return self < 10 ? "0\(self)" : "\(self)"
  }
}

extension Int64 {
  func toString() -> String {
    return "\(self)"
  }
}

