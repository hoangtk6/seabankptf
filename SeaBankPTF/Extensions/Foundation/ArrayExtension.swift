//
//  ArrayExtension.swift
//  CountriesList
//
//  Created by HoangVu on 10/1/17.
//  Copyright © 2017 1Life2Live. All rights reserved.
//

extension Array {
  
  func filterDuplicates( includeElement: @escaping (_ lhs:Element, _ rhs:Element) -> Bool) -> [Element]{
    var results = [Element]()
    
    forEach { (element) in
      let existingElements = results.filter {
        return includeElement(element, $0)
      }
      if existingElements.count == 0 {
        results.append(element)
      }
    }
    
    return results
  }
  
  mutating func appendIfNotNil(_ item: Element?) {
    if let item = item {
      self.append(item)
    }
  }
}



