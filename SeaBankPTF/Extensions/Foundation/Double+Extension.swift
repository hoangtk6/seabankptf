//
//  Double+Extension.swift
//  Comic
//
//  Created by Hoang Vu on 12/5/19.
//  Copyright © 2019 Techlab Corp. All rights reserved.
//

import Foundation

public extension Double {
  
  var shortStringRepresentation: String {
    if self.isNaN {
      return "NaN"
    }
    if self.isInfinite {
      return "\(self < 0.0 ? "-" : "+")Infinity"
    }
    if self == 0 {
      return "0"
    }
    
    let units = ["", "K", "M"]
    var interval = self
    var i = 0
    while i < units.count - 1 {
      if abs(interval) < 1000.0 {
        break
      }
      i += 1
      interval /= 1000.0
    }
    // + 2 to have one digit after the comma, + 1 to not have any.
    // Remove the * and the number of digits argument to display all the digits after the comma.
    return "\(String(format: "%0.*g", Int(log10(abs(interval))) + 2, interval))\(units[i])"
  }
  
  func roundToPlaces(places:Int) -> Double {
    let divisor = pow(10.0, Double(places))
    return Darwin.round(self * divisor) / divisor
  }
  
  func rounded2Number() -> String {
    return String(format:"%.1f", self)
  }
  
  
  var cleanValue: String
  {
    return self.truncatingRemainder(dividingBy: 1) == 0 ? String(format: "%.1f", self) : String(self)
  }
}

extension Float {
  var cleanValue: String
  {
    //let a = self.truncatingRemainder(dividingBy: 1) == 0 ? String(format: "%.1f", self) : String(self)
    //log.warning(a)
    return self.truncatingRemainder(dividingBy: 1) == 0 ? String(format: "%.1f", self) : String(self)
  }
  
  func rounded2Number() -> String {
    return String(format:"%.1f", self)
  }
  
}
