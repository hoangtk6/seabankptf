//
//  Date+Extension.swift
//  Live
//
//  Created by TungPT on 
//  Copyright © 2019 Techlab. All rights reserved.
//

import Foundation

// MARK: - General
extension Date {
  // MARK: - Variables
  static let currentCalendar = Calendar(identifier: .gregorian)
  static let currentTimeZone = TimeZone.ReferenceType.local
  
  var currentAge: Int? {
    let ageComponents = Date.currentCalendar.dateComponents([.year], from: self, to: Date())
    return ageComponents.year
  }
  
   func getTimeCR4() -> String {
      let interval = Calendar.current.dateComponents([.day, .weekOfYear, .hour, .minute, .second], from: self, to: Date())
      let day = interval.day ?? 0
      let week = interval.weekOfYear ?? 0
      if week > 0 || day > 7 {
        return String(format: "%2d tuần", week)
      } else if day > 0 {
        return String(format: "%2d ngày", day)
      } else if let hour = interval.hour, hour > 0 {
        return String(format: "%2d giờ", hour)
      } else if let min = interval.minute, min > 1 {
        return String(format: "%2d phút", min)
      } else if let second = interval.second, second > 3 {
        return String(format: "%2d giây", second)
      } else {
        return "Vừa xong"
      }
  }
  func getTimeCR3() -> String {
    let interval = Calendar.current.dateComponents([.day, .weekOfYear, .hour, .minute, .second], from: self, to: Date())
    let day = interval.day ?? 0
    let week = interval.weekOfYear ?? 0
    if week > 0 || day > 7 {
      return String(format: "%2d tuần trước", week)
    } else if day > 0 {
      return String(format: "%2d ngày trước", day)
    } else if let hour = interval.hour, hour > 0 {
      return String(format: "%2d giờ trước", hour)
    } else if let min = interval.minute, min > 1 {
      return String(format: "%2d phút trước", min)
    } else if let second = interval.second, second > 3 {
      return String(format: "%2d giây trước", second)
    } else {
      return "Vừa xong"
    }
  }
  
  func getTimeCreated(getDetail: Bool = false) -> String {
    let interval = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute, .second], from: self, to: Date())
    let day = interval.day ?? 0
    let month = interval.month ?? 0
    let year = interval.year ?? 0
    if year > 0 || month > 0 || day > 7 {
      return self.stringBy(format: getDetail == true ? "dd/MM/yyyy HH:mm" : "dd/MM/yyyy")
    } else if day > 0 {
      if getDetail && self.isYesterday() {
        return "Hôm qua " + self.stringBy(format: "HH:mm")
      } else {
        return String(format: "%2d ngày trước", day)
      }
    } else if let hour = interval.hour, hour > 0 {
      return String(format: "%2d giờ trước", hour)
    } else if let min = interval.minute, min > 1 {
      return String(format: "%2d phút trước", min)
    } else if let second = interval.second, second > 3 {
      return String(format: "%2d giây trước", second)
    } else {
      return "Vừa xong"
    }
  }
  
  func getTimeCreatedRule2() -> String {
    let interval = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute, .second], from: self, to: Date())
    if self.isToday() {
      if let hour = interval.hour, hour > 0 {
        return String(format: "%2d giờ trước", hour)
      } else if let min = interval.minute, min > 1 {
        return String(format: "%2d phút trước", min)
      } else if let second = interval.second, second > 3 {
        return String(format: "%2d giây trước", second)
      } else {
        return "Vừa xong"
      }
    } else if self.isYesterday() {
      return "Hôm qua " + self.stringBy(format: "HH:mm")
    } else {
      return self.stringBy(format: "dd/MM/yyyy HH:mm")
    }
  }
  
  func getTimeCreatedRule5() -> String {
    let interval = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute, .second], from: self, to: Date())
    if self.isToday() {
      if let hour = interval.hour, hour > 0 {
        return String(format: "%2d giờ trước", hour)
      } else if let min = interval.minute, min > 1 {
        return String(format: "%2d phút trước", min)
      } else if let second = interval.second, second > 0 {
        if second > 3 {
          return String(format: "%2d giây trước", second)
        } else {
          return "Vừa xong"
        }
      } else {
        let newFormat = "Hôm nay"  + "  •  " + "HH:mm"
        return self.stringBy(format: newFormat)
      }
    } else if self.isYesterday() {
      return "Hôm qua " + self.stringBy(format: "HH:mm")
    } else {
      let newFormat = "dd/MM/yyyy" + "  •  " + "HH:mm"
      return self.stringBy(format: newFormat)
    }
  }
  
  func getTimeCreatedRule6() -> String {
    let interval = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute, .second], from: self, to: Date())
    if self.isToday() {
      if let hour = interval.hour, hour > 0 {
        return String(format: "%2d giờ trước", hour)
      } else if let min = interval.minute, min > 1 {
        return String(format: "%2d phút trước", min)
      } else if let second = interval.second, second > 3 {
        return String(format: "%2d giây trước", second)
      } else {
        return "Vừa xong"
      }
    } else if self.isYesterday() {
      return "Hôm qua" + "  •  " + self.stringBy(format: "HH:mm")
    } else {
      let newFormat = "dd/MM/yyyy" + "  •  " + "HH:mm"
      return self.stringBy(format: newFormat)
    }
  }
  
  func getTimeCreatedRule4() -> String {
    return self.stringBy(format: "dd/MM/yyyy")
  }
  
  static func getTimeNewsCR2FromString(string: String) -> String { // •
       let isoDateFormatter = DateFormatter()
       isoDateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
       isoDateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
       let trimmedIsoString = string// string.replacingOccurrences(of: "\\.\\d+Z", with: "", options: .regularExpression)
       isoDateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
       if let realDate = isoDateFormatter.date(from: trimmedIsoString) {
         return realDate.getTimeCreatedRule2()
       }
       
       return ""
     }
     static func getTimeNewsFromString(string: String) -> String { // •
       let isoDateFormatter = DateFormatter()
            isoDateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
            isoDateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
      // let trimmedIsoString = string.replacingOccurrences(of: "\\.\\d+Z", with: "", options: .regularExpression)
       if let realDate = isoDateFormatter.date(from: string) {
         return realDate.getTimeCR3()
       }
       return ""
     }
     
  
  var yesterday: Date? {
    return Date.currentCalendar.date(byAdding: .day, value: -1, to: self)
  }
  var tomorrow: Date? {
    return Date.currentCalendar.date(byAdding: .day, value: 1, to: self)
  }
  var weekday: Int {
    return Date.currentCalendar.component(.weekday, from: self)
  }
  
  var day: Int? {
    return components().day
  }
  
  var month: Int? {
    return components().month
  }
  
  var year: Int? {
    return components().year
  }
  
  var hour: Int? {
    return components().hour
  }
  
  var minute: Int? {
    return components().minute
  }
  
  var second: Int? {
    return components().second
  }
  
  // MARK: - Init
  init(year: Int, month: Int, day: Int, calendar: Calendar = Date.currentCalendar) {
    var dc = DateComponents()
    dc.year = year
    dc.month = month
    dc.day = day
    if let date = calendar.date(from: dc) {
      self.init(timeInterval: 0, since: date)
    } else {
      fatalError("Date component values were invalid.")
    }
  }
  
  init(hour: Int, minute: Int, second: Int, calendar: Calendar = Date.currentCalendar) {
    var dc = DateComponents()
    dc.hour = hour
    dc.minute = minute
    dc.second = second
    if let date = calendar.date(from: dc) {
      self.init(timeInterval: 0, since: date)
    } else {
      fatalError("Date component values were invalid.")
    }
  }
  
  init(year: Int, month: Int, day: Int, hour: Int, minute: Int, second: Int, calendar: Calendar = Date.currentCalendar) {
    var dc = DateComponents()
    dc.year = year
    dc.month = month
    dc.day = day
    dc.hour = hour
    dc.minute = minute
    dc.second = second
    if let date = calendar.date(from: dc) {
      self.init(timeInterval: 0, since: date)
    } else {
      fatalError("Date component values were invalid.")
    }
  }
  
  // MARK: - Static functions
  static func isNew(string: String) -> Bool {
    let isoDateFormatter = ISO8601DateFormatter()
    let trimmedIsoString = string.replacingOccurrences(of: "\\.\\d+", with: "", options: .regularExpression)
    isoDateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
    if let realDate = isoDateFormatter.date(from: trimmedIsoString) {
      let distance = Date().fullDistance(from: realDate, resultIn: .day) ?? 0
      return distance >= 0 && distance <= 2
    }
    return false
  }
  
  func fullDistance(from date: Date, resultIn component: Calendar.Component, calendar: Calendar = .current) -> Int? {
      calendar.dateComponents([component], from: date, to: self).value(for: component)
  }
  
  static func getTimeFromString(string: String) -> String { // •
    let isoDateFormatter = ISO8601DateFormatter()
    let trimmedIsoString = string.replacingOccurrences(of: "\\.\\d+", with: "", options: .regularExpression)
    isoDateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
    if let realDate = isoDateFormatter.date(from: trimmedIsoString) {
      return realDate.getTimeCR3()
    }
    return ""
  }
    static func getTimeFromStringCR4(string: String) -> String { // •
      let isoDateFormatter = ISO8601DateFormatter()
      let trimmedIsoString = string.replacingOccurrences(of: "\\.\\d+", with: "", options: .regularExpression)
      isoDateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
      if let realDate = isoDateFormatter.date(from: trimmedIsoString) {
        return realDate.getTimeCR4()
      }
      return ""
    }
  
  static func dateHoursFromString(string: String) -> Date {
    let isoDateFormatter = ISO8601DateFormatter()
    let trimmedIsoString = string.replacingOccurrences(of: "\\.\\d+", with: "", options: .regularExpression)
    isoDateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
    if let realDate = isoDateFormatter.date(from: trimmedIsoString) {
      return realDate
    }
    return Date()
  }
  
  static func getTimeCR2FromString(string: String) -> String { // •
    let isoDateFormatter = ISO8601DateFormatter()
    let trimmedIsoString = string.replacingOccurrences(of: "\\.\\d+", with: "", options: .regularExpression)
    isoDateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
    if let realDate = isoDateFormatter.date(from: trimmedIsoString) {
      return realDate.getTimeCreatedRule2()
    }
    
    return ""
  }
  
  static func getTimeCR4FromString(string: String) -> String {
    let isoDateFormatter = ISO8601DateFormatter()
    let trimmedIsoString = string.replacingOccurrences(of: "\\.\\d+", with: "", options: .regularExpression)
    isoDateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
    if let realDate = isoDateFormatter.date(from: trimmedIsoString) {
      return realDate.getTimeCreatedRule4()
    }
    return ""
  }
  
  static func getTimeNow() -> String{
    let date = Date()
    
    let iso8601DateFormatter = ISO8601DateFormatter()
    iso8601DateFormatter.formatOptions = [.withInternetDateTime]
    /*
    if #available(iOS 11.0, *) {
      iso8601DateFormatter.formatOptions = [.withInternetDateTime, .withFractionalSeconds]
    } else {
      iso8601DateFormatter.formatOptions = [.withInternetDateTime]
    }
    */
    let DateString = iso8601DateFormatter.string(from: date)
    return DateString
  }
  
  static func componentsBy(string: String, format: String, calendar: Calendar = Date.currentCalendar, timeZone: TimeZone = Date.currentTimeZone) -> DateComponents? {
    if let date = dateBy(string: string, format: format, calendar: calendar, timeZone: timeZone) {
      return date.components(calendar: calendar, timeZone: timeZone)
    }
    return nil
  }
  
  static func dateBy(string: String, format: String, calendar: Calendar = Date.currentCalendar, timeZone: TimeZone = Date.currentTimeZone) -> Date? {
    let formatter = DateFormatter()
    formatter.calendar = calendar
    formatter.timeZone = timeZone
    formatter.dateFormat = format
    return formatter.date(from: string)
  }
  
  static func dateAt(timeInterval: Int, calendar: Calendar = Date.currentCalendar, timeZone: TimeZone = Date.currentTimeZone) -> DateComponents {
    let date = Date(timeIntervalSince1970: TimeInterval(timeInterval / 1000))
    return date.components(calendar: calendar, timeZone: timeZone)
  }
  
  static func startOfMonth(date: Date, calendar: Calendar = Date.currentCalendar) -> Date? {
    return calendar.date(from: calendar.dateComponents([.year, .month], from: calendar.startOfDay(for: date)))
  }
  
  static func endOfMonth(date: Date, calendar: Calendar = Date.currentCalendar) -> Date? {
    if let startOfMonth = startOfMonth(date: date, calendar: calendar) {
      return calendar.date(byAdding: DateComponents(month: 1, day: -1), to: startOfMonth)
    }
    return nil
  }
  
  static func hourMinuteSecondFrom(secondValue: Int) -> (hour: Int, minute: Int, second: Int) {
    return (secondValue / 3600, (secondValue % 3600) / 60, (secondValue % 3600) % 60)
  }
  
  static func daysBetween(date start: Date, andDate end: Date) -> Int {
    return Calendar.current.dateComponents([.day], from: start, to: end).day!
  }
  
  // MARK: - Local functions
  func isToday(calendar: Calendar = Date.currentCalendar) -> Bool {
    return calendar.isDateInToday(self)
  }
  
  func isYesterday(calendar: Calendar = Date.currentCalendar) -> Bool {
    return calendar.isDateInYesterday(self)
  }
  
  func isTomorrow(calendar: Calendar = Date.currentCalendar) -> Bool {
    return calendar.isDateInTomorrow(self)
  }
  
  func isWeekend(calendar: Calendar = Date.currentCalendar) -> Bool {
    return calendar.isDateInWeekend(self)
  }
  
  func isSamedayWith(date: Date, calendar: Calendar = Date.currentCalendar) -> Bool {
    return calendar.isDate(self, inSameDayAs: date)
  }
  
  func components(calendar: Calendar = Date.currentCalendar, timeZone: TimeZone = Date.currentTimeZone) -> DateComponents {
    let dateComponents = DateComponents(calendar: calendar,
                                        timeZone: timeZone,
                                        era: calendar.component(.era, from: self),
                                        year: calendar.component(.year, from: self),
                                        month: calendar.component(.month, from: self),
                                        day: calendar.component(.day, from: self),
                                        hour: calendar.component(.hour, from: self),
                                        minute: calendar.component(.minute, from: self),
                                        second: calendar.component(.second, from: self),
                                        nanosecond: calendar.component(.nanosecond, from: self),
                                        weekday: calendar.component(.weekday, from: self),
                                        weekdayOrdinal: calendar.component(.weekdayOrdinal, from: self),
                                        quarter: calendar.component(.quarter, from: self),
                                        weekOfMonth: calendar.component(.weekOfMonth, from: self),
                                        weekOfYear: calendar.component(.weekOfYear, from: self),
                                        yearForWeekOfYear: calendar.component(.yearForWeekOfYear, from: self))
    return dateComponents
  }
  
  func add(day: Int? = nil, month: Int? = nil, year: Int? = nil, hour: Int? = nil, minute: Int? = nil, second: Int? = nil, calendar: Calendar = Date.currentCalendar) -> Date? {
    var dateComponent = DateComponents()
    if let year = year { dateComponent.year = year }
    if let month = month { dateComponent.month = month }
    if let day = day { dateComponent.day = day }
    if let hour = hour { dateComponent.hour = hour }
    if let minute = minute { dateComponent.minute = minute }
    if let second = second { dateComponent.second = second }
    return calendar.date(byAdding: dateComponent, to: self)
  }
  
  func stringBy(format: String, calendar: Calendar = Date.currentCalendar, timeZone: TimeZone = Date.currentTimeZone) -> String {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = format
    dateFormatter.calendar = calendar
    dateFormatter.timeZone = timeZone
    return dateFormatter.string(from: self)
  }
  
  func years(from date: Date, calendar: Calendar = Date.currentCalendar) -> Int {
    return calendar.dateComponents([.year], from: date, to: self).year ?? 0
  }
  
  func months(from date: Date, calendar: Calendar = Date.currentCalendar) -> Int {
    return calendar.dateComponents([.month], from: date, to: self).month ?? 0
  }
  
  func weeks(from date: Date, calendar: Calendar = Date.currentCalendar) -> Int {
    return calendar.dateComponents([.weekOfMonth], from: date, to: self).weekOfMonth ?? 0
  }
  
  func days(from date: Date, calendar: Calendar = Date.currentCalendar) -> Int {
    return calendar.dateComponents([.day], from: date, to: self).day ?? 0
  }
  
  func hours(from date: Date, calendar: Calendar = Date.currentCalendar) -> Int {
    return calendar.dateComponents([.hour], from: date, to: self).hour ?? 0
  }
  
  func minutes(from date: Date, calendar: Calendar = Date.currentCalendar) -> Int {
    return calendar.dateComponents([.minute], from: date, to: self).minute ?? 0
  }
  
  func seconds(from date: Date, calendar: Calendar = Date.currentCalendar) -> Int {
    return calendar.dateComponents([.second], from: date, to: self).second ?? 0
  }
  
  func set(year: Int, month: Int, day: Int, calendar: Calendar = Date.currentCalendar, timeZone: TimeZone = Date.currentTimeZone) -> Date? {
    return self.set(year: year, calendar: calendar, timeZone: timeZone)?
      .set(month: month, calendar: calendar, timeZone: timeZone)?
      .set(day: day, calendar: calendar, timeZone: timeZone)
  }
  
  func set(hour: Int, minute: Int, second: Int, calendar: Calendar = Date.currentCalendar, timeZone: TimeZone = Date.currentTimeZone) -> Date? {
    return self.set(hour: hour, calendar: calendar, timeZone: timeZone)?
      .set(minute: minute, calendar: calendar, timeZone: timeZone)?
      .set(second: second, calendar: calendar, timeZone: timeZone)
  }
  
  func set(year: Int, calendar: Calendar = Date.currentCalendar, timeZone: TimeZone = Date.currentTimeZone) -> Date? {
    var components = self.components(calendar: calendar, timeZone: timeZone)
    components.year = year
    return calendar.date(from: components)
  }
  
  func set(month: Int, calendar: Calendar = Date.currentCalendar, timeZone: TimeZone = Date.currentTimeZone) -> Date? {
    var components = self.components(calendar: calendar, timeZone: timeZone)
    components.month = month
    return calendar.date(from: components)
  }
  
  func set(day: Int, calendar: Calendar = Date.currentCalendar, timeZone: TimeZone = Date.currentTimeZone) -> Date? {
    var components = self.components(calendar: calendar, timeZone: timeZone)
    components.day = day
    return calendar.date(from: components)
  }
  
  func set(hour: Int, calendar: Calendar = Date.currentCalendar, timeZone: TimeZone = Date.currentTimeZone) -> Date? {
    var components = self.components(calendar: calendar, timeZone: timeZone)
    components.hour = hour
    return calendar.date(from: components)
  }
  
  func set(minute: Int, calendar: Calendar = Date.currentCalendar, timeZone: TimeZone = Date.currentTimeZone) -> Date? {
    var components = self.components(calendar: calendar, timeZone: timeZone)
    components.minute = minute
    return calendar.date(from: components)
  }
  
  func set(second: Int, calendar: Calendar = Date.currentCalendar, timeZone: TimeZone = Date.currentTimeZone) -> Date? {
    var components = self.components(calendar: calendar, timeZone: timeZone)
    components.second = second
    return calendar.date(from: components)
  }
  
}


extension Date {
  func currentHour() -> Int {
    let date = self
    let calendar = Calendar.current
    let comp = calendar.dateComponents([.hour], from: date)
    let hour = comp.hour
    return hour ?? 0
  }
  
  func currentMiliSecond() -> TimeInterval {
    return self.timeIntervalSince1970*1000
  }
  
  func diffTime(fromTime: TimeInterval) -> Int64 {
    let tmpFromTime = fromTime
    if tmpFromTime < 1000 {
      return 0
    }
    let startDate = Date.init(timeIntervalSince1970: tmpFromTime/1000)
    let toDate = self
    let calendar = Calendar.current
    let dateComponents = calendar.dateComponents([Calendar.Component.second], from: startDate, to: toDate)
    let second:Int64 = Int64(dateComponents.second ?? 0)
    return second*1000
  }
  
  func diffTime(fromDate: Date) -> Int {
    let toDate = self
    let calendar = Calendar.current
    let dateComponents = calendar.dateComponents([Calendar.Component.second], from: fromDate, to: toDate)
    let second = dateComponents.second ?? 0
    return second
  }
}

extension Date {
  public func setTime(hour: Int, min: Int, sec: Int, timeZoneAbbrev: String = "UTC") -> Date? {
    let x: Set<Calendar.Component> = [.year, .month, .day, .hour, .minute, .second]
    let cal = Calendar.current
    var components = cal.dateComponents(x, from: self)
    
    components.timeZone = TimeZone(abbreviation: timeZoneAbbrev)
    components.hour = hour
    components.minute = min
    components.second = sec
    
    return cal.date(from: components)
  }
  
  init(milliseconds:Double) {
    self = Date(timeIntervalSince1970: TimeInterval(milliseconds) / 1000)
  }
  
  func toMillis() -> Int64! {
    return Int64(self.timeIntervalSince1970 * 1000)
  }
}

extension Date {
  static func dateFromString(_ string: String) -> Date {
    let formatter = DateFormatter()
    formatter.dateFormat = "dd/MM/yyyy"
    if let date = formatter.date(from: string) {
      return date
    }
    return Date()
  }
  
  static func minDate() -> Date {
    return Calendar.current.date(byAdding: .year, value: -100, to: Date()) ?? Date()
  }
  
  static func maxDate() -> Date {
    return Calendar.current.date(byAdding: .year, value: -6, to: Date()) ?? Date()
  }
    func toISOString() -> String {
       let isoDateFormatter = ISO8601DateFormatter()
       isoDateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
       return isoDateFormatter.string(from: self)
    }
    func toNewsDateString() -> String { // •
      let isoDateFormatter = DateFormatter()
           isoDateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
           isoDateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
        
      return isoDateFormatter.string(from: self)
    }
}
