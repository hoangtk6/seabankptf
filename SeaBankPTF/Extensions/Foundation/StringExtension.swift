//
//  StringExtension.swift
//  CountriesList
//
//  Created by HoangVu on 9/30/17.
//  Copyright © 2017 1Life2Live. All rights reserved.
//

import UIKit

extension String {
  var trimWhiteSpacesAndNewLines: String {
    return self.trimmingCharacters(in: .whitespacesAndNewlines)
  }
  
  var length: Int {
    get {
      return self.count
    }
  }
  
  static func localize(_ key: String) -> String {
    return Bundle.main.localizedString(forKey: key, value: nil, table: nil)
  }
  
  func trim() -> String
  {
    return self.trimmingCharacters(in: NSCharacterSet.whitespaces)
  }
  
  func isValidEmail() -> Bool {
    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,50}"
    
    let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
    return emailTest.evaluate(with: self)
  }
  
  func isValidPhone() -> Bool {
    //        let regEx = "\\d{8}$"//"^[+0-9]{0,1}+[0-9]"
    //        let regEx = "^[0-9+]+-[0-9]*$"
    let regEx = "\\++[0-9 -]*"   // for input must start at "+" and allow number, '-' and ' '
    return regCheck(regEx: regEx)
  }
  
  func isNumberic() -> Bool {
    let regEx = "^[0-9,.]*$" //"^[0-9]*$"
    return regCheck(regEx: regEx)
  }
  
  func isValidPassword() -> Bool {
    //let regEx = "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[~!@#$%^&*()_+|}{:\"?><.]).{8,}$"
    let regEx = "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{8,}$"
    return regCheck(regEx: regEx)
  }
  
  func isValidUserName() -> Bool {
    let regEx = "^[A-Za-z][a-zA-Z0-9-._]{5,16}$"
    return regCheck(regEx: regEx)
  }
  
  func isValidFuntoonId() -> Bool {
    let regEx = "^[a-zA-Z0-9._]{1,20}$"
    return regCheck(regEx: regEx)
  }
  private func regCheck(regEx: String) -> Bool {
    let regTest = NSPredicate(format:"SELF MATCHES %@", regEx)
    return regTest.evaluate(with: self)
  }
  
  func getPathExtension() -> String {
    return (self as NSString).pathExtension
  }
  
  func getFileName() -> String {
    return (self as NSString).lastPathComponent
  }
  
  func toDouble() -> Double? {
    return NumberFormatter().number(from: self)?.doubleValue
  }
  
  func toInt() -> Int? {
    return NumberFormatter().number(from: self)?.intValue
  }
  
  func getTextHeight(width: CGFloat, font: UIFont) -> CGFloat {
    let constraintRect = CGSize(width: width, height: CGFloat.greatestFiniteMagnitude)
    let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [kCTFontAttributeName as NSAttributedString.Key: font], context: nil)
    return boundingBox.height
  }
  
  func getTextHeight(width: CGFloat, font: UIFont, attribute: [NSAttributedString.Key : Any]) -> CGFloat {
    let constraintRect = CGSize(width: width, height: CGFloat.greatestFiniteMagnitude)
    let boundingBox = self.boundingRect(with: constraintRect, options: [.usesLineFragmentOrigin, .usesFontLeading], attributes: attribute, context: nil)
    return boundingBox.height
  }
  
  static func getSizeFile(size:CGFloat)->String{
    let sizeMb = CGFloat(size)
    if sizeMb > 1024*1024*1024{
      let sizeGb = CGFloat(sizeMb)/(1024*1024*1024)
      let sizeRound = Double(sizeGb).roundToPlaces(places: 1)
      return "\(sizeRound) Gb"
    }else if sizeMb > 1024*1024{
      let sizeGb = CGFloat(sizeMb)/(1024*1024)
      let sizeRound = Double(sizeGb).roundToPlaces(places: 1)
      return "\(sizeRound) Mb"
    }else if sizeMb > 1024{
      let sizeGb = CGFloat(sizeMb)/1024
      let sizeRound = Double(sizeGb).roundToPlaces(places: 1)
      return "\(sizeRound) Kb"
    }else{
      let sizeGb = CGFloat(sizeMb)
      let sizeRound = Double(sizeGb).roundToPlaces(places: 1)
      return "\(sizeRound) Bytes"
    }
  }
  
  //MARK:- Date
  static func convertToFormatedDate(string: String) -> String {
    //    let dateIsoFormatter = DateFormatter()
    //    dateIsoFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z" // Format ISO
    //    guard let date = dateIsoFormatter.date(from: string) else { return ""}
    //    let formatter = DateFormatter()
    //    formatter.dateFormat = "dd/MM/yyyy"
    //    return formatter.string(from: date)
    let isoDateFormatter = ISO8601DateFormatter()
    let trimmedIsoString = string.replacingOccurrences(of: "\\.\\d+", with: "", options: .regularExpression)
    isoDateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
    if let realDate = isoDateFormatter.date(from: trimmedIsoString) {
      let formatter = DateFormatter()
      formatter.dateFormat = "dd/MM/yyyy"
      return formatter.string(from: realDate)
    }
    
    return ""
  }
  
  static func convertToFormatedDateHour(string: String, format:String) -> String { // •
    let isoDateFormatter = ISO8601DateFormatter()
    let trimmedIsoString = string.replacingOccurrences(of: "\\.\\d+", with: "", options: .regularExpression)
    isoDateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
    if let realDate = isoDateFormatter.date(from: trimmedIsoString) {
      let formatter = DateFormatter()
      formatter.dateFormat = format
      return formatter.string(from: realDate)
    }
    
    return ""
  }
  
  static func getDate(time:String)-> Date?{
    let isoDateFormatter = ISO8601DateFormatter()
    let trimmedIsoString = time.replacingOccurrences(of: "\\.\\d+", with: "", options: .regularExpression)
    isoDateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
    if let targedDate = isoDateFormatter.date(from: trimmedIsoString) {
      return targedDate
    }
    return nil
  }
  
  static func stringFromDate(_ date: Date) -> String {
    let formatter = DateFormatter()
    formatter.dateFormat = "dd/MM/yyyy"
    return formatter.string(from: date)
  }
  
  static func addSeemore(lines: [String], font: UIFont, normalTextColor: UIColor, seeMoreColor: UIColor, limitLine: Int) -> NSAttributedString {
    let attributes = NSMutableAttributedString()
    let limit = lines.count < limitLine ? lines.count : limitLine
    
    for index in 0..<limit {
      if (index == limit - 1) { // Last line
        var text = lines[index]
        text = text.components(separatedBy: .newlines).joined()
        let length = text.count
        var cutString: Substring = ""
        if (length > 14) {
          cutString = text.prefix(length - 14)
        } else {
          cutString = text.prefix(length)
        }
        
        let firstAttribute = NSAttributedString(string: String("\(cutString)... "), attributes: [NSAttributedString.Key.foregroundColor: normalTextColor])
        let seeMoreAttr = NSAttributedString(string: String.sSeeMore, attributes: [NSAttributedString.Key.foregroundColor: seeMoreColor])
        attributes.append(firstAttribute)
        attributes.append(seeMoreAttr)
      } else {
        let attribute = NSMutableAttributedString(string: lines[index], attributes: [NSAttributedString.Key.foregroundColor: normalTextColor])
        attributes.append(attribute)
      }
    }
    
    return attributes
  }
  
  static func stringLines(text: String, font: UIFont, width: CGFloat) -> [String] {
    let ctFont = CTFontCreateWithName(font.fontName as CFString, font.pointSize, nil)
    let attStr = NSMutableAttributedString(string: text)
    attStr.addAttribute(kCTFontAttributeName as NSAttributedString.Key, value: ctFont, range: NSRange(location: 0, length: attStr.length))
    let frameSetter = CTFramesetterCreateWithAttributedString(attStr as CFAttributedString)
    let path = CGMutablePath()
    path.addRect(CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude), transform: .identity)
    let frame = CTFramesetterCreateFrame(frameSetter, CFRangeMake(0, 0), path, nil)
    guard let lines = CTFrameGetLines(frame) as? [Any] else { return [] }
    return lines.map { line in
      let lineRef = line as! CTLine
      let lineRange: CFRange = CTLineGetStringRange(lineRef)
      let range = NSRange(location: lineRange.location, length: lineRange.length)
      return (text as NSString).substring(with: range)
    }
  }
}

extension RangeReplaceableCollection where Self: StringProtocol {
  mutating func replaceOccurrences<Target: StringProtocol, Replacement: StringProtocol>(of target: Target, with replacement: Replacement, options: String.CompareOptions = [], range searchRange: Range<String.Index>? = nil) {
    self = .init(replacingOccurrences(of: target, with: replacement, options: options, range: searchRange))
  }
}

extension Bundle {
  var releaseVersionNumber: String? {
    return infoDictionary?["CFBundleShortVersionString"] as? String
  }
  var buildVersionNumber: String? {
    return infoDictionary?["CFBundleVersion"] as? String
  }
}

extension Formatter {
  static let withDotSeparator: NumberFormatter = {
    let formatter = NumberFormatter()
    formatter.groupingSeparator = "."
    formatter.numberStyle = .decimal
    return formatter
  }()
}

extension BinaryInteger {
  var formattedWithDotSeparator: String {
    return Formatter.withDotSeparator.string(for: self) ?? ""
  }
}


extension String {
  func widthOfString(usingFont font: UIFont) -> CGFloat {
    let fontAttributes = [NSAttributedString.Key.font: font]
    let size = self.size(withAttributes: fontAttributes)
    return size.width
  }
  
  func heightOfString(usingFont font: UIFont) -> CGFloat {
    let fontAttributes = [NSAttributedString.Key.font: font]
    let size = self.size(withAttributes: fontAttributes)
    return size.height
  }
  
  func sizeOfString(usingFont font: UIFont) -> CGSize {
    let fontAttributes = [NSAttributedString.Key.font: font]
    return self.size(withAttributes: fontAttributes)
  }
  
  func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
    let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
    let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
    
    return ceil(boundingBox.height)
  }
  
  func heightWith(width: CGFloat, font: UIFont) -> CGFloat {
    if self.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 {
      return 0
    }
    let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
    let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [.font: font], context: nil)
    return ceil(boundingBox.height)
  }
  
  func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
    let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
    let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
    
    return ceil(boundingBox.width)
  }
}

extension String {
  subscript (bounds: CountableClosedRange<Int>) -> String {
    let start = index(startIndex, offsetBy: bounds.lowerBound)
    let end = index(startIndex, offsetBy: bounds.upperBound)
    return String(self[start...end])
  }
  
  subscript (bounds: CountableRange<Int>) -> String {
    let start = index(startIndex, offsetBy: bounds.lowerBound)
    let end = index(startIndex, offsetBy: bounds.upperBound)
    return String(self[start..<end])
  }
}



extension URL {
  func valueOf(_ queryParamaterName: String) -> String? {
    guard let url = URLComponents(string: self.absoluteString) else { return nil }
    return url.queryItems?.first(where: { $0.name == queryParamaterName })?.value
  }
}

extension String {
  var htmlToAttributedString: NSMutableAttributedString? {
    guard let data = data(using: .utf8) else { return nil }
    do {
      return try NSMutableAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
    } catch {
      return nil
    }
  }
  
  var htmlToString: String {
    return htmlToAttributedString?.string ?? ""
  }
  
}

extension String
{
  func encodeUrl() -> String?
  {
    return self.addingPercentEncoding( withAllowedCharacters: NSCharacterSet.urlQueryAllowed)
  }
  func decodeUrl() -> String?
  {
    return self.removingPercentEncoding
  }
  
  func encodeParam() ->String?{
    if !self.hasPrefix("?"){
      let params = self.components(separatedBy: "?")
      if params.count > 1{
        var link = params[0]
        if let param = params[1].encodeUrl(){
          link.append("?")
          link.append(param)
        }
        return link
      }
    }
    return self
  }
  
  func decodeParam() -> String?{
    if self.hasPrefix("?"){
      let params = self.components(separatedBy: "?")
      if params.count > 1{
        var link = params[0]
        if let param = params[1].decodeUrl(){
          link.append("?")
          link.append(param)
        }
        return link
      }
    }
    return self
  }
}

extension NSAttributedString {
  
  convenience init(htmlString html: String, font: UIFont? = nil, useDocumentFontSize: Bool = true, color: UIColor = .black) throws {
    let options: [NSAttributedString.DocumentReadingOptionKey : Any] = [
      .documentType: NSAttributedString.DocumentType.html,
      .characterEncoding: String.Encoding.utf8.rawValue
    ]
    
    let data = html.data(using: .utf8, allowLossyConversion: true)
    guard (data != nil), let fontFamily = font?.familyName, let attr = try? NSMutableAttributedString(data: data!, options: options, documentAttributes: nil) else {
      try self.init(data: data ?? Data(html.utf8), options: options, documentAttributes: nil)
      return
    }
    
    let fontSize: CGFloat? = useDocumentFontSize ? nil : font!.pointSize
    let range = NSRange(location: 0, length: attr.length)
    attr.enumerateAttribute(.font, in: range, options: .longestEffectiveRangeNotRequired) { attrib, range, _ in
      if let htmlFont = attrib as? UIFont {
        let traits = htmlFont.fontDescriptor.symbolicTraits
        var descrip = htmlFont.fontDescriptor.withFamily(fontFamily)
        
        if (traits.rawValue & UIFontDescriptor.SymbolicTraits.traitBold.rawValue) != 0 {
          descrip = descrip.withSymbolicTraits(.traitBold)!
        }
        
        if (traits.rawValue & UIFontDescriptor.SymbolicTraits.traitItalic.rawValue) != 0 {
          descrip = descrip.withSymbolicTraits(.traitItalic)!
        }
        //        let attrs = [NSAttributedString.Key.font : font, NSAttributedString.Key.foregroundColor: self.lblDesc.textColor]
        attr.addAttribute(.font, value: UIFont(descriptor: descrip, size: fontSize ?? htmlFont.pointSize), range: range)
      }
    }
    
    self.init(attributedString: attr)
  }
  
}
