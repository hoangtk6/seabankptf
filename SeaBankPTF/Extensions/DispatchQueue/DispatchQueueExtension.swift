//
//  DispatchQueueExtension.swift
//  CountriesList
//
//  Created by HoangVu on 9/30/17.
//  Copyright © 2017 1Life2Live. All rights reserved.
//

import Foundation

extension DispatchQueue {
  func safeAsync(_ block: @escaping ()->()) {
    if self === DispatchQueue.main && Thread.isMainThread {
      block()
    } else {
      async { block() }
    }
  }
}

extension DispatchQueue {
  private static func getCurrentThreadLabel() -> String? {
    let name = __dispatch_queue_get_label(nil)
    return String(cString: name, encoding: .utf8)
  }
  
  static func executeInMainThread(_ block: @escaping () -> Void) {
    let currentThreadName = self.getCurrentThreadLabel()
    guard currentThreadName == self.main.label else {
      self.main.async { block() }
      return
    }
    block()
  }
}
