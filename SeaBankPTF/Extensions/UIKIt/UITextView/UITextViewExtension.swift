//
//  UITextViewExtension.swift
//  CountriesList
//
//  Created by HoangVu on 9/30/17.
//  Copyright © 2017 1Life2Live. All rights reserved.
//

import UIKit

extension UITextView {
    func formatBorder() {
        layer.borderColor = UIColor.black.cgColor
        layer.borderWidth = 1
    }
}

extension UISearchBar {
  func changeColorBackground(_ color: UIColor) {
    for view in self.subviews {
      for subView in view.subviews {
        if (subView.isKind(of: UITextField.self)) {
          let textField: UITextField = subView as! UITextField
          textField.backgroundColor = color
        }
      }
    }
  }
}
