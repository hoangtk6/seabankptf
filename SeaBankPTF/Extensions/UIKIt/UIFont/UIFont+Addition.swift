//
//  UIFont+Addition.swift
//  Comic
//
//  Created by HoangVu on 5/5/18.
//  Copyright © 2018 HoangVu. All rights reserved.
//

import Foundation
import UIKit


extension UIFont {
  // Custom fonts go here
  func fontCustom(name: String, size:CGFloat) -> UIFont {
    return UIFont(name: name, size: size)!
  }
  
  static func fontButtomBar() -> UIFont {
    return RobotoFont.fontWithType(.regular, size: 15)
  }
  
  static func fontMenuHighLight() -> UIFont {
    return RobotoFont.fontWithType(.medium, size: 20)
  }
  
  static func fontMenuNormal() -> UIFont {
    return RobotoFont.fontWithType(.regular, size: 17)
  }
  
  static func fontNavigationBarBag() -> UIFont {
    return RobotoFont.fontWithType(.medium, size: 12)
  }
  
  // Dùng bold hay italic thì thêm 2 phương thức bold và italic vào
  func withTraits(traits:UIFontDescriptor.SymbolicTraits) -> UIFont {
    let descriptor = fontDescriptor.withSymbolicTraits(traits)
    return UIFont(descriptor: descriptor!, size: 0) //size 0 means keep the size as it is
  }
  
  func bold() -> UIFont {
    return withTraits(traits: .traitBold)
  }
  
  func italic() -> UIFont {
    return withTraits(traits: .traitItalic)
  }

}
