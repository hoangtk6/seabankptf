//
//  UICollectionView+Paging.swift
//  Setel
//
//  Created by Evgeniy Gurtovoy on 9/28/18.
//  Copyright © 2018 Setel. All rights reserved.
//

import Foundation
import UIKit

extension UICollectionView {
  func goToNextPage(animated: Bool = true) {
    guard let indexPath = indexPathsForVisibleItems.first else {
      return
    }
    
    let updatedPath = IndexPath(item: indexPath.row + 1, section: 0)
    scrollToItem(at: updatedPath, at: .centeredHorizontally, animated: animated)
  }
  
  func goToPreviousPage(animated: Bool = true) {
    guard let indexPath = indexPathsForVisibleItems.first else {
      return
    }
    
    let updatedPath = IndexPath(item: indexPath.row - 1, section: 0)
    scrollToItem(at: updatedPath, at: .centeredHorizontally, animated: animated)
  }
}
