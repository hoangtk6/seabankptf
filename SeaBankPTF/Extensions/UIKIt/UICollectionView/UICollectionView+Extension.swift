//
//  UICollectionView+Extension.swift
//  Comic
//
//  Created by Hoang Vu on 3/18/21.
//  Copyright © 2021 TechLab Corp. All rights reserved.
//

import Foundation
import UIKit

 extension UICollectionView {
  func scrollToHeaderSection(indexSection:Int, tolerance:CGFloat){
    if let attributes = self.layoutAttributesForSupplementaryElement(ofKind: UICollectionView.elementKindSectionHeader, at: IndexPath(item: 0, section: indexSection)) {
      var offsetY = attributes.frame.origin.y - self.contentInset.top
      if #available(iOS 11.0, *) {
        offsetY -= self.safeAreaInsets.top
      }
      self.setContentOffset(CGPoint(x: 0, y: offsetY + tolerance), animated: true) // or animated: false
    }
  }
}

extension UICollectionView {
  func register<T: UICollectionViewCell>(_ aClass: T.Type) {
    let name = String(describing: aClass)
    let bundle = Bundle.main
    if bundle.path(forResource: name, ofType: "nib") != nil {
      let nib = UINib(nibName: name, bundle: bundle)
      register(nib, forCellWithReuseIdentifier: name)
    } else {
      register(aClass, forCellWithReuseIdentifier: name)
    }
  }
  
  func dequeue<T: UICollectionViewCell>(_ aClass: T.Type, indexPath: IndexPath) -> T {
    let name = String(describing: aClass)
    guard let cell = dequeueReusableCell(withReuseIdentifier: name, for: indexPath) as? T else {
      fatalError("\(name) is not registed")
    }
    return cell
  }
}
