//
//  UIActivityIndicatorView+Extension.swift
//  Comic
//
//  Created by Hoang Vu on 10/15/20.
//  Copyright © 2020 Techlab Corp. All rights reserved.
//

import Foundation
import UIKit

extension UIActivityIndicatorView {
    func scale(factor: CGFloat) {
        guard factor > 0.0 else { return }

        transform = CGAffineTransform(scaleX: factor, y: factor)
    }
}
