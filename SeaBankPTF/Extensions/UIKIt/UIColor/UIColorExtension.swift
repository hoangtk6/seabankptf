//
//  UIColorExtension.swift
//  CountriesList
//
//  Created by HoangVu on 9/30/17.
//  Copyright © 2017 1Life2Live. All rights reserved.
//

import UIKit

extension UIColor {
  
  convenience init(hexString: String, alpha: CGFloat = 1) {
    let scanner = Scanner(string: hexString)
    scanner.scanLocation = 0
    
    var rgbValue: UInt64 = 0
    
    scanner.scanHexInt64(&rgbValue)
    
    let r = (rgbValue & 0xff0000) >> 16
    let g = (rgbValue & 0xff00) >> 8
    let b = rgbValue & 0xff
    
    self.init(
      red: CGFloat(r) / 0xff,
      green: CGFloat(g) / 0xff,
      blue: CGFloat(b) / 0xff, alpha: alpha
    )
  }
  
  //    class func primary() -> UIColor {
  //        return UIColor(hexString: "0x4CAF50")
  //    }
  
  class func secondary() -> UIColor {
    return UIColor.darkGray
  }

  static func titleTextColor() -> UIColor {
    return UIColor(hexString: "525252")
  }
  
  static func headerTitleTextColor() -> UIColor {
    return UIColor(hexString: "424345")
  }
  
  static func errorColor() -> UIColor {
    return UIColor(hexString: "D72228")
  }

  static func descTextColor() -> UIColor {
    return UIColor(hexString: "BEBEBE")
  }
  
  static func lightTextColor() -> UIColor {
    return UIColor(hexString: "EEEEEE")
  }
  
  static func grayBackgroundColor() -> UIColor {
    return UIColor(hexString: "E5E5E5")
  }
  
  static func darkNaviColor() -> UIColor {
    return UIColor(hexString: "1B1C23")
  }
  
  static func stateLikedToon() -> UIColor {
    return UIColor(hexString: "00ADE9")
  }

  static func comicColorBase() -> UIColor {
    return UIColor(hexString: "007AFF")
  }
  
  static func unseenBlue() -> UIColor {
    return UIColor(hexString: "EAFCFF")
  }
  
  static func comicBlueColor() -> UIColor {
    return UIColor(hexString: "3499FF")
  }
  
  static func colorWeeboo() -> UIColor {
    return UIColor(hexString: "1FCF84")
  }
  
  static func colorFunToon() -> UIColor {
    return UIColor(hexString: "FF782D")
  }
  
  static func colorBorderLightGray() -> UIColor {
    return UIColor(hexString: "F2F2F2")
  }
  
  static func colorActionGenres() -> UIColor {
    return UIColor(hexString: "4DC8F0")
  }
  
  static func separatorColorLine() -> UIColor {
    return UIColor(hexString: "efeff4")
  }
  
  static func comicNavigationColor() -> UIColor {
    return UIColor(hexString: "333333")
  }
  
  static func backgroundGreen() -> UIColor {
    return UIColor(hexString: "F0FFF8")
  }
  
  static func backgroundDarkCommunity() -> UIColor {
    return UIColor(hexString: "414668")
  }
  
  static func weeBooDarkText() -> UIColor {
    return UIColor(hexString: "25294B")
  }
  
  static func darkColorNews() -> UIColor {
    return UIColor(hexString: "707070")
  }
  
  static func backgroundContentLight() -> UIColor {
    return UIColor(hexString: "F5F5F5")
  }
  
  static func comicLineColor() -> UIColor {
    return UIColor(hexString: "EFEFF4")
  }
  
  static func comicbgTagColor() -> UIColor {
    return UIColor(hexString: "FAFAFA")
  }
  
  static func colorOrange() -> UIColor {
    return UIColor(hexString: "FF854A")
  }
  
  static func novelbgComment() -> UIColor {
    return UIColor(hexString: "FAFAFA")
  }
  
  static func comicbgTextField() -> UIColor {
    return UIColor(hexString: "EFEFF3")
  }
  
  static func comicbgButtonGray() -> UIColor {
    return UIColor(hexString: "16161F")
  }
  
  static func buttonDisableColor() -> UIColor {
    return UIColor(hexString: "DADADA")
  }


  static func menuColorToon() -> UIColor {
    return UIColor(hexString: "14151A")
  }
  
  static func bgColorToon() -> UIColor {
    return UIColor(hexString: "111217")
  }
  
  static func bgColorViewLike() -> UIColor {
    return UIColor.init(hexString: "262831")
  }
  
  static func bgBoxCommentLight() -> UIColor {
    return UIColor.init(hexString: "F5F5F5")
  }
  
  // MARK: - Color text
  static func descComicGrayColor() -> UIColor {
    return UIColor(hexString: "8E8E93")
  }
  
  static func menuComicRedColor() -> UIColor {
    return UIColor(hexString: "FF0000")
  }
  
  static func cateComicColor() -> UIColor {
    return UIColor(hexString: "E5E5EA")
  }
  
  static func colorPreview() -> UIColor {
    return UIColor(hexString: "121212")
  }
  
  // MARK: - Background color
  static func segmentComicBgColor() -> UIColor {
    return UIColor.init(red: 248, green: 248, blue: 248, a: 0.92)
  }
  
  static func menuComicBgColor() -> UIColor {
    return UIColor.init(red: 0, green: 0, blue: 0, a: 0.6)
  }
  
  static func headerComicBgColor() -> UIColor {
    return UIColor.init(red: 0, green: 0, blue: 0, a: 0.4)
  }
  
  static func facebookColor() -> UIColor {
    return UIColor(hexString: "335795")
  }
  static func boxVerifyColor() -> UIColor {
    return UIColor(hexString: "F4F4F4")
  }
  
  static func searchbarBackgroundColor() -> UIColor {
    return UIColor.init(red: 142, green: 142, blue: 147, a: 0.12)
  }
  
}

extension UIColor {
  convenience init(red: Int, green: Int, blue: Int, a: CGFloat = 1.0) {
    self.init(
      red: CGFloat(red) / 255.0,
      green: CGFloat(green) / 255.0,
      blue: CGFloat(blue) / 255.0,
      alpha: a
    )
  }
  
  convenience init(rgb: Int, a: CGFloat = 1.0) {
    self.init(
      red: (rgb >> 16) & 0xFF,
      green: (rgb >> 8) & 0xFF,
      blue: rgb & 0xFF,
      a: a
    )
  }
}
