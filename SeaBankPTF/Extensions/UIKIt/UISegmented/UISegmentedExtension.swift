//
//  UISegmentedExtension.swift
//  Comic
//
//  Created by Hoang Vu on 2/6/20.
//  Copyright © 2020 Techlab Corp. All rights reserved.
//

import Foundation
import UIKit

extension UISegmentedControl {
  
  func setFontSize(normalColor:UIColor, selectedColor:UIColor, normalFont:UIFont, selectedFont:UIFont) {

    let normalTextAttributes: [NSAttributedString.Key : AnyObject] = [
      NSAttributedString.Key.foregroundColor : normalColor,
      NSAttributedString.Key.font : normalFont
    ]
    
    let selectedTextAttributes: [NSAttributedString.Key : AnyObject] = [
      NSAttributedString.Key.foregroundColor : selectedColor,
      NSAttributedString.Key.font : selectedFont
    ]
    
    self.setTitleTextAttributes(normalTextAttributes, for: .normal)
    self.setTitleTextAttributes(selectedTextAttributes, for: .selected)
  }
}
