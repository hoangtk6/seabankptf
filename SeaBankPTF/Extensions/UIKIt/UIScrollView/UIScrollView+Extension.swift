//
//  UIScrollView+Extension.swift
//  Comic
//
//  Created by Hoang Vu on 5/25/20.
//  Copyright © 2020 Techlab Corp. All rights reserved.
//

import UIKit

extension UIScrollView {
  var currentPage:Int{
    return Int((self.contentOffset.y+(0.5*self.frame.size.height))/self.frame.height)+1
  }
  
  func scrollToBottom() {
    let bottomOffset = CGPoint(x: 0, y: contentSize.height - bounds.size.height + contentInset.bottom)
    setContentOffset(bottomOffset, animated: true)
  }
  
  func scrollToBottom(animated: Bool) {
    self.layoutIfNeeded()
    let y = contentSize.height - bounds.size.height + contentInset.bottom
    let bottomOffset = CGPoint(x: 0,
                               y: max(0, y))
    setContentOffset(bottomOffset, animated: animated)
  }
}
