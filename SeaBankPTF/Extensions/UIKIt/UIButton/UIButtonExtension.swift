//
//  UIButtonExtension.swift
//  CountriesList
//
//  Created by HoangVu on 9/30/17.
//  Copyright © 2017 1Life2Live. All rights reserved.
//

import UIKit

extension UIButton {
  func formatClear() {
    layer.borderColor = UIColor.clear.cgColor
    layer.borderWidth = 0
    layer.cornerRadius = 0
  }
  
  func formatHighlighted() {
    layer.borderColor = UIColor.white.cgColor
    layer.borderWidth = 1
    layer.cornerRadius = 5
  }
  
  func formatButtonComicWithTitle(title:String, fRadius:CGFloat) {
    self.setTitle(title, for: .normal)
    //    self.applyGradient(cgColors:[UIColor(hexString:"1AC9FC").cgColor, UIColor(hexString:"1D73F1").cgColor], startPoint: CGPoint(x: 0, y: 1), endPoint: CGPoint(x:1, y:1))
    self.applyGradient(withColours: [UIColor(hexString:"1AC9FC"), UIColor(hexString:"1D73F1")], gradientOrientation: .horizontal)
    self.cornerRadius = fRadius;
    self.clipsToBounds = true
  }
  
  func formatButtonComicOrangeWithTitle(title:String, fRadius:CGFloat) {
    self.setTitle(title, for: .normal)
    //     self.applyGradient(cgColors:[UIColor(hexString:"FF9500").cgColor, UIColor(hexString:"FF5E3A").cgColor], startPoint: CGPoint(x: 0, y: 1), endPoint: CGPoint(x:1, y:1))
    self.applyGradient(withColours: [UIColor(hexString:"FF9500"), UIColor(hexString:"FF5E3A")], gradientOrientation: .horizontal)
    self.cornerRadius = fRadius;
    self.clipsToBounds = true
  }
  
  func centerTextAndImage(spacing: CGFloat) {
    let insetAmount = spacing / 2
    let writingDirection = UIApplication.shared.userInterfaceLayoutDirection
    let factor: CGFloat = writingDirection == .leftToRight ? 1 : -1
    
    self.imageEdgeInsets = UIEdgeInsets(top: 0, left: -insetAmount*factor, bottom: 0, right: insetAmount*factor)
    self.titleEdgeInsets = UIEdgeInsets(top: 0, left: insetAmount*factor, bottom: 0, right: -insetAmount*factor)
    self.contentEdgeInsets = UIEdgeInsets(top: 0, left: insetAmount, bottom: 0, right: insetAmount)
  }
  
  func leftImage(image: UIImage, padding: CGFloat, renderMode: UIImage.RenderingMode) {
    self.setImage(image.withRenderingMode(renderMode), for: .normal)
    contentHorizontalAlignment = .left
    let availableSpace = bounds.inset(by: contentEdgeInsets)
    let availableWidth = availableSpace.width - imageEdgeInsets.right - (imageView?.frame.width ?? 0) - (titleLabel?.frame.width ?? 0)
    titleEdgeInsets = UIEdgeInsets(top: 0, left: availableWidth / 2, bottom: 0, right: 0)
    imageEdgeInsets = UIEdgeInsets(top: 0, left: padding, bottom: 0, right: 0)
  }
  
  func rightImage(image: UIImage, padding: CGFloat, renderMode: UIImage.RenderingMode){
    self.setImage(image.withRenderingMode(renderMode), for: .normal)
    semanticContentAttribute = .forceRightToLeft
    contentHorizontalAlignment = .right
    let availableSpace = bounds.inset(by: contentEdgeInsets)
    let availableWidth = availableSpace.width - imageEdgeInsets.left - (imageView?.frame.width ?? 0) - (titleLabel?.frame.width ?? 0)
    titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: availableWidth / 2)
    imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: padding)
  }
  
  func leftImage(image: UIImage, renderMode: UIImage.RenderingMode) {
    self.setImage(image.withRenderingMode(renderMode), for: .normal)
    self.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: image.size.width / 2)
    self.contentHorizontalAlignment = .left
    self.imageView?.contentMode = .scaleAspectFit
  }
  
  func rightImage(image: UIImage, renderMode: UIImage.RenderingMode){
    self.setImage(image.withRenderingMode(renderMode), for: .normal)
    self.imageEdgeInsets = UIEdgeInsets(top: 0, left:image.size.width / 2, bottom: 0, right: 0)
    self.contentHorizontalAlignment = .right
    self.imageView?.contentMode = .scaleAspectFit
  }
  
  
  func underline() {
    guard let text = self.titleLabel?.text else { return }
    let attributedString = NSMutableAttributedString(string: text)
    //NSAttributedStringKey.foregroundColor : UIColor.blue
    attributedString.addAttribute(NSAttributedString.Key.underlineColor, value: self.titleColor(for: .normal)!, range: NSRange(location: 0, length: text.count))
    attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: self.titleColor(for: .normal)!, range: NSRange(location: 0, length: text.count))
    attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: NSRange(location: 0, length: text.count))
    self.setAttributedTitle(attributedString, for: .normal)
  }
  
  func loadingIndicator(_ show: Bool) {
    let tagBackgroundView = 111112
    let tagIndicator = 111111
    if show {
      self.isEnabled = false
      
      let v = UIView(frame: self.bounds)
      v.tag = tagBackgroundView
      v.backgroundColor = UIColor.comicbgTagColor()
      self.addSubview(v)
      
      let indicator = UIActivityIndicatorView()
      let buttonHeight = self.bounds.size.height
      let buttonWidth = self.bounds.size.width
      indicator.center = CGPoint(x: buttonWidth/2, y: buttonHeight/2)
      indicator.color = .black
      indicator.tag = tagIndicator
      self.addSubview(indicator)
      indicator.startAnimating()
    } else {
      self.isEnabled = true
      
      if let indicator = self.viewWithTag(tagIndicator) as? UIActivityIndicatorView, let backgroundView = self.viewWithTag(tagBackgroundView) {
        indicator.stopAnimating()
        indicator.removeFromSuperview()
        backgroundView.removeFromSuperview()
      }
    }
  }
  
  func setBackgroundColor(_ color: UIColor, forState: UIControl.State) {
    self.clipsToBounds = true  // add this to maintain corner radius
    UIGraphicsBeginImageContext(CGSize(width: 1, height: 1))
    if let context = UIGraphicsGetCurrentContext() {
      context.setFillColor(color.cgColor)
      context.fill(CGRect(x: 0, y: 0, width: 1, height: 1))
      let colorImage = UIGraphicsGetImageFromCurrentImageContext()
      UIGraphicsEndImageContext()
      self.setBackgroundImage(colorImage, for: forState)
    }
  }
}
