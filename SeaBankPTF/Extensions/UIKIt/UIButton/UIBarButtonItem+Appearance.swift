import UIKit

public extension UIBarButtonItem {
  
  class func ptfBackButton(target: Any?, action: Selector?) -> UIBarButtonItem {
    let backIcon = UIImage(named: "ic-back-arrow")
    let item = UIBarButtonItem(image: backIcon, style: .done, target: target, action: action)
    item.imageInsets = UIEdgeInsets(top: 3, left: -6, bottom: 0, right: 0)
    return item
  }
  
  class func ptfCrossButton(target: Any?, action: Selector?) -> UIBarButtonItem {
    let backIcon = UIImage(named: "ic-back-cross")
    return UIBarButtonItem(image: backIcon, style: .done, target: target, action: action)
  }
  
  class func ptfReloadButton(target: Any?, action: Selector?) -> UIBarButtonItem {
    let backIcon = UIImage(named: "ic-reload")
    return UIBarButtonItem(image: backIcon, style: .done, target: target, action: action)
  }
  
  class func pdfTextButton(title: String?, target: Any?, action: Selector?) -> UIBarButtonItem {
    let paragraphStyle = NSMutableParagraphStyle()
    paragraphStyle.minimumLineHeight = 14
    
    let attributes = [
      NSAttributedString.Key.font: RobotoFont.fontWithType(.regular, size: 12),
      NSAttributedString.Key.paragraphStyle: paragraphStyle,
      NSAttributedString.Key.kern: 1 as Any
    ]
    let item = UIBarButtonItem(title: title?.uppercased(), style: .done, target: target, action: action)
    item.tintColor = UIColor.darkGray
    item.setTitleTextAttributes(attributes, for: .normal)
    item.setTitleTextAttributes(attributes, for: .selected)
    return item
  }
}
