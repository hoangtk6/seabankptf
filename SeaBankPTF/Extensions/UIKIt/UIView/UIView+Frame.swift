//
//  UIView+Frame.swift
//  MochaSwift
//
//  Created by HoangVu on 4/4/19.
//  Copyright © 2019 HoangVu. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
  class func mc_viewFromNib<T>(_ aClass: T.Type) -> T {
    let name = String(describing: aClass)
    if Bundle.main.path(forResource: name, ofType: "nib") != nil {
      return UINib(nibName: name, bundle:nil).instantiate(withOwner: nil, options: nil)[0] as! T
    } else {
      fatalError("\(String(describing: aClass)) nib is not exist")
    }
  }
  var width: CGFloat {
    get { return self.frame.size.width }
    set {
      var frame = self.frame
      frame.size.width = newValue
      self.frame = frame
    }
  }
  
  var height: CGFloat {
    get { return self.frame.size.height }
    set {
      var frame = self.frame
      frame.size.height = newValue
      self.frame = frame
    }
  }
  
  var size: CGSize  {
    get { return self.frame.size }
    set {
      var frame = self.frame
      frame.size = newValue
      self.frame = frame
    }
  }
  
  var origin: CGPoint {
    get { return self.frame.origin }
    set {
      var frame = self.frame
      frame.origin = newValue
      self.frame = frame
    }
  }
  
  var x: CGFloat {
    get { return self.frame.origin.x }
    set {
      var frame = self.frame
      frame.origin.x = newValue
      self.frame = frame
    }
  }
  var y: CGFloat {
    get { return self.frame.origin.y }
    set {
      var frame = self.frame
      frame.origin.y = newValue
      self.frame = frame
    }
  }
  
  var centerX: CGFloat {
    get { return self.center.x }
    set {
      self.center = CGPoint(x: newValue, y: self.center.y)
    }
  }
  
  var centerY: CGFloat {
    get { return self.center.y }
    set {
      self.center = CGPoint(x: self.center.x, y: newValue)
    }
  }
  
  var top : CGFloat {
    get { return self.frame.origin.y }
    set {
      var frame = self.frame
      frame.origin.y = newValue
      self.frame = frame
    }
  }
  
  var bottom : CGFloat {
    get { return frame.origin.y + frame.size.height }
    set {
      var frame = self.frame
      frame.origin.y = newValue - self.frame.size.height
      self.frame = frame
    }
  }
  
  var right : CGFloat {
    get { return self.frame.origin.x + self.frame.size.width }
    set {
      var frame = self.frame
      frame.origin.x = newValue - self.frame.size.width
      self.frame = frame
    }
  }
  
  var left : CGFloat {
    get { return self.frame.origin.x }
    set {
      var frame = self.frame
      frame.origin.x  = newValue
      self.frame = frame
    }
  }
  
  var heightConstraint: NSLayoutConstraint? {
    get {
      return constraints.first(where: {
        $0.firstAttribute == .height && $0.relation == .equal
      })
    }
    set { setNeedsLayout() }
  }
  
  var widthConstraint: NSLayoutConstraint? {
    get {
      return constraints.first(where: {
        $0.firstAttribute == .width && $0.relation == .equal
      })
    }
    set { setNeedsLayout() }
  }
  
}

extension UIView {
  var globalFrame: CGRect? {
    let rootView = UIApplication.shared.keyWindow?.rootViewController?.view
    return self.superview?.convert(self.frame, to: rootView)
  }
}
