//
//  UIView+CorectRadius.swift
//  Comic
//
//  Created by Hoang Vu Van on 11/6/19.
//  Copyright © 2019 Techlab Corp. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
class LeftAlignedIconButton: UIButton {
  override func titleRect(forContentRect contentRect: CGRect) -> CGRect {
    let titleRect = super.titleRect(forContentRect: contentRect)
    let imgSize = currentImage?.size ?? .zero
    let availableWidth = contentRect.width - imageEdgeInsets.right - imgSize.width - titleRect.width
    return titleRect.offsetBy(dx: round(availableWidth / 2), dy: 0)
  }
}

@IBDesignable
class RightAlignedIconButton: UIButton {
    override func layoutSubviews() {
        super.layoutSubviews()
        semanticContentAttribute = .forceRightToLeft
        contentHorizontalAlignment = .right
        let availableSpace = bounds.inset(by: contentEdgeInsets)
        let availableWidth = availableSpace.width - imageEdgeInsets.left - (imageView?.frame.width ?? 0) - (titleLabel?.frame.width ?? 0)
        titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: availableWidth / 2)
    }
}

@IBDesignable
class DesignableView: UIView {
}

@IBDesignable
class DesignableButton: UIButton {
}

@IBDesignable
class DesignableLabel: UILabel {
}


extension UIView {
  
  @IBInspectable var cornerRadius: CGFloat {
    get {
      return layer.cornerRadius
    }
    set {
      layer.cornerRadius = newValue
    }
  }
  
  @IBInspectable var masksToBounds: Bool {
    get {
      return layer.masksToBounds
    }
    set {
      layer.masksToBounds = newValue
    }
  }
  
  @IBInspectable var borderWidth: CGFloat {
    get {
      return layer.borderWidth
    }
    set {
      layer.borderWidth = newValue
    }
  }
  
  @IBInspectable var borderColor: UIColor? {
    get {
      return UIColor(cgColor: layer.borderColor ?? UIColor.clear.cgColor)
    }
    set {
      layer.borderColor = newValue?.cgColor
    }
  }
  
  @IBInspectable
  var shadowRadius: CGFloat {
    get {
      return layer.shadowRadius
    }
    set {
      layer.shadowRadius = newValue
    }
  }
  
  @IBInspectable
  var shadowOpacity: Float {
    get {
      return layer.shadowOpacity
    }
    set {
      layer.shadowOpacity = newValue
    }
  }
  
  @IBInspectable
  var shadowOffset: CGSize {
    get {
      return layer.shadowOffset
    }
    set {
      layer.shadowOffset = newValue
    }
  }
  
  @IBInspectable
  var shadowColor: UIColor? {
    get {
      if let color = layer.shadowColor {
        return UIColor(cgColor: color)
      }
      return nil
    }
    set {
      if let color = newValue {
        layer.shadowColor = color.cgColor
      } else {
        layer.shadowColor = nil
      }
    }
  }
  
  func addDefaultShaDow(scale: Bool = true, radius: CGFloat = 10, color: CGColor = UIColor.black.withAlphaComponent(0.3).cgColor, size: CGSize? = nil){
    self.layer.cornerRadius = radius
      if let size = size {
          self.layer.shadowPath =
              UIBezierPath(roundedRect: CGRect.init(x: 0, y: 0, width: size.width, height: size.height),
          cornerRadius: radius).cgPath
      } else {
          self.layer.shadowPath =
          UIBezierPath(roundedRect: self.bounds,
          cornerRadius: radius).cgPath
      }
          
      self.layer.shadowColor = color
      self.layer.shadowOffset = CGSize(width: 0, height: 0.5)
      self.layer.shadowOpacity = 0.6
      self.layer.shadowRadius = 2.5
      self.layer.masksToBounds = false
  }
}

extension UIView {
//  func applyGradient(cgColors:[CGColor], startPoint:CGPoint, endPoint:CGPoint) {
//    let gradient = CAGradientLayer()
//    gradient.colors = cgColors
//    gradient.locations = [0.2,1.0] //cgColor.count = 2
//    //gradient.locations = [0.0,0.5,1.0] cgColor.count = 3
//    gradient.frame = self.bounds
//    gradient.startPoint = startPoint
//    gradient.endPoint = endPoint
//    self.layer.insertSublayer(gradient, at: 0)
//  }
  
//  func applyGradient(withColours colours: [UIColor], locations: [NSNumber]? = nil) {
//    let gradient: CAGradientLayer = CAGradientLayer()
//    gradient.frame = self.bounds
//    gradient.colors = colours.map { $0.cgColor }
//    gradient.locations = locations
//    self.layer.insertSublayer(gradient, at: 0)
//  }
  
  func applyGradient(withColours colours: [UIColor], gradientOrientation orientation: GradientOrientation) {
    let gradient: CAGradientLayer = CAGradientLayer()
    gradient.frame = self.bounds
    gradient.locations = [0.2,1.0]
    gradient.colors = colours.map { $0.cgColor }
    gradient.startPoint = orientation.startPoint
    gradient.endPoint = orientation.endPoint
    self.layer.insertSublayer(gradient, at: 0)
  }
  
  
  class func customUIView(view: UIView, radius: CGFloat, boderWidth: CGFloat, boderColor: UIColor?, backGroundColor: UIColor?) {
    view.layer.cornerRadius = radius
    view.layer.drawsAsynchronously = true
    if let boderColor = boderColor {
      view.layer.borderColor = boderColor.cgColor
    } else {
      view.layer.borderColor = UIColor.clear.cgColor
    }
    
    view.layer.borderWidth = boderWidth
    
    if backGroundColor != nil {
      view.backgroundColor = backGroundColor
    }
    
    view.layer.masksToBounds = true
  }
  
  func roundCorners(corners: UIRectCorner, radius: CGFloat) {
    let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
    let mask = CAShapeLayer()
    mask.path = path.cgPath
    self.layer.mask = mask
  }
  
  typealias GradientPoints = (startPoint: CGPoint, endPoint: CGPoint)

  enum GradientOrientation {
    case topRightBottomLeft
    case topLeftBottomRight
    case horizontal
    case vertical

  var startPoint: CGPoint {
      return points.startPoint
  }

  var endPoint: CGPoint {
      return points.endPoint
  }

  var points: GradientPoints {
      switch self {
      case .topRightBottomLeft:
          return (CGPoint(x: 0.0, y: 1.0), CGPoint(x: 1.0, y: 0.0))
      case .topLeftBottomRight:
          return (CGPoint(x: 0.0, y: 0.0), CGPoint(x: 1, y: 1))
      case .horizontal:
          return (CGPoint(x: 0.0, y: 0.5), CGPoint(x: 1.0, y: 0.5))
      case .vertical:
          return (CGPoint(x: 0.0, y: 0.0), CGPoint(x: 0.0, y: 1.0))
      }
    }
  }
}

