//
//  UIView+Loading
//  MochaSwift
//
//  Created by HoangVu on 4/8/19.
//  Copyright © 2019 HoangVu. All rights reserved.
//

import Foundation
import UIKit
import NVActivityIndicatorView
import SVProgressHUD

extension UIView {
  func showLoadingIndicator() {
    let viewContaint = UIView.init(frame: self.bounds)
    viewContaint.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    viewContaint.backgroundColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5)
    viewContaint.tag = 123
    let activityIndicator = UIActivityIndicatorView(style: .white)
    activityIndicator.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
    activityIndicator.startAnimating()
    
    viewContaint.addSubview(activityIndicator)
    activityIndicator.center = viewContaint.center
    
    self.addSubview(viewContaint)
    UIApplication.shared.beginIgnoringInteractionEvents()
  }
  
  func hideLoadingIndicator() {
    for view in self.subviews {
      if view.tag == 123 {
        view.removeFromSuperview()
        UIApplication.shared.endIgnoringInteractionEvents()
      } else if view.tag == 124 {
        view.removeFromSuperview()
        UIApplication.shared.endIgnoringInteractionEvents()
      }
    }
  }
  
  func showLoadingViewAndBlockUI() {
    UIApplication.shared.beginIgnoringInteractionEvents()
    SVProgressHUD.setContainerView(self)
    SVProgressHUD.show()
  }
  
  func hideLoadingViewAndUnBlockUI() {
    SVProgressHUD.dismiss()
    UIApplication.shared.endIgnoringInteractionEvents()
  }
  
  func showLoadingView(frame:CGRect? = nil) {
    SVProgressHUD.setContainerView(self)
    SVProgressHUD.show()
  }
  
  func hideLoadingView() {
    SVProgressHUD.dismiss()
  }
  
  func showActivityIndicatorView(frame:CGRect? = nil) {
    self.hideLoadingView()
    var currentFrame:CGRect = self.bounds
    if let frame = frame {
      currentFrame = frame;
    }
    let viewContaint = UIView.init(frame: currentFrame)
    viewContaint.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    viewContaint.backgroundColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5)
    viewContaint.tag = 123
    
    let activityIndicatorView = NVActivityIndicatorView(frame: CGRect.init(x: 0, y: 0, width: 40, height: 40), type:NVActivityIndicatorType.ballBeat, color: .white)
    activityIndicatorView.padding = 5
    self.addSubview(activityIndicatorView)
    activityIndicatorView.startAnimating()
    
    viewContaint.addSubview(activityIndicatorView)
    activityIndicatorView.center = viewContaint.center
    
    self.addSubview(viewContaint)
    UIApplication.shared.beginIgnoringInteractionEvents()
  }
  
  
  func showLoadingIndicatorWithString(_ title: String) {
    let strLabel = UILabel(frame: CGRect(x: 50, y: 0, width: 160, height: 46))
    strLabel.text = title
    strLabel.font = UIFont.systemFont(ofSize: 14.0)
    strLabel.textColor = UIColor(white: 0.9, alpha: 1.0)
    
    let viewContaint = UIView.init(frame: CGRect(x: self.frame.midX - strLabel.frame.width/2, y: self.frame.midY - strLabel.frame.height/2 , width: 160, height: 46))
    viewContaint.layer.cornerRadius = 15
    viewContaint.layer.masksToBounds = true
    viewContaint.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    viewContaint.backgroundColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.7)
    viewContaint.tag = 124
    
    let activityIndicator = UIActivityIndicatorView(style: .white)
    activityIndicator.frame = CGRect(x: 0, y: 0, width: 46, height: 46)
    activityIndicator.startAnimating()
    
    viewContaint.addSubview(activityIndicator)
    viewContaint.addSubview(strLabel)
    self.addSubview(viewContaint)
    UIApplication.shared.beginIgnoringInteractionEvents()
  }
  //
  //    func popupError() {
  //        Utils.showAlert(title: "", message: Constants.common.ERROR_MESSAGE_DEFAULT, titleActionLeft: nil, titleActionRight: "OK", action: { (position, alert) in
  //
  //        }, complete: nil)
  //
  //    }
  //
  //    func popupEmtyData(){
  //        Utils.showAlert(title: "", message: Constants.common.EMTY_DATA_DEFAULT, titleActionLeft: nil, titleActionRight: "OK".localized(), action: {(index, alert) in
  //
  //        })
  //    }
  
}

//MARK - Blur

extension UIView {
  func blur(style: UIBlurEffect.Style) {
    let blurEffect = UIBlurEffect(style: style)
    let blurEffectView = UIVisualEffectView(effect: blurEffect)
    blurEffectView.frame = self.bounds
    blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    self.addSubview(blurEffectView)
  }
  
  func unBlur() {
    for subview in self.subviews {
      if subview is UIVisualEffectView {
        subview.removeFromSuperview()
      }
    }
  }
}

extension UIView {
  class func fromNib<T: UIView>() -> T {
    return Bundle(for: T.self).loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
  }
}

extension UIView {
  func fadeIn(duration: TimeInterval = 0.5, delay: TimeInterval = 0.0, completion: @escaping ((Bool) -> Void) = {(finished: Bool) -> Void in }) {
    self.alpha = 0.0
    
    UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.curveEaseIn, animations: {
      self.isHidden = false
      self.alpha = 1.0
    }, completion: completion)
  }
  
  func fadeOut(duration: TimeInterval = 0.5, delay: TimeInterval = 0.0, completion: @escaping (Bool) -> Void = {(finished: Bool) -> Void in }) {
    self.alpha = 1.0
    
    UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.curveEaseOut, animations: {
      self.isHidden = true
      self.alpha = 0.0
    }, completion: completion)
  }
}

extension UIView {
  func setShadowBottom(color:UIColor){
    self.layer.drawsAsynchronously = true
    self.layer.shadowColor = color.cgColor
    self.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
    self.layer.shadowOpacity = 1.0
    self.layer.shadowRadius = 1
    self.layer.masksToBounds = false
  }
  
  func setGradientBackground(colorTop: UIColor, colorBottom: UIColor) {
    let gradientLayer = CAGradientLayer()
    gradientLayer.colors = [colorBottom.cgColor, colorTop.cgColor]
    gradientLayer.startPoint = CGPoint(x: 0.5, y: 1.0)
    gradientLayer.endPoint = CGPoint(x: 0.5, y: 0.0)
    gradientLayer.locations = [0, 1]
    gradientLayer.frame = bounds
    
    layer.insertSublayer(gradientLayer, at: 0)
  }
}

extension UIView {
  func showEmptyComment() -> UIView {
    let v = UIView(frame: self.bounds)
    v.tag = 101010
    v.backgroundColor = .clear
    let imgNoComment = UIImageView(image: UIImage(named: "empty_comment"))
    let lb = UILabel()
    lb.text = "Chưa có bình luận nè!"
    lb.font = RobotoFont.fontWithType(.regular, size: 15)
    lb.textColor = .descTextColor()
    v.addSubview(imgNoComment)
    v.addSubview(lb)
    imgNoComment.snp.makeConstraints { (make) in
      make.width.height.equalTo(100)
      make.centerX.equalToSuperview()
      make.centerY.equalToSuperview().offset(-100)
    }
    lb.snp.makeConstraints { (make) in
      make.centerX.equalToSuperview()
      make.centerY.equalToSuperview().offset(-20)
    }
    self.addSubview(v)
    return v
  }
     
  
  func removeEmptyComment() {
    for sub in self.subviews {
      if (sub.tag == 101010) {
        sub.removeFromSuperview()
        break
      }
    }
  }
  
  func checkLogin() -> Bool {
    return UserData.sharedInstance().isLogin
  }
}

extension UIView {
  func screenshot() -> UIImage {
    if #available(iOS 10.0, *) {
      return UIGraphicsImageRenderer(size: bounds.size).image { _ in
        drawHierarchy(in: CGRect(origin: .zero, size: bounds.size), afterScreenUpdates: true)
      }
    } else {
      UIGraphicsBeginImageContextWithOptions(self.bounds.size, false, UIScreen.main.scale)
      drawHierarchy(in: self.bounds, afterScreenUpdates: true)
      let image = UIGraphicsGetImageFromCurrentImageContext() ?? UIImage()
      UIGraphicsEndImageContext()
      return image
    }
  }
}
