//
//  UIViewExtension.swift
//  CountriesList
//
//  Created by HoangVu on 9/30/17.
//  Copyright © 2017 1Life2Live. All rights reserved.
//

import UIKit

extension UIView {
    class func fromNib(nibNameOrNil: String? = nil) -> UIView {
        var view: UIView?
        let name: String
        
        if let nibName = nibNameOrNil {
            name = nibName
        }
        else {
            name = "\(self)".components(separatedBy: ".").last!
        }
        
        if let nibViews = Bundle.main.loadNibNamed(name, owner: nil, options: nil) {
            for v in nibViews {
                if let v = v as? UIView {
                    view = v
                }
            }			
        }
        
        return view!
    }
    
    class func nibObject() -> UINib {
        let name = "\(self)".components(separatedBy: ".").last!
        let hasNib: Bool = Bundle.main.path(forResource: name, ofType: "nib") != nil
        guard hasNib else {
            assert(!hasNib, "Invalid parameter") // assert
            return UINib()
        }
        return UINib(nibName: name, bundle:nil)
    }
}

extension UIView {
    var topView: UIView? {
        var top = superview
        while top?.superview != nil {
            top = top?.superview
        }

        return top
    }

    func addTapGesToDismissKeyboard() {
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(self.dissmissKeyboard))
        self.addGestureRecognizer(tap)
    }

    @objc func dissmissKeyboard() {
        topView?.endEditing(true)
    }
}

extension NSLayoutConstraint {
    /**
     Change multiplier constraint
     
     - parameter multiplier: CGFloat
     - returns: NSLayoutConstraint
     */
    func setMultiplier(_ multiplier:CGFloat) -> NSLayoutConstraint {
        
        NSLayoutConstraint.deactivate([self])
        
        let newConstraint = NSLayoutConstraint(
            item: firstItem,
            attribute: firstAttribute,
            relatedBy: relation,
            toItem: secondItem,
            attribute: secondAttribute,
            multiplier: multiplier,
            constant: constant)
        
        newConstraint.priority = priority
        newConstraint.shouldBeArchived = self.shouldBeArchived
        newConstraint.identifier = self.identifier
        
        NSLayoutConstraint.activate([newConstraint])
        return newConstraint
    }
}


extension UIView {

  func dropShadow(scale: Bool = true) {
    layer.masksToBounds = false
    layer.shadowColor = UIColor.black.cgColor
    layer.shadowOpacity = 0.5
    layer.shadowOffset = CGSize(width: -1, height: 1)
    layer.shadowRadius = 1

    layer.shadowPath = UIBezierPath(rect: bounds).cgPath
    layer.shouldRasterize = true
    layer.rasterizationScale = scale ? UIScreen.main.scale : 1
  }

  func dropShadow(color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1, scale: Bool = true) {
    layer.masksToBounds = false
    layer.shadowColor = color.cgColor
    layer.shadowOpacity = opacity
    layer.shadowOffset = offSet
    layer.shadowRadius = radius

    layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
    layer.shouldRasterize = true
    layer.rasterizationScale = scale ? UIScreen.main.scale : 1
  }
  
  func addShadowCellDefault() {
    self.layer.shadowColor = UIColor.black.withAlphaComponent(0.1).cgColor
    self.layer.shadowOpacity = 1
    self.layer.shadowOffset = .zero
    self.layer.shadowRadius = 3
  }
}

extension UIView {
  func shake(vals: [Double] = [-2, 2, -2, 2, 0], duration: CGFloat = 0.3) {
      layer.removeAnimation(forKey: "shakeview")
      let translation = CAKeyframeAnimation(keyPath: "transform.translation.x")
      translation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
      translation.values = vals
      
      let rotation = CAKeyframeAnimation(keyPath: "transform.rotation.z")
      rotation.values = vals.map { (degrees: Double) in
          let radians: Double = (Double.pi * degrees) / 180.0
          return radians
      }
      
      let shakeGroup: CAAnimationGroup = CAAnimationGroup()
      shakeGroup.animations = [translation, rotation]
      shakeGroup.duration = CFTimeInterval(duration)
      self.layer.add(shakeGroup, forKey: "shakeview")
  }
  
  func showMentionToolTipComment(fromView: UIView) {
    fromView.showMentionToolTipComment()
  }
  
  func showMentionToolTipComment() {
//    if (UserData.sharedInstance().numberShowMentionTool < 3) {
//      DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
//        ComicUtils.shared.tipMentionView.show(forView: self.addTipView())
//        UserData.sharedInstance().numberShowMentionTool = UserData.sharedInstance().numberShowMentionTool + 1
//      }
//    }
  }
  
  func addTipView() -> UIView {
    let tipView = UIView()
    tipView.frame = CGRect.init(x: 20, y: 0, width: 1, height: 1)
    self.superview?.addSubview(tipView)
    self.bringSubviewToFront(tipView)
    return tipView
  }
}
