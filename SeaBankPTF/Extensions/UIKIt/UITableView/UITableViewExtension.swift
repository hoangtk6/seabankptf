//
//  UITableViewExtension.swift
//  Comic
//
//  Created by Nguyễn Văn Tú on 9/10/20.
//  Copyright © 2020 Techlab Corp. All rights reserved.
//

import Foundation
import UIKit

extension UITableView {
  func hideEmptySeparators() {
    tableFooterView = UIView(frame: CGRect.zero)
  }
  
  func scrollToSecond(row: Int = 0, totalItems: Int, animated: Bool = true) {
      if self.numberOfSections > 0 {
          var currentIndex = row
          var isTop = false
          if currentIndex - 1 > 0 {
              isTop = true
              if currentIndex < totalItems - 2 {
                  currentIndex = currentIndex - 1
              }
          }
          
          let indexPath = IndexPath.init(row: row, section: 0)
          if self.indexPathExists(indexPath: indexPath) {
              self.scrollToRow(at: indexPath, at: isTop ? .top : .bottom, animated: animated)
          }
      }
  }
  
  func indexPathExists(indexPath:IndexPath) -> Bool {
      if indexPath.section >= self.numberOfSections {
          return false
      }
      if indexPath.row >= self.numberOfRows(inSection: indexPath.section) {
          return false
      }
      return true
  }
  
  func scrollToBottom(animated: Bool, scrollPosition: UITableView.ScrollPosition = UITableView.ScrollPosition.bottom) {
    let sectionsCount = numberOfSections
    if sectionsCount > 0 {
      let lastSection = sectionsCount - 1
      let rowsCount = numberOfRows(inSection: lastSection)
      if rowsCount != NSNotFound && rowsCount > 0 {
        let indexPath = IndexPath(row: rowsCount - 1, section: lastSection)
        scrollToRow(at: indexPath, at: scrollPosition, animated: animated)
      }
    }
  }
  
  func reloadData(_ completion: @escaping () -> Void ) {
    UIView.animate(withDuration: 0, animations: {
      self.reloadData()
    }) { _ in
      completion()
    }
  }
}

extension UITableView {
  func register<T: UITableViewCell>(_ aClass: T.Type) {
    let name = String(describing: aClass)
    let bundle = Bundle.main
    if bundle.path(forResource: name, ofType: "nib") != nil {
      let nib = UINib(nibName: name, bundle: bundle)
      register(nib, forCellReuseIdentifier: name)
    } else {
      register(aClass, forCellReuseIdentifier: name)
    }
  }
  
  func dequeue<T: UITableViewCell>(_ aClass: T.Type) -> T {
    let name = String(describing: aClass)
    guard let cell = dequeueReusableCell(withIdentifier: name) as? T else {
      fatalError("`\(name)` is not registed")
    }
    return cell
  }
}
