//
//  NSAttributedString+Extension.swift
//  Comic
//
//  Created by Hoang Vu on 12/13/19.
//  Copyright © 2019 Techlab Corp. All rights reserved.
//

import Foundation
import UIKit

extension NSAttributedString {
  func height(withConstrainedWidth width: CGFloat) -> CGFloat {
    let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
    let boundingBox = boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, context: nil)
    
    return ceil(boundingBox.height)
  }
  
  func width(withConstrainedHeight height: CGFloat) -> CGFloat {
    let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
    let boundingBox = boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, context: nil)
    
    return ceil(boundingBox.width)
  }
  
}

extension NSMutableAttributedString {
  func semibold(_ value:String,_ font:UIFont = .boldSystemFont(ofSize: Constants.FontSize.FONT_SIZE_15)) -> NSMutableAttributedString {
    
    let attributes:[NSAttributedString.Key : Any] = [
      .font : font
    ]
    
    self.append(NSAttributedString(string: value, attributes:attributes))
    return self
  }
  
  func bold(_ value:String,_ font:UIFont = .boldSystemFont(ofSize: Constants.FontSize.FONT_SIZE_15)) -> NSMutableAttributedString {
    
    let attributes:[NSAttributedString.Key : Any] = [
      .font : font
    ]
    
    self.append(NSAttributedString(string: value, attributes:attributes))
    return self
  }
  
  func bold(_ value:String,_ font:UIFont = .boldSystemFont(ofSize: Constants.FontSize.FONT_SIZE_15),_ color:UIColor = .black) -> NSMutableAttributedString {
    
    let attributes:[NSAttributedString.Key : Any] = [
      .font : font,
      .foregroundColor: color,
    ]
    
    self.append(NSAttributedString(string: value, attributes:attributes))
    return self
  }
  
  func normal(_ value:String,_ font: UIFont = RobotoFont.fontWithType(.regular, size: 15),_ color:UIColor = .black) -> NSMutableAttributedString {
    let attributes:[NSAttributedString.Key : Any] = [
      .font : font,
      .foregroundColor: color,
    ]
    
    self.append(NSAttributedString(string: value, attributes:attributes))
    return self
  }
}
