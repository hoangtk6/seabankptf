import Foundation

extension TTTAttributedLabel {
  func setupURLLinks() {
    guard let string = self.attributedText?.string,
          let dataDetector = try? NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue) else { return }
    let range = NSRange(string.startIndex..<string.endIndex, in: string)
    dataDetector.enumerateMatches(in: string, options: [], range: range) { (result, _, _) in
      if result?.resultType == .link, let url = result?.url, let range = result?.range {
        self.addLink(to: url, with: range)
      }
    }
  }
}
