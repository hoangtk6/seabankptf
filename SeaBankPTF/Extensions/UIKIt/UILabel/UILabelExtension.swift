//
//  UILabelExtension.swift
//  CountriesList
//
//  Created by HoangVu on 9/30/17.
//  Copyright © 2017 1Life2Live. All rights reserved.
//

import UIKit

extension UILabel {
  func format(){
    self.lineBreakMode = .byWordWrapping
    self.numberOfLines = 0
  }
  
  func formatSize(size: CGFloat) {
    self.format()
    self.font = UIFont(name: self.font.fontName, size: size)
  }
  
  func formatBoldSize(size: CGFloat) {
    self.format()
    self.font = UIFont.boldSystemFont(ofSize: size)
  }
  
  
  var maxNumberOfLines: Int {
    let maxSize = CGSize(width: frame.size.width, height: CGFloat(Float.infinity))
    let charSize = self.font.lineHeight
    let text = (self.text ?? "") as NSString
    let textSize = text.boundingRect(with: maxSize, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: self.font!], context: nil)
    let linesRoundedUp = Int(ceil(textSize.height/charSize))
    return linesRoundedUp
  }
  
  var stringLines: [String] {
      guard let text = text, let font = font else { return [] }
      let ctFont = CTFontCreateWithName(font.fontName as CFString, font.pointSize, nil)
      let attStr = NSMutableAttributedString(string: text)
      attStr.addAttribute(kCTFontAttributeName as NSAttributedString.Key, value: ctFont, range: NSRange(location: 0, length: attStr.length))
      let frameSetter = CTFramesetterCreateWithAttributedString(attStr as CFAttributedString)
      let path = CGMutablePath()
      path.addRect(CGRect(x: 0, y: 0, width: self.frame.size.width, height: CGFloat.greatestFiniteMagnitude), transform: .identity)
      let frame = CTFramesetterCreateFrame(frameSetter, CFRangeMake(0, 0), path, nil)
      guard let lines = CTFrameGetLines(frame) as? [Any] else { return [] }
      return lines.map { line in
          let lineRef = line as! CTLine
          let lineRange: CFRange = CTLineGetStringRange(lineRef)
          let range = NSRange(location: lineRange.location, length: lineRange.length)
          return (text as NSString).substring(with: range)
      }
  }
  
  func getLines(width:CGFloat)-> [String]{
    guard let text = text, let font = font else { return [] }
    let ctFont = CTFontCreateWithName(font.fontName as CFString, font.pointSize, nil)
    let attStr = NSMutableAttributedString(string: text)
    attStr.addAttribute(kCTFontAttributeName as NSAttributedString.Key, value: ctFont, range: NSRange(location: 0, length: attStr.length))
    let frameSetter = CTFramesetterCreateWithAttributedString(attStr as CFAttributedString)
    let path = CGMutablePath()
    path.addRect(CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude), transform: .identity)
    let frame = CTFramesetterCreateFrame(frameSetter, CFRangeMake(0, 0), path, nil)
    guard let lines = CTFrameGetLines(frame) as? [Any] else { return [] }
    return lines.map { line in
        let lineRef = line as! CTLine
        let lineRange: CFRange = CTLineGetStringRange(lineRef)
        let range = NSRange(location: lineRange.location, length: lineRange.length)
        return (text as NSString).substring(with: range)
    }
  }
  
  func set(text:String, leftIcon: UIImage? = nil, rightIcon: UIImage? = nil, width:CGFloat, height:CGFloat, yRight:CGFloat = 0) {

      let leftAttachment = NSTextAttachment()
      leftAttachment.image = leftIcon
      leftAttachment.bounds = CGRect(x: 0, y: -2.5, width: width, height: height)
      if let leftIcon = leftIcon {
          leftAttachment.bounds = CGRect(x: 0, y: -2.5, width: leftIcon.size.width, height: leftIcon.size.height)
      }
      let leftAttachmentStr = NSAttributedString(attachment: leftAttachment)

      let myString = NSMutableAttributedString(string: "")

      let rightAttachment = NSTextAttachment()
      rightAttachment.image = rightIcon
      rightAttachment.bounds = CGRect(x: 0, y: yRight, width: width, height: height)
      let rightAttachmentStr = NSAttributedString(attachment: rightAttachment)


      if semanticContentAttribute == .forceRightToLeft {
          if rightIcon != nil {
              myString.append(rightAttachmentStr)
              myString.append(NSAttributedString(string: " "))
          }
          myString.append(NSAttributedString(string: text))
          if leftIcon != nil {
              myString.append(NSAttributedString(string: " "))
              myString.append(leftAttachmentStr)
          }
      } else {
          if leftIcon != nil {
              myString.append(leftAttachmentStr)
              myString.append(NSAttributedString(string: " "))
          }
          myString.append(NSAttributedString(string: text))
          if rightIcon != nil {
              myString.append(NSAttributedString(string: " "))
              myString.append(rightAttachmentStr)
          }
      }
      attributedText = myString
  }
}
