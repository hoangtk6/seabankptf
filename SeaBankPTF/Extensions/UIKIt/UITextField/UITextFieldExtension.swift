//
//  UITextFieldExtension.swift
//  CountriesList
//
//  Created by HoangVu on 9/30/17.
//  Copyright © 2017 1Life2Live. All rights reserved.
//

import Foundation
import UIKit

extension UITextField {
  func updateCursorPosition(_ cursorPosition: Int) {
    if let newPosition = position(from: beginningOfDocument, offset: cursorPosition) {
      selectedTextRange = textRange(from: newPosition, to: newPosition)
    }
  }
  
  func moveCursorPositionToTheEnd() {
    if let text = text {
      updateCursorPosition(text.count)
    }
  }
  
  func cursorPosition() -> Int? {
    if let selectedRange = selectedTextRange {
      return offset(from: beginningOfDocument, to: selectedRange.start)
    }
    return nil
  }
}

private var maxLengths = [UITextField: Int]()

extension UITextField {
  
  @IBInspectable var maxLength: Int {
    get {
      guard let length = maxLengths[self] else {
        return Int.max
      }
      return length
    }
    set {
      maxLengths[self] = newValue
      addTarget(
        self,
        action: #selector(limitLength),
        for: UIControl.Event.editingChanged
      )
    }
  }
  
  @objc func limitLength(textField: UITextField) {
    guard let prospectiveText = textField.text, prospectiveText.count > maxLength else {
      return
    }
    text = String(prospectiveText.prefix(maxLength))
  }
}

extension UITextField {
  
  @IBInspectable var paddingLeftCustom: CGFloat {
    get {
      return leftView!.frame.size.width
    }
    set {
      let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: newValue, height: frame.size.height))
      leftView = paddingView
      leftViewMode = .always
    }
  }
  
  @IBInspectable var paddingRightCustom: CGFloat {
    get {
      return rightView!.frame.size.width
    }
    set {
      let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: newValue, height: frame.size.height))
      rightView = paddingView
      rightViewMode = .always
    }
  }
}

extension UITextField {
  
  func addInputViewDatePicker(target: Any, selector: Selector) {
    let screenWidth = UIScreen.main.bounds.width
    //Add DatePicker as inputView
    let datePicker = UIDatePicker(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 216))
    if #available(iOS 13.4, *) {
      datePicker.preferredDatePickerStyle = .wheels
    }
    datePicker.datePickerMode = .date
    self.inputView = datePicker
    
    //Add Tool Bar as input AccessoryView
    let toolBar = UIToolbar(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 44))
    let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
    let cancelBarButton = UIBarButtonItem(title: "Bỏ qua", style: .plain, target: self, action: #selector(cancelPressed))
    let doneBarButton = UIBarButtonItem(title: "Xong", style: .plain, target: target, action: selector)
    toolBar.setItems([cancelBarButton, flexibleSpace, doneBarButton], animated: false)
    
    self.inputAccessoryView = toolBar
  }
  
  func addDoneToolBar() {
    let bar = UIToolbar()
    let space = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
    let doneBtn = UIBarButtonItem(title: "Xong", style: .plain, target: self, action: #selector(doneAction))
    bar.items = [space, doneBtn]
    bar.sizeToFit()
    self.inputAccessoryView = bar
  }
  
  @objc func cancelPressed() {
    self.resignFirstResponder()
  }
  
  @objc
  private func doneAction() {
    self.resignFirstResponder()
  }
}


