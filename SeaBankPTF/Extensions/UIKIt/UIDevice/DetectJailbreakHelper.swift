import Foundation
import UIKit

final class DetectJailbreakHelper {
  //check if cydia is installed (using URI Scheme)
  static func hasCydiaInstalled() -> Bool {
    return UIApplication.shared.canOpenURL(URL(string: "cydia://")!)
  }
  
  //Check if suspicious apps (Cydia, FakeCarrier, Icy etc.) is installed
  static func isContainsSuspiciousApps() -> Bool {
    for path in suspiciousAppsPathToCheck where isFileExist(at: path) {
      return true
    }
    return false
  }
  
  //Check if system contains suspicious files
  static func isSuspiciousSystemPathsExists() -> Bool {
    for path in suspiciousSystemPathsToCheck where canOpen(path: path) {
        return true
    }
    return false
  }
  
  //Check if app can edit system files
  static func canEditSystemFiles() -> Bool {
    let stringToWrite = "Seabank is good to go"
    let path = "/private/" + NSUUID().uuidString
    do {
      try stringToWrite.write(toFile: path, atomically: true, encoding: String.Encoding.utf8)
      return true
    } catch {
      return false
    }
  }
  
  // MARK: - Private
  
  private static func canOpen(path: String) -> Bool {
    let file = fopen(path, "r")
    guard file != nil else { return false }
    fclose(file)
    return true
  }
  
  private static func isFileExist(at path: String) -> Bool {
    FileManager.default.fileExists(atPath: path)
  }
  
  private static var suspiciousAppsPathToCheck: [String] {
    return [
      "/Applications/Cydia.app",
      "/Applications/FakeCarrier.app"
    ]
  }
  
  private static var suspiciousSystemPathsToCheck: [String] {
    return [
      "/usr/bin/ssh",
      "/usr/sbin/sshd",
      "/etc/apt",
      "/bin/bash",
      "/Library/MobileSubstrate/MobileSubstrate.dylib"
    ]
  }
}
