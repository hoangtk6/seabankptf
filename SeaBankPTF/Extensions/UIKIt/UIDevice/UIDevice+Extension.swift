//
//  UIDevice+Extension.swift
//  Comic
//
//  Created by Hoang Vu on 12/3/19.
//  Copyright © 2019 Techlab Corp. All rights reserved.
//

import Foundation
import UIKit

public extension UIDevice {
  
  static let deviceName: String = {
    var systemInfo = utsname()
    uname(&systemInfo)
    let machineMirror = Mirror(reflecting: systemInfo.machine)
    let identifier = machineMirror.children.reduce("") { identifier, element in
      guard let value = element.value as? Int8, value != 0 else { return identifier }
      return identifier + String(UnicodeScalar(UInt8(value)))
    }
    
    func mapToDevice(identifier: String) -> String { // swiftlint:disable:this cyclomatic_complexity
      #if os(iOS)
      switch identifier {
        case "iPod5,1":                                 return "iPod touch (5th generation)"
        case "iPod7,1":                                 return "iPod touch (6th generation)"
        case "iPod9,1":                                 return "iPod touch (7th generation)"
        case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
        case "iPhone4,1":                               return "iPhone 4s"
        case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
        case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
        case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
        case "iPhone7,2":                               return "iPhone 6"
        case "iPhone7,1":                               return "iPhone 6 Plus"
        case "iPhone8,1":                               return "iPhone 6s"
        case "iPhone8,2":                               return "iPhone 6s Plus"
        case "iPhone8,4":                               return "iPhone SE"
        case "iPhone9,1", "iPhone9,3":                  return "iPhone 7"
        case "iPhone9,2", "iPhone9,4":                  return "iPhone 7 Plus"
        case "iPhone10,1", "iPhone10,4":                return "iPhone 8"
        case "iPhone10,2", "iPhone10,5":                return "iPhone 8 Plus"
        case "iPhone10,3", "iPhone10,6":                return "iPhone X"
        case "iPhone11,2":                              return "iPhone XS"
        case "iPhone11,4", "iPhone11,6":                return "iPhone XS Max"
        case "iPhone11,8":                              return "iPhone XR"
        case "iPhone12,1":                              return "iPhone 11"
        case "iPhone12,3":                              return "iPhone 11 Pro"
        case "iPhone12,5":                              return "iPhone 11 Pro Max"
        case "iPhone12,8":                              return "iPhone SE (2nd generation)"
        case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
        case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad (3rd generation)"
        case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad (4th generation)"
        case "iPad6,11", "iPad6,12":                    return "iPad (5th generation)"
        case "iPad7,5", "iPad7,6":                      return "iPad (6th generation)"
        case "iPad7,11", "iPad7,12":                    return "iPad (7th generation)"
        case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
        case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
        case "iPad11,4", "iPad11,5":                    return "iPad Air (3rd generation)"
        case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad mini"
        case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad mini 2"
        case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad mini 3"
        case "iPad5,1", "iPad5,2":                      return "iPad mini 4"
        case "iPad11,1", "iPad11,2":                    return "iPad mini (5th generation)"
        case "iPad6,3", "iPad6,4":                      return "iPad Pro (9.7-inch)"
        case "iPad7,3", "iPad7,4":                      return "iPad Pro (10.5-inch)"
        case "iPad8,1", "iPad8,2", "iPad8,3", "iPad8,4":return "iPad Pro (11-inch)"
        case "iPad8,9", "iPad8,10":                     return "iPad Pro (11-inch) (2nd generation)"
        case "iPad6,7", "iPad6,8":                      return "iPad Pro (12.9-inch)"
        case "iPad7,1", "iPad7,2":                      return "iPad Pro (12.9-inch) (2nd generation)"
        case "iPad8,5", "iPad8,6", "iPad8,7", "iPad8,8":return "iPad Pro (12.9-inch) (3rd generation)"
        case "iPad8,11", "iPad8,12":                    return "iPad Pro (12.9-inch) (4th generation)"
        case "AppleTV5,3":                              return "Apple TV"
        case "AppleTV6,2":                              return "Apple TV 4K"
        case "AudioAccessory1,1":                       return "HomePod"
        case "i386", "x86_64":                          return "Simulator \(mapToDevice(identifier: ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "iOS"))"
        default:                                        return identifier
      }
      #elseif os(tvOS)
      switch identifier {
        case "AppleTV5,3": return "Apple TV 4"
        case "AppleTV6,2": return "Apple TV 4K"
        case "i386", "x86_64": return "Simulator \(mapToDevice(identifier: ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "tvOS"))"
        default: return identifier
      }
      #endif
    }
    
    return mapToDevice(identifier: identifier)
  }()
  
  
  
  var deviceIOSVersion: String {
    return UIDevice.current.systemVersion
  }
  
  var deviceScreenWidth: CGFloat {
    let screenSize: CGRect = UIScreen.main.bounds
    let width = screenSize.width;
    return width
  }
  var deviceScreenHeight: CGFloat {
    let screenSize: CGRect = UIScreen.main.bounds
    let height = screenSize.height;
    return height
  }
  
  
  var deviceOrientationString: String {
    var orientation : String
    switch UIDevice.current.orientation{
      case .portrait:
        orientation="Portrait"
      case .portraitUpsideDown:
        orientation="Portrait Upside Down"
      case .landscapeLeft:
        orientation="Landscape Left"
      case .landscapeRight:
        orientation="Landscape Right"
      case .faceUp:
        orientation="Face Up"
      case .faceDown:
        orientation="Face Down"
      default:
        orientation="Unknown"
    }
    return orientation
  }
  
  //  Landscape Portrait
  var isDevicePortrait: Bool {
    return UIDevice.current.orientation.isPortrait
  }
  
  var isDeviceLandscape: Bool {
    return UIDevice.current.orientation.isLandscape
  }
  
  var isIPhone: Bool {
    return UIDevice().userInterfaceIdiom == .phone
  }
  var isIPad: Bool {
    return UIDevice().userInterfaceIdiom == .pad
  }
  
  enum ScreenType: String {
    case iPhone4
    case iPhone5
    case iPhone6
    case iPhone6Plus
    case Unknown
  }
  
  var screenType: ScreenType? {
    guard isIPhone else { return nil }
    switch UIScreen.main.nativeBounds.height {
      case 960:
        return .iPhone4
      case 1136:
        return .iPhone5
      case 1334:
        return .iPhone6
      case 2208:
        return .iPhone6Plus
      default:
        return nil
    }
  }
  
  var isIphoneX:Bool {
    if UIDevice().userInterfaceIdiom == .phone {
      switch UIScreen.main.nativeBounds.height {
        case 1136:
          print("iPhone 5 or 5S or 5C")
          return false
        case 1334:
          print("iPhone 6/6S/7/8")
          return false
        case 1920, 2208:
          print("iPhone 6+/6S+/7+/8+")
          return false
        case 2436:
          print("iPhone X/XS/11 Pro/iPhone 12 mini")
          return true
        case 2688:
          print("iPhone XS Max/11 Pro Max")
          return true
        case 1792:
          print("iPhone XR/ 11 ")
          return true
          
        case 2532:
          print("iPhone 12/iPhone 12 Pro")
          return true

        case 2778:
          print("iPhone 12 Pro Max")
          return true
          
        default:
          print("Unknown")
          return false
      }
    }
    return false
  }
  
  // helper funcs
  static func isScreen35inch() -> Bool {
    return UIDevice().screenType == .iPhone4
  }
  
  static func isScreen4inch() -> Bool {
    return UIDevice().screenType == .iPhone5
  }
  
  static func isLessThanScreen4inch() -> Bool {
    if UIDevice().screenType == .iPhone4 || UIDevice().screenType == .iPhone5{
      return true
    }
    return false
  }
  
  static func isScreen47inch() -> Bool {
    return UIDevice().screenType == .iPhone6
  }
  
  static func isScreen55inch() -> Bool {
    return UIDevice().screenType == .iPhone6Plus
  }
  
  //MARK: - Rotate
  static func lockOrientation(_ orientation: UIInterfaceOrientationMask) {
    if let delegate = UIApplication.shared.delegate as? AppDelegate {
      delegate.orientationLock = orientation
    }
  }
  
  static func lockOrientation(_ orientation: UIInterfaceOrientationMask, andRotateTo rotateOrientation:UIInterfaceOrientation) {
    self.lockOrientation(orientation)
    UIDevice.current.setValue(rotateOrientation.rawValue, forKey: "orientation")
  }
  
}

extension UIDevice {
  var modelName: String {
    if let _ = ProcessInfo.processInfo.environment["SIMULATOR_MODEL_IDENTIFIER"] { return "Simulator" }
    var info = utsname()
    uname(&info)
    return String(String.UnicodeScalarView(
      Mirror(reflecting: info.machine)
        .children
        .compactMap {
          guard let value = $0.value as? Int8 else { return nil }
          let unicode = UnicodeScalar(UInt8(value))
          return unicode.isASCII ? unicode : nil
    }))
  }
}

extension UIDevice {
  var systemSize: Int64? {
    guard let systemAttributes = try? FileManager.default.attributesOfFileSystem(forPath: NSHomeDirectory() as String),
      let totalSize = (systemAttributes[.systemSize] as? NSNumber)?.int64Value else {
        return nil
    }
    
    return totalSize
  }
  
  var systemFreeSize: Int64? {
    guard let systemAttributes = try? FileManager.default.attributesOfFileSystem(forPath: NSHomeDirectory() as String),
      let freeSize = (systemAttributes[.systemFreeSize] as? NSNumber)?.int64Value else {
        return nil
    }
    
    return freeSize
  }
}
