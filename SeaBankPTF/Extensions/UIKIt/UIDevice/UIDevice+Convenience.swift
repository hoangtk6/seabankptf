//
//  UIDevice+Convenience.swift
//  Setel
//
//  Created by Evgeniy Gurtovoy on 3/7/18.
//  Copyright © 2018 Setel. All rights reserved.
//

import Foundation
import UIKit
import SystemConfiguration

enum DeviceSizeType {
  case small
  case large
  case normal
}

// MARK: - SafeArea

extension UIDevice {
  static var hasNotch: Bool {
    if #available(iOS 11.0, *) {
      let keyWindow = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
      return keyWindow?.safeAreaInsets.bottom ?? 0 > 0
    }
    return false
  }
  
  static var topSafeAreaInset: CGFloat {
    if #available(iOS 11.0, *) {
      if let topPadding = UIApplication.shared.keyWindow?.safeAreaInsets.top {
        return topPadding
      }
    }
    return 0
  }
  
  static var bottomSafeAreaInset: CGFloat {
    if #available(iOS 11.0, *) {
      if let bottomPadding = UIApplication.shared.keyWindow?.safeAreaInsets.bottom {
        return bottomPadding
      }
    }
    return 0
  }
}

extension UIDevice {
  
  static let isSimulator: Bool = {
    #if targetEnvironment(simulator)
    return true
    #else
    return false
    #endif
  }()
  
  static var isJailbreak: Bool {
    get {
      if DetectJailbreakHelper.hasCydiaInstalled() {
        return true
      }
      if DetectJailbreakHelper.isContainsSuspiciousApps() {
        return true
      }
      if DetectJailbreakHelper.isSuspiciousSystemPathsExists() {
        return true
      }
      return DetectJailbreakHelper.canEditSystemFiles()
    }
  }
  
  // The device is valid if it is not a simulator, rooted/jailbroken device
  static var isValid: Bool {
    return !(isSimulator || isJailbreak)
  }
}
