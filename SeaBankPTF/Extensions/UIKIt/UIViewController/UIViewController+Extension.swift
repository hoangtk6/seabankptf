//
//  UIViewController+Extension.swift
//  MochaSwift
//
//  Created by HoangVu on 4/4/19.
//  Copyright © 2019 HoangVu. All rights reserved.
//

import Foundation
import UIKit

//import Toast_Swift

enum PositionBarButton {
  case left
  case right
}

extension UIViewController {
  // MARK: - Alert, Toast
  
  //    func makeToast(_ message: String, _ duration: TimeInterval = 3.0, _ positon: ToastPosition = .center ) {
  //        self.view.makeToast(message , duration: duration, position: positon)
  //    }
  // MARK: - NavigationBar
  public static var topViewController: UIViewController? {
    var presentedVC = UIApplication.shared.keyWindow?.rootViewController
    while let pVC = presentedVC?.presentedViewController {
      presentedVC = pVC
    }
    
    if presentedVC == nil {
      print("Error: You don't have any views set. You may be calling them in viewDidLoad. Try viewDidAppear instead.")
    }
    return presentedVC
  }
  
  fileprivate func ptfPushViewController(_ viewController: UIViewController, animated: Bool, hideTabbar: Bool) {
    viewController.hidesBottomBarWhenPushed = hideTabbar
    self.navigationController?.pushViewController(viewController, animated: animated)
  }
  
  /**
   push
   */
  public func comicPushAndHideTabbar(_ viewController: UIViewController) {
    self.ptfPushViewController(viewController, animated: true, hideTabbar: true)
  }
  
  public func comicPushAndHideTabbar(_ viewController: UIViewController, _ animated: Bool) {
    self.ptfPushViewController(viewController, animated: animated, hideTabbar: true)
  }
  
  /**
   present
   */
  public func ptfPresentViewController(_ viewController: UIViewController, modalPresentationStyle:UIModalPresentationStyle = .fullScreen, completion:(() -> Void)?) {
    DispatchQueue.main.async{
      let navigationController = UINavigationController(rootViewController: viewController)
      navigationController.modalPresentationStyle = modalPresentationStyle
      self.present(navigationController, animated: true, completion: completion)
    }
  }
  
  func addTitle(title:String){
    self.title = title
    let attrs = [
      NSAttributedString.Key.foregroundColor: UIColor.red,
      NSAttributedString.Key.font: RobotoFont.fontWithType(.medium, size: 20)
    ]
    
    UINavigationBar.appearance().titleTextAttributes = attrs
  }
  
  func addBackButton(icon:UIImage = UIImage(named: "btnBack") ?? ComicAsset.Common_back_black.image, width:CGFloat = Constants.BarButton.Width, height:CGFloat = Constants.BarButton.Height) {
    UINavigationBar.appearance().titleTextAttributes = [.foregroundColor : UIColor.comicNavigationColor()]
    if let count =  self.navigationController?.viewControllers.count, count > 0 {
      let btnLeftMenu = UIButton(frame: CGRect(x: 0, y: 0, width: width, height: height))
      btnLeftMenu.contentHorizontalAlignment = .left
      
      btnLeftMenu.setImage(icon, for: .normal)
      btnLeftMenu.setTitle(emptyString, for: .normal);
      //  btnLeftMenu.sizeToFit()
      btnLeftMenu.addTarget(self, action: #selector (backButtonClick(sender:)), for: .touchUpInside)
      let barButton = UIBarButtonItem(customView: btnLeftMenu)
      self.navigationItem.leftBarButtonItem = barButton
    }
  }
  
  func addLeftButtonWithLogo(icon: String, width: CGFloat = Constants.BarButton.Width, height: CGFloat = Constants.BarButton.Height, logo: String) {
    let btn = UIButton(frame: CGRect(x: 0, y: 0, width: width, height: height))
    btn.setImage(UIImage(named: icon), for: .normal)
    btn.addTarget(self, action: #selector(backButtonClick(sender:)), for: .touchUpInside)
    let leftBtn = UIBarButtonItem(customView: btn)
    
    let imgLogo = UIImage(named: logo)
    let imgView = UIImageView(frame: CGRect(x: 0, y: 0, width: imgLogo?.size.width ?? 0, height: imgLogo?.size.height ?? 0))
    imgView.image = imgLogo
    let tap = UITapGestureRecognizer(target: self, action: #selector(self.searchClick(_:)))
    imgView.addGestureRecognizer(tap)
    let logoView = UIBarButtonItem(customView: imgView)

    self.navigationItem.leftBarButtonItems = [leftBtn, logoView]
  }
    
  func addLeftButton(icon: String, width: CGFloat = Constants.BarButton.Width, height: CGFloat = Constants.BarButton.Height) {
    let btn = UIButton(frame: CGRect(x: 0, y: 0, width: width, height: height))
    btn.setImage(UIImage(named: icon), for: .normal)
    btn.addTarget(self, action: #selector(backButtonClick(sender:)), for: .touchUpInside)
    let leftBtn = UIBarButtonItem(customView: btn)

    self.navigationItem.leftBarButtonItems = [leftBtn]

  }
  
  func makeTranslucentNavigationBar() {
    self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
    self.navigationController?.navigationBar.shadowImage = UIImage()
    self.navigationController?.navigationBar.isTranslucent = true
    self.navigationController?.navigationBar.barTintColor = .white
    self.navigationController?.navigationBar.tintColor = .white
    self.navigationController?.view.backgroundColor = .clear
  }
  
  @objc func backButtonClick( sender : UIButton? = nil) {
    if isModal {
      self.dismiss(animated: true, completion: nil)
    } else {
      self.navigationController?.popViewController(animated: true)
    }
  }
  
  @objc func searchClick(_ sender: UIBarButtonItem) {
   
  }
  
  @objc func selectedDropdownClick( sender : UIButton? = nil) {
    
  }
  
  @objc func rightButtonClick(_ sender: BadgedBarButtonItem) {
    
  }
  
  @objc func rightButtonTapped() {
    
  }
  
  func createLeftButton(withImage imageName: String, title: String) {
    
    let widthBarItem = CGFloat(250)
    
    let view = UIView()
    view.frame = CGRect(x: 0, y: 0, width: widthBarItem, height: Constants.BarButton.Height)
    
    let leftButton = UIButton()
    leftButton.setImage(UIImage(named: imageName), for: .normal)
    leftButton.frame = CGRect(x: 0, y: 0, width: widthBarItem, height: Constants.BarButton.Height)
    leftButton.contentEdgeInsets = UIEdgeInsets(top:0, left:0, bottom:0, right: widthBarItem - Constants.BarButton.Width)
    leftButton.contentHorizontalAlignment = .left
    leftButton.addTarget(self, action: #selector (backButtonClick(sender:)), for: .touchUpInside)
    view.addSubview(leftButton)
    
    let lblTitle = UILabel()
    lblTitle.frame = CGRect(x: Constants.BarButton.Width - 10, y: 0, width: widthBarItem - Constants.BarButton.Width - 5, height: Constants.BarButton.Height)
    lblTitle.text = title
    lblTitle.textColor = UIColor.black
    lblTitle.font = .fontMenuHighLight()
    view.addSubview(lblTitle)
    
    navigationItem.leftBarButtonItem = UIBarButtonItem(customView: view)
  }
  
  
  func addNavBarWithTitle(_ title: String, _ color: UIColor, _ font:UIFont? = .fontMenuHighLight(), _ position: PositionBarButton) {
    let widthBarItem = CGFloat(50)
    
    let button = UIButton()
    button.frame = CGRect(x: 0, y: 0, width: widthBarItem, height: Constants.BarButton.Height)
    button.setTitle(title, for: .normal)
    button.titleLabel?.font = font
    button.setTitleColor(color, for: .normal)
    
    switch position {
      case .left:
        button.contentHorizontalAlignment = .left
        button.addTarget(self, action: #selector (backButtonClick(sender:)), for: .touchUpInside)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: button)
        
      case .right:
        button.contentHorizontalAlignment = .right
        button.addTarget(self, action: #selector (rightButtonTapped), for: .touchUpInside)
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: button)
        
    }
  }
  
  func addNavBarWithButton( _ button: UIButton, _ position: PositionBarButton) {
    switch position {
      case .left:
        button.contentHorizontalAlignment = .left
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: button)
        break
      case .right:
        button.contentHorizontalAlignment = .right
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: button)
        break
    }
  }
  
  func createLeftButtonMenu(withImage imageName: String, title: String) {
    
    let widthBarItem = CGFloat(250)
    
    let view = UIView()
    //view.backgroundColor = .gray
    view.frame = CGRect(x: 0, y: 0, width: widthBarItem, height: Constants.BarButton.Height)
    
    let leftButton = UIButton()
    //leftButton.backgroundColor = .blue
    leftButton.setImage(UIImage(named: imageName), for: .normal)
    leftButton.frame = CGRect(x: 0, y: 0, width: Constants.BarButton.Width-10, height: Constants.BarButton.Height)
    leftButton.imageEdgeInsets = UIEdgeInsets(top:0, left:0, bottom:0, right: 0)
    leftButton.contentHorizontalAlignment = .left
    leftButton.addTarget(self, action: #selector (backButtonClick(sender:)), for: .touchUpInside)
    view.addSubview(leftButton)
    
    let lblTitle = UILabel()
    let font  = UIFont.fontMenuHighLight()
    let width = title.widthOfString(usingFont: font)
    
    lblTitle.font = font
    lblTitle.frame = CGRect(x: Constants.BarButton.Width - 10, y: 0, width: width, height: Constants.BarButton.Height)
    lblTitle.text = title
    lblTitle.textAlignment = .left
    //lblTitle.backgroundColor = .green
    lblTitle.textColor = UIColor.black
    view.addSubview(lblTitle)
    let leftButtonMenu = UIButton()
    
    
    leftButtonMenu.backgroundColor = .clear
    //leftButtonMenu.setTitle(title, for: .normal)
    leftButtonMenu.contentHorizontalAlignment = .left;
    leftButtonMenu.titleLabel?.font = font
    leftButtonMenu.setTitleColor(.black, for: .normal)
    leftButtonMenu.frame = CGRect(x: Constants.BarButton.Width - 10, y: 0, width: width + 20, height: Constants.BarButton.Height)
    leftButtonMenu.setImage(UIImage(named: "ic_drop_down"), for: .normal)
    leftButtonMenu.imageEdgeInsets = UIEdgeInsets(top: 0, left: width + 5, bottom: 0, right: 0);
    //leftButtonMenu.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 40);
    leftButtonMenu.addTarget(self, action: #selector (selectedDropdownClick(sender:)), for: .touchUpInside)
    view.addSubview(leftButtonMenu)
    view.bringSubviewToFront(leftButtonMenu)
    
    navigationItem.leftBarButtonItem = UIBarButtonItem(customView: view)
  }
  
  func createRightBarButton(imageName:String){
    let image = UIImage(imageLiteralResourceName: imageName)
    let buttonFrame: CGRect = CGRect(x: 0.0, y: 0, width: image.size.width, height: image.size.height)
    
    let barButton = BadgedBarButtonItem(
      startingBadgeValue: 0,
      frame: buttonFrame,
      image: image,
      badgeProperties: BadgeProperties.init(originalFrame: CGRect(x: 0, y: -5, width: 12, height: 12), minimumWidth: 12, horizontalPadding: 0, verticalPadding: 0, font: .fontNavigationBarBag(), textColor: .white, backgroundColor: .red)
    )
    var rightBarButton: BadgedBarButtonItem?
    rightBarButton = barButton
    rightBarButton?.addTarget(self, action: #selector(rightButtonClick(_:)))
    navigationItem.rightBarButtonItem = rightBarButton
  }
  
  
  func rightBarButtonWith(image: UIImage, title: String, font: UIFont,bgColor:UIColor = .white) {
    let button: UIButton = UIButton(type: UIButton.ButtonType.custom)
    button.semanticContentAttribute = .forceLeftToRight
    button.titleLabel?.font = font
    button.setImage(image, for: .normal)
    button.setTitle(title, for: .normal)
    button.backgroundColor = bgColor
    let spacing: CGFloat = 5 // the amount of spacing to appear between image and title
    
    button.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: spacing)
    button.titleEdgeInsets = UIEdgeInsets(top: 0, left: spacing, bottom: 0, right: 0)
    button.setTitleColor(UIColor.black, for: .normal)
    
    let fontAttributes = [NSAttributedString.Key.font: font]
    let size = (title as NSString).size(withAttributes: fontAttributes as [NSAttributedString.Key : Any])
    button.frame = CGRect(x: 0, y: 0, width: ceil(size.width + image.size.width + spacing), height: image.size.height)
    
    button.addTarget(self, action: #selector(rightButtonClick(_:)), for: .touchUpInside)
    navigationItem.rightBarButtonItem = UIBarButtonItem(customView: button)
  }
  
  func rightBarButtonWith(button:UIButton) {
    button.addTarget(self, action: #selector(rightButtonTapped), for: .touchUpInside)
    navigationItem.rightBarButtonItem = UIBarButtonItem(customView: button)
  }
  
  func createRightButton(withTitle title: String, light:Bool, textColor:UIColor?) {
    let rightButton = UIButton()
    var with = title.widthOfString(usingFont: .fontButtomBar())
    if with < Constants.BarButton.Width - 10 {
      with = Constants.BarButton.Width
    }
    if with > 200 {
      with = 200
    }
    
    rightButton.setTitle(title, for: .normal)
    rightButton.titleLabel?.font = .fontButtomBar()
    if let textColor = textColor{
      rightButton.setTitleColor(textColor, for: .normal)
    }else{
      rightButton.setTitleColor(light ? .black : .white, for: .normal)
    }
    rightButton.frame = CGRect(x: 0, y: 0, width: with, height: Constants.BarButton.Height)
    rightButton.addTarget(self, action: #selector(rightButtonTapped), for: .touchUpInside)
    navigationItem.rightBarButtonItem = UIBarButtonItem(customView: rightButton)
  }
  
  // MARK: - NotificationCenter
  func addNotificationObserver(name: Notification.Name, selector: Selector) {
    NotificationCenter.default.addObserver(self, selector: selector, name: name, object: nil)
  }
  
  func removeNotificationObserver(name: Notification.Name) {
    NotificationCenter.default.removeObserver(self, name: name, object: nil)
  }
  
  func removeNotificationsObserver() {
    NotificationCenter.default.removeObserver(self)
  }
  // MARK: - From Nib
  class func initFromNib() -> UIViewController {
    let hasNib: Bool = Bundle.main.path(forResource: self.nameOfClass, ofType: "nib") != nil
    guard hasNib else {
      assert(!hasNib, "Invalid parameter") // here
      return UIViewController()
    }
    return self.init(nibName: self.nameOfClass, bundle: nil)
  }
  
  func hideBorderBottomNavigationBar(){
    self.navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
  }
  
  func showBorderBottomNavigationBar(){
    self.navigationController?.navigationBar.setValue(false, forKey: "hidesShadow")
  }
  
  // get class from Storyboard
  //    class func instantiate<T: UIViewController>(mochaStoryboard: MochaStoryboard) -> T {
  //
  //        let storyboard = UIStoryboard(name: mochaStoryboard.rawValue, bundle: nil)
  //        let identifier = String(describing: self)
  //        return storyboard.instantiateViewController(withIdentifier: identifier) as! T
  //    }
  //ex:
  //    let profileVC: ProfileVC = ProfileVC.instantiate(mochaStoryboard: .profile)
  //    self.navigationController?.pushViewController(profileVC,animated:true)
  
}

extension UIViewController {
  // Lấy ra đôi tượng viewcontroller từ storyboard với tên của viewcontroller trong storyboard phải trùng với tên class swift
  class func instantiateViewControllerFromStoryboard(storyBoard: String) -> AnyObject {
    let storyboardObject = UIStoryboard(name: storyBoard, bundle: nil)
    return storyboardObject.instantiateViewController(withIdentifier: String(describing: self))
  }
}

extension UIViewController {

  func addTopButton() {
    let btn = UIButton(frame: CGRect(x: Constants.ScreenSize.SCREEN_WIDTH - 13 - 48, y: Constants.ScreenSize.SCREEN_HEIGHT - 13 - 48, width: 48, height: 48))
    btn.setImage(UIImage(named: "goTopImage"), for: .normal)
    self.view.addSubview(btn)
  }

  func showMentionToolTipComment(fromView: UIView) {
    //    if (UserData.sharedInstance().numberShowMentionTool < 3) {
    //      DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
    //        ComicUtils.shared.tipMentionView.show(forView: fromView)
    //        UserData.sharedInstance().numberShowMentionTool = UserData.sharedInstance().numberShowMentionTool + 1
    //      }
    //    }
  }
}

extension UIViewController {
  func addDefaultNaviAppearance() {
    self.navigationController?.navigationBar.barTintColor = .white
    self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.titleTextColor()]
  }
  
  func addNavigationBarColorToon() {
    self.navigationController?.navigationBar.isTranslucent = true
    self.navigationController?.navigationBar.barTintColor = UIColor.darkNaviColor()
    self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
  }
}

