//
//  UiViewControllerAlert+Extension.swift
//  Comic
//
//  Created by Hoang Vu on 12/11/19.
//  Copyright © 2019 Techlab Corp. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController{
  func showAlertWithDistructiveButton(title:String?, message:String, leftButtonTitle:String, rightButtonTitle:String, completion:@escaping (_ result: Bool)->()) {
    let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
    
    alert.addAction(UIAlertAction(title: leftButtonTitle, style: UIAlertAction.Style.default, handler: { _ in
      completion(false)
      
    }))
    
    alert.addAction(UIAlertAction(title: rightButtonTitle,
                                  style: UIAlertAction.Style.destructive,
                                  handler: {(_: UIAlertAction!) in
                                    completion(true)
    }))
    
    DispatchQueue.main.async {
      self.present(alert, animated: true, completion: {
        print("completion block")
      })
    }
  }
  
  /**
   Simple Alert with more than 2 buttons
   */
  func showAlertWithOption(title:String?, message:String?, menuTitle:[String], menuKeyTitle:[String], titleCloseButton:String, completion:@escaping (_ result: String)->()) {
    let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
    
    if menuTitle.count > 0 {
      for index in 0...menuTitle.count - 1 {
        alert.addAction(UIAlertAction(title: menuTitle[index], style: .default, handler: { (_) in
          completion(menuTitle[index])
        }))
      }
    }
    
    if menuKeyTitle.count > 0 {
      for index in 0...menuKeyTitle.count - 1 {
        alert.addAction(UIAlertAction(title: menuKeyTitle[index], style: .destructive, handler: { (_) in
          completion(menuKeyTitle[index])
        }))
      }
    }
    
    alert.addAction(UIAlertAction(title: titleCloseButton , style: .cancel, handler: { (_) in
      print("User click Dismiss button")
    }))
    
    DispatchQueue.main.async {
      self.present(alert, animated: true, completion: {
        print("completion block")
      })
    }
  }
  
  
  func showMenuActionSheet(title:String?, message:String?, menuTitle:[String], menuKeyTitle:[String], titleCloseButton:String, completion:@escaping (_ result: String)->()) {
    var alertStyle = UIAlertController.Style.actionSheet
    if (UIDevice().isIPad) {
      alertStyle = UIAlertController.Style.alert
    }
    
    let alert = UIAlertController(title: title, message:message, preferredStyle: alertStyle)
    if menuTitle.count > 0 {
      for index in 0...menuTitle.count - 1 {
        alert.addAction(UIAlertAction(title: menuTitle[index], style: .default, handler: { (_) in
          completion(menuTitle[index])
        }))
      }
    }
    
    if menuKeyTitle.count > 0 {
      for index in 0...menuKeyTitle.count - 1 {
        alert.addAction(UIAlertAction(title: menuKeyTitle[index], style: .destructive, handler: { (_) in
          completion(menuKeyTitle[index])
        }))
      }
    }
    
    alert.addAction(UIAlertAction(title: titleCloseButton , style: .cancel, handler: { (_) in
      print("User click Dismiss button")
    }))
    
    DispatchQueue.main.async {
      self.present(alert, animated: true, completion: {
        print("completion block")
      })
    }
  }
  
  func showAlertWithTextField(title:String?,placeHolder:String?, value:String?, leftButtonTitle:String, rightButtonTitle:String, keyboardType: UIKeyboardType = .default, completion:@escaping (_ result: Bool, _ newValue:String?)->()) {
    let alertController = UIAlertController(title: title, message: nil, preferredStyle: .alert)
    let cancelAction = UIAlertAction(title: leftButtonTitle, style: .default) { (_) in
      completion(false, nil)
    }
    
    let confirmAction = UIAlertAction(title: rightButtonTitle , style: .destructive) { (_) in
      if let txtField = alertController.textFields?.first, let text = txtField.text {
        completion(true, text)
        //print("Text==>" + text)
      }
    }
    
    alertController.addTextField { (textField) in
      textField.placeholder = placeHolder
      textField.text = value
      textField.keyboardType = keyboardType
    }
    
    alertController.addAction(cancelAction)
    alertController.addAction(confirmAction)
    
    DispatchQueue.main.async {
      self.present(alertController, animated: true, completion: {
        print("completion block")
      })
    }
  }
  
  func showShareMenu(link:String){
    let items = [URL(string: link)!]
    let ac = UIActivityViewController(activityItems: items, applicationActivities: nil)
    self.present(ac, animated: true, completion: {
      print("completion block")
    })
  }
  
  func showAlertWith(title: String, cancelTitle: String, completion: @escaping() -> Void) {
    let alertController = UIAlertController(title: "", message: title, preferredStyle: .alert)
    let cancelAction = UIAlertAction(title: "OK", style: .cancel) { (_) in
      completion()
    }
    alertController.addAction(cancelAction)
    self.present(alertController, animated: true, completion: completion)
  }
}
