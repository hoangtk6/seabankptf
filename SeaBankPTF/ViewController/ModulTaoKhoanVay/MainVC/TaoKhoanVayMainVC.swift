//
//  TaoKhoanVayMainVC.swift
//  SeaBankPTF
//
//  Created by Hoang Vu Van on 13/09/2021.
//

import UIKit
import StepIndicator
import SnapKit

final class TaoKhoanVayMainVC: BaseViewController {
  
  @IBOutlet weak var stepIndicatorView:StepIndicatorView!
  @IBOutlet weak var contentView: UIView!
  
  private var pageViewController: CustomPageViewController!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    setupUI()
  }
  
  override func applyLocalization() {
    navigationItem.rightBarButtonItem = UIBarButtonItem.pdfTextButton(title: "Ngôn ngữ".localized(), target: self, action: #selector(languageButtonDidTap))
  }
}

private extension TaoKhoanVayMainVC {
  func setupUI() {
    addBackButton()
    setupStepView()
    setupPage()
  }
  
  func setupStepView(){
    stepIndicatorView.numberOfSteps = 8
    stepIndicatorView.currentStep = 0
    stepIndicatorView.circleColor = UIColor.init(hexString: "D72228")
    stepIndicatorView.circleTintColor = UIColor.init(hexString: "4DC41F")
    stepIndicatorView.circleStrokeWidth = 1.0
    stepIndicatorView.circleRadius = 12.0
    stepIndicatorView.lineColor = self.stepIndicatorView.circleColor
    stepIndicatorView.lineTintColor = self.stepIndicatorView.circleTintColor
    stepIndicatorView.lineMargin = 1.0
    stepIndicatorView.lineStrokeWidth = 1.0
    stepIndicatorView.displayNumbers = true //indicates if it displays numbers at the center instead of the core circle
    stepIndicatorView.direction = .leftToRight
  }
  
  func setupPage() {
    pageViewController = CustomPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
    pageViewController.customDelegate = self
    pageViewController.controllers = [ThongTinChungVC(), ThongTinTrungLapVC(), ThongTinYeuCauVC(), ThongTinCVKDVC(), ThongTinLienHeVC(), ThongTinNNVC(), ThongTinGiaiNganVC(), GuiHoSoHTVC()]
    pageViewController.controllers = [ThongTinLienHeVC()]
    addChild(pageViewController)
    contentView.addSubview(pageViewController.view)
    pageViewController.view.snp.makeConstraints {
      $0.top.leading.equalToSuperview()
      $0.trailing.bottom.equalToSuperview()
    }
  }
  
  @objc func languageButtonDidTap(_ sender: Any) {
    let languageViewController = LanguagesViewController()
    languageViewController.hidesBottomBarWhenPushed = true
    navigationController?.pushViewController(languageViewController, animated: true)
  }
}

extension TaoKhoanVayMainVC: CustomPageViewControllerDelegate {
  func pageViewController(pageViewController: CustomPageViewController, didUpdatePageIndex index: Int) {
    self.stepIndicatorView.currentStep = index
  }
}
