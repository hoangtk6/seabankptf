//
//  GuiHoSsHTUploadDocView.swift
//  SeaBankPTF
//
//  Created by Hoang Vu Van on 19/09/2021.
//

import Foundation

final class NguoiLienHeView: NibView {
  
  @IBOutlet private weak var hoVaTenNLQView: SBSInputTextView!
  @IBOutlet private weak var nguoiLQView: SBSInputTextView!
  @IBOutlet private weak var sdtNLQView: SBSInputTextView!
  
  
  // MARK: - Lifecycle
  
  override func configureView() {
    setupUI()
  }
  
  func setupData(){
    
  }
  
}

private extension NguoiLienHeView {
  func setupUI(){
    hoVaTenNLQView.isRequire = true
    hoVaTenNLQView.isHiddenArrowDown = true
    nguoiLQView.isRequire = true
    nguoiLQView.isHiddenArrowDown = false
    sdtNLQView.isRequire = true
    sdtNLQView.isHiddenArrowDown = true
    
    hoVaTenNLQView.title = Text.localizedString("Họ và tên người liên quan")
    nguoiLQView.title = Text.localizedString("Người liên quan")
    sdtNLQView.title = Text.localizedString("Số điện thoại")
  }
}
