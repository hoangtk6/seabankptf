//
//  ThongTinTrungLapVC.swift
//  SeaBankPTF
//
//  Created by Hoang Vu Van on 13/09/2021.
//

import UIKit

final class ThongTinLienHeVC: BaseViewController {
  @IBOutlet private weak var sctionThongTinView: InfoSectionView!
  @IBOutlet private weak var sdtChinhView: SBSInputTextView!
  @IBOutlet private weak var sdtNhaView: SBSInputTextView!
  @IBOutlet private weak var sdtThuHaiView: SBSInputTextView!
  @IBOutlet private weak var emailView: SBSInputTextView!
  @IBOutlet private weak var loaiTKXHView: SBSInputTextView!
  @IBOutlet private weak var chiTietTKXHView: SBSInputTextView!
  
  @IBOutlet private weak var sctionThongTinCDView: InfoSectionView!
  @IBOutlet private weak var tinhTPCDView: SBSInputTextView!
  @IBOutlet private weak var quanHuyenCDView: SBSInputTextView!
  @IBOutlet private weak var phuongXaCDView: SBSInputTextView!
  @IBOutlet private weak var soNhaCDView: SBSInputTextView!
  @IBOutlet private weak var dCTTCDView: SBSLargeInput!
  
  @IBOutlet private weak var sctionThongTinHTView: InfoSectionView!
  @IBOutlet private weak var tinhTPHTView: SBSInputTextView!
  @IBOutlet private weak var quanHuyenHTView: SBSInputTextView!
  @IBOutlet private weak var phuongXaHTView: SBSInputTextView!
  @IBOutlet private weak var soNhaHTView: SBSInputTextView!
  @IBOutlet private weak var dCTTHTView: SBSLargeInput!
  
  @IBOutlet private weak var sctionThongTinBSView: InfoSectionView!
  @IBOutlet private weak var tinhTrangHNView: SBSInputTextView!
  @IBOutlet private weak var nguoiPTView: SBSInputTextView!
  @IBOutlet private weak var soConView: SBSInputTextView!
  @IBOutlet private weak var tdHVView: SBSInputTextView!
  
  @IBOutlet private weak var sctionThongTinTKView: InfoSectionView!
  @IBOutlet private weak var listLienHeView: UIStackView!
  @IBOutlet private weak var nguoiLienHeView: NguoiLienHeView!


  
  @IBOutlet weak var btnNext: UIButton!
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    setupUI()
    setupMultiLanguage()
  }
  
  
  // MARK: Action
  @IBAction func addNewNLHClick(_ sender: Any) {
    addNewNLHVIew()
  }
  
  @IBAction func clickNext(_ sender: Any) {
    
  }
}

private extension ThongTinLienHeVC {
  func setupUI() {
    view.backgroundColor = .clear
    setupInput()
    PTFUIUtils.setupButton(button: self.btnNext, title: Text.localizedString("Tiếp tục"), font:RobotoFont.fontWithType(.bold, size: 18), isShowShadow: true)
  }
  
  func setupInput() {
    sctionThongTinView.viewType = .taokhoanvaystep22
    sctionThongTinView.setupData(type:.taokhoanvaystep22, leftTitle: Text.localizedString("Thông tin"))
    sctionThongTinView.onClickTitleView = {
    
    }

    sctionThongTinCDView.viewType = .taokhoanvaystep22
    sctionThongTinCDView.setupData(type:.taokhoanvaystep22, leftTitle: Text.localizedString("Thông tin cố định"))
    sctionThongTinCDView.onClickTitleView = {
    
    }
                                  
    sctionThongTinHTView.viewType = .taokhoanvaystep22
    sctionThongTinHTView.setupData(type:.taokhoanvaystep22, leftTitle: Text.localizedString("Thông tin hiện tại"))
    sctionThongTinHTView.onClickTitleView = {
    
    }
    
    sctionThongTinBSView.viewType = .taokhoanvaystep22
    sctionThongTinBSView.setupData(type:.taokhoanvaystep22, leftTitle: Text.localizedString("Thông tin bổ sung"))
    sctionThongTinBSView.onClickTitleView = {
    
    }
    
    sctionThongTinTKView.viewType = .taokhoanvaystep22
    sctionThongTinTKView.setupData(type:.taokhoanvaystep22, leftTitle: Text.localizedString("Thông tin tham khảo"))
    sctionThongTinTKView.onClickTitleView = {
    
    }
    
    sdtChinhView.isRequire = true
    sdtChinhView.isHiddenArrowDown = true
    sdtNhaView.isRequire = false
    sdtNhaView.isHiddenArrowDown = true
    sdtThuHaiView.isRequire = false
    sdtThuHaiView.isHiddenArrowDown = true
    emailView.isRequire = false
    emailView.isHiddenArrowDown = true
    loaiTKXHView.isRequire = false
    loaiTKXHView.isHiddenArrowDown = false
    chiTietTKXHView.isRequire = false
    chiTietTKXHView.isHiddenArrowDown = true

    tinhTPCDView.isRequire = true
    tinhTPCDView.isHiddenArrowDown = false
    quanHuyenCDView.isRequire = true
    quanHuyenCDView.isHiddenArrowDown = false
    phuongXaCDView.isRequire = true
    phuongXaCDView.isHiddenArrowDown = false
    soNhaCDView.isRequire = true
    soNhaCDView.isHiddenArrowDown = true
    dCTTCDView.isRequire = true
    
    tinhTPHTView.isRequire = true
    tinhTPHTView.isHiddenArrowDown = false
    quanHuyenHTView.isRequire = true
    quanHuyenHTView.isHiddenArrowDown = false
    phuongXaHTView.isRequire = true
    phuongXaHTView.isHiddenArrowDown = false
    soNhaHTView.isRequire = true
    soNhaHTView.isHiddenArrowDown = true
    dCTTHTView.isRequire = true
    
    tinhTrangHNView.isRequire = true
    tinhTrangHNView.isHiddenArrowDown = false
    nguoiPTView.isRequire = true
    nguoiPTView.isHiddenArrowDown = false
    soConView.isRequire = true
    soConView.isHiddenArrowDown = true
    tdHVView.isRequire = true
    tdHVView.isHiddenArrowDown = false

  }
  
  
  func setupMultiLanguage() {
    sdtChinhView.title = Text.localizedString("Số điện thoại chính")
    sdtNhaView.title = Text.localizedString("Số điện thoại nhà")
    sdtThuHaiView.title = Text.localizedString("Số điện thoại thứ 2")
    emailView.title = Text.localizedString("Email")
    loaiTKXHView.title = Text.localizedString("Loại  tài khoản xã hội")
    chiTietTKXHView.title = Text.localizedString("Chi tiết tài khoản xã hội")
    
    tinhTPCDView.title = Text.localizedString("Tỉnh/ thành phố")
    quanHuyenCDView.title = Text.localizedString("Quận/huyện")
    phuongXaCDView.title = Text.localizedString("Phường xã")
    soNhaCDView.title = Text.localizedString("Số nhà, phố")
    dCTTCDView.title = Text.localizedString("Địa chỉ thường trú (đầy đủ)")
    
    tinhTPHTView.title = Text.localizedString("Tỉnh/ thành phố")
    quanHuyenHTView.title = Text.localizedString("Quận/huyện")
    phuongXaHTView.title = Text.localizedString("Phường xã")
    soNhaHTView.title = Text.localizedString("Số nhà, phố")
    dCTTHTView.title = Text.localizedString("Địa chỉ thường trú (đầy đủ)")
    
    tinhTrangHNView.title = Text.localizedString("Tình trạng hôn nhân")
    nguoiPTView.title = Text.localizedString("Người phụ thuộc")
    soConView.title = Text.localizedString("Số con")
    tdHVView.title = Text.localizedString("Trình độ học vấn")
    
  }
  
  func addNewNLHVIew(){
    let view = NguoiLienHeView.init()
    view.heightAnchor.constraint(equalToConstant: 133.0).isActive = true
    view.widthAnchor.constraint(equalToConstant: nguoiLienHeView.bounds.width).isActive = true
   
    self.listLienHeView.addArrangedSubview(view)
    self.listLienHeView.layoutIfNeeded()
  }
}
