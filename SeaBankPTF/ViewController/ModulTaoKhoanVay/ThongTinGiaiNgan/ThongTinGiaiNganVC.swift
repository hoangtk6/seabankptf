//
//  ThongTinTrungLapVC.swift
//  SeaBankPTF
//
//  Created by Hoang Vu Van on 13/09/2021.
//

import UIKit

final class ThongTinGiaiNganVC: BaseViewController {
  @IBOutlet private weak var phuongThucGNView: SBSInputTextView!
  @IBOutlet private weak var tenNHView: SBSInputTextView!
  @IBOutlet private weak var chiNhanhNHView: SBSInputTextView!
  @IBOutlet private weak var tinhTPView: SBSInputTextView!
  @IBOutlet private weak var maCNView: SBSInputTextView!
  @IBOutlet private weak var sotkthView: SBSInputTextView!
  @IBOutlet private weak var btnCheck: UIButton!
  @IBOutlet private weak var tenTKTHView: SBSInputTextView!
  @IBOutlet private weak var tenDTView: SBSInputTextView!
  @IBOutlet private weak var chiNhanhDTView: SBSInputTextView!

  
  
  @IBOutlet weak var btnNext: UIButton!
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    setupUI()
    setupMultiLanguage()
  }
  
  
  // MARK: Action
  @IBAction func clickNext(_ sender: Any) {
    
  }
}

private extension ThongTinGiaiNganVC {
  func setupUI() {
    view.backgroundColor = .clear
    setupInput()

    PTFUIUtils.setupButton(button: self.btnNext, title: Text.localizedString("Tiếp tục"), font:RobotoFont.fontWithType(.bold, size: 18), isShowShadow: true)
  }
  

  func setupInput() {

    phuongThucGNView.isRequire = true
    phuongThucGNView.isHiddenArrowDown = false
    
    tenNHView.isRequire = true
    tenNHView.isHiddenArrowDown = false
    
    chiNhanhNHView.isRequire = true
    chiNhanhNHView.isHiddenArrowDown = false
    
    tinhTPView.isRequire = true
    tinhTPView.isHiddenArrowDown = false
    
    maCNView.isRequire = true
    maCNView.isHiddenArrowDown = true
    
    sotkthView.isRequire = true
    sotkthView.isHiddenArrowDown = true
    
    tenTKTHView.isRequire = true
    tenTKTHView.isHiddenArrowDown = true
    
    tenDTView.isRequire = true
    tenDTView.isHiddenArrowDown = false
    
    chiNhanhDTView.isRequire = true
    chiNhanhDTView.isHiddenArrowDown = true
  }

  
  func setupMultiLanguage() {
    phuongThucGNView.title = Text.localizedString("Phương thức giải ngân")
    tenNHView.title = Text.localizedString("Tên ngân hàng")
    chiNhanhNHView.title = Text.localizedString("Chi nhánh")
    tinhTPView.title = Text.localizedString("Tỉnh thành phố")
    maCNView.title = Text.localizedString("Mã chi nhánh")
    sotkthView.title = Text.localizedString("Số tài khoản thụ hưởng")
    
    btnCheck.setTitle(Text.localizedString("Kiểm tra"), for: .normal)
    tenTKTHView.title = Text.localizedString("Tên tài khoản thụ hưởng")
    tenDTView.title = Text.localizedString("Tên đối tác")
    chiNhanhDTView.title = Text.localizedString("Chi nhánh đối tác")
  }
}
