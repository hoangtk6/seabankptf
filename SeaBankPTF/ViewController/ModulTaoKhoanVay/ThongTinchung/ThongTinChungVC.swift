//
//  ThongTinChungVC.swift
//  SeaBankPTF
//
//  Created by Hoang Vu Van on 13/09/2021.
//

import UIKit

final class ThongTinChungVC: BaseViewController {
    
    @IBOutlet private weak var scrollView: UIScrollView!
    @IBOutlet private weak var contentStackView: UIStackView!
    
    @IBOutlet private weak var hoTenView: SBSInputTextView!
    @IBOutlet private weak var soHoKhauView: SBSInputTextView!
    @IBOutlet private weak var ngaySinhView: SBSInputCalendar!
    @IBOutlet private weak var gioiTinhView: SBSInputTextView!
    @IBOutlet private weak var loaiKHView: SBSInputTextView!
    @IBOutlet private weak var danhXungView: SBSInputTextView!
  
    @IBOutlet private weak var gtttView: GTTTView!
    @IBOutlet private weak var listGtttView: UIStackView!
  
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func applyLocalization() {
        hoTenView.title = "Họ và tên".localized()
        soHoKhauView.title = "Số sổ hộ khẩu".localized()
        ngaySinhView.title = "Ngày sinh".localized()
        gioiTinhView.title = "Giới tính".localized()
        loaiKHView.title = "Loại khách hàng".localized()
        danhXungView.title = "Danh xưng".localized()
    }
}

private extension ThongTinChungVC {
    func setupUI() {
        view.backgroundColor = .clear
        gtttView.delegate = self
        
        setupInput()
    }
    
    func setupInput() {
        hoTenView.isRequire = true
        hoTenView.contentMaxLength = 80
        
        soHoKhauView.isRequire = true
        soHoKhauView.contentMaxLength = 30
        
        gioiTinhView.isRequire = true
        gioiTinhView.isHiddenArrowDown = false
        gioiTinhView.addTapGes { [weak self] in
            self?.showGioiTinhPicker()
        }
        
        loaiKHView.isRequire = true
        loaiKHView.isHiddenArrowDown = false
        
        danhXungView.isRequire = true
        danhXungView.isHiddenArrowDown = false
        
        ngaySinhView.isRequire = true
    }
    
    func showGioiTinhPicker() {
        let genders: [PTFGender] = [.male, .female]
        let titles = genders.map({ $0.rawValue })
        gioiTinhView.showDropDown(titles, parentView: scrollView, onSelected: { [weak self] index in
            self?.gioiTinhView.content = titles[index]
        })
    }
}

extension ThongTinChungVC: GTTTViewViewDelegate {
    func gtttViewDidTapAdd(_ view: GTTTView) {
        view.setVisibleAddButton(false)
        let newView = GTTTView()
        newView.delegate = self
        newView.widthAnchor.constraint(equalToConstant: listGtttView.bounds.width).isActive = true
        
        listGtttView.addArrangedSubview(newView)
        listGtttView.layoutIfNeeded()
    }
    
    func gtttViewDidTapDetele(_ view: GTTTView) {
        
    }
}
