//
//  GTTTView.swift
//  SeaBankPTF
//
//  Created by TrungNguyen on 9/22/21.
//

import UIKit

protocol GTTTViewViewDelegate: AnyObject {
  func gtttViewDidTapAdd(_ view: GTTTView)
  func gtttViewDidTapDetele(_ view: GTTTView)
}

final class GTTTView: NibView {
  
  @IBOutlet private weak var gtttTitleLabel: UILabel!
  @IBOutlet private weak var loaiGtttView: SBSInputTextView!
  @IBOutlet private weak var soGtttView: SBSInputTextView!
  @IBOutlet private weak var ngayCapView: SBSInputCalendar!
  @IBOutlet private weak var ngayHetHanView: SBSInputCalendar!
  @IBOutlet private weak var noiCapView: SBSInputTextView!
  @IBOutlet private weak var addButton: UIButton!
  
  weak var delegate: GTTTViewViewDelegate?
  
  // MARK: - Lifecycle
  
  override func configureView() {
    setupUI()
    applyLocalization()
  }
  
  func setVisibleAddButton(_ isVisible: Bool) {
    addButton.isHidden = !isVisible
  }
  
  @IBAction func themGtttDidTap(_ sender: Any) {
    delegate?.gtttViewDidTapAdd(self)
  }
}

private extension GTTTView {
  func setupUI(){
    loaiGtttView.isRequire = true
    
    soGtttView.isRequire = true
    
    ngayCapView.isRequire = true
    ngayHetHanView.isRequire = true
    noiCapView.isRequire = true
  }
  
  func applyLocalization() {
    gtttTitleLabel.text = "Giấy tờ tùy thân".localized()
    loaiGtttView.title = "Loại GTTT".localized()
    soGtttView.title = "Số GTTT".localized()
    ngayCapView.title = "Ngày cấp".localized()
    ngayHetHanView.title = "Ngày hết hạn hiệu lực".localized()
    noiCapView.title = "Nơi cấp".localized()
    addButton.setTitle("Thêm GTTT".localized(), for: .normal)
  }
}
