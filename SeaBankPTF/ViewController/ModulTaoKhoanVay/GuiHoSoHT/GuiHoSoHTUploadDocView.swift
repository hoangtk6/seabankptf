//
//  GuiHoSsHTUploadDocView.swift
//  SeaBankPTF
//
//  Created by Hoang Vu Van on 19/09/2021.
//

import Foundation

final class GuiHoSoHTUploadDocView: NibView {
  
  @IBOutlet weak var tfUpload: UITextField!
  @IBOutlet weak var btnUpload: UIButton!

  var onClickUpload: (() -> Void)?
  
  // MARK: - Lifecycle
  
  override func configureView() {
    setupUI()
  }
  
  func setupData(){

  }

  
  @IBAction func touchUpload(sender:UIButton) {
    if let onClickUpload = onClickUpload {
      onClickUpload()
    }
  }
}

private extension GuiHoSoHTUploadDocView {
  func setupUI(){
    PTFUIUtils.setupButton(button: self.btnUpload, title: Text.localizedString("Tải dữ liệu"),color: .titleTextColor(), bg: .white,font:  RobotoFont.fontWithType(.bold, size: 12), isShowShadow: true)
    self.btnUpload.borderColor = .white
  }
}
