
//  Created by Hoang Vu on 6/29/20.
//  Copyright © 2020 Techlab Corp. All rights reserved.
//

import UIKit

class GuiHoSoHTPopUp: BaseViewController {
  @IBOutlet weak var lbTitle: UILabel!
  @IBOutlet weak var lblDesc: UILabel!
  @IBOutlet weak var tfPass: UITextField!
  @IBOutlet weak var btnConfirm: UIButton!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    configUI()
    setupData()
  }
  
  private func configUI() {
    self.lbTitle.font = RobotoFont.fontWithType(.bold, size: 24)
    self.lbTitle.textColor = .headerTitleTextColor()
    self.lblDesc.font = RobotoFont.fontWithType(.regular, size: 12)
    self.lblDesc.textColor = .descTextColor()
    self.tfPass.isSecureTextEntry = true
    PTFUIUtils.setupButton(button: self.btnConfirm, title: Text.localizedString("DongY"), isShowShadow: true)
  }
  
 
  func setupData() {
    self.lbTitle.text = Text.localizedString("GiaoDichKhac")
    self.lblDesc.text = Text.localizedString("HenTraGiuCoKyHan")
  }
  
  // MARK: Action
  @IBAction func clickClose(_ sender: Any) {
    self.dismiss(animated: true, completion: nil)
  }
  
  
  // MARK: Action
  @IBAction func clickConfirm(_ sender: Any) {
    self.dismiss(animated: true, completion: nil)
  }
  
  
}


