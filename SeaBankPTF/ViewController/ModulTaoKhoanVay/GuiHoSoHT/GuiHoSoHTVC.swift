//
//  ThongTinTrungLapVC.swift
//  SeaBankPTF
//
//  Created by Hoang Vu Van on 13/09/2021.
//

import UIKit

final class GuiHoSoHTVC: BaseViewController {
  
  @IBOutlet private weak var sctionGTTTView: InfoSectionView!
  @IBOutlet private weak var viewGTTTContent: UIView!
  @IBOutlet private weak var gTTTView: GuiHoSoHTUploadDocView!
  @IBOutlet private weak var btnAddGTTT: UIButton!
  @IBOutlet private weak var listGTTTView: UIStackView!
  
  @IBOutlet private weak var sctionHSCMTNView: InfoSectionView!
  @IBOutlet private weak var viewHSCMTNContent: UIView!
  @IBOutlet private weak var hSCMTNView: GuiHoSoHTUploadDocView!
  @IBOutlet private weak var btnAddHSCMTN: UIButton!
  @IBOutlet private weak var listHSCMTNView: UIStackView!
  
  @IBOutlet private weak var sctionHSCMVLView: InfoSectionView!
  @IBOutlet private weak var viewHSCMVLContent: UIView!
  @IBOutlet private weak var hSCMVLView: GuiHoSoHTUploadDocView!
  @IBOutlet private weak var btnAddHSCMVL: UIButton!
  @IBOutlet private weak var listHSCMVLView: UIStackView!
  
  @IBOutlet private weak var sctionHSKVView: InfoSectionView!
  @IBOutlet private weak var viewHSKVContent: UIView!
  @IBOutlet private weak var hSKVView: GuiHoSoHTUploadDocView!
  @IBOutlet private weak var btnAddHSKV: UIButton!
  @IBOutlet private weak var listHSKVView: UIStackView!
  
  @IBOutlet private weak var sctionHSGNView: InfoSectionView!
  @IBOutlet private weak var viewHSGNContent: UIView!
  @IBOutlet private weak var hSGNView: GuiHoSoHTUploadDocView!
  @IBOutlet private weak var btnAddHSGN: UIButton!
  @IBOutlet private weak var listHSGNView: UIStackView!
  
  @IBOutlet private weak var sctionCHSKView: InfoSectionView!
  @IBOutlet private weak var viewCHSKContent: UIView!
  @IBOutlet private weak var cHSKView: GuiHoSoHTUploadDocView!
  @IBOutlet private weak var btnAddCHSK: UIButton!
  @IBOutlet private weak var listCHSKView: UIStackView!
  
  @IBOutlet private weak var btnSave: UIButton!
  @IBOutlet private weak var btnNext: UIButton!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    setupUI()
  }
  
  override func applyLocalization() {

  }
  
  // MARK: Action
  @IBAction func addNewGTTTClick(_ sender: Any) {
    addNewGTTTVIew()
  }
  
  @IBAction func addNewHSCMTNClick(_ sender: Any) {
    addNewHSCMTNVIew()
  }
  
  @IBAction func addNewHSCMVLClick(_ sender: Any) {
    addNewHSCMVLVIew()
  }
  
  @IBAction func addNewHSKVClick(_ sender: Any) {
    addNewHSKVVIew()
  }
  
  @IBAction func addNewHSGNClick(_ sender: Any) {
    addNewHSGNVIew()
  }
  
  @IBAction func addNewCHSKClick(_ sender: Any) {
    addNewCHSKVIew()
  }
  
  // MARK: Action
  @IBAction func clickNext(_ sender: Any) {
    
  }
}

private extension GuiHoSoHTVC {
  
  func removeGTTTVIew(){
    for view in listGTTTView.arrangedSubviews {
      view.removeFromSuperview()
      self.listGTTTView.removeArrangedSubview(view)
    }
  }
  
  func addNewGTTTVIew(){
    let view = GuiHoSoHTUploadDocView.init()
    view.heightAnchor.constraint(equalToConstant: 95.0).isActive = true
    view.widthAnchor.constraint(equalToConstant: gTTTView.bounds.width).isActive = true
    view.onClickUpload = {[weak self] in
      //guard let `self` = self else { return }
      
    }
    self.listGTTTView.addArrangedSubview(view)
    self.listGTTTView.layoutIfNeeded()
  }
  
  func addNewHSCMTNVIew(){
    let view = GuiHoSoHTUploadDocView.init()
    view.heightAnchor.constraint(equalToConstant: 95.0).isActive = true
    view.widthAnchor.constraint(equalToConstant: hSCMTNView.bounds.width).isActive = true
    view.onClickUpload = {[weak self] in
      //guard let `self` = self else { return }
      
    }
    self.listHSCMTNView.addArrangedSubview(view)
    self.listHSCMTNView.layoutIfNeeded()
  }
  
  func addNewHSCMVLVIew(){
    let view = GuiHoSoHTUploadDocView.init()
    view.heightAnchor.constraint(equalToConstant: 95.0).isActive = true
    view.widthAnchor.constraint(equalToConstant: hSCMVLView.bounds.width).isActive = true
    view.onClickUpload = {[weak self] in
      //guard let `self` = self else { return }
      
    }
    self.listHSCMVLView.addArrangedSubview(view)
    self.listHSCMVLView.layoutIfNeeded()
  }
  
  func addNewHSKVVIew(){
    let view = GuiHoSoHTUploadDocView.init()
    view.heightAnchor.constraint(equalToConstant: 95.0).isActive = true
    view.widthAnchor.constraint(equalToConstant: hSKVView.bounds.width).isActive = true
    view.onClickUpload = {[weak self] in
      //guard let `self` = self else { return }
      
    }
    self.listHSKVView.addArrangedSubview(view)
    self.listHSKVView.layoutIfNeeded()
  }
  
  func addNewHSGNVIew(){
    let view = GuiHoSoHTUploadDocView.init()
    view.heightAnchor.constraint(equalToConstant: 95.0).isActive = true
    view.widthAnchor.constraint(equalToConstant: hSGNView.bounds.width).isActive = true
    view.onClickUpload = {[weak self] in
      //guard let `self` = self else { return }
      
    }
    self.listHSGNView.addArrangedSubview(view)
    self.listHSGNView.layoutIfNeeded()
  }
  
  func addNewCHSKVIew(){
    let view = GuiHoSoHTUploadDocView.init()
    view.heightAnchor.constraint(equalToConstant: 95.0).isActive = true
    view.widthAnchor.constraint(equalToConstant: cHSKView.bounds.width).isActive = true
    view.onClickUpload = {[weak self] in
      //guard let `self` = self else { return }
      
    }
    self.listCHSKView.addArrangedSubview(view)
    self.listCHSKView.layoutIfNeeded()
  }
  
  
  func setupUI() {
    view.backgroundColor = .clear
    setupGTTTView()
    setupHSCMTNView()
    setupHSCMVLView()
    setupHSKVView()
    setupHSGNView()
    setupCHSKView()
    PTFUIUtils.setupButton(button: btnSave, title: Text.localizedString("Lưu"),color: .errorColor(), bg:.white,font: RobotoFont.fontWithType(.bold, size: 18), isShowShadow: true)
    PTFUIUtils.setupButton(button: btnNext, title: Text.localizedString("Xác thực Sales"), font:RobotoFont.fontWithType(.bold, size: 18), isShowShadow: true)
    btnSave.borderWidth = 0
  }
  
  func setupGTTTView() {
    gTTTView.configureView()
    sctionGTTTView.viewType = .taokhoanvaystep22
    sctionGTTTView.setupData(type:.taokhoanvaystep22, leftTitle: "Giấy tờ tùy thân", rightIcon:UIImage.init(named: "ic_arrow_down_gray"))
    sctionGTTTView.onClickTitleView = { [weak self] in
      guard let `self` = self else { return }
      self.viewGTTTContent.isHidden = !self.viewGTTTContent.isHidden
    }
    PTFUIUtils.setupButton(button: self.btnAddGTTT, title: Text.localizedString("Thêm"),color: .errorColor(), bg: .white,font:  RobotoFont.fontWithType(.bold, size: 12), isShowShadow: false)
    self.btnAddGTTT.borderColor = .white
    
   

  }
  
  func setupHSCMTNView() {
    hSCMTNView.configureView()
    sctionHSCMTNView.viewType = .taokhoanvaystep22
    sctionHSCMTNView.setupData(type:.taokhoanvaystep22, leftTitle: "Hồ sơ chứng minh thu nhập", rightIcon:UIImage.init(named: "ic_arrow_down_gray"))
    sctionHSCMTNView.onClickTitleView = { [weak self] in
      guard let `self` = self else { return }
      self.viewHSCMTNContent.isHidden = !self.viewHSCMTNContent.isHidden
    }
    PTFUIUtils.setupButton(button: self.btnAddHSCMTN, title: Text.localizedString("Thêm"),color: .errorColor(), bg: .white,font:  RobotoFont.fontWithType(.bold, size: 12), isShowShadow: false)
    self.btnAddHSCMTN.borderColor = .white
  }
  
  func setupHSCMVLView() {
    hSCMTNView.configureView()
    sctionHSCMVLView.viewType = .taokhoanvaystep22
    sctionHSCMVLView.setupData(type:.taokhoanvaystep22, leftTitle: "Hồ sơ chứng minh việc làm", rightIcon:UIImage.init(named: "ic_arrow_down_gray"))
    sctionHSCMVLView.onClickTitleView = { [weak self] in
      guard let `self` = self else { return }
      self.viewHSCMVLContent.isHidden = !self.viewHSCMVLContent.isHidden
    }
    PTFUIUtils.setupButton(button: self.btnAddHSCMVL, title: Text.localizedString("Thêm"),color: .errorColor(), bg: .white,font:  RobotoFont.fontWithType(.bold, size: 12), isShowShadow: false)
    self.btnAddHSCMVL.borderColor = .white
  }
  
  func setupHSKVView() {
    hSKVView.configureView()
    sctionHSKVView.viewType = .taokhoanvaystep22
    sctionHSKVView.setupData(type:.taokhoanvaystep22, leftTitle: "Hồ sơ khoản vay", rightIcon:UIImage.init(named: "ic_arrow_down_gray"))
    sctionHSKVView.onClickTitleView = { [weak self] in
      guard let `self` = self else { return }
      self.viewHSKVContent.isHidden = !self.viewHSKVContent.isHidden
    }
    PTFUIUtils.setupButton(button: self.btnAddHSKV, title: Text.localizedString("Thêm"),color: .errorColor(), bg: .white,font:  RobotoFont.fontWithType(.bold, size: 12), isShowShadow: false)
    self.btnAddHSKV.borderColor = .white
  }
  
  
  func setupHSGNView() {
    hSGNView.configureView()
    sctionHSGNView.viewType = .taokhoanvaystep22
    sctionHSGNView.setupData(type:.taokhoanvaystep22, leftTitle: "Hồ sơ giải ngân", rightIcon:UIImage.init(named: "ic_arrow_down_gray"))
    sctionHSGNView.onClickTitleView = { [weak self] in
      guard let `self` = self else { return }
      self.viewHSGNContent.isHidden = !self.viewHSGNContent.isHidden
    }
    PTFUIUtils.setupButton(button: self.btnAddHSGN, title: Text.localizedString("Thêm"),color: .errorColor(), bg: .white,font:  RobotoFont.fontWithType(.bold, size: 12), isShowShadow: false)
    self.btnAddHSGN.borderColor = .white
  }
  
  func setupCHSKView() {
    cHSKView.configureView()
    sctionCHSKView.viewType = .taokhoanvaystep22
    sctionCHSKView.setupData(type:.taokhoanvaystep22, leftTitle: "Các hồ sơ khác", rightIcon:UIImage.init(named: "ic_arrow_down_gray"))
    sctionCHSKView.onClickTitleView = { [weak self] in
      guard let `self` = self else { return }
      self.viewCHSKContent.isHidden = !self.viewCHSKContent.isHidden
    }
    PTFUIUtils.setupButton(button: self.btnAddCHSK, title: Text.localizedString("Thêm"),color: .errorColor(), bg: .white,font:  RobotoFont.fontWithType(.bold, size: 12), isShowShadow: false)
    self.btnAddCHSK.borderColor = .white
  }
  
}
