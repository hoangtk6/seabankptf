//
//  ThongTinTrungLapVC.swift
//  SeaBankPTF
//
//  Created by Hoang Vu Van on 13/09/2021.
//

import UIKit

final class ThongTinCVKDVC: BaseViewController {
    
    @IBOutlet private weak var maCVKDView: SBSInputTextView!
    @IBOutlet private weak var btnKTMaCVKD: UIButton!
    @IBOutlet private weak var tenCVKDView: SBSInputTextView!
    @IBOutlet private weak var emailView: SBSInputTextView!
    @IBOutlet private weak var dienThoaiView: SBSInputTextView!
    
    @IBOutlet private weak var kenhBanView: SBSInputTextView!
    @IBOutlet private weak var vungMienView: SBSInputTextView!
    @IBOutlet private weak var maCNView: SBSInputTextView!
    @IBOutlet private weak var nhomCNView: SBSInputTextView!
    @IBOutlet private weak var maPGDView: SBSInputTextView!
    @IBOutlet private weak var tenPGDView: SBSInputTextView!
    @IBOutlet private weak var ddgdView: SBSInputTextView!
    @IBOutlet private weak var nhomDDGDView: SBSInputTextView!
    
    @IBOutlet private weak var maNGTView: SBSInputTextView!
    @IBOutlet private weak var btnKTMaNguoiGT: UIButton!
    @IBOutlet private weak var loaiNguoiGTView: SBSInputTextView!
    @IBOutlet private weak var tenNguoiGTView: SBSInputTextView!
    
    
    @IBOutlet private weak var btnNext: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    
    override func applyLocalization() {
        maCVKDView.title = "Mã CVKD".localized()
        btnKTMaCVKD.setTitle("Kiểm tra".localized(), for:.normal)
        tenCVKDView.title = "Tên của CVKD".localized()
        emailView.title = "Email".localized()
        dienThoaiView.title = "Điện thoại".localized()
        
        kenhBanView.title = "Kênh bán".localized()
        vungMienView.title = "Vùng miền".localized()
        maCNView.title = "Mã chi nhánh".localized()
        nhomCNView.title = "Nhóm".localized()
        maPGDView.title = "Mã PGD".localized()
        tenPGDView.title = "Tên PGD".localized()
        ddgdView.title = "Địa điểm  giao dịch".localized()
        nhomDDGDView.title = "Nhóm".localized()
        
        maNGTView.title = "Mã người giới thiệu".localized()
        btnKTMaNguoiGT.setTitle("Kiểm tra".localized(), for:.normal)
        loaiNguoiGTView.title = "Loại người giới thiệu".localized()
        tenNguoiGTView.title = "Tên người giới thiệu".localized()
    }
    
    // MARK: Action
    @IBAction func clickNext(_ sender: Any) {
        
    }
}

private extension ThongTinCVKDVC {
    func setupUI() {
        view.backgroundColor = .clear
        setupInput()
        PTFUIUtils.setupButton(button: self.btnNext, title: Text.localizedString("Tiếp tục"), font:RobotoFont.fontWithType(.bold, size: 18), isShowShadow: true)
    }
    
    func setupInput() {
        maCVKDView.isRequire = true
        maCVKDView.isHiddenArrowDown = true
        tenCVKDView.isRequire = false
        tenCVKDView.isHiddenArrowDown = true
        emailView.isRequire = false
        emailView.isHiddenArrowDown = true
        dienThoaiView.isRequire = false
        dienThoaiView.isHiddenArrowDown = true
        
        kenhBanView.isRequire = false
        kenhBanView.isHiddenArrowDown = true
        vungMienView.isRequire = false
        vungMienView.isHiddenArrowDown = true
        maCNView.isRequire = false
        maCNView.isHiddenArrowDown = true
        nhomCNView.isRequire = false
        nhomCNView.isHiddenArrowDown = true
        maPGDView.isRequire = false
        maPGDView.isHiddenArrowDown = true
        tenPGDView.isRequire = false
        tenPGDView.isHiddenArrowDown = true
        ddgdView.isRequire = false
        ddgdView.isHiddenArrowDown = true
        nhomDDGDView.isRequire = false
        nhomDDGDView.isHiddenArrowDown = true
        
        maNGTView.isRequire = false
        maNGTView.isHiddenArrowDown = true
        loaiNguoiGTView.isRequire = false
        loaiNguoiGTView.isHiddenArrowDown = true
        tenNguoiGTView.isRequire = false
        tenNguoiGTView.isHiddenArrowDown = true
    }
}
