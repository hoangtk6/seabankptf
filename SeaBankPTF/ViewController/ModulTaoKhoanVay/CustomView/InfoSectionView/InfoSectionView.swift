//
//  HeaderTitleView.swift
//  SeaBankPTF
//
//  Created by Hoang Vu Van on 15/09/2021.
//

import Foundation

enum InfoSectionViewType:String{
  case normal
  case taokhoanvaystep1
  case taokhoanvaystep21
  case taokhoanvaystep22
  
}

final class InfoSectionView: NibView {
  
  @IBOutlet weak var lblLeftTitle: UILabel!
  @IBOutlet weak var lblRightTitle: UILabel!
  @IBOutlet weak var imgRightIcon: UIImageView!
  
  var viewType: InfoSectionViewType = .normal
  var onClickTitleView : (() -> Void)?
  
  // MARK: - Lifecycle

  override func configureView() {
    setupUI()
  }
  
  func setupData(type:InfoSectionViewType = .normal,
                 leftTitle: String,
                 rightIcon: UIImage? = nil,
                 rightTitle: String? = nil){
    lblLeftTitle.text =  leftTitle
    lblRightTitle.text = rightTitle
    imgRightIcon.image = rightIcon
    
    switch type {
    case .taokhoanvaystep1:
      imgRightIcon.isHidden = true
      lblRightTitle.isHidden = true
      
    case .taokhoanvaystep21:
      break
      
    case .taokhoanvaystep22:
      imgRightIcon.isHidden = false
      lblRightTitle.isHidden = true
      
    default:
      imgRightIcon.isHidden = true
      lblRightTitle.isHidden = true
    }
  }
  
  @IBAction func touchTitle(sender:UIButton) {
    if let onClickTitleView = onClickTitleView {
      onClickTitleView()
    }
    imgRightIcon.transform = imgRightIcon.transform.rotated(by: .pi)
  }
  
}

private extension InfoSectionView {
  func setupUI(){
    imgRightIcon.transform = imgRightIcon.transform.rotated(by: .pi)
    lblLeftTitle.font = RobotoFont.fontWithType(.bold, size: 18)
    lblLeftTitle.textColor = .titleTextColor()
    lblRightTitle.font = RobotoFont.fontWithType(.bold, size: 14)
    lblRightTitle.textColor = .errorColor()
  }
}
