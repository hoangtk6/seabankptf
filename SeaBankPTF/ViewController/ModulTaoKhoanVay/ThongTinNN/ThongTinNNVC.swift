//
//  ThongTinTrungLapVC.swift
//  SeaBankPTF
//
//  Created by Hoang Vu Van on 13/09/2021.
//

import UIKit

final class ThongTinNNVC: BaseViewController {
  @IBOutlet private weak var sctionNNKDView:InfoSectionView!
  @IBOutlet private weak var nhomNKDView: SBSInputTextView!
  @IBOutlet private weak var nganhNKDView: SBSInputTextView!
  @IBOutlet private weak var chiTietNView: SBSInputTextView!
  
  @IBOutlet private weak var sctionTTCTView:InfoSectionView!
  @IBOutlet private weak var tenCTTDHView: SBSInputTextView!
  @IBOutlet private weak var chucVuView: SBSInputTextView!
  @IBOutlet private weak var dtCTVitew: SBSInputTextView!
  @IBOutlet private weak var thuThapView: SBSInputTextView!
  @IBOutlet private weak var tgLTCTView: SBSInputTextView!
  @IBOutlet private weak var thanhToanCKVKView: SBSInputTextView!
  @IBOutlet private weak var tinhTPView: SBSInputTextView!
  @IBOutlet private weak var quanHuyenView: SBSInputTextView!
  @IBOutlet private weak var soNhaPhoView: SBSInputTextView!
  @IBOutlet private weak var nguonTNView: SBSInputTextView!
  @IBOutlet private weak var loaiHTNView: SBSInputTextView!
  @IBOutlet private weak var masoThueCTView: SBSInputTextView!
  
  
  @IBOutlet weak var btnNext: UIButton!
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    setupUI()
    setupMultiLanguage()
  }
  
  
  // MARK: Action
  @IBAction func clickNext(_ sender: Any) {
    
  }
}

private extension ThongTinNNVC {
  func setupUI() {
    view.backgroundColor = .clear
    setupInput()
    PTFUIUtils.setupButton(button: self.btnNext, title: Text.localizedString("Tiếp tục"), font:RobotoFont.fontWithType(.bold, size: 18), isShowShadow: true)
  }
  

  func setupInput() {
    sctionNNKDView.viewType = .taokhoanvaystep22
    sctionNNKDView.setupData(type:.taokhoanvaystep22, leftTitle: Text.localizedString("Ngành nghề kinh doanh"))
    sctionNNKDView.onClickTitleView = {
    
    }
    
    sctionTTCTView.viewType = .taokhoanvaystep22
    sctionTTCTView.setupData(type:.taokhoanvaystep22, leftTitle: Text.localizedString("Thông tin công ty/ Trường đại học"))
    sctionTTCTView.onClickTitleView = {
    
    }
    
    nhomNKDView.isRequire = true
    nhomNKDView.isHiddenArrowDown = false
    
    nganhNKDView.isRequire = true
    nganhNKDView.isHiddenArrowDown = false
    
    chiTietNView.isRequire = true
    chiTietNView.isHiddenArrowDown = false
    
    tenCTTDHView.isRequire = true
    tenCTTDHView.isHiddenArrowDown = true
    
    chucVuView.isRequire = true
    chucVuView.isHiddenArrowDown = false
    
    dtCTVitew.isRequire = false
    dtCTVitew.isHiddenArrowDown = true
    
    thuThapView.isRequire = true
    thuThapView.isHiddenArrowDown = true
    
    tgLTCTView.isRequire = true
    tgLTCTView.isHiddenArrowDown = true
    
    thanhToanCKVKView.isRequire = true
    thanhToanCKVKView.isHiddenArrowDown = true
    
    tinhTPView.isRequire = true
    tinhTPView.isHiddenArrowDown = false
    
    quanHuyenView.isRequire = true
    quanHuyenView.isHiddenArrowDown = false
    
    soNhaPhoView.isRequire = false
    soNhaPhoView.isHiddenArrowDown = true
    
    nguonTNView.isRequire = false
    nguonTNView.isHiddenArrowDown = false
    
    loaiHTNView.isRequire = false
    loaiHTNView.isHiddenArrowDown = false
    
    masoThueCTView.isRequire = false
    masoThueCTView.isHiddenArrowDown = true
  }

  func setupMultiLanguage() {
    nhomNKDView.title = Text.localizedString("Nhóm ngành kinh doanh")
    nganhNKDView.title = Text.localizedString("Ngành nghề kinh doanh")
    chiTietNView.title = Text.localizedString("Chi tiết ngành")
    tenCTTDHView.title = Text.localizedString("Tên công ty/ trường đại học")
    chucVuView.title = Text.localizedString("Chức vụ")
    dtCTVitew.title = Text.localizedString("Điện thoại công ty")
    thuThapView.title = Text.localizedString("Thu nhập")
    tgLTCTView.title = Text.localizedString("Thời gian làm tại công ty")
    
    thanhToanCKVKView.title = Text.localizedString("Thanh toán cho khoản vay khác")
    tinhTPView.title = Text.localizedString("Tỉnh/ thành phố")
    quanHuyenView.title = Text.localizedString("Quận/huyện")
    soNhaPhoView.title = Text.localizedString("Số nhà, phố")
    nguonTNView.title = Text.localizedString("Nguồn thu nhập")
    loaiHTNView.title = Text.localizedString("Loại hình thu nhập")
    masoThueCTView.title = Text.localizedString("Mã số thuế công ty")

  }
}
