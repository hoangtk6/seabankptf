//
//  GioiTinh.swift
//  SeaBankPTF
//
//  Created by TrungNguyen on 9/17/21.
//

import Foundation

enum PTFGender: String {
  case male = "Nam"
  case female = "Nữ"
}
