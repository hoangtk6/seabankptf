//
//  ThongTinTrungLapVC.swift
//  SeaBankPTF
//
//  Created by Hoang Vu Van on 13/09/2021.
//

import UIKit

final class ThongTinYeuCauVC: BaseViewController {
    
    @IBOutlet private weak var luongHSNView: SBSInputTextView!
    @IBOutlet private weak var kenhDTView: SBSInputTextView!
    @IBOutlet private weak var loaiSPView: SBSInputTextView!
    @IBOutlet private weak var spConView: SBSInputTextView!
    @IBOutlet private weak var maKHView: SBSInputTextView!
    @IBOutlet private weak var ngayKHTView: SBSInputCalendar!
    
    @IBOutlet private weak var soTienDXiew: SBSInputTextView!
    @IBOutlet private weak var donViView: SBSInputTextView!
    @IBOutlet private weak var mucdichVayView: SBSInputTextView!
    @IBOutlet private weak var laiSuatView: SBSInputTextView!
    @IBOutlet private weak var thVayView: SBSInputTextView!
    @IBOutlet private weak var ngayTNView: SBSInputTextView!
    
    @IBOutlet private weak var chienDichiew: SBSInputTextView!
    @IBOutlet private weak var bhView: SBSInputTextView!
    @IBOutlet private weak var maSPBHView: SBSInputTextView!
    @IBOutlet private weak var tenNCCView: SBSInputTextView!
    @IBOutlet private weak var tyLePBHView: SBSInputTextView!
    @IBOutlet private weak var tyLeSTBHView: SBSInputTextView!
    @IBOutlet private weak var ngaytinhHHView: SBSInputCalendar!
    @IBOutlet private weak var cachTinhSTGNView: SBSInputTextView!
    @IBOutlet private weak var soTienChuyenView: SBSInputTextView!
    @IBOutlet private weak var tienTPBHView: SBSInputTextView!


    @IBOutlet private weak var khacView: SBSLargeInput!
    @IBOutlet private weak var btnNext: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func applyLocalization() {
        luongHSNView.title = "Luồng hồ sơ nhanh".localized()
        kenhDTView.title = "Kênh đối tác".localized()
        loaiSPView.title = "Loại sản phẩm".localized()
        spConView.title = "Sản phẩm con".localized()
        maKHView.title = "Mã khoản vay".localized()
        ngayKHTView.title = "Ngày khởi tạo".localized()
        
        soTienDXiew.title = "Số tiền đề xuất".localized()
        donViView.title = "Đơn vị".localized()
        mucdichVayView.title = "Mục đích vay".localized()
        laiSuatView.title = "Lãi suất".localized()
        thVayView.title = "Thời hạn vay".localized()
        ngayTNView.title = "Ngày trả nợ".localized()
        
        chienDichiew.title = "Chiến dịch".localized()
        bhView.title = "Bảo hiểm".localized()
        maSPBHView.title = "Mã sản phẩm bảo hiểm".localized()
        tenNCCView.title = "Tên nhà cung cấp".localized()
        tyLePBHView.title = "Tỉ lệ phí bảo hiểm".localized()
        tyLeSTBHView.title = "Tỉ lệ số tiền được bảo hiểm".localized()
        ngaytinhHHView.title = "Ngày tính hết hạn".localized()
        cachTinhSTGNView.title = "Cách tính số tiền giải ngân".localized()
        
        soTienChuyenView.title = "Số tiền chuyển".localized()
        tienTPBHView.title = "Tiền tính phí bảo hiểm".localized()
        
        khacView.title = "Khác".localized()
    }
    
    // MARK: Action
    @IBAction func clickNext(_ sender: Any) {
        
    }
}

private extension ThongTinYeuCauVC {
    func setupUI() {
        view.backgroundColor = .clear
        setupInput()
        PTFUIUtils.setupButton(button: self.btnNext, title: Text.localizedString("Tiếp tục"), font:RobotoFont.fontWithType(.bold, size: 18), isShowShadow: true)
    }
    
    func setupInput() {
        luongHSNView.isRequire = true
        luongHSNView.isHiddenArrowDown = false
        
        kenhDTView.isRequire = true
        kenhDTView.isHiddenArrowDown = false
        
        loaiSPView.isRequire = true
        loaiSPView.isHiddenArrowDown = false
        
        spConView.isRequire = true
        spConView.isHiddenArrowDown = false
        
        maKHView.isRequire = true
        maKHView.isHiddenArrowDown = true
        
        ngayKHTView.isRequire = false

    
        soTienDXiew.isRequire = true
        soTienDXiew.isHiddenArrowDown = true
        
        donViView.isRequire = false
        donViView.isHiddenArrowDown = false
        
        mucdichVayView.isRequire = true
        mucdichVayView.isHiddenArrowDown = false
        
        laiSuatView.isRequire = true
        laiSuatView.isHiddenArrowDown = false
        
        thVayView.isRequire = true
        thVayView.isHiddenArrowDown = false
        
        ngayTNView.isRequire = true
        ngayTNView.isHiddenArrowDown = false
        
        chienDichiew.isRequire = false
        chienDichiew.isHiddenArrowDown = false
        
        bhView.isRequire = true
        bhView.isHiddenArrowDown = false
        
        maSPBHView.isRequire = true
        maSPBHView.isHiddenArrowDown = false
        
        tenNCCView.isRequire = false
        tenNCCView.isHiddenArrowDown = true
        
        tyLePBHView.isRequire = false
        tyLePBHView.isHiddenArrowDown = true
        
        tyLeSTBHView.isRequire = false
        tyLeSTBHView.isHiddenArrowDown = true
        
        ngaytinhHHView.isRequire = false
        
        cachTinhSTGNView.isRequire = false
        cachTinhSTGNView.isHiddenArrowDown = true
        
        soTienChuyenView.isRequire = false
        soTienChuyenView.isHiddenArrowDown = true
        
        tienTPBHView.isRequire = false
        tienTPBHView.isHiddenArrowDown = true
        
        khacView.isRequire = false

    }

    
   
}
