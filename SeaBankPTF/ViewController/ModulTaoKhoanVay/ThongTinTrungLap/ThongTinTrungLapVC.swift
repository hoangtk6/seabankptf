//
//  ThongTinTrungLapVC.swift
//  SeaBankPTF
//
//  Created by Hoang Vu Van on 13/09/2021.
//

import UIKit

final class ThongTinTrungLapVC: BaseViewController {
    
    @IBOutlet private weak var sctionCIFView: InfoSectionView!
    @IBOutlet private weak var viewCIFContent: UIView!
    @IBOutlet private weak var hoTenView: SBSInputTextView!
    @IBOutlet private weak var cifView: SBSInputTextView!
    @IBOutlet private weak var ngaySinhView: SBSInputCalendar!
    @IBOutlet private weak var loaiGTTTView: SBSInputTextView!
    @IBOutlet private weak var soGTTTView: SBSInputTextView!
    @IBOutlet private weak var diachiTTView: SBSInputTextView!
    @IBOutlet private weak var diachiHTView: SBSInputTextView!
    
    @IBOutlet private weak var sctionKVHTView: InfoSectionView!
    @IBOutlet private weak var viewKVHTContent: UIView!
    @IBOutlet private weak var cifKVHTView: SBSInputTextView!
    @IBOutlet private weak var idContactView: SBSInputTextView!
    @IBOutlet private weak var TenSPView: SBSInputTextView!
    @IBOutlet private weak var loaiTinDungView: SBSInputTextView!
    @IBOutlet private weak var tTKHView: SBSInputTextView!
    @IBOutlet private weak var ngayGNView: SBSInputTextView!
    @IBOutlet private weak var soTienGNHTView: SBSInputTextView!
    @IBOutlet private weak var dVTTView: SBSInputTextView!
    @IBOutlet private weak var traGopHTView: SBSInputTextView!
    
    
    @IBOutlet private weak var sctionKVDXLView: InfoSectionView!
    @IBOutlet private weak var viewKVDXLContent: UIView!
    @IBOutlet private weak var losCaseIdView: SBSInputTextView!
    @IBOutlet private weak var soGTTT_KVDXLView: SBSInputTextView!
    @IBOutlet private weak var hoVaTen_KVDXLView: SBSInputTextView!
    @IBOutlet private weak var sdtKVDXLView: SBSInputTextView!
    @IBOutlet private weak var soHoKhauView: SBSInputTextView!
    @IBOutlet private weak var dcht_KVDXLView: SBSInputTextView!
    @IBOutlet private weak var dctt_KVDXLView: SBSInputTextView!
    @IBOutlet private weak var tenSPView_KVDXLView: SBSInputTextView!
    @IBOutlet private weak var soTienVayView: SBSInputTextView!
    @IBOutlet private weak var dvtt_KVDXLView: SBSInputTextView!
    @IBOutlet private weak var ngayApDungView: SBSInputTextView!
    @IBOutlet private weak var trangThaiLOSView: SBSInputTextView!
    @IBOutlet private weak var ngayTTLView: SBSInputTextView!
    
    
    @IBOutlet private weak var sctionDSDView: InfoSectionView!
    @IBOutlet private weak var viewDSDContent: UIView!
    @IBOutlet private weak var hoVaTen_DSDView: SBSInputTextView!
    @IBOutlet private weak var ngaysinh_DSDView: SBSInputTextView!
    @IBOutlet private weak var sdt_DSDView: SBSInputTextView!
    @IBOutlet private weak var loaiGTT_DSDView: SBSInputTextView!
    @IBOutlet private weak var soGTT_DSDView: SBSInputTextView!
    @IBOutlet private weak var soHK_DSDView: SBSInputTextView!
    @IBOutlet private weak var trangThaiBLView: SBSInputTextView!
    
    @IBOutlet weak var btnNext: UIButton!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func applyLocalization() {
        hoTenView.title = "Họ và tên".localized()
        cifView.title = "CIF".localized()
        ngaySinhView.title = "Ngày sinh".localized()
        loaiGTTTView.title = "Loại GTTT".localized()
        soGTTTView.title = "Số GTTT".localized()
        diachiTTView.title =  "Địa chỉ thường trú".localized()
        diachiHTView.title = "Địa chỉ hiện tại".localized()
        
        cifKVHTView.title = "CIF".localized()
        idContactView.title = "ID liên hệ".localized()
        TenSPView.title = "Tên sản phẩm".localized()
        loaiTinDungView.title = "Loại tín dụng".localized()
        tTKHView.title = "Trạng thái khoản vay".localized()
        soTienGNHTView.title = "Số tiền giải ngân".localized()
        ngayGNView.title = "Ngày giải ngân".localized()
        dVTTView.title = "Đơn vị tiền tệ".localized()
        traGopHTView.title = "Trả góp/hằng tháng".localized()
        
        losCaseIdView.title = "LOS Case ID".localized()
        soGTTT_KVDXLView.title = "Số GTTT".localized()
        hoVaTen_KVDXLView.title = "Họ và tên".localized()
        sdtKVDXLView.title = "Số điện thoại".localized()
        soHoKhauView.title = "Sổ hộ khẩu".localized()
        dcht_KVDXLView.title = "Địa chỉ hiện tại".localized()
        dctt_KVDXLView.title = "Địa chỉ thường trú".localized()
        tenSPView_KVDXLView.title = "Tên sản phẩm".localized()
        soTienVayView.title = "Số tiền vay".localized()
        dvtt_KVDXLView.title = "Đơn vị tiền tệ".localized()
        ngayApDungView.title = "Ngày áp dụng".localized()
        trangThaiLOSView.title = "Trạng thái LOS".localized()
        ngayTTLView.title = "Ngày trạng thái LOS".localized()
      
        hoVaTen_DSDView.title = "Họ và tên".localized()
        ngaysinh_DSDView.title = "Ngày sinh".localized()
        sdt_DSDView.title = "Số điện thoại".localized()
        loaiGTT_DSDView.title = "Loại GTTT".localized()
        soGTT_DSDView.title = "Số GTTT".localized()
        soHK_DSDView.title = "Số hộ khẩu".localized()
        trangThaiBLView.title = "Trạng thái Blacklist".localized()
    }
    
    // MARK: Action
    @IBAction func clickNext(_ sender: Any) {

    }
}

private extension ThongTinTrungLapVC {
    func setupUI() {
        view.backgroundColor = .clear
        setupInputCIF()
        setupInputSectionKVHT()
        setupInputSectionKVDXL()
        setupInputDSD()
        PTFUIUtils.setupButton(button: self.btnNext, title: Text.localizedString("Tiếp tục"), font:RobotoFont.fontWithType(.bold, size: 18), isShowShadow: true)
    }
    
    func setupInputCIF() {
        sctionCIFView.viewType = .taokhoanvaystep22
        sctionCIFView.setupData(type:.taokhoanvaystep22, leftTitle: "Thông tin CIF", rightIcon:UIImage.init(named: "ic_arrow_down_gray"))
        sctionCIFView.onClickTitleView = {
            self.viewCIFContent.isHidden = !self.viewCIFContent.isHidden
        }
        
        hoTenView.isRequire = true
        hoTenView.contentMaxLength = 80
        
        cifView.isRequire = true
        cifView.contentMaxLength = 30
        
        soGTTTView.isRequire = true
        soGTTTView.isHiddenArrowDown = false
        
        loaiGTTTView.isRequire = true
        loaiGTTTView.isHiddenArrowDown = false
        
        soGTTTView.isRequire = true
        soGTTTView.isHiddenArrowDown = false
        
        diachiTTView.isRequire = false
        diachiTTView.isHiddenArrowDown = true
        
        diachiHTView.isRequire = false
        diachiHTView.isHiddenArrowDown = true
        
        ngaySinhView.isRequire = true
    }
    
    func setupInputSectionKVHT() {
        sctionKVHTView.viewType = .taokhoanvaystep22
        sctionKVHTView.setupData(type:.taokhoanvaystep22, leftTitle: "Các khoản vay hiện tại", rightIcon:UIImage.init(named: "ic_arrow_down_gray"))
        sctionKVHTView.onClickTitleView = {
            self.viewKVHTContent.isHidden = !self.viewKVHTContent.isHidden
        }
        
        losCaseIdView.isRequire = false
        //cifKVHTView.contentMaxLength = 80
        
        idContactView.isRequire = false
        //idContactView.contentMaxLength = 100
        
        TenSPView.isRequire = false
        TenSPView.isHiddenArrowDown = true
        
        loaiTinDungView.isRequire = false
        loaiTinDungView.isHiddenArrowDown = true
        
        tTKHView.isRequire = false
        tTKHView.isHiddenArrowDown = true
        
        soTienGNHTView.isRequire = false
        soTienGNHTView.isHiddenArrowDown = true
        
        ngayGNView.isRequire = false
        ngayGNView.isHiddenArrowDown = true
        
        dVTTView.isRequire = false
        dVTTView.isHiddenArrowDown = true
        
        traGopHTView.isRequire = false
        traGopHTView.isHiddenArrowDown = true
    }

    
    func setupInputSectionKVDXL() {
        sctionKVDXLView.viewType = .taokhoanvaystep22
        sctionKVDXLView.setupData(type:.taokhoanvaystep22, leftTitle: "Các khoản vay đang xử lí", rightIcon:UIImage.init(named: "ic_arrow_down_gray"))
        sctionKVDXLView.onClickTitleView = {
            self.viewKVDXLContent.isHidden = !self.viewKVDXLContent.isHidden
        }
        
        cifKVHTView.isRequire = false
        //cifKVHTView.contentMaxLength = 80
        
        soGTTT_KVDXLView.isRequire = false
        //idContactView.contentMaxLength = 100
        
        hoVaTen_KVDXLView.isRequire = false
        hoVaTen_KVDXLView.isHiddenArrowDown = true
        
        sdtKVDXLView.isRequire = false
        sdtKVDXLView.isHiddenArrowDown = true
        
        soHoKhauView.isRequire = false
        soHoKhauView.isHiddenArrowDown = true
        
        dcht_KVDXLView.isRequire = false
        dcht_KVDXLView.isHiddenArrowDown = true
        
        dctt_KVDXLView.isRequire = false
        dctt_KVDXLView.isHiddenArrowDown = true
        
        tenSPView_KVDXLView.isRequire = false
        tenSPView_KVDXLView.isHiddenArrowDown = true
        
        soTienVayView.isRequire = false
        soTienVayView.isHiddenArrowDown = true
        
        dvtt_KVDXLView.isRequire = false
        dvtt_KVDXLView.isHiddenArrowDown = true
        
        ngayApDungView.isRequire = false
        ngayApDungView.isHiddenArrowDown = true
        
        trangThaiLOSView.isRequire = false
        trangThaiLOSView.isHiddenArrowDown = true
        
        ngayTTLView.isRequire = false
        ngayTTLView.isHiddenArrowDown = true
    }
    
    
    func setupInputDSD() {
        sctionDSDView.setupData(type:.taokhoanvaystep22, leftTitle: "Danh sách đen", rightIcon:UIImage.init(named: "ic_arrow_down_gray"))
        sctionDSDView.onClickTitleView = {
            self.viewDSDContent.isHidden = !self.viewDSDContent.isHidden
        }
        
        hoVaTen_DSDView.isRequire = true
        hoVaTen_DSDView.contentMaxLength = 80
        
        ngaysinh_DSDView.isRequire = true
        ngaysinh_DSDView.contentMaxLength = 30
        
        sdt_DSDView.isRequire = true
        sdt_DSDView.isHiddenArrowDown = true
        
        loaiGTT_DSDView.isRequire = true
        loaiGTT_DSDView.isHiddenArrowDown = true
        
        soGTT_DSDView.isRequire = true
        soGTT_DSDView.isHiddenArrowDown = true
        
        soHK_DSDView.isRequire = false
        soHK_DSDView.isHiddenArrowDown = true
        
        trangThaiBLView.isRequire = false
        trangThaiBLView.isHiddenArrowDown = true

    }
}
