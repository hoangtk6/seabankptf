//
//  HSCanXuLyLaiCell.swift
//  SeaBankPTF
//
//  Created by Hoang Vu Van on 16/09/2021.
//

import UIKit

class HSCanXuLyLaiCell: UITableViewCell {
  @IBOutlet weak var lblTenKH: UILabel!
  @IBOutlet weak var lblNgayVay: UILabel!
  @IBOutlet weak var lblMaKV: UILabel!
  @IBOutlet weak var lblMaKVValue: UILabel!
  @IBOutlet weak var lblSoTien: UILabel!
  @IBOutlet weak var lblSoTienValue: UILabel!
  @IBOutlet weak var lblTrangThaiHS: UILabel!
  @IBOutlet weak var lblTrangThaiHSValue: UILabel!
  
  @IBOutlet weak var lblNguoiDuyet: UILabel!
  @IBOutlet weak var lblNgayDuyet: UILabel!
  @IBOutlet weak var lblGhiChu: UILabel!

  
    override func awakeFromNib() {
      super.awakeFromNib()
      configUI()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
  
  private func configUI() {
    self.lblTenKH.font = RobotoFont.fontWithType(.bold, size: 18)
    self.lblTenKH.textColor = .headerTitleTextColor()
    self.lblNgayVay.font = RobotoFont.fontWithType(.bold, size: 12)
    self.lblNgayVay.textColor = .titleTextColor()
    
    self.lblMaKV.font = RobotoFont.fontWithType(.regular, size: 12)
    self.lblMaKV.textColor = .titleTextColor()
    self.lblMaKVValue.font = RobotoFont.fontWithType(.regular, size: 12)
    self.lblMaKVValue.textColor = .titleTextColor()
    
    self.lblSoTien.font = RobotoFont.fontWithType(.regular, size: 12)
    self.lblSoTien.textColor = .titleTextColor()
    self.lblSoTienValue.font = RobotoFont.fontWithType(.bold, size: 12)
    self.lblNgayVay.textColor = .titleTextColor()
    
    self.lblTrangThaiHS.font = RobotoFont.fontWithType(.regular, size: 12)
    self.lblTrangThaiHS.textColor = .titleTextColor()
    self.lblTrangThaiHSValue.font = RobotoFont.fontWithType(.bold, size: 12)
    self.lblTrangThaiHSValue.textColor = .errorColor()
    
    self.lblNguoiDuyet.font = RobotoFont.fontWithType(.bold, size: 12)
    self.lblNguoiDuyet.textColor = .titleTextColor()
    self.lblNgayDuyet.font = RobotoFont.fontWithType(.regular, size: 12)
    self.lblNgayDuyet.textColor = .titleTextColor()
    
    self.lblGhiChu.font = RobotoFont.fontWithType(.regular, size: 12)
    self.lblGhiChu.textColor = .titleTextColor()


  }
    
}
