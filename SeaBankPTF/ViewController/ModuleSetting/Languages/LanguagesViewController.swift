//
//  LanguagesViewController.swift
//  SeaBankPTF
//
//  Created by TrungNguyen on 9/19/21.
//

import UIKit

final class LanguagesViewController: BaseViewController {
  
  @IBOutlet private weak var tableView: UITableView!
  
  private let localizationService = LocalizationServiceImpl()
  private var availableLanguages: [Language] = []
  
  private var selectedLanguage: Language? {
    willSet(newValue) {
      guard let language = newValue else { return }
      applyLanguage(language)
    }
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    setupUI()
    setupTableView()
    setupData()
  }
  
  // MARK: - Localization

  override func applyLocalization() {
    title = "Cài đặt ngôn ngữ".localized()
  }
}

// MARK: - Private

private extension LanguagesViewController {
  func setupUI() {
    tableView.hideEmptySeparators()
  }
  
  func setupTableView() {
    tableView.dataSource = self
    tableView.delegate = self
    tableView.register(LanguageTableViewCell.self)
    tableView.rowHeight = 56.0
  }
  
  func setupData() {
    availableLanguages = localizationService.availableLanguages()
    tableView.reloadData()
  }
  
  func applyLanguage(_ language: Language) {
    localizationService.setCurrentLanguage(language)
    view?.hideLoadingView()
    navigationController?.popViewController(animated: true)
  }
}

extension LanguagesViewController: UITableViewDataSource, UITableViewDelegate {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return availableLanguages.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeue(LanguageTableViewCell.self)
    let model = availableLanguages[indexPath.row]
    cell.update(with: model)
    return cell
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: true)
    let model = availableLanguages[indexPath.row]
    view?.showLoadingView()
    selectedLanguage = model
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 56.0
  }
  
  func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    return .leastNonzeroMagnitude
  }
}
