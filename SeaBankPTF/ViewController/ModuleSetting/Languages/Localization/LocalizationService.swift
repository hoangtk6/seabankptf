import Foundation

private let defaultLanguage = "vi"

struct LocalizationNotification {
  static let didChange = Notification.Name(rawValue: "LocalizationDidChange")
}

protocol LocalizationService: class {
  func availableLanguages() -> [Language]
  func setCurrentLanguage(_ language: Language)
  func obtainCurrentLanguageID() -> LanguageIdentifier
}

final class LocalizationServiceImpl: LocalizationService {

}

// MARK: - LocalizationService

extension LocalizationServiceImpl {
  func availableLanguages() -> [Language] {
    let languages = systemAvailableLanguages()
    let systemLanguageIdentifiers = languages.compactMap { LanguageIdentifier(rawValue: $0) }
    return systemLanguageIdentifiers.map { Language(systemIdentifier: $0,
                                          displayName: displayNameForLanguageIdentifier($0),
                                          isCurrent: $0.rawValue == currentLanguage() ? true : false) }
  }

  func setCurrentLanguage(_ language: Language) {
    let selectedLanguage = systemAvailableLanguages().contains(language.systemIdentifier.rawValue) ? language.systemIdentifier :
                                                                                                     LanguageIdentifier(rawValue: getDefaultLanguage())
    if (selectedLanguage != LanguageIdentifier(rawValue: currentLanguage())) {
      UserData.sharedInstance().currentLanguage = selectedLanguage?.rawValue
      NotificationCenter.default.post(name: LocalizationNotification.didChange, object: nil)
    }
  }
  
  func obtainCurrentLanguageID() -> LanguageIdentifier {
    return LanguageIdentifier(rawValue: currentLanguage()) ?? .english
  }
}

// MARK: - Private

private extension LocalizationServiceImpl {

  func systemAvailableLanguages() -> [String] {
    return [LanguageIdentifier.vi.rawValue,
            LanguageIdentifier.english.rawValue]
  }

  func currentLanguage() -> String {
    guard let currentLanguage = UserData.sharedInstance().currentLanguage else { return getDefaultLanguage() }
    return currentLanguage
  }

  func displayNameForLanguageIdentifier(_ identifier: LanguageIdentifier) -> String {
    switch identifier {
    case .english: return "Tiếng Anh".localized()
    case .vi: return "Tiếng Việt".localized()
    }
  }

  func getDefaultLanguage() -> String {
    var primaryLanguage: String!
    guard let preferredLanguage = Bundle.main.preferredLocalizations.first else { return defaultLanguage }
    let availableLanguages = systemAvailableLanguages()

    if (availableLanguages.contains(preferredLanguage)) {
      primaryLanguage = preferredLanguage
    } else {
      primaryLanguage = defaultLanguage
    }
    UserData.sharedInstance().currentLanguage = primaryLanguage
    return primaryLanguage
  }

}
