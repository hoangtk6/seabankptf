import Foundation

struct Language {
  let systemIdentifier: LanguageIdentifier
  let displayName: String
  let isCurrent: Bool
  
  func nuetralDisplayName() -> String {
    switch systemIdentifier {
    case .english: return Constants.LanguageLaunchDisplay.english
    case .vi: return Constants.LanguageLaunchDisplay.vi
    }
  }
}

// MARK: - Equatable

extension Language: Equatable {
  static func == (lhs: Language, rhs: Language) -> Bool {
    return lhs.systemIdentifier == rhs.systemIdentifier && lhs.displayName == rhs.displayName && lhs.isCurrent == rhs.isCurrent
  }
}

enum LanguageIdentifier: String {
  case english = "en"
  case vi = "vi"
}
