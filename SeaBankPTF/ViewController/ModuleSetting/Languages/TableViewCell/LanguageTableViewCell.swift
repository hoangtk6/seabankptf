//
//  LanguageTableViewCell.swift
//  SeaBankPTF
//
//  Created by TrungNguyen on 9/19/21.
//

import UIKit

final class LanguageTableViewCell: UITableViewCell {
  
  // MARL: - IBOutlet

  @IBOutlet private weak var titleLabel: UILabel!
  @IBOutlet private weak var checkMarkImageView: UIImageView!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    self.separatorInset = .zero
  }
  
  func update(with model: Language) {
    titleLabel.text = model.displayName
    checkMarkImageView.isHidden = !model.isCurrent
  }
}
