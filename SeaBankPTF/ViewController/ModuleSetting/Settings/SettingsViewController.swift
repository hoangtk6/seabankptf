//
//  SettingsViewController.swift
//  SeaBankPTF
//
//  Created by TrungNguyen on 9/25/21.
//

import UIKit

final class SettingsViewController: BaseViewController {
  
  override func viewDidLoad() {
    super.viewDidLoad()
    setupUI()
  }
  
  // MARK: - Localization

  override func applyLocalization() {
    title = "Cài đặt".localized()
  }
}

private extension SettingsViewController {
  func setupUI() {
    
  }
}
