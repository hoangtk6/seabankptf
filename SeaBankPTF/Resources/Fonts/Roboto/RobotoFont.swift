//
//  GilroyDisplayFont.swift
//  Course-iOS
//
//  Created by Nguyen Thanh Trung on 8/3/20.
//  Copyright © 2020 Nguyen Thanh Trung. All rights reserved.
//

import UIKit

enum RobotoFontStyle: String {
  case bold
  case medium
  case regular
  case light
  
  func fontName() -> String {
    switch self {
    case .bold:
      return "Roboto-Bold"
    case .medium:
      return "Roboto-Medium"
    case .regular:
      return "Roboto-Regular"
    case .light:
      return "Roboto-Light"
    }
  }
}

final class RobotoFont: UIFont {
  
  class func fontWithType(_ type: RobotoFontStyle, size: CGFloat) -> UIFont {
    if let font = UIFont(name: type.fontName(), size: size) {
      return font
    } else {
      // default return system font
      return UIFont.systemFont(ofSize: size)
    }
  }
  
}
