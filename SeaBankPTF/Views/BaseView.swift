//
//  BaseView.swift
//  Comic
//
//  Created by Hoang Vu Van on 11/5/19.
//  Copyright © 2019 Techlab Corp. All rights reserved.
//

import UIKit
//Popup View animation option
public enum NHPopupAnimation {
    case normal
    case top
    case bottom
    case bottomBounce
    case topBounce
}

//Popup View alignment option
public enum NHPopupAlign {
    case top
    case center
    case bottom
    case custom
}
class BaseView: UIView {
    // Là root view của file xib hoặc view gốc được truyền vào
    var view: UIView!

    //MARK:-Popup view ==========================================================
    // Là view cha thêm view của file zib này vào
    var mainView: UIView!
    
    convenience init?(mainView: UIView, frame: CGRect) {
        self.init(frame: frame)
        self.mainView = mainView
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.initDefaultData()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.initDefaultData()
    }
    
    //MARK:- Khởi tạo liên quan đến popup
    var overlayColor: UIColor?
    
    //Touch Dismiss :- dismiss popup view on touch background
    var touchDismiss: Bool?
    
    //Popup Align :- Popup alignment options
    var popupAlign: NHPopupAlign?
    
    //Popup Animation :- Popup animation options
    var popupAnimation: NHPopupAnimation?
    
    //Popup Size :- Popup View Width and Height
    var popupSize: CGSize?
    
    //Popup Corner :- Corner Radius of popup view
    var popupCorner: CGFloat?
    
    //Popup custom Align :- Custom origin for popup
    var popupCustomAlign: CGPoint?
    //let containerView
    var containerView : UIView?
    
    private func initDefaultData() {
        popupCorner = 0
        popupAnimation = .normal
        popupAlign = .bottom
        overlayColor = UIColor(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 0.5)
    }
    //Present popup VC
    public func presentPopup(completion: ((Bool)->())?){
        
        containerView = UIView(frame: mainView.bounds)
        
        if let overlay = self.overlayColor {
            containerView!.backgroundColor = overlay
        }else{
            containerView!.backgroundColor = UIColor(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 0.5)
        }
        
        let dismissButton = UIButton(frame: mainView.bounds)
        if self.touchDismiss ?? true{
            dismissButton.addTarget(self, action: #selector(self.dismissPopupView), for: .touchUpInside)
        }
        
        
        if let cornerRadius = self.popupCorner {
            self.view.layer.cornerRadius = cornerRadius
            self.view.layer.masksToBounds = true
        }
        
        if let popupSize = self.popupSize {
            self.view.frame.size = popupSize
        }else{
            self.view.frame.size = CGSize(width: (mainView.bounds.width), height: (mainView.bounds.height))
        }
        
        if let customPoint = self.popupCustomAlign {
            self.view.frame.origin = customPoint
        }
        
        mainView.addSubview(containerView!)
        mainView.addSubview(self)
        self.setupPresentAnimation(type: self.popupAnimation!)
    }
    
    //Subview the popup view
    fileprivate func addSubview(subView:UIView, toView parentView:UIView) {
        subView.translatesAutoresizingMaskIntoConstraints = false
        parentView.addSubview(subView)
        
        var viewBindingsDict = [String: AnyObject]()
        
        viewBindingsDict["subView"] = subView
        
        parentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[subView]|",
                                                                 
                                                                 options: [], metrics: nil, views: viewBindingsDict))
        
        parentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[subView]|",
                                                                 
                                                                 options: [], metrics: nil, views: viewBindingsDict))
        
    }
    
    //Popup Present Animation
    fileprivate func setupPresentAnimation(type:NHPopupAnimation){
        switch type {
        case .normal:
            fromNone()
        case .top:
            fromTop()
        case .bottom:
            fromBottom()
        case .topBounce:
            fromTopBounce()
        case .bottomBounce:
            fromBottomBounce()
        }
    }
    
    //Popup Dismiss Animation
    fileprivate func setupDismissAnimation(type:NHPopupAnimation){
        switch type {
        case .normal:
            toNone()
        case .top:
            toTop()
        case .bottom:
            toBottom()
        case .topBounce:
            toTop()
        case .bottomBounce:
            toBottom()
        }
    }
    
    //Present from normal animation
    fileprivate func fromNone() {
        self.view.alpha = 0
        UIView.animate(withDuration: 0.2, animations: {
            self.view.alpha = 1
        }, completion: nil)
    }
    
    //Present from top animation
    fileprivate func fromTop() {
        let frame = self.view.frame
        self.view.frame.origin.y = mainView.frame.origin.y - self.view.frame.height
        UIView.animate(withDuration: 0.3, animations: {
            self.view.frame = frame
        }, completion: nil)
    }
    
    //Present from bottom animation
    fileprivate func fromBottom() {
        let frame = self.view.frame
        self.view.frame.origin.y = mainView.frame.height + self.view.frame.height
        UIView.animate(withDuration: 0.3, animations: {
            self.view.frame = frame
        }, completion: nil)
    }
    
    //Present from top with bounce animation
    fileprivate func fromTopBounce() {
        
        let frame = self.view.frame
        self.view.frame.origin.y = mainView.frame.origin.y - self.view.frame.height
        UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 0.75, initialSpringVelocity: 0.5, options: .curveEaseIn, animations: {
            self.view.frame = frame
        }, completion: nil)
        
    }
    
    //Present from bottom with animation
    fileprivate func fromBottomBounce() {
        
        let frame = self.view.frame
        self.view.frame.origin.y = mainView.frame.height + self.view.frame.height
        UIView.animate(withDuration: 0.6, delay: 0.0, usingSpringWithDamping: 0.75, initialSpringVelocity: 0.5, options: .curveEaseIn, animations: {
            self.view.frame = frame
        }, completion: nil)
        
    }
    
    //Dismiss from normal animation
    fileprivate func toNone() {
        UIView.animate(withDuration: 0.2, animations: {
            self.view.alpha = 0
        }, completion: { (animated) in
            self.removeContentView()
        })
    }
    
    //Dismiss from top animation
    fileprivate func toTop() {
        UIView.animate(withDuration: 0.3, animations: {
            self.view.frame.origin.y = self.mainView.frame.origin.y - self.view.frame.height
        }, completion: { (animated) in
            self.removeContentView()
        })
    }
    
    //Dismiss from bottom animation
    fileprivate func toBottom() {
        UIView.animate(withDuration: 0.3, animations: {
            self.view.frame.origin.y = self.mainView.frame.height + self.view.frame.height
        }, completion: { (animated) in
            self.removeContentView()
        })
    }
    
    //Remove Content view from superView
    fileprivate func removeContentView(){
        self.removeFromSuperview()
        self.view.removeFromSuperview()
        containerView?.removeFromSuperview()
    }
    
    //Dismiss Popup
    @objc func dismissPopupView() {
        setupDismissAnimation(type: self.popupAnimation!)
    }
    
    //Dissmiss pop
    public func dismissPopup(completion: ((Bool)->())?){
        setupDismissAnimation(type: self.popupAnimation!)
    }

}
