//
//  ComicRefreshControl.swift
//  Comic
//
//  Copyright © 2020 Techlab Corp. All rights reserved.
//

import UIKit

final class ComicRefreshControl: UIRefreshControl {

  override init() {
    super.init()
//    self.attributedTitle = NSAttributedString(string: "Đang tải ...", attributes: [NSAttributedString.Key.foregroundColor:UIColor.lightGray])
    self.tintColor = UIColor.lightGray
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}

extension UIRefreshControl {
    func programaticallyBeginRefreshing(in tableView: UITableView) {
        beginRefreshing()
        let offsetPoint = CGPoint.init(x: 0, y: -frame.size.height)
        tableView.setContentOffset(offsetPoint, animated: true)
    }
}
