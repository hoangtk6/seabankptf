import Foundation
import UIKit

final class FTBadgeButton: UIButton {
  
  var badgeLabel: UILabel?
  
  var badge: String? {
    didSet {
      addBadgeToButon(badge: badge)
    }
  }
  
  var number: Int? {
    didSet {
      addBadgeToButton(number: self.number ?? 0)
    }
  }
  
  public var badgeBackgroundColor = UIColor.red {
    didSet {
      badgeLabel?.backgroundColor = badgeBackgroundColor
    }
  }
  
  public var badgeTextColor = UIColor.white {
    didSet {
      badgeLabel?.textColor = badgeTextColor
    }
  }
  
  public var badgeFont = UIFont.boldSystemFont(ofSize: 8) {
    didSet {
      badgeLabel?.font = badgeFont
    }
  }
  
  public var badgeEdgeInsets: UIEdgeInsets? {
    didSet {
      addBadgeToButon(badge: badge)
    }
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    addBadgeToButon(badge: nil)
  }
  
  func addBadgeToButton(number: Int) {
    self.badgeLabel?.isHidden = number == 0
    if number < 99 {
      addBadgeToButon(badge: "\(number)")
    } else {
      addBadgeToButon(badge: "99+")
    }
  }
  
  func addBadgeToButon(badge: String?) {
    let height:Double = 11
    let textWidth:Double = Double(badge?.widthOfString(usingFont: self.badgeFont) ?? CGFloat(height))  + Double(2*3)
    var width = height
    if width < textWidth {
      width = textWidth
    }
    let x:Double = (Double(bounds.size.width) - width/2 - 10)
    let y:Double = 5
    badgeLabel?.frame = CGRect(x: x, y: y, width: width, height: height)
    
    badgeLabel?.text = badge
  }
  
  func addBadge() {
    if badgeLabel == nil {
      badgeLabel = UILabel()
      badgeLabel?.textColor = badgeTextColor
      badgeLabel?.backgroundColor = badgeBackgroundColor
      badgeLabel?.font = badgeFont
      badgeLabel?.sizeToFit()
      badgeLabel?.textAlignment = .center

      let height:Double = 10
      let width = height
      
      let x:Double = (Double(bounds.size.width) - width - 5)
      let y:Double = 5
      badgeLabel?.frame = CGRect(x: x, y: y, width: width, height: height)
      
      if let badgeLabel = badgeLabel {
        badgeLabel.layer.cornerRadius = badgeLabel.frame.height/2
        badgeLabel.layer.masksToBounds = true
        addSubview(badgeLabel)
      }
    }
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    self.addBadgeToButon(badge: nil)
    //fatalError("init(coder:) has not been implemented")
  }
}
