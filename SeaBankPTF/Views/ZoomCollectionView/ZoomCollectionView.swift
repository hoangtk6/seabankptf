import Foundation
import UIKit
extension Collection where Indices.Iterator.Element == Index {
  subscript (safe index: Index) -> Iterator.Element? {
    return indices.contains(index) ? self[index] : nil
  }
}

extension UICollectionView {
  func getVerticalIndicator() -> UIView? {
    guard self.subviews.count >= 2 else {
      return nil
    }
    func viewCanBeScrollIndicator(view: UIView) -> Bool {
      let viewClassName = NSStringFromClass(type(of: view))
      if viewClassName == "_UIScrollViewScrollIndicator" || viewClassName == "UIImageView" {
        return true
      }
      return false
    }
    
    if #available(iOS 13, *) {
      let verticalScrollViewIndicatorPosition = self.subviews.count - 1
      if let viewForVerticalScrollViewIndicator = self.subviews[safe: verticalScrollViewIndicatorPosition] {
        if viewCanBeScrollIndicator(view: viewForVerticalScrollViewIndicator) {
          return viewForVerticalScrollViewIndicator
        }
      }
      
    } else {
      if let verticalIndicator: UIImageView = (self.subviews[safe:(self.subviews.count - 1)] as? UIImageView) {
        return verticalIndicator
      }
    }
    return nil
  }
}

extension UIView {
  func addToView(parentView: UIView){
    let top = NSLayoutConstraint(item: self, attribute: .top, relatedBy: .equal, toItem: parentView, attribute: .top, multiplier: 1, constant: 0)
    let left = NSLayoutConstraint(item: self, attribute: .left, relatedBy: .equal, toItem: parentView, attribute: .left, multiplier: 1, constant: 0)
    let bottom = NSLayoutConstraint(item: self, attribute: .bottom, relatedBy: .equal, toItem: parentView, attribute: .bottom, multiplier: 1, constant: 0)
    let right = NSLayoutConstraint(item: self, attribute: .right, relatedBy: .equal, toItem: parentView, attribute: .right, multiplier: 1, constant: 0)
    parentView.addSubview(self)
    self.translatesAutoresizingMaskIntoConstraints = false
    parentView.addConstraints([top, left, bottom, right])
    
  }
  
  func autoResizeView() {
    self.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    self.translatesAutoresizingMaskIntoConstraints = true
  }
}
open class ZoomCollectionView : UIView, UIScrollViewDelegate {
  public var collectionView: UICollectionView? = nil
  var maxZoomScale:CGFloat = 2
  var minZoomScale:CGFloat = 1
  var indicatorAlpha:CGFloat = 0.3
  private var indicator:UIView?
  var tmpIndicator = UIView()
  var lastZoomOffSet = CGPoint.init(x: 0, y: 0)
  var lastCollectionOffSet = CGPoint.init(x: 0, y: 0)
  var didZoom = false
  var isSetContentScrollFirst = true
  lazy var scrollView: UIScrollView? = {
    let sw = UIScrollView()
    sw.showsVerticalScrollIndicator = false
    sw.showsHorizontalScrollIndicator = false
    sw.isDirectionalLockEnabled = true
    sw.delegate = self
    sw.minimumZoomScale = minZoomScale
    sw.maximumZoomScale = maxZoomScale
    sw.bounces = true
    sw.isDirectionalLockEnabled = true
    return sw
  }()
  
  public init(frame: CGRect, tap: UITapGestureRecognizer?) {
    super.init(frame: frame)
    let lt = UICollectionViewFlowLayout()
    lt.sectionInset = .zero
    lt.minimumLineSpacing = 0
    lt.minimumInteritemSpacing = 0
    collectionView = UICollectionView(frame: frame, collectionViewLayout: lt)
    tmpIndicator.frame = CGRect.init(x: 0, y: 0, width: 3, height: 7)
    tmpIndicator.layer.cornerRadius = 1.5
    tmpIndicator.backgroundColor = UIColor.black
    tmpIndicator.alpha = 0
    let doubleTap = UITapGestureRecognizer(target: self, action: #selector(doubleTapAction(recognizer:)))
    doubleTap.numberOfTapsRequired = 2
    scrollView?.addGestureRecognizer(doubleTap)
    tap?.require(toFail: doubleTap)
    scrollView?.frame = frame
    collectionView?.backgroundColor = .white
    collectionView?.clipsToBounds = false
    scrollView?.delegate = self
    scrollView?.showsVerticalScrollIndicator = false
    if let collectionView = collectionView, let scrollView = scrollView {
      scrollView.autoResizeView()
      collectionView.autoResizeView()
      scrollView.addSubview(collectionView)
      addSubview(scrollView)
      addSubview(tmpIndicator)
      
    }
  }
  
  @objc func doubleTapAction(recognizer:  UITapGestureRecognizer) {
    if let scrollView = scrollView, let collectionView = self.collectionView {
      var zoomScale = scrollView.zoomScale
      if zoomScale > minZoomScale {
        scrollView.setZoomScale(minZoomScale, animated: false)
      } else {
        zoomScale = maxZoomScale
        if zoomScale != scrollView.zoomScale {
          let point = recognizer.location(in: collectionView)
          
          let scrollSize = scrollView.frame.size
          let size = CGSize(width: scrollSize.width / zoomScale,
                            height: scrollSize.height / zoomScale)
          let origin = CGPoint(x: point.x - size.width / 2,
                               y: point.y - size.height / 2)
          didZoom = false
          scrollView.zoom(to:CGRect(origin: origin, size: size), animated: false)
          let lastOffset = self.scrollView?.contentOffset ?? CGPoint(x: 0, y: 0)
          let y = (self.collectionView?.contentOffset.y ?? 0) + (lastOffset.y/(self.scrollView?.zoomScale ?? 1)) - 10
          self.scrollView?.contentSize = CGSize.init(width: (self.scrollView?.contentSize ?? CGSize.zero).width, height: 0)
          self.collectionView?.setContentOffset(CGPoint(x: 0, y: y), animated: false)
          didZoom = true
        }
      }
    }
  }
  
  func showIndicator(isShow: Bool) {
    if isShow {
      self.tmpIndicator.alpha = indicatorAlpha
    } else {
      UIView.animate(withDuration: 0.2) { [weak self] in
        self?.tmpIndicator.alpha = 0
      }
    }
  }
  
  required public init?(coder: NSCoder) {
    super.init(coder: coder)
  }
  
  open func viewForZooming(in scrollView: UIScrollView) -> UIView? {
    if scrollView == self.scrollView {
      return collectionView
    } else {
      return nil
    }
  }
  
  public func scrollViewWillBeginZooming(_ scrollView: UIScrollView, with view: UIView?) {
    self.didZoom = false
  }
  
  public func scrollViewDidZoom(_ scrollView: UIScrollView) {
    self.didZoom = true
    self.showIndicator(isShow: false)
    self.isSetContentScrollFirst = false
  }
  
  
  
  public func scrollViewDidScroll(_ scrollView: UIScrollView) {
    if scrollView == self.collectionView {
      self.indicator = self.collectionView?.getVerticalIndicator()
      if self.indicator != nil {
        self.indicator?.isHidden = true
        let height = (self.scrollView?.frame.height ?? 0)
        let padding:CGFloat = 3
        let rec = CGRect.init(x: (self.scrollView?.frame.width ?? 0) - padding - (self.indicator?.size.width ?? 0), y: ((self.indicator?.frame.origin.y ?? 0)/scrollView.contentSize.height)*height, width: self.indicator?.size.width ?? 0, height: self.indicator?.size.height ?? 0)
        self.tmpIndicator.frame = rec
        self.showIndicator(isShow: true)
      } else {
        self.showIndicator(isShow: false)
      }
    }
    if scrollView == self.scrollView {
      
      if (self.lastZoomOffSet.y - scrollView.contentOffset.y) < 5 && !self.isSetContentScrollFirst && self.didZoom && !scrollView.isZooming {
        isSetContentScrollFirst = true
        let lastOffset = self.scrollView?.contentOffset ?? CGPoint(x: 0, y: 0)
        if lastOffset.y > 0 {
          let tmpLastOffset = self.scrollView?.contentOffset ?? CGPoint(x: 0, y: 0)
          self.scrollView?.contentSize = CGSize.init(width: (self.scrollView?.contentSize ?? CGSize.zero).width, height: 0)
          let y = (self.collectionView?.contentOffset.y ?? 0) + (tmpLastOffset.y/scrollView.zoomScale) - 10
          self.collectionView?.setContentOffset(CGPoint(x: 0, y: y), animated: false)
          
        }
      }
      self.lastZoomOffSet = scrollView.contentOffset
    }
    
  }
  
  public func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
    self.showIndicator(isShow: false)
  }
  
  
  public func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
    self.showIndicator(isShow: false)
  }
  
  public func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
    if scrollView == self.scrollView {
      let actualPosition = scrollView.panGestureRecognizer.translation(in: scrollView.superview)
      if (actualPosition.y < 0){
      }else{
      }
    } else {
    }
    
  }
  
}
